import { ISubject } from '../typescript';
import { Dispatch, GetState } from '../reducers/types';

export const CREATE_SUBJECT_ERROR = 'CREATE_SUBJECT_ERROR';

function ExceptionSubject(error: any) {
   return error;
}

export function loadSubjects() {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.subjects.findAll()
         .then((doc: any) => console.log('FIND ALL SUBJECTS :', doc))
         .catch((err: any) => console.error(err.stack));
   };
}

export function createSubject(newSubject: {
   name: string;
   icon: string | null;
}) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.subjects.create(newSubject)
         .then(() => {})
         .catch((err: any) => {
            console.log(err.stack);
            throw ExceptionSubject(err);
         });
}

export function updateSubject(toUpdate: ISubject) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.subjects.update(toUpdate)
         .then(() => {})
         .catch((err: any) => {
            console.log(err.stack);
            throw ExceptionSubject(err);
         });
}

export function removeSubject(toRemove: ISubject) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.subjects.remove(toRemove)
         .then(() => {})
         .catch((err: any) => {
            console.error(err.stack);
            throw ExceptionSubject(err);
         });
}
