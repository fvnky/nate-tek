import * as React from 'react';
import cx from 'classnames';

// Component
import Modal from '../Base/Modal';

// Utils
import { capitalize } from '../utils';

// Style
import stylesErr from './ModalError.less';
import styles from './ModalFolder.less';

// typescript
import { IDatabase, IFolder, UpdateFolder, ILanguage } from '../../typescript';

interface IProps {
   id: string;
   updateFolder: UpdateFolder;
   db: IDatabase;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   folder: IFolder | null;
   title: string;
   message: string;
}

export default class ModalEditFolder extends React.Component<IProps, IState> {
   input: React.RefObject<HTMLInputElement> = React.createRef<
      HTMLInputElement
   >();

   constructor(props: IProps) {
      super(props);
      this.state = {
         folder: null,
         title: '',
         message: ''
      };
   }

   componentDidMount() {
      const { id, db } = this.props;
      const input = this.input.current;

      db.folders
         .findById(id)
         .then((folder: IFolder) => {
            if (input) {
               input.focus();
               input.value = folder.name;
               input.setSelectionRange(0, input.selectionEnd);
            }
            this.setState({ folder, title: folder.name });
         })
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   }

   handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { target } = event;
      const input = this.input.current;

      if (input) {
         if (input.className !== styles.input) {
            input.className = styles.input;
            this.setState({ message: '' });
         }
      }
      this.setState({ title: capitalize(target.value) });
   };

   handleOk = () => {
      const { updateFolder, handleDisplayModal } = this.props;
      const { title, folder } = this.state;
      const input = this.input.current;

      if (!folder) return;
      folder.name = title;
      updateFolder(folder)
         .then(() => handleDisplayModal())
         .catch((err: any) => {
            this.setState({ message: err.message });
            if (input) {
               input.className = `${stylesErr.inputErrorColor} ${styles.input}`;
               input.focus();
               input.setSelectionRange(0, input.selectionEnd);
            }
         });
   };

   renderInputName = () => {
      const { lang, theme } = this.props;
      const { title, message } = this.state;

      return (
         <div>
            <div className={stylesErr.errorMessage}>{message}</div>
            <input
               ref={this.input}
               type="text"
               value={title}
               onChange={this.handleChange}
               className={cx(theme, styles.input)}
               placeholder={lang.enterTitle}
            />
         </div>
      );
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;
      const { title } = this.state;

      return (
         <Modal
            theme={theme}
            titleLess={true}
            //  title={lang.renameFolder}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.update}
            cancelText={lang.cancel}
         >
            {lang.renameFolder}
            {this.renderInputName()}
         </Modal>
      );
   }
}
