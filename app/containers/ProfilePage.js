import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import ProfilePage from "../components/ProfilePage/ProfilePage";
import * as UserAction from "../actions/user";
import * as ModalAction from "../actions/modal";

function mapStateToProps(state) {
   return { user: state.user, lang: state.language, theme: state.theme, db: state.database };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators({ ...UserAction, ...ModalAction }, dispatch);
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(ProfilePage);
