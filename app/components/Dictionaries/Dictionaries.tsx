import React from 'react';

// Typescript
import { IDatabase, IDictionary } from '../../typescript';

// Constant

// Attributes of database object [dictionaries] && methods for attributes [CREATE, REMOVE, UPDATE]
import {
   DICTIONARY_DB,
   CREATE,
   REMOVE,
   UPDATE,
   COLLECTION_DB
} from '../../constants/models/database';

// Attributes of model dictionary
import {
   NAME_DICTIONARY,
   SUBJECT_DICTIONARY,
   DEFINITION_DICTIONARY
} from '../../constants/models/dictionary';

interface IDictionaries {
   db: IDatabase;
   dictionaries: IDictionary[];
}

export default function Dictionaries(props: IDictionaries) {
   const { dictionaries } = props;

   // Exemple to add one element in the list of dictionaries
   const handleAddDictionary = () => {
      const { db } = props;

      // db.dictionaries.create({name: "", subject_id: ""})
      db[DICTIONARY_DB][CREATE]({
         [NAME_DICTIONARY]: 'shit',
         [SUBJECT_DICTIONARY]: 'dedede'
      })
         .then(() => {})
         .catch(err => console.warn(err));
   };

   // Exemple of remove the first element in the list of dictionaries
   const handleRemoveDictionary = () => {
      const { db, dictionaries } = props;

      if (dictionaries.length < 1) return;

      const dictionary = dictionaries[0];
      const { collection } = dictionary;

      // db.dictionaries.remove(dictionary)
      db[COLLECTION_DB[collection]]
         [REMOVE](dictionary)
         .then(() => {})
         .catch(err => console.warn(err));
   };

   // Exemple to update one element in the list of dictionarie
   const handleUpdateDictionary = () => {
      const { db, dictionaries } = props;

      if (dictionaries.length < 1) return;
      const dictionary = dictionaries[0];

      // db.dictionaries.update(dictionary)
      db[DICTIONARY_DB][UPDATE]({
         ...dictionary,
         [NAME_DICTIONARY]: 'TOTO',
         [DEFINITION_DICTIONARY]: 'DODU'
      })
         .then(() => {})
         .catch(err => console.warn(err));
   };

   console.warn('Dictionaries :', dictionaries);

   return (
      <>
         <div>Dictionaries</div>
         <button onClick={handleAddDictionary}>Add dico</button>
         <button onClick={handleRemoveDictionary}>Remove dico</button>
         <button onClick={handleUpdateDictionary}>Update dico</button>
      </>
   );
}
