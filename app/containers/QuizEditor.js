import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import QuizEditor from '../components/QuizPage/QuizEditor';
import * as QuizsActions from '../actions/quizs';

function mapStateToProps(state) {
   return {
      lang: state.language,
      theme: state.theme,
      quizs: state.quizs,
      subjects: state.subjects,
      folders: state.folders
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...QuizsActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(QuizEditor);
