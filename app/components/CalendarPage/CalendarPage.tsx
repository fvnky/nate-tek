import React from 'react';
import FullCalendar from '@fullcalendar/react';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import dayGridMonth from '@fullcalendar/daygrid';

// Components
import {
   handleDateToggleModal,
   // useDisplaySchedulesEvent,
   handleEventToggleModal,
   handleDrop,
   handleReceive
} from './schedules/CalendarHandleEvent';

// Constante

// Styles
import styles from './CalendarPage.less';

// Todos
import Todo from '../Todos/Todo';
import TodoCreate from '../Todos/TodoCreate';
import { ITodo } from '../../typescript';

// Typescript
import { ToggleModal, ISchedule, IDatabase } from '../../typescript';

export interface ICalendar {
   toggleModal: ToggleModal;
   schedules: ISchedule[];
   db: IDatabase;
   theme: string;
   todos: Array<ITodo>;
}

function shuffleArray(array) {
   for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
   }
   return array;
}

export default function Calendar(props: ICalendar) {
   console.log('Db :', props.db);
   const { schedules, db } = props;
   const res = React.useRef(null);
   const [event, setEvent] = React.useState([...schedules]);

   React.useEffect(() => {
      // console.log("TRIGER");
      // setEvent([...schedules]);
      setEvent(shuffleArray([...schedules]));
      // const calendarApi = res.current.getApi();
      // console.log(calendarApi);
      // // calendarApi.needsRerender = true
      // console.log(calendarApi.rerenderEvents());
      // res.current.resetOptions(options)
      // calendarApi
      // $('#fullCalendar').fullcalendar('rerenderEvents');
      // jQuery(res.current.elRef.current).fullCalendar('rerenderEvents');
      // console.warn(res.current);
      // const test = res.current.getApi();
      // console.log('Test :', test);
      // test.gotoDate('2019-11-13');
   }, [schedules]);

   const handleDrag = () => {
      // console.log(info);
      console.log('CLICK');
   };

   const options = {
      defaultView: 'dayGridMonth',
      plugins: [dayGridPlugin, interactionPlugin, timeGridPlugin],
      header: {
         left: 'prev,next today',
         center: 'title',
         right: 'dayGridMonth,timeGridWeek,listWeek'
      },
      slotDuration: '00:30:00',
      events: event,
      dateClick: handleDateToggleModal(props),
      editable: true,
      droppable: true,
      selectable: true,
      eventClick: handleEventToggleModal(props),
      eventResize: handleReceive(props),
      fixedWeekCount: false,
      showNonCurrentDates: false,
      eventDragStop: handleDrag,
      eventDragStart: handleDrag,
      eventReceive: handleReceive(props),
      eventDrop: handleDrop(props),
      drop: arg => {
         console.log('drop function');
      }
   };

   return (
      <div style={{ display: 'flex' }}>
         <div className={styles.containerCalendar}>
            <div
               ref={calendar => {
                  calendar;
               }}
            ></div>
            <FullCalendar ref={res} {...options} />
         </div>
         <div className={styles.todosWrapper}>
            {props.todos.map(todo => (
               <Todo database={db} todo={todo} theme={props.theme} />
            ))}
            <TodoCreate database={db} theme={props.theme} />
         </div>
      </div>
   );
}
