// Constant
import {
   FIND_BY_ID,
   CREATE,
   UPDATE,
   REMOVE,
   FIND_ALL
} from '../../constants/models';

interface IParent {
   id: string;
   collection: string;
}

export interface ISubject {
   _id: string;
   _rev: string;
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   name?: string;
   icon: string;
   children: IParent[];
}

export type FindAll = () => Promise<ISubject[]>;
export type FindSubjectById = (id: string) => Promise<ISubject>;
export type CreateSubject = (newSubject: {
   name: string;
   icon: string | null;
}) => Promise<ISubject>;
export type UpdateSubject = (toUpdate: ISubject) => Promise<ISubject>;
export type RemoveSubject = (toRemove: ISubject) => Promise<ISubject>;

export type IDatabaseSubject = {
   [FIND_ALL]: FindAll;
   [FIND_BY_ID]: FindSubjectById;
   [CREATE]: CreateSubject;
   [UPDATE]: UpdateSubject;
   [REMOVE]: RemoveSubject;
};
