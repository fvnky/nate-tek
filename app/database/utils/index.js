import _ from 'lodash';
import async from 'async';
import crypto from 'crypto';

import parseQuestion from './ParseModelQuestion';

import {
   InvalidParameterFormat,
   InvalidDocumentFormat,
   InvalidFieldValue,
   InvalidUpdate
} from '../errors';

// =========================================================== //
// ==================== Queries Selection ==================== //
// =========================================================== //

export function findAllChildren(id, collection) {
   return new Promise((ok, ko) => {
      if (!_.isString(id))
         return ko(
            new InvalidParameterFormat(`The id parameter is not a String`)
         );
      if (!_.isString(collection))
         return ko(
            new InvalidParameterFormat(
               `The collection parameter is not a String`
            )
         );
      this._main
         .find({
            selector: { 'parent.collection': collection, 'parent.id': id }
         })
         .then(res => res.docs)
         .then(ok)
         .catch(ko);
   });
}

// ========================================================= //
// ==================== Data Generation ==================== //
// ========================================================= //

export const timestamp = () => new Date().getTime();

export const generateId = () => crypto.randomBytes(10).toString('hex');

// =========================================================== //
// ==================== Data Manipulation ==================== //
// =========================================================== //

export const hex = str => Buffer.from(str, 'utf8').toString('hex');

export const sync = (array, task) =>
   new Promise((ok, ko) => {
      const tasks = _.map(
         array,
         (item, index) =>
            function(next) {
               task(item, index, next);
            }
      );

      async.series(tasks, (err, res) => (err ? ko(err) : ok(res)));
   });

// ====================================================== //
// ==================== Data Parsing ==================== //
// ====================================================== //

export const isNotFound = err =>
   err.constructor.name === 'PouchError' &&
   err.status === 404 &&
   err.message === 'missing';

export const parseData = (data, mandatory, optional) =>
   new Promise(next => {
      if (!_.isObject(data))
         throw new InvalidParameterFormat(`The data argument is not an Object`);

      if (!_.isArray(mandatory))
         throw new InvalidParameterFormat(
            `The mandatory argument is not an Array`
         );

      if (optional && !_.isArray(optional))
         throw new InvalidParameterFormat(
            `The optional argument is not an Array`
         );

      const fields = [];

      _.forEach(mandatory, field => {
         if (_.isNil(_.get(data, field))) fields.push(field);
      });

      if (!_.isEmpty(fields))
         throw new InvalidParameterFormat(
            `The data parameter is missing mandatory fields: '${fields
               .toString()
               .replace(',', ', ')}'`
         );

      const all = _.union(mandatory, optional);

      _.forIn(data, (value, key) => {
         if (
            !_.union(all, _.uniq(_.map(all, v => v.split('.')[0]))).includes(
               key
            )
         )
            delete data[key];
      });

      next(data);
   });

export const parseDoc = (schema, doc) =>
   new Promise(ok => {
      if (
         !_.isObject(schema) ||
         !_.isObject(schema.definition) ||
         !_.isArray(schema.fields)
      )
         throw new InvalidParameterFormat(`The schema argument is invalid`);

      if (!_.isObject(doc))
         throw new InvalidDocumentFormat(`The document is not an Object`);

      const fields = [];

      _.forIn(schema.fields, field => {
         if (_.isNil(_.get(doc, field))) fields.push(field);
      });

      if (!_.isEmpty(fields))
         throw new InvalidDocumentFormat(
            `The document is missing fields: '${fields
               .toString()
               .replace(',', ', ')}'`
         );

      _.forIn(doc, (value, key) => {
         if (
            !_.union(
               schema.fields,
               _.uniq(_.map(schema.fields, v => v.split('.')[0]))
            ).includes(key)
         )
            delete doc[key];
      });

      _.forIn(schema.fields, field => {
         if (field === 'collection') return;

         const s = _.get(schema.definition, field);
         const d = _.get(doc, field);

         if (_.isArray(s)) {
            if (!_.isArray(d))
               throw new InvalidDocumentFormat(
                  `The document's field '${field}' does not match the required type 'Array'`
               );

            _.map(d, (item, index) => {
               _.forIn(item, (nested, key) => {
                  if (!_.keys(s[0]).includes(key))
                     delete doc[field][index][key];
               });

               _.forIn(s[0], (nested, key) =>
                  checkType(
                     `${field}.${index}.${key}`,
                     nested,
                     _.get(item, key)
                  )
               );
            });
         } else checkType(field, s, d);
      });

      if (doc.collection !== schema.definition.collection)
         throw new InvalidDocumentFormat(
            `The document's collection '${
               doc.collection
            }' doest not math schema's collection '${
               schema.definition.collection
            }'`
         );

      ok(doc);
   });

function checkType(field, schema, doc) {
   if (schema.required && _.isNil(doc))
      throw new InvalidFieldValue(
         `The document's field '${field}' has not been properly initialized, it's value is: '${doc}'`
      );

   if (
      !(function() {
         switch (schema.type) {
            case String:
               return _.isString(doc);
            case Number:
               return _.isNumber(doc);
            case Array:
               return _.isArray(doc);
            case Object:
               return _.isObject(doc);
            case Boolean:
               return _.isBoolean(doc);
            case Date:
               return _.isDate(doc);
            case Function:
               return _.isFunction(doc);
            default:
               throw new InvalidParameterFormat(
                  `The type ${schema.type.name ||
                     schema.type} is not recognized, the schema definition is invalid`
               );
         }
      })()
   )
      throw new InvalidDocumentFormat(
         `The document's field '${field}' does not match the required type '${
            schema.type.name
         }'`
      );
}

export const parseUpdate = (old, doc, fields) =>
   new Promise(ok => {
      if (!_.isObject(doc))
         throw new InvalidDocumentFormat(`The document is not an Object`);

      if (!_.isObject(old))
         throw new InvalidDocumentFormat(
            `The document's reference is not an Object`
         );

      if (_.isNil(fields)) fields = [];

      fields.push('_id');
      fields.push('_rev');
      fields.push('createdAt');
      fields.push('updatedAt');
      fields.push('collection');

      fields = _.uniq(fields);

      _.forIn(doc, (value, key) => {
         if (!fields.includes(key)) return;

         if (!_.isEqual(old[key], value))
            throw new InvalidUpdate(
               `The document's key '${key}' can not be updated`
            );
      });

      ok(doc);
   });

export const parseQuizQuestions = quiz =>
   new Promise(ok => {
      const parsedQuestions = [];

      if (!quiz.questions)
         throw new InvalidUpdate(
            `The document's is not a Quiz, missing questions field`
         );
      if (!Array.isArray(quiz.questions))
         throw new InvalidUpdate(
            `The document quiz, get wrong type for is field questions. Need an array`
         );

      quiz.questions.forEach(question => {
         // console.error("QUESTION :", question);
         if (question.type && parseQuestion[question.type]) {
            if (!question.form)
               throw new Error('Missing field form in question True/False');
            else
               parsedQuestions.push(
                  parseQuestion[question.type](
                     question.form,
                     question.type,
                     question._id
                  )
               );
         }
      });
      quiz.questions = parsedQuestions;
      ok(quiz);
   });
