import * as React from 'react';
import { History } from 'history';
import cx from 'classnames';
import { orderBy } from 'lodash';
import { RouteComponentProps, withRouter, match } from 'react-router';

// Component
import { NAME_LIST, ICON_LIST } from '../Base/List/ElementsList';
import {
   ONCLICK_LIST,
   DROPPABLE_LIST,
   //DRAGGABLE_LIST,
   POPUP_LIST
} from '../Base/List/ActionsList';
// import List from '../Base/List/List';
import { HeaderList } from '../Base/List/HeaderList';

import {
   fillHeader,
   fillDocument,
   EDIT_POPUP,
   DELETE_POPUP,
   MOVE_POPUP,
   LAST_UPDATED_LIST
} from '../Base/List/Descriptor';

// Icon
import IconPlusButton from '../../assets/icons/IconPlusButton';

// Constant
import { ALL } from '../../constants/tabs';

// Style
import styles from './FoldersPage.less';

// typescript
import {
   ICourse,
   IFolder,
   ISubject,
   IQuiz,
   ToggleModal,
   ILanguage,
   IMatchParams,
   CreateCourse
} from '../../typescript';
import CourseCard, { CourseContainer } from '../CourseCard/CourseCard';
import { capitalize } from '../utils';
import NateFileUploader from '../Base/NateFileUploader';
import ImportDocx from './ImportDocx';

const FOLDER_ACTIONS = [
   //DRAGGABLE_LIST,
   ONCLICK_LIST,
   DROPPABLE_LIST
];
const COURSE_ACTIONS = [
   //DRAGGABLE_LIST,
   ONCLICK_LIST
];
const LIST_COLUMNS = [ICON_LIST, NAME_LIST, LAST_UPDATED_LIST, POPUP_LIST];
const LIST_POPUP = [EDIT_POPUP, DELETE_POPUP, MOVE_POPUP];

interface IProps extends RouteComponentProps {
   id: string;
   collection: string;
   folders: IFolder[];
   courses: ICourse[];
   subjects: ISubject[];
   quizs: IQuiz[];
   toggleModal: ToggleModal;
   match: match<IMatchParams>;
   history: History;
   lang: ILanguage;
   theme: string;
   router: any;
   resourcesNavigation: { lastTab: string; lastBrowserPath: string };
   setLastActiveNavigationTab: (tab: string) => void;
   setLastActiveBrowserPath: (path: string) => void;
   createCourse: CreateCourse;
   droppedFiles?: any;
}

class FoldersPage extends React.Component<IProps> {
   componentDidMount() {
      const {
         resourcesNavigation,
         setLastActiveNavigationTab,
         history,
         setLastActiveBrowserPath
      } = this.props;
      setLastActiveNavigationTab(ALL);
      // if (resourcesNavigation.lastTab !== ALL) this.animateChildrenList();
      setLastActiveBrowserPath(history.location.pathname);
   }

   componentDidUpdate(prevProps: IProps) {
      const { id } = this.props;

      this.setLastActiveBrowserPath();
      // if (prevProps.id !== id) {
      //    this.animateChildrenList();
      // }
   }

   // animateChildrenList = () => {
   //    const childrenList = document.getElementById('childrenList');
   //    childrenList.className = cx(
   //       styles.childrenList,
   //       styles.listBeforeTransition
   //    );
   //    setTimeout(() => {
   //       childrenList.className = cx(
   //          styles.childrenList,
   //          styles.listAfterTransition,
   //          styles.listTransition
   //       );
   //    }, 0);
   //    setTimeout(() => {
   //       childrenList.className = styles.childrenList;
   //    }, 400);
   // };

   setLastActiveBrowserPath = () => {
      const {
         history,
         setLastActiveBrowserPath,
         resourcesNavigation
      } = this.props;
      if (
         resourcesNavigation.lastBrowserPath &&
         resourcesNavigation.lastBrowserPath !== history.location.pathname
      )
         setLastActiveBrowserPath(history.location.pathname);
   };

   handleCreateResource = () => {
      const { toggleModal, id, collection } = this.props;
      toggleModal('ModalCreateResource', { parentId: id, collection });
   };

   folderDescriptor = () => {
      const { folders, id, history, courses } = this.props;
      const childrenFolders: IFolder[] = orderBy(
         folders.filter((folder: IFolder) => folder.parent.id === id),
         'name',
         'asc'
      );
      const patternFolders = [];
      const LIST_ITEMS = LIST_COLUMNS.concat(FOLDER_ACTIONS);
      childrenFolders.forEach((folder: IFolder) => {
         const descriptor = fillDocument(
            folder,
            history,
            LIST_POPUP,
            LIST_ITEMS,
            [folders, courses]
         );
         patternFolders.push(descriptor);
      });
      return patternFolders;
   };

   courseDescriptor = () => {
      const { courses, id, history } = this.props;
      const childrenCourses: ICourse[] = orderBy(
         courses.filter((course: ICourse) => course.parent.id === id),
         ['name'],
         'asc'
      );
      const patternCourse = [];
      const LIST_ITEMS = LIST_COLUMNS.concat(COURSE_ACTIONS);
      childrenCourses.forEach((course: ICourse) => {
         const descriptor = fillDocument(
            course,
            history,
            LIST_POPUP,
            LIST_ITEMS
         );
         patternCourse.push(descriptor);
      });
      return patternCourse;
   };

   quizDescriptor = () => {
      const { quizs, id, history } = this.props;
      const childrenQuizs: IQuiz[] = orderBy(
         quizs.filter((quiz: IQuiz) => quiz.parent.id === id),
         ['name'],
         'asc'
      );
      const patternQuiz = [];
      const LIST_ITEMS = LIST_COLUMNS.concat(COURSE_ACTIONS);
      childrenQuizs.forEach((quiz: IQuiz) => {
         const descriptor = fillDocument(quiz, history, LIST_POPUP, LIST_ITEMS);
         patternQuiz.push(descriptor);
      });
      // console.log("Pattern Quiz :", patternQuiz);
      return patternQuiz;
   };

   headerDescriptor = () => {
      const descriptor: any = fillHeader(LIST_COLUMNS);

      descriptor.icon.className = '';
      descriptor.icon.display = false;
      descriptor.popup.display = false;
      // descriptor.popup.className = ;
      return descriptor;
   };

   onDrop = e => {
      e.preventDefault();

      for (let f of e.dataTransfer.files) {
         console.log('Files you dragged here: ', f);
      }

      return false;
   };

   render() {
      const {
         theme,
         folders,
         courses,
         quizs,
         id,
         lang,
         history,
         toggleModal
      } = this.props;
      const items = [
         this.folderDescriptor(),
         // this.courseDescriptor(),
         this.quizDescriptor()
      ];
      const curFolders = folders.filter(
         (folder: IFolder) => folder.parent.id === id
      );
      const curCourses = courses.filter(
         (course: ICourse) => course.parent.id === id
      );
      const curQuizzs = quizs.filter((quiz: IQuiz) => quiz.parent.id === id);
      // console.log("ITEMS :", items);
      return (
         <div className={styles.padding} onDrop={this.onDrop}>
            {curFolders.length === 0 &&
            curCourses.length === 0 &&
            curQuizzs.length === 0 ? (
               <div className={cx(theme, 'empty-list')}>{lang.emptyFolder}</div>
            ) : (
               <>
                  <div className={cx(theme, styles.layoutPage)}>
                     <div className={styles.layoutContent}>
                        <HeaderList
                           header={this.headerDescriptor()}
                           items={items}
                           {...this.props}
                        />
                     </div>
                  </div>
                  {curCourses.length > 0 && (
                     <>
                        <div className={cx(theme, styles.categoryHeader)}>
                           {capitalize(lang.course)}
                        </div>
                        <div
                           style={{ display: 'flex', justifyContent: 'center' }}
                        >
                           <CourseContainer>
                              {courses
                                 .filter(
                                    (course: ICourse) => course.parent.id === id
                                 )
                                 .map(c => (
                                    <CourseCard
                                       course={c}
                                       history={history}
                                       toggleModal={toggleModal}
                                       editable
                                    />
                                 ))}
                           </CourseContainer>
                        </div>
                     </>
                  )}
               </>
            )}

            <div className={cx(theme, styles.addSubjectContainer)}>
               <IconPlusButton
                  className={cx(theme, styles.addSubject)}
                  onClick={this.handleCreateResource}
               />
            </div>

            {/* <ImportDocx {...this.props} /> */}
         </div>
      );
   }
}

export default withRouter(FoldersPage);
