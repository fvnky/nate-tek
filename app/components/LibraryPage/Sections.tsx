import React, { useEffect, useState } from 'react';
import cx from 'classnames';
import withTheme from '../withTheme';

// Utils
import { capitalize } from '../utils';

import routes from '../../constants/routes.json';

// Typescript
import { IMatch, ILanguage } from '../../typescript';

import styles from './Sections.less';

interface IProps {
   theme: string;
   match: IMatch;
   history: any;
   lang: ILanguage;
}

const TABS = ['recommended','search'];
const links = {
   search: routes.LIBRARY_SEARCH,
   recommended: routes.LIBRARY_RECOMMENDED
};

const Sections: React.FC<IProps> = ({ theme, lang, match, history }) => {
   // console.warn (history.location.pathname)
   const goTo = (item: string) => () => {
      history.push(links[item]);
   };

   const tabs = TABS.map((section: string) => {
      let active = links[section] === history.location.pathname ? true : false;
      const name: string = lang[section];

      return (
         <div
            key={section}
            className={cx(theme, styles.tab)}
            role="button"
            onClick={goTo(section)}
            id={section}
         >
            <div
               className={cx(
                  theme,
                  styles.name,
                  active ? styles.nameActive : ''
               )}
            >
               {capitalize(name)}
            </div>
         </div>
      );
   });

   return <div className={cx(theme, styles.layout)}>{tabs}</div>;
};

export default withTheme(Sections);
