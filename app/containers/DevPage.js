import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DevPage from '../components/DevPage/DevPage';
import * as UserActions from '../actions/user';
import * as SubjectsActions from '../actions/subjects';
import * as FoldersActions from '../actions/folders';
import * as CoursesActions from '../actions/courses';

function mapStateToProps(state) {
   return {
      db: state.database,
      folders: state.folders,
      courses: state.courses,
      subjects: state.subjects,
      todos: state.todo
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...UserActions,
         ...SubjectsActions,
         ...FoldersActions,
         ...CoursesActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(DevPage);
