import {
   SCHEMA_TRUE_FALSE,
   TRUE_FALSE,
   TYPE_FORM,
   FORM,
   SCHEMA_ID
} from '../../constants/models';
import { SCHEMA_QCM, QCM } from '../../constants/models/questions/qcm';
import {
   SCHEMA_QUESTION_ANSWER,
   QUESTION_ANSWER
} from '../../constants/models/questions/question_answer';

const TYPE_FORM_SCHEMA = {
   [TRUE_FALSE]: SCHEMA_TRUE_FALSE,
   [QCM]: SCHEMA_QCM,
   [QUESTION_ANSWER]: SCHEMA_QUESTION_ANSWER
};

const ParseModelQuestion = (form, type, id) => {
   const parsedForm = { [TYPE_FORM]: type, [FORM]: {}, [SCHEMA_ID]: id };
   const schema = TYPE_FORM_SCHEMA[type];

   Object.keys(form).forEach(key => {
      // console.log("Form :", form, " key:", key, " type(form[key]) :", typeof(form[key]), " schema :", schema);
      if (schema[key] && schema[key].type === typeof form[key])
         parsedForm[FORM][key] = form[key];
      else throw new Error('Wrong field in form :', type);
   });
   // console.warn('PASED FORM :', parsedForm);
   return parsedForm;
};

export default {
   [TRUE_FALSE]: ParseModelQuestion,
   [QCM]: ParseModelQuestion,
   [QUESTION_ANSWER]: ParseModelQuestion
};
