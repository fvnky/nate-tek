import React from "react";
import cx from "classnames";
import { connect } from "react-redux";

// Style
import styles from "./PopUp.less";

function mapThemeToProps(state: any) {
   return {
      theme: state.theme
   };
}

export const PopUpIcon = connect(mapThemeToProps)(({ children, theme }: any) => {
   const { props } = children;

   return <children.type className={cx(theme, styles.popupIcon)} {...props} />;
});

export const PopUpText = connect(mapThemeToProps)(({ children, theme }: { children: any, theme: string }) => (
   <div className={cx(theme, styles.popupText)}>{children}</div>
));

export function PopUpAction({ lang, onClick, Icon }: { lang: string, onClick: () => void, Icon: any }) {
   return (
      <PopUpElement onClick={onClick}>
         <PopUpIcon>
            <Icon />
         </PopUpIcon>
         <PopUpText>{lang}</PopUpText>
      </PopUpElement>
   );
}

interface IPopUpElement {
   children: any;
   onClick: (e?: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

export function PopUpElement(props: IPopUpElement) {
   const { children, onClick } = props;

   const handleOnClick = (event: any) => {
      event.stopPropagation();
      onClick();
   };

   return (
      <div role="button" onClick={handleOnClick} className={styles.containerPopupElement}>
         {children}
      </div>
   );
}

export function PopUpElements(props: any) {
   const { children } = props;

   return <div className={styles.popUpElements}>{children}</div>;
}
