import React from 'react';

// Component
import NateTimePicker from '../../Base/NateTimePicker';

// Constants
import { START_SCHEDULE, END_SCHEDULE } from '../../../constants/models';

// Styles
import styles from './ModalCreateCalendarEvent.less';

const getTime = date => {
   // console.error(date);
   if (!date) return [0, 0];

   const time = new Date(date);
   // console.log("TIME :", time);
   return [time.getHours(), time.getMinutes()];
};

export const TimePickerStartCalendarEvent = ({ event, setEvent }) => {
   const { [START_SCHEDULE]: timeEvent } = event;
   const [hours, minutes] = getTime(new Date(timeEvent));

   const handleOnChange = (minute, hour) => {
      try {
         const date = new Date(event[START_SCHEDULE]);
         date.setHours(hour);
         date.setMinutes(minute);
         setEvent({ ...event, [START_SCHEDULE]: date.toISOString() });
      } catch (err) {
         // console.error(err);
      }
   };

   return (
      <NateTimePicker
         onChange={handleOnChange}
         hours={hours}
         minutes={minutes}
         minHour={'0'}
         minMin={'0'}
      />
   );
};

export const TimePickerEndCalendarEvent = ({ event, setEvent }) => {
   const { [END_SCHEDULE]: timeEvent, [START_SCHEDULE]: time } = event;
   const [hours, minutes] = getTime(event.end);
   const start = new Date(time);
   const minHour = start.getHours();
   const minMin = start.getMinutes();

   const handleOnChange = (minute, hour) => {
      try {
         const date = new Date(event.end);
         date.setHours(hour);
         date.setMinutes(minute);
         setEvent({ ...event, [END_SCHEDULE]: date.toISOString() });
      } catch (err) {}
   };

   React.useEffect(() => {
      if (minHour > hours) {
         handleOnChange(minutes, minHour);
      }
   }, [minHour]);

   React.useEffect(() => {
      if (hours === minHour && minutes < minMin) handleOnChange(minMin, hours);
   }, [minMin]);

   return (
      <NateTimePicker
         onChange={handleOnChange}
         hours={hours}
         minutes={minutes}
         minHour={minHour}
         minMin={minMin}
      />
   );
};

const TimePickerCalendarEvent = props => {
   const { event, setEvent } = props;

   return (
      <div className={styles.containerTimePicker}>
         <TimePickerStartCalendarEvent event={event} setEvent={setEvent} />
         <div className={styles.spaceTimePicker}>to</div>
         <TimePickerEndCalendarEvent event={event} setEvent={setEvent} />
      </div>
   );
};

export default TimePickerCalendarEvent;
