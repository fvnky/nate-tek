import _ from 'lodash';
import axios, { AxiosStatic, AxiosInstance } from 'axios';
import PouchDB from 'pouchdb-browser';
import PouchDBFind from 'pouchdb-find';

import config from '../config.json';
import { sync } from './utils';

import {
   user,
   subjects,
   folders,
   courses,
   quizs,
   library,
   dictionary,
   todos,
   schedules
} from './collections';

// Constant
import {
   QUIZS_DB,
   SUBJECTS_DB,
   FOLDERS_DB,
   COURSES_DB,
   USER_DB,
   DICTIONARY_DB,
   TODO_DB,
   LIBRARY,
   SCHEDULES_DB
} from '../constants/models';

// Typescript
import {
   IDatabaseQuiz,
   IDatabaseSubject,
   IDatabaseFolder,
   IDatabaseCourse,
   IDatabaseDictionary,
   ITodo,
   IDatabaseTodo,
   IDatabaseSchedule,
   IDatabase
} from '../typescript';
import { Dispatch, GetState, Store } from '../reducers/types';

PouchDB.plugin(PouchDBFind);

const { LOCAL } = process.env;

const endpoint = `${config.remote.protocol}://${
   LOCAL ? 'localhost' : config.remote.host
}:${config.remote.port}`;

// console.log(endpoint);

class Database implements IDatabase {
   dispatch: Dispatch;
   _main: PouchDB.Database;
   _session: PouchDB.Database;
   _cloud: PouchDB.Database;
   request: AxiosInstance;

   [QUIZS_DB]: IDatabaseQuiz;
   [SUBJECTS_DB]: IDatabaseSubject;
   [FOLDERS_DB]: IDatabaseFolder;
   [COURSES_DB]: IDatabaseCourse;
   [DICTIONARY_DB]: IDatabaseDictionary;
   [TODO_DB]: IDatabaseTodo;
   [SCHEDULES_DB]: IDatabaseSchedule;
   [USER_DB]: any;
   [LIBRARY]: any;
   convertibles: any;

   // sync: () => void;
   _sync: any;
   init: (store: Store) => void;
   connect: (id: string, token: string) => void;
   sync: any;
   refresh: any;
   fetch: () => any;
   close: () => any;

   constructor() {
      this._sync = null;
      this.dispatch = null;

      this._main = new PouchDB(config.local.main);
      this._session = new PouchDB(config.local.session);
      this.request = axios.create({ baseURL: `${endpoint}/api` });

      this._main.createIndex({
         index: {
            fields: ['collection'],
            name: 'collectionIndex',
            ddoc: 'CollectionDesignDoc'
         }
      });

      this._main.createIndex({
         index: {
            fields: ['parent.collection', 'parent.id'],
            name: 'parentIndex',
            ddoc: 'parentDesignDoc'
         }
      });

      this._main
         .changes({ since: 'now' })
         .on('change', change => console.warn('--------------- change', change))
         .on('error', change => console.warn('--------------- change', change));

      this[USER_DB] = user.call(this);
      this[SUBJECTS_DB] = subjects.call(this);
      this[FOLDERS_DB] = folders.call(this);
      this[COURSES_DB] = courses.call(this);
      this[QUIZS_DB] = quizs.call(this);
      this[LIBRARY] = library.call(this);
      this[DICTIONARY_DB] = dictionary.call(this);
      this[TODO_DB] = todos.call(this);
      this[SCHEDULES_DB] = schedules.call(this);
   }
}

Database.prototype.init = function(this: IDatabase, store) {
   this.dispatch = store.dispatch;

   this[USER_DB].loadUser().catch(err => console.log(err));
};

Database.prototype.connect = function(this: IDatabase, id, token) {
   const url = `${endpoint}/sync/userdb-${id}`;
   this._cloud = new PouchDB(url, {
      fetch: (url, opts: any) => {
         opts.headers.set('Authorization', `JWT ${token}`);
         return PouchDB.fetch(url, opts);
      }
   });
};

Database.prototype.fetch = function(this: IDatabase) {
   return this._cloud
      .allDocs({ include_docs: true, attachments: true })
      .then(res => {
         const task = function(item, index, cb) {
            this._main
               .get(item.doc._id)
               .then(doc => {
                  if (doc._rev >= item.doc._rev) return;

                  return this._main.put(item.doc);
               })
               .then(() => cb())
               .catch(() => {
                  this._main
                     .put(_.omit(item.doc, ['_rev']))
                     .then(() => cb())
                     .catch(() => cb());
               });
         };

         return sync(res.rows, task.bind(this));
      });
};

Database.prototype.sync = function(this: IDatabase) {
   if (this._sync !== null) return;

   this._sync = this._main
      .sync(this._cloud, { live: true, retry: true })
      .on('change', e => {
         console.log('sync: change', e);
         return this.refresh()
            .then(() => console.log('dispatched'))
            .catch(err => console.log('dispatch error', err));
      })
      .on('paused', () => {
         /*console.log('sync: paused')*/
      })
      .on('active', () => {
         /*console.log('sync: active')*/
      })
      .on('denied', e => console.log('sync: denied', e))
      .on('complete', e => console.log('sync: complete', e))
      .on('error', e => console.log('sync: error', e));
};

Database.prototype.refresh = function() {
   return this.subjects
      .findAll()
      .then(() => this.folders.findAll())
      .then(() => this.courses.findAll())
      .then(() => this.quizs.findAll())
      .then(() => this.dictionaries.findAll())
      .then(() => this.todos.findAll())
      .then(() => this.schedules.findAll());
};

Database.prototype.close = function() {
   return this._main.close();
};

export default Database;
