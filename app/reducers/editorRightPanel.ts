import { SHOW_EDITOR_RIGHT_PANEL } from '../actions/editorRightPanel';

export function editorRightPanel(state: boolean = false, action: { type: string, payload: boolean}) {
   switch (action.type) {
      case SHOW_EDITOR_RIGHT_PANEL: {
         return action.payload;
      }
      default:
         return state;
   }
}
