import { CustomError } from '../factory';

export class TodoAlreadyExists extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The todo already exists';
   }
}

export class TodoNotFound extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The todo could not be found';
   }
}

export class NoTodos extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'No todos could be found';
   }
}
