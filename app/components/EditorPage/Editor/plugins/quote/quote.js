import React from "react";
import QuoteNode from "./QuoteNode";

import { ITALIC } from "../basicFormatting";
import { DEFAULT_NODE } from "../../constants";

export const QUOTE = "quote";

export default function heading() {
   return {
      commands: {
         insertQuote(editor) {
            editor.setBlocks({ type: QUOTE })
                  .toggleMark(ITALIC);
         }
      },
      queries: {
         isQuote(editor) {
            return (editor.value.blocks.some(node => node.type === QUOTE))
         }
      },
      onKeyDown(event, editor, next) {
         // if (editor.value.blocks.get(0).type === QUOTE
         //    && event.key === "Backspace"
         //    && editor.value.blocks.size === 1
         //    && editor.value.blocks.get(0).text === "") {
         //    console.log("Entering Backspace on an Empty Quote  => setting the node back to paragraph")
         //    editor.setBlocks({ type: DEFAULT_NODE })
         //    return;
         // }
         return next();
      },
      renderNode(props, editor, next) {
         const { attributes, children, node } = props;
         if (node.type === QUOTE) {
            return <QuoteNode {...attributes}>{children}</QuoteNode>
         }
         return next();
      }
   };
}
