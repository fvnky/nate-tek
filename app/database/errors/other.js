import { CustomError } from '../factory';

export class InvalidParameterFormat extends CustomError {
   constructor(message) {
      super(message);
      this.message =
         message || 'The parameters do not contains the required fields';
   }
}

export class InvalidDocumentFormat extends CustomError {
   constructor(message) {
      super(message);
      this.message =
         message || 'The document do not contains the required fields';
   }
}

export class InvalidUpdate extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The requested update is invalid';
   }
}

export class InvalidFieldValue extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The field has an invalid value';
   }
}
