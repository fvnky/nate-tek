import _ from 'lodash';
import { IActionSubject } from './types';
import { ISubject } from '../typescript';

export const ADD_SUBJECT = 'add_subject';
export const FETCH_SUBJECT = 'fetch_subject';
export const UPDATE_SUBJECT = 'update_subject';
export const REMOVE_SUBJECT = 'remove_subject';

export function subjects(state: ISubject[] = [], action: IActionSubject) {
   switch (action.type) {
      case ADD_SUBJECT: {
         const res = _.union(state, [action.payload]);
         return res;
      }
      case FETCH_SUBJECT:
         return action.payload;
      case UPDATE_SUBJECT: {
         const ref = _.clone(state);
         const subject = _.find(ref, { _id: action.payload._id });

         _.assign(subject, action.payload);
         return ref;
      }
      case REMOVE_SUBJECT: {
         const ref = _.clone(state);

         _.remove(ref, { _id: action.payload._id });
         return ref;
      }
      default: {
         // const ref = _.clone(state);

         return state;
      }
   }
}
