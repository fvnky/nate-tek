import * as React from 'react';

// Component
import Modal from '../Base/Modal';

// Utils
import { capitalize } from '../utils';

// Style
import styles from './ModalCourse.less';
import stylesErr from './ModalError.less';

// typescript
import { IQuiz, IDatabase, UpdateQuiz, ILanguage } from '../../typescript';

interface IProps {
   id: string;
   db: IDatabase;
   updateQuiz: UpdateQuiz;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   quiz: IQuiz | null;
   title: string;
   message: string;
}

export default class ModalEditQuiz extends React.Component<IProps, IState> {
   input: React.RefObject<HTMLInputElement> = React.createRef<
      HTMLInputElement
   >();

   constructor(props: IProps) {
      super(props);
      this.state = {
         quiz: null,
         title: '',
         message: ''
      };
   }

   componentDidMount() {
      const { id, db } = this.props;
      const input = this.input.current;

      db.quizs
         .findById(id)
         .then((quiz: IQuiz) => {
            if (input) {
               input.value = quiz.name;
               input.focus();
               input.setSelectionRange(0, input.selectionEnd);
            }
            this.setState({ quiz, title: quiz.name });
         })
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   }

   handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { target } = event;
      const input = this.input.current;

      if (input && input.className !== styles.input) {
         input.className = styles.className;
         this.setState({ message: '' });
      }
      this.setState({ title: capitalize(target.value) });
   };

   handleOk = () => {
      const { handleDisplayModal, updateQuiz } = this.props;
      const { title, quiz } = this.state;
      const input = this.input.current;

      if (!quiz) return;
      quiz.name = title;
      updateQuiz(quiz)
         .then(() => handleDisplayModal())
         .catch((err: any) => {
            if (input) {
               input.focus();
               input.setSelectionRange(0, input.selectionEnd);
               input.className = `${styles.input} ${stylesErr.inputErrorColor}`;
            }
            this.setState({ message: err.message });
         });
   };

   renderInputName = () => {
      const { lang } = this.props;
      const { title, message } = this.state;

      return (
         <div>
            <div className={stylesErr.errorMessage}>{message}</div>
            <input
               ref={this.input}
               type="text"
               value={title}
               onChange={this.handleChange}
               className={styles.input}
               placeholder={lang.enterTitle}
            />
         </div>
      );
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;

      return (
         <Modal
            theme={theme}
            title={lang.renameQuiz}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.update}
            cancelText={lang.cancel}
         >
            {this.renderInputName()}
         </Modal>
      );
   }
}
