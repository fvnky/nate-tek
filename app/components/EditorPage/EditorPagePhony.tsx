import React, { Component } from "react";
import { RouteComponentProps, withRouter } from "react-router";

interface IProps extends RouteComponentProps {

}

interface IState {
}

class EditorPagePhony extends Component<IProps, IState> {
   render() {
      return null
   }
}

const editorPageRouter = withRouter(EditorPagePhony);
export default editorPageRouter;
