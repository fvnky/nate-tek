import { Dispatch } from '../reducers/types';

export const SET_COLLAPSE = 'SET_COLLAPSE';

export function setCollapse(collapsed: boolean) {
   return (dispatch: Dispatch) => {
      dispatch({
         type: SET_COLLAPSE,
         payload: collapsed
      });
   };
}
