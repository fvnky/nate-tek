import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';
import { CSSTransition } from 'react-transition-group';

import SliderInput from '../../../../Base/SliderInput';

import IconClose from '../../../../../assets/icons/IconCloseThin';

import styles from './ImageViewer.less';

interface IProps {
   theme: string;
   editor: any;
   src: string;
   caption: string;
   close: () => void;
   show: boolean;
}

interface IState {
   zoomFactor: number;
}

export default class ImageViewer extends Component<IProps, IState> {
   state = {
      zoomFactor: 1
   };

   componentDidMount() {
      document.addEventListener('keydown', this.handleKeyDown);
   }

   componentWillUnmount() {
      document.addEventListener('keydown', this.handleKeyDown);
   }

   handleKeyDown = e => {
      const { close } = this.props;

      if (e.keyCode === 27) {
         // Escape key
         close();
      }
   };

   dontPropagate = e => {
      e.preventDefault();
      e.stopPropagation();
   };

   onSliderChange = e => {
      e.preventDefault();
      e.stopPropagation();
      const zoomFactor = parseFloat(e.target.value);
      this.setState({ zoomFactor });
   };

   render() {
      const { theme, src, caption, close, show } = this.props;
      const { zoomFactor } = this.state;
      const root = window.document.getElementById('root')
      const container = document.getElementById("image-viewer-container");
      // if (container) {
      //    container.scrollLeft = (container.scrollWidth - container.clientWidth) / 2 ;
      //    container.scrollTop = (container.scrollHeight - container.clientHeight) / 2 ;
      // }

      return ReactDOM.createPortal(
         <CSSTransition
         in={show}
         timeout={200}
         classNames={{
            appear: '',
            appearActive: '',
            enter: styles.enter,
            enterActive: styles.enterActive,
            enterDone: '',
            exit: styles.exit,
            exitActive: styles.exitActive,
            exitDone: '',
         }}
         unmountOnExit
      >
         <div className={styles.layout}>
            {/* <div className={cx(theme, styles.mask)} onMouseDown={this.dontPropagate} /> */}
            <div className={styles.container} >
               <IconClose className={styles.iconClose} onClick={close}/>
               <div className={styles.imageContainer} id="image-viewer-container">
                  {/* <span className={styles.helper}></span> */}
                  <img
                     style={{transform: `scale(${zoomFactor}, ${zoomFactor})`}}
                     // style={{width: `${100 * zoomFactor}%`}}
                     className={cx(theme, styles.image)}
                     src={src}
                     onClick={close}

                  />
               </div>
               <figcaption className={styles.caption}>{caption}</figcaption>
               <div className={styles.sliderWrapper}>
                  <div className={styles.slider}>
                     <SliderInput
                        min="1"
                        max="3"
                        onChange={this.onSliderChange}
                        value={zoomFactor}
                     />
                  </div>
               </div>
            </div>
         </div>
         </CSSTransition>,
         root
      );
   }
}
