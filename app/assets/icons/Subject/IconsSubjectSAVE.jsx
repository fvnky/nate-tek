import * as React from 'react';
import IconQuestionMark from './IconQuestionMark';

const iconsSubject = {
   IconQuestionMark: <IconQuestionMark />
};

export { iconsSubject };
