import React from 'react';
import { RouteComponentProps } from 'react-router';

import styles from './NateFileUploader.less';
// typescript
import { ICourse, IMatch } from '../../../typescript';

interface IProps {
   db: any;
   courses: ICourse[];
   id: string;
   back: () => void;
}

const ResourceViewer: React.FC<IProps> = ({ back, id }) => {
   return (
      <div style={{ backgroundColor: 'red', height: '100%', width: '100%' }}>
         <button onClick={back}>BACK</button>
         {id}
      </div>
   );
};

export default ResourceViewer;
