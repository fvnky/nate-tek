import * as React from 'react';
import { Link } from 'react-router-dom';

// Component
import Dictionary from './Dictionaries/Dictionaries';
import TimePicker from './Base/NateTimePicker';

// Style
import styles from './Home.less';

// Constant
import routes from '../constants/routes.json';
// import { DICTIONARY_DB, CREATE, NAME_DICTIONARY, SUBJECT_DICTIONARY, REMOVE, UPDATE } from '../constants/models';

// Typescript
import { IDictionary, IDatabase } from '../typescript';
// import { DELETE } from '../constants/modal';
// import { IDatabase } from '../typescript/flow';

function allowDrop(ev) {
   ev.preventDefault();
}

function drag(ev) {
   ev.dataTransfer.setData('id', ev.target.id);
}

function drop(ev) {
   // ev.preventDefault();
   const data = ev.dataTransfer.getData('id');
   console.log('DATA :', data);
   ev.target.appendChild(document.getElementById(data));
}

interface IHome {
   dictionaries: IDictionary[];
   db: IDatabase;
}

export default class Home extends React.Component<IHome> {
   constructor(props) {
      super(props);
   }

   handleOnChange = (min, hours) => {

   }

   render() {
      // const { dictionaries, db } = this.props;

      // console.log("Dictionary :", dictionaries);
      return (
         <div>
            Select a view
            <br />
            <Link to={routes.LOGIN}>LOGIN PAGE</Link>
            <br />
            <div
               id="div1"
               className={styles.dropArea}
               onDrop={drop}
               onDragOver={allowDrop}
            />
            <div
               id="div2"
               className={styles.dropArea}
               onDrop={drop}
               onDragOver={allowDrop}
            />
            <div>
               <p id="drag1" draggable={true} onDragStart={drag}>
                  Deplace le text
               </p>
               <p id="drag2" draggable={true} onDragStart={drag}>
                  Deplace le text
               </p>
            </div>
            <div className={styles.container}>
               <div className={styles.element1}>Icon</div>
               <div className={styles.element2}> Naaaaaaaame</div>
               <div className={styles.element3}> button</div>
            </div>
            <Dictionary {...this.props} />
            {/* <div>
               {dictionary}
            </div> */}
            {/* <button onClick={this.handleOnClickDico}>Add dico</button>
            <button onClick={this.handleRemoveDictionary}>Remove dico</button>
            <button onClick={this.handleOnClickUpdate}>Update dico</button> */}
            {/* <iframe style={{border: "1px solid red", width: "100%", height: "100%"}} src="./pdf.pdf" ></iframe> */}
         </div>
      );
   }
}
