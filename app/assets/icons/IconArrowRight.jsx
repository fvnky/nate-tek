import * as React from 'react';

const IconArrowRight = props => (
   <svg viewBox="0 0 100 100" {...props}>
      <g transform="translate(0,-952.36218)">
         <path d="m 32.78125,962.34375 a 3.0003,3.0003 0 0 0 -2,5.0625 l 32.125,34.96875 -32.125,34.9688 a 3.0080933,3.0080933 0 1 0 4.4375,4.0624 l 34,-37 a 3.0003,3.0003 0 0 0 0,-4.0624 l -34,-37.00005 a 3.0003,3.0003 0 0 0 -2.4375,-1 z" />
      </g>
   </svg>
);

export default IconArrowRight;
