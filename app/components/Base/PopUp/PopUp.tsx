import * as React from 'react';
import cx from 'classnames';
import { CSSTransition } from 'react-transition-group';

import withTheme from '../../withTheme';

import IconClose from '../../../assets/icons/IconClose';
// Utils
import { setPosition } from './PopUpPosition';
import { MIDDLE, BOTTOM_LEFT } from '../../../constants/PopUp';

// Style
import styles from './PopUp.less';

// EXPORT
export {
   PopUpElements,
   PopUpElement,
   PopUpText,
   PopUpIcon,
   PopUpAction
} from './PopUpElements';
export {
   RIGHT,
   LEFT,
   TOP,
   TOP_RIGHT,
   TOP_LEFT,
   BOTTOM,
   BOTTOM_RIGHT,
   BOTTOM_LEFT,
   MIDDLE
} from '../../../constants/PopUp';

interface IPropsPopUp {
   children: React.ReactNode | React.ReactNode[];
   content: React.ReactNode | React.ReactNode[];
   placement?: string;
   onVisibleChange?: (display: boolean) => void;
   theme: string;
   position: string;
}

function PopUp(props: IPropsPopUp) {
   const { children, content, placement = BOTTOM_LEFT, theme, position = "" } = props;
   const popup = React.useRef<HTMLDivElement>(null);
   const triggerPopup = React.useRef<HTMLDivElement>(null);
   const [display, setDisplay] = React.useState(false);

   const hide = () => {
      const { onVisibleChange } = props;

      if (onVisibleChange) onVisibleChange(false);
      setDisplay(false);
   };

   React.useEffect(() => {
      document.addEventListener('click', hide);
      return () => {
         document.removeEventListener('click', hide);
      };
   });

   React.useEffect(() => {
      if (triggerPopup.current && popup.current)
         setPosition(placement, triggerPopup.current, popup.current);
   }, [display]);

   const displayPopUp = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      e.stopPropagation();
      setDisplay(true);
   };

   const stopPropagation = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      e.stopPropagation();
   };

   return (
      <div className={`${styles.containerPopup} ${position}`}>
         <div
            className={styles.triggerEvent}
            role="button"
            ref={triggerPopup}
            onClick={displayPopUp}
         >
            {children}
         </div>
         <div
            role="presentation"
            className={styles.containerPopup}
            onClick={stopPropagation}
         >
            <div ref={popup} className={styles.test}>
               <CSSTransition
                  in={display}
                  timeout={200}
                  classNames={{
                     enter: styles.enter,
                     enterActive: styles.enterActive,
                     exit: styles.exit,
                     exitActive: styles.exitActive,
                  }}
                  onExited={hide}
                  mountOnEnter
                  unmountOnExit
               >
                  <div className={cx(styles.popup, styles.hidden)} >
                     <div className={cx(theme, styles.closeButton)}>
                        <IconClose className={cx(theme, styles.iconClose)} />
                     </div>
                     {content}
                  </div>
               </CSSTransition>
            </div>
         </div>
      </div>
   );
}

export default withTheme(PopUp);
