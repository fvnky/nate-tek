import * as React from 'react';
import {
   NAME_LIST,
   ICON_LIST,
   CREATE_LIST,
   UPDATE_LIST,
   LAST_UPDATED_LIST,
   ONCLICK_LIST,
   DROPPABLE_LIST,
   DRAGGABLE_LIST,
   POPUP_LIST,
   TIME_HEADER_LIST,
   EDIT_POPUP,
   DELETE_POPUP,
   POPUP_TRIGGER_LIST
} from '../constants/list';
import { IDrag, IDrop, ILanguage } from '.';

interface IClassName {
   className?: string;
   lang?: ILanguage;
   theme?: string;
}

export interface IDocumentHeaderTime extends IClassName {
   [TIME_HEADER_LIST]: string;
   [key: string]: any;
}

export interface IDocumentName extends IClassName {
   [NAME_LIST]: string;
}

export interface IDocumentLastUpdated extends IClassName {
   [LAST_UPDATED_LIST]: number;
   relativeToNow: boolean;
}

export interface IDocumentHeader extends IClassName {
   text: string;
   display: boolean;
}

export interface IDocumentIcon extends IClassName {
   [ICON_LIST]: any;
}

export interface IOnClick extends IClassName {
   [ONCLICK_LIST]: (e: any) => void; // (link: string) => (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

export interface IActionPopUp extends IClassName {
   [POPUP_LIST]: React.ReactNode;
   [POPUP_TRIGGER_LIST]: React.ReactNode;
   [key: string]: any;
}

export interface IListItem extends IClassName {
   id?: string;
   [NAME_LIST]?: IDocumentName;
   [ICON_LIST]?: IDocumentIcon;
   [ONCLICK_LIST]?: any;
   [DROPPABLE_LIST]?: IDrop;
   [DRAGGABLE_LIST]?: IDrag;
   [POPUP_LIST]?: IActionPopUp;
   [TIME_HEADER_LIST]?: IDocumentHeaderTime;
   [LAST_UPDATED_LIST]?: IDocumentLastUpdated;
   className?: string;
}
