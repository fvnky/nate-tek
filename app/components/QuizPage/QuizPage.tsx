import React from 'react';
import { RouteComponentProps } from 'react-router';
import { Route, Switch, Redirect } from 'react-router';

// Component
import QuizEditor from '../../containers/QuizEditor';
import QuizGame from '../../containers/QuizGame';
import QuizHome from '../../containers/QuizHome';

// Style
// import styles from './QuizPage.less';

// Constant
import routes from '../../constants/routes.json';

// Typescript
import {
   IMatch,
   IQuiz,
   UpdateQuiz,
   IFolder,
   ISubject,
   ILanguage
} from '../../typescript';

export interface IQuizPage extends RouteComponentProps {
   lang: ILanguage;
   updateQuiz: UpdateQuiz;
   match: IMatch;
   quizs: IQuiz[];
   folders: IFolder[];
   subjects: ISubject[];
}

export default function QuizPage(props: IQuizPage) {
   const { match } = props;
   // console.log("MATCH :", match);
   // const [ view, setView ] = React.useState("HOME");

   // const displayContent = () => {
   //    switch(view) {
   //       case "HOME":
   //          return <QuizHome setView={setView} {...props}/>;
   //       case "EDIT":
   //          return <QuizEditor setView={setView} {...props}/>;
   //       case "GAME":
   //          return <QuizGame setView={setView} {...props} />;
   //       default:
   //          return <QuizHome setView={setView} {...props} />;
   //    };
   // };
   // const HOME_DEDE = <QuizHome setView={setView} {...props}/>;

   //return displayContent();
   return (
      <Switch>
         <Route exact={true} path={routes.QUIZ} component={QuizHome} />
         <Route exact={true} path={routes.QUIZ_HOME} component={QuizHome} />
         <Route exact={true} path={routes.QUIZ_EDITOR} component={QuizEditor} />
         <Route exact={true} path={routes.QUIZ_GAME} component={QuizGame} />
      </Switch>
   );
}
