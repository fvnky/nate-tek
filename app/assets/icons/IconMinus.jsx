import * as React from "react";

const IconMinus = props => (
   <svg viewBox="0 0 394 91.25" {...props}>
    <path  d="M357 0c21,0 37,16 37,37 0,20 -16,36 -37,36 -227,0 -93,0 -320,0 -21,0 -37,-16 -37,-36 0,-21 16,-37 37,-37 227,0 93,0 320,0z"/>
   </svg>
);

export default IconMinus;
