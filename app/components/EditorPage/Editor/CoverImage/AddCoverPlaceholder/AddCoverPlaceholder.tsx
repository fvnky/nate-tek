import React from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';
import axios from 'axios';

import withTheme from '../../../../withTheme';
import styles from './AddCoverPlaceholder.less';
import IconPlus from '../../../../../assets/icons/IconPlus';
import { ICourse } from '../../../../../typescript';

type Props = {
   theme: string;
   editor: any;
   db: any;
   course: ICourse;
};

const AddCoverPlaceholder: React.FC<Props> = ({
   editor,
   theme,
   db,
   course
}) => {
   const [active, setActive] = React.useState(false);
   const [term, setTerm] = React.useState('');
   const [images, setImages] = React.useState([]);

   const onWindowClick = e => {
      console.log('clicked');
      if (active) {
         setActive(false);
         setTerm('');
         setImages([]);
      }
   };

   const onClickOverall = e => {
      if (active) e.stopPropagation();
   };

   React.useEffect(() => {
      window.addEventListener('click', onWindowClick);
      return () => {
         window.removeEventListener('click', onWindowClick);
      };
   });

   const onClickPlaceholder = e => {
      console.log('cliked placeholder');

      if (!active) {
         setActive(true);
      }

      // const newCourse = course;
      // newCourse.coverImage = {
      //    full: 'www.test.com',
      //    raw: 'www.test.com',
      //    regular: 'www.test.com',
      //    small: 'www.test.com',
      //    thumb: 'www.test.com',
      // }
      // console.log("***** got", newCourse)
      // db.courses.update(newCourse)
      // .then((res) => console.warn("got", res))
      // .catch((err) => console.error(err))
   };

   const onInputChange = e => {
      setTerm(e.target.value);
   };

   const onKeyDown = e => {
      if (e.key === 'Enter' && term !== '') {
         console.warn('searched ', term);
         onSearchSubmit(term);
      }
   };

   const onSearchSubmit = async term => {
      const response = await axios.get(
         'https://api.unsplash.com/search/photos',
         {
            params: { query: term },
            headers: {
               Authorization:
                  'Client-ID d60aa088ae5715726904155e57dee3ffb903eb64dc5ce8b197f09fb43db62042'
            }
         }
      );
      setImages(response.data.results);
      // this.setState({ images: response.data.results })
   };

   const onImageClick = image => e => {
      e.stopPropagation();
      const newCourse = Object.assign({}, course, { coverImage: image.urls });
      // console.log(image.urls)
      // newCourse.coverImage = image.urls;
      console.log('image.urls', image.urls);
      console.log('***** got', newCourse);
      // console.log("is frozen", Object.isFrozen(newCourse))

      db.courses
         .update(newCourse)
         .then(res => console.warn('got', res))
         .catch(err => console.error(err));
   };

   const renderSearch = () => {
      if (active) {
         return (
            <div className={styles.layout}>
               <input
                  type="text"
                  value={term}
                  onChange={onInputChange}
                  onKeyDown={onKeyDown}
                  className={cx(theme, styles.input)}
                  autoFocus
                  placeholder=" Search for a cover..."
               />
               <div className={styles.wrapper}>
                  <div className={styles.masonry}>
                     {images.map(image => (
                        <div className={styles.brick}>
                           <img
                              src={image.urls.thumb}
                              key={image.id}
                              onClick={onImageClick(image)}
                           />
                        </div>
                     ))}
                  </div>
               </div>
               {images.length !== 0 && <hr className={styles.hr} />}
            </div>
         );
      }
   };

   console.log('images', images);

   return (
      <div onClick={onClickOverall}>
         <div className={cx(theme, styles.layout)} onClick={onClickPlaceholder}>
            <IconPlus className={cx(theme, styles.plus)} />
            Add a cover image
            {/* <button onClick={onImageClick({urls: {full: "test"} })}> add image</button> */}
         </div>
         {renderSearch()}
      </div>
   );
};

export default withTheme(AddCoverPlaceholder);
