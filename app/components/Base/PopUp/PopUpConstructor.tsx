import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as ModalActions from '../../../actions/modal';

// Constant
import {
   MODAL,
   UPDATE,
   DELETE,
   MOVE,
   MODAL_MOVE_RESOURCE
} from '../../../constants/modal';

// Utils
import { capitalize } from '../../utils';

// Component
import { PopUpElements, PopUpAction } from './PopUpElements';

// Icon
import IconDelete from '../../../assets/icons/IconDelete';
import IconEdit from '../../../assets/icons/IconEdit';
import IconFolder from '../../../assets/icons/IconFolder';

// Typescript
import { IDbDocument, ILanguage, ToggleModal } from '../../../typescript';

export const EDIT_POPUP = 'edit_popup';
export const DELETE_POPUP = 'delete_popup';
export const MOVE_POPUP = 'move_popup';
export const POPUP_TRIGGER_LIST = 'popup_trigger_list';

export function PopUpEdit({
   lang,
   handleMenuClick,
   doc
}: {
   lang: ILanguage;
   handleMenuClick: (
      modalName: string,
      item: { id: string; collection: string }
   ) => () => void;
   doc: IDbDocument;
}) {
   const { collection, _id } = doc;

   return (
      <PopUpAction
         key={`${_id}_POPUP_EDIT`}
         lang={lang.update}
         onClick={handleMenuClick(MODAL + UPDATE + collection, {
            id: _id,
            collection: collection
         })}
         Icon={IconEdit}
      />
   );
}

export function PopUpDelete({
   lang,
   handleMenuClick,
   doc
}: {
   lang: ILanguage;
   handleMenuClick: (
      modalName: string,
      item: { id: string; collection: string }
   ) => () => void;
   doc: IDbDocument;
}) {
   const { collection, _id } = doc;

   return (
      <PopUpAction
         key={`${_id}_POPUP_DELETE`}
         lang={lang.delete}
         onClick={handleMenuClick(MODAL + DELETE + collection, {
            id: _id,
            collection: collection
         })}
         Icon={IconDelete}
      />
   );
}

export function PopUpMove({
   lang,
   handleMenuClick,
   doc
}: {
   lang: ILanguage;
   handleMenuClick: (modalName: string, modalProps: any) => () => void;
   doc: IDbDocument;
}) {
   const { collection, _id } = doc;

   return (
      <PopUpAction
         key={`${_id}_POPUP_MOVE`}
         lang={lang.move}
         onClick={handleMenuClick(MODAL_MOVE_RESOURCE, { resource: doc })}
         Icon={IconFolder}
      />
   );
}

const buildPopUp = {
   [EDIT_POPUP]: PopUpEdit,
   [DELETE_POPUP]: PopUpDelete,
   [MOVE_POPUP]: PopUpMove
};

interface IProps {
   popup: string[];
   doc: IDbDocument;
   lang: ILanguage;
   toggleModal: ToggleModal;
}

function PopUpConstructor(props: IProps) {
   const { popup, doc, lang } = props;
   const tab = [];

   const handleMenuClick = (modalName: string, modalProps: any) => () => {
      const { toggleModal } = props;
      toggleModal(modalName, modalProps);
   };

   popup.forEach((elem: string) => {
      if (buildPopUp[elem])
         tab.push(buildPopUp[elem]({ lang, handleMenuClick, doc }));
   });
   return <PopUpElements>{tab}</PopUpElements>;
}

function mapStateToProps(state: any) {
   return {
      lang: state.language
   };
}

function mapDispatchToProps(dispatch: any) {
   return bindActionCreators<any, any>(ModalActions, dispatch);
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(PopUpConstructor);
