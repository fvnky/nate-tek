import _ from 'lodash';

export default class Enumeration {
   constructor(data) {
      if (_.isArray(data)) {
         this.instance = Array;
         _.map(data, (value, i) => {
            this[i] = value;
         });
      } else {
         this.instance = Object;
         _.forIn(data, (value, key) => {
            this[key] = value;
         });
      }

      return Object.freeze(this);
   }

   includes(item) {
      if (this.instance === Array) return _.includes(this, item);

      if (this.instance === Object) return _.has(this, item);
   }

   find(item) {
      if (this.instance === Array) return _.find(this, e => e === item);

      if (this.instance === Object) return this[item];
   }
}
