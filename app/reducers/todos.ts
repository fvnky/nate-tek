import _ from 'lodash';
import { ITodo } from '../typescript';
import { IActionTodo } from './types';

export const ADD_TODO = 'add_todo';
export const FETCH_TODO = 'fetch_todo';
export const UPDATE_TODO = 'update_todo';
export const REMOVE_TODO = 'remove_todo';

export function todo(state: ITodo[] = [], action: IActionTodo) {
   switch (action.type) {
      case ADD_TODO:
         return _.union(state, [action.payload]);
      case FETCH_TODO: {
         return action.payload;
      }
      case UPDATE_TODO: {
         const ref = _.clone(state);
         const quiz = _.find(ref, { _id: action.payload._id });

         _.assign(quiz, action.payload);
         return ref;
      }
      case REMOVE_TODO: {
         const ref = _.clone(state);

         _.remove(ref, { _id: action.payload._id });
         return ref;
      }

      default: {
         return state;
      }
   }
}
