import React from "react";
import styles from "../../Editor.less";

import {DEFAULT_NODE} from "../../constants";

export const HEADING = "heading";

export default function heading(options = {}) {
   return {
      commands: {
         toggleHeading(editor, level) {
            if (editor.value.blocks.some(node => node.type === HEADING && node.data.get("level") === level)) editor.setBlocks(DEFAULT_NODE);
            else editor.setBlocks( {type: HEADING, data: {level}});
         }
      },
      queries: {
         isHeading(editor, level) {
            return (editor.value.blocks.some(node => node.type === HEADING && node.data.get("level") === level))
         }
      },
      // onKeyDown(event, editor, next) {
      //    const isHeading = editor.value.blocks.some(node => node.type === HEADING );
      //    if (isHeading && event.key === "Enter") {
      //       editor.splitBlock()
      //             .setBlocks(DEFAULT_NODE);
      //       return;
      //    }
      //    return next();
      // },
      renderNode(props, editor, next) {
         const { attributes, children, node } = props;
         const level = node.data.get("level");
         if (node.type === HEADING) {
            switch (level) {
               case 1:
                  return <h1 id={node.key} {...attributes}>{children}</h1>;
               case 2:
                  return <h2  id={node.key}{...attributes}>{children}</h2>;
               case 3:
                  return <h3 id={node.key} {...attributes}>{children}</h3>;
               case 4:
                  return <h4 id={node.key} {...attributes}>{children}</h4>;
               default:
                  return next();
            }
         }
         return next();
      }
   };
}
