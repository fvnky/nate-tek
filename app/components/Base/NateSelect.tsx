import * as React from 'react';
import cx from 'classnames';
import { CSSTransition } from 'react-transition-group';

import withTheme from '../withTheme';

import IconArrowDown from '../../assets/icons/IconArrowDown';
// Style
import styles from './NateSelect.less';

interface IProps {
   value?: any;
   onChange?: (value: any) => void;
   children: Array<any>;
   theme: string;
   className?: string;
}

export function Option({ value, children }) {
   return <div>{children}</div>;
}

const NateSelect: React.FC<IProps> = ({
   value,
   onChange,
   children,
   theme,
   className
}) => {
   const [inputRef, setInputRef] = React.useState(null);
   const [node, setNode] = React.useState(null);
   const [opened, setOpened] = React.useState(false);
   const [inputFocusedOrHovered, setInputFocusedOrHovered] = React.useState(
      false
   );

   React.useEffect(() => {
      document.addEventListener('click', handleClick, false);
      return () => {
         document.removeEventListener('click', handleClick, false);
      };
   });

   const handleClick = e => {
      if (node.contains(e.target)) return;
      setOpened(false);
   };

   const onSelectClick = () => {
      setOpened(!opened);
   };

   const getActiveValue = () => {
      if (children && Array.isArray(children) && value) {
         return children.find(c => c.props.value === value);
      }
      return null;
   };

   const handleOptionClick = (value: any) => {
      onChange(value);
      setOpened(false);
   };

   const onInputMouseEnter = () => {
      setInputFocusedOrHovered(true);
   };

   const onInputMouseLeave = () => {
      setInputFocusedOrHovered(false);
   };

   const renderDropdownItems = () => {
      if (children && value) {
         return children.map(c => {
            const active = value === c.props.value;
            return (
               <div
                  key={c.props.value}
                  role="button"
                  className={cx(
                     theme,
                     styles.option,
                     active ? styles.optionActive : ''
                  )}
                  onClick={() => handleOptionClick(c.props.value)}
               >
                  {c}
               </div>
            );
         });
      }
      return null;
   };

   const renderDropdown = () => {
      const style = {
         top: 0,
         left: 0,
         width: 100,
         zIndex: 10
      };
      if (inputRef) {
         style.top = inputRef.offsetTop + inputRef.offsetHeight;
         style.left = inputRef.offsetLeft;
         style.width = inputRef.offsetWidth;
      }

      return (
         <CSSTransition
            in={opened}
            timeout={500}
            classNames={{
               appear: '',
               appearActive: '',
               enter: styles.enter,
               enterActive: styles.enterActive,
               enterDone: '',
               exit: styles.exit,
               exitActive: styles.exitActive,
               exitDone: styles.exitDone
            }}
            unmountOnExit
         >
            <div className={cx(theme, styles.dropDownContainer)} style={style}>
               {renderDropdownItems()}
            </div>
         </CSSTransition>
      );
   };

   const active = getActiveValue();
   return (
      <div ref={n => setNode(n)} className={cx(styles.node, className)}>
         <div
            role="button"
            className={cx(theme, styles.input, className)}
            onClick={onSelectClick}
            ref={i => setInputRef(i)}
            onMouseLeave={onInputMouseLeave}
            onMouseEnter={onInputMouseEnter}
         >
            {active}
            <div>
               <IconArrowDown
                  className={cx(
                     theme,
                     styles.iconArrowDown,
                     inputFocusedOrHovered || opened
                        ? styles.iconArrowDownActive
                        : '',
                     opened ? styles.iconUpsideDown : ''
                  )}
               />
            </div>
         </div>
         {renderDropdown()}
      </div>
   );
};

export default withTheme(NateSelect);
