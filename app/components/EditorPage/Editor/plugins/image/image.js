import React from "react";
import { Block } from "slate";
import ImageNode from "./ImageNode";
import { DEFAULT_NODE } from "../../constants";

export const IMAGE = "image";

export const IMAGE_STYLE_LEFT = "left"
export const IMAGE_STYLE_CENTER = "center"
export const IMAGE_STYLE_EXTENDED = "extended"
// export const IMAGE_STYLE_RIGHT = "right"


export default function image(options = {}) {
   return {
      schema: {
         document: {
            last: { type: DEFAULT_NODE },
            normalize: (editor, { code, node, child }) => {
               // eslint-disable-next-line default-case
               switch (code) {
                  case "last_child_type_invalid": {
                     const paragraph = Block.create(DEFAULT_NODE);
                     return editor.insertNodeByKey(node.key, node.nodes.size, paragraph);
                  }
               }
            }
         },
         blocks: {
            image: {
               isVoid: true
            }
         }
      },
      commands: {
         insertImage(editor, src, options) {
            let width = 100;
            if (options && options.width <= options.height) {
                  width = 60;
            }
            editor.insertBlock({
               type: IMAGE,
               data: { src, style: IMAGE_STYLE_CENTER, caption: "", width}
            });
         },
         setImageStyle(editor, node, style ) {
            // const width = style === IMAGE_STYLE_LEFT ? 60 : 100;
            // const width = 60;
            const data = { style  }
            editor.setNodeByKey(node.key, { data: node.data.merge(data)})
         },
         setImageWidth(editor, node, width ) {
            const data = { width }
            editor.setNodeByKey(node.key, { data: node.data.merge(data)})
         }
      },
      queries: {
         isImage(editor) {
            return editor.value.blocks.some(node => node.type === IMAGE);
         }
      },
      renderNode(props, editor, next) {
         const { attributes, node, isFocused } = props;
         // console.log(editor.value.fragment.text)
         // console.warn(editor.value.selection.anchor.offset)
         switch (node.type) {
            case IMAGE: {
               return (
                  <>
                     <ImageNode editor={editor} node={node} attributes={attributes} isFocused={isFocused} />
                  </>
               );
            }
            default: {
               return next();
            }
         }
      }
   };
}
