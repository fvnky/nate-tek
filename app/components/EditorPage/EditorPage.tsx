import React from 'react';
import { CSSTransition } from 'react-transition-group';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as CourseActions from '../../actions/courses';
import { toggleModal } from '../../actions/modal';
import { showToast } from '../../actions/toast';

import cx from 'classnames';

import Topbar from './Topbar/Topbar';
import Toolbar from './Toolbar/Toolbar';
import Editor from './Editor/Editor';
import Footer from './Footer/Footer';

import styles from './EditorPage.less';

// Typescript
import {
   ICourse,
   ILanguage,
   ISubject,
   IFolder,
   EditorTab,
   ToastOptions,
   ToggleModal
} from '../../typescript';

interface IProps {
   // Redux
   toggleModal: ToggleModal;
   db: any;
   lang: ILanguage;
   showToast: (options: ToastOptions) => void;
   courses: ICourse[];
   subjects: ISubject[];
   folders: IFolder[];
   theme: string;

   editorRightPanel: boolean;
   toggleEditorRightPanel: () => void;

   show: boolean;
   setRef: (editor: any) => void;
   onChange: (value: any) => void;
   setZoom: (value: number) => void;
   changeCourseName: (id: string, newName: any) => void;
   // saveCourse: (id: string, value: any) => void;
   closeEditorTabIndex: (index: number) => void;
   switchEditorTabIndex: (a: number, b: number) => void;
   updateEditorTabIndex: (index: number, tab: EditorTab) => void;
   setActiveEditorTabIndex: (index: number) => void;
   addNewTab: () => void;

   activeIndex: number;
   tabs: EditorTab[];
   zoom: number;

   editor: any;
}

class EditorPage extends React.Component<IProps> {
   renderNoTab = () => {
      const { theme } = this.props;
      return (
         <div className={cx(theme, styles.noTab)}>
            <p className={cx(theme, styles.noTabText)}>
               There are no tabs opened right now.
               <br />
               Fire one up!
            </p>
         </div>
      );
   };

   renderEditor = () => {
      const {
         toggleModal,
         theme,
         courses,
         folders,
         subjects,
         showToast,
         zoom,
         tabs,
         activeIndex,
         editor,
         setRef,
         onChange,
         updateEditorTabIndex,
         db,
         // saveCourse,
         changeCourseName,
         setZoom,
         editorRightPanel,
         toggleEditorRightPanel,
         lang,
         closeEditorTabIndex
      } = this.props;

      return (
         <>
            {/* className={cx(styles.onLoadFadeIn, tabs[activeIndex].status !== "LOADING" ? styles.opaque : styles.transparent)}> */}
            <div>
               <Toolbar
                  theme={theme}
                  editor={editor}
                  editorRightPanel={editorRightPanel}
                  toggleEditorRightPanel={toggleEditorRightPanel}
               />
            </div>
            <div className={cx(theme, styles.editorPageContent2)}>
               <div
                  className={styles.page4}
                  style={{
                     transform: `scale(${zoom}, ${zoom})`,
                     transformOrigin: zoom > 1 ? '0 0' : ''
                  }}
               >
                  <Editor
                     closeEditorTabIndex={closeEditorTabIndex}
                     lang={lang}
                     toggleModal={toggleModal}
                     db={db}
                     showToast={showToast}
                     index={activeIndex}
                     ref={setRef}
                     theme={theme}
                     activeTab={tabs[activeIndex]}
                     onChange={onChange}
                     courses={courses}
                     folders={folders}
                     subjects={subjects}
                     updateEditorTabIndex={updateEditorTabIndex}
                     // saveCourse={saveCourse}
                     changeCourseName={changeCourseName}
                  />
               </div>
            </div>
            <div>
               <Footer
                  editor={editor}
                  courses={courses}
                  activeTab={tabs[activeIndex]}
                  theme={theme}
                  setZoom={setZoom}
                  zoom={zoom}
               />
            </div>
         </>
      );
   };

   render() {
      const {
         editorRightPanel,
         show,
         tabs,
         lang,
         theme,
         activeIndex,
         setActiveEditorTabIndex,
         switchEditorTabIndex,
         closeEditorTabIndex,
         addNewTab
      } = this.props;

      return (
         <div
            className={cx(
               show ? '' : styles.hide,
               styles.superWrapper0,
               editorRightPanel ? styles.showRightPanel : styles.hideRightPanel
            )}
         >
            <div className={styles.editorPageLayout1} id="editor-page">
               <Topbar
                  lang={lang}
                  theme={theme}
                  activeIndex={activeIndex}
                  setActiveEditorTabIndex={setActiveEditorTabIndex}
                  switchEditorTabIndex={switchEditorTabIndex}
                  closeEditorTabIndex={closeEditorTabIndex}
                  addNewTab={addNewTab}
                  tabs={tabs}
               />
               {tabs.length === 0 ? this.renderNoTab() : this.renderEditor()}
            </div>
            {/* <RightPanel theme={theme} editor={this.editor} setZoom={this.setZoom} {...this.props} /> */}
         </div>
      );
   }
}

function mapStateToProps(state: any) {
   return {
      lang: state.language,
      db: state.database,
      courses: state.courses,
      subjects: state.subjects,
      folders: state.folders,
      theme: state.theme
   };
}

function mapDispatchToProps(dispatch: any) {
   return bindActionCreators(
      {
         ...CourseActions,
         toggleModal,
         showToast
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(EditorPage);
