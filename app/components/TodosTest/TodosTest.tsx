import React from 'react';

// Typescript
import { IDatabase, ITodo } from '../../typescript';

// Constant

// Attributes of database object [todos] && methods for attributes [CREATE, REMOVE, UPDATE]
import {
   TODO_DB,
   CREATE,
   REMOVE,
   UPDATE,
   COLLECTION_DB
} from '../../constants/models/database';

// Attributes of model todo
import { NAME_TODO } from '../../constants/models/todo';

interface ITodos {
   db: IDatabase;
   todos: ITodo[];
}

export default function Todos(props: ITodos) {
   const { todos } = props;

   // Exemple to add one element in the list of todos
   const handleAddTodo = () => {
      const { db } = props;
      const timestamp = Date.now();
      // db.todos.create({name: "", subject_id: ""})
      db[TODO_DB][CREATE]({ name: 'todo' + timestamp })
         .then(() => console.log('created todo ' + timestamp))
         .catch(err => console.warn(err));
   };

   // Exemple of remove the first element in the list of todos
   const handleRemoveTodo = () => {
      const { db, todos } = props;

      if (todos.length < 1) return;

      const todo = todos[0];
      const { collection } = todo;

      // db.todos.remove(todo)
      db[COLLECTION_DB[collection]]
         [REMOVE](todo)
         .then(
            /* () => {} */
            console.log(collection)
         )
         .then(console.log(todo))
         .catch(err => console.warn(err));
   };

   // Exemple to update one element in the list of todo
   const handleUpdateTodo = () => {
      const { db, todos } = props;

      if (todos.length < 1) return;
      const todo = todos[0];

      // db.todos.update(todo)
      //db[TODO_DB][UPDATE]({...todo, [NAME_TODO]: "TOTO"})
      db[TODO_DB][UPDATE]({ ...todo, [NAME_TODO]: 'TOTO_UPDATE' })
         .then(() => {})
         .catch(err => console.warn(err));
   };

   console.warn('Todos :', todos);

   return (
      <>
         <div>Todos</div>
         <button onClick={handleAddTodo}>Add todo</button>
         <button onClick={handleRemoveTodo}>Remove todo</button>
         <button onClick={handleUpdateTodo}>Update todo</button>
         <h2>Selected Todo</h2>
         {/* Name:{todos[0].name}<br></br> */}
         Created:{todos[0].createdAt}
         <br />
         Collection:{todos[0].collection}
         <br />
         Updated:{todos[0].updatedAt}
         <br />
      </>
   );
}
