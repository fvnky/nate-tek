import { Dispatch } from '../reducers/types';

export const SHOW_EDITOR_RIGHT_PANEL = 'SHOW_EDITOR_RIGHT_PANEL';
export const TOGGLE_EDITOR_RIGHT_PANEL = 'TOGGLE_EDITOR_RIGHT_PANEL';

export function showEditorRightPanel(show: boolean) {
   return (dispatch: Dispatch) => {
      dispatch({
         type: SHOW_EDITOR_RIGHT_PANEL,
         payload: show
      });
   };
}

export function toggleEditorRightPanel() {
   return (dispatch: Dispatch, getState: any) => {
      const state = getState().editorRightPanel
      dispatch({
         type: SHOW_EDITOR_RIGHT_PANEL,
         payload: !state
      });
   };
}
