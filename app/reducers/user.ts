import { IActionUser } from './types';
import { IReducerUser } from '../typescript';

export const LOAD_PROFILE = 'load_profile';
export const UPDATE_PROFILE = 'update_profile';
export const UPDATE_PROFILE_PICTURE = 'update_profile_picture';
export const REMOVE_PROFILE_PICTURE = 'remove_profile_picture';

export function user(state: IReducerUser = {}, action: IActionUser) {
   switch (action.type) {
      case LOAD_PROFILE:
         return action.payload;
      case UPDATE_PROFILE:
         return action.payload;
      case UPDATE_PROFILE_PICTURE:
         return action.payload;
      case REMOVE_PROFILE_PICTURE:
         return action.payload;
      default:
         return state;
   }
}
