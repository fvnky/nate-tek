
import * as React from "react";

const LogoNate = props => (
   <svg viewBox="0 0 999 999 " {...props}>
<path d="M679.7,675.8h-48.3V459.3c0-72.7-59.1-131.8-131.9-131.8c-72.7,0-131.8,59.1-131.8,131.8v216.5h-48.3V459.3
	c0-99.3,80.8-180.1,180.1-180.1c99.3,0,180.2,80.8,180.2,180.1V675.8z"/>
<path d="M970.1,991.2H28.9c-11.7,0-21.2-9.5-21.2-21.2V28.9c0-11.7,9.5-21.2,21.2-21.2h941.2c11.7,0,21.2,9.5,21.2,21.2v941.2
	C991.2,981.8,981.8,991.2,970.1,991.2z M43.8,955.2h911.5V43.7H43.8V955.2z"/>
   </svg>
);

export default LogoNate;
