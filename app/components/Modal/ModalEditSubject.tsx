import * as React from 'react';
import cx from 'classnames';

// Component
import DisplayIcons from '../Base/DisplayIcons';
import Modal from '../Base/Modal';

// Utis
import { capitalize, errorModal } from '../utils';

// Icon
import { iconsSubject } from '../../assets/icons/Subject/IconsSubject';

// Style
import styles from './ModalEditSubject.less';
import stylesErr from './ModalError.less';

// typescript
import {
   IDatabase,
   ISubject,
   UpdateSubject,
   ILanguage
} from '../../typescript';

interface IProps {
   id: string;
   db: IDatabase;
   updateSubject: UpdateSubject;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   subject: ISubject | null;
   title: string;
   selectIcon: string;
   message: string;
}

export default class SubjectModal extends React.Component<IProps, IState> {
   input: React.RefObject<HTMLInputElement> = React.createRef<
      HTMLInputElement
   >();

   constructor(props: IProps) {
      super(props);
      this.state = {
         subject: null,
         title: '',
         selectIcon: 'IconQuestionMark',
         message: ''
      };
   }

   componentDidMount() {
      const { id, db } = this.props;
      const input = this.input.current;

      db.subjects
         .findById(id)
         .then((subject: ISubject) => {
            if (input) {
               input.focus();
               input.value = subject.name;
               // input.setSelectionRange(0, input.selectionEnd);
            }
            this.setState({
               subject,
               title: subject.name,
               selectIcon: subject.icon
            });
         })
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   }

   handleOnClickIcon = (id: string) => {
      this.setState({ selectIcon: id });
   };

   handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { target } = event;
      const { value } = target;
      const input = this.input.current;

      this.setState({ title: capitalize(value) });
      if (!input) return;
      if (input.className !== styles.inputNameModal) {
         input.className = styles.inputNameModal;
         this.setState({ message: '' });
      }
   };

   handleOk = () => {
      const { title, selectIcon, subject } = this.state;
      const { handleDisplayModal, updateSubject } = this.props;
      const input = this.input.current;

      if (!subject) return;
      subject.name = title;
      subject.icon = selectIcon;
      updateSubject(subject)
         .then(() => handleDisplayModal())
         .catch((err: any) => {
            this.setState({ message: err.message });
            if (input) {
               input.className = `${stylesErr.inputErrorColor} ${
                  styles.inputNameModal
               }`;
               input.focus();
               input.setSelectionRange(0, input.selectionEnd);
            }
         });
   };

   renderInputName = () => {
      const { message, title } = this.state;
      const { lang, theme } = this.props;

      return (
         <div className={styles.inputNameContainer}>
            <div className={stylesErr.errorMessage}>{message}</div>
            <input
               ref={this.input}
               type="text"
               value={title}
               onChange={this.handleChange}
               className={cx(theme, styles.inputNameModal)}
               placeholder={lang.enterTitle}
            />
         </div>
      );
   };

   renderSelectIcon = () => {
      const { selectIcon } = this.state;
      const { theme } = this.props;
      const ICON = iconsSubject[selectIcon];

      if (!ICON) return errorModal(this.constructor.name, selectIcon);
      return (
         <div className={`${cx(theme, styles.selectedIcon)}`}>
            <ICON />
         </div>
      );
   };

   renderIcons = () => {
      const { theme } = this.props;
      return (
         <div>
            <div className={cx(theme, styles.gradient)} />
            <div className={styles.containerIcons}>
               <DisplayIcons
                  styles={cx(theme, styles.iconsDisplay)}
                  onClick={this.handleOnClickIcon}
                  icons={iconsSubject}
                  activeStyle={cx(theme, styles.iconsDisplaySelected)}
               />
            </div>
         </div>
      );
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;

      return (
         // <Modal
         //    theme={theme}
         //    title={lang.editSubject}
         //    onCancel={handleDisplayModal}
         //    onOk={this.handleOk}
         //    okText={lang.update}
         //    cancelText={lang.cancel}
         // >
         //    {this.renderInputName()}
         //    {this.renderSelectIcon()}
         //    {this.renderIcons()}
         // </Modal>
         <Modal
            theme={theme}
            title={lang.editSubject}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.update}
            cancelText={lang.cancel}
         >
            <div className={styles.layout}>
               {this.renderIcons()}
               <div className={styles.iconAndNameLayout}>
                  {this.renderSelectIcon()}
                  {this.renderInputName()}
               </div>
            </div>
         </Modal>
      );
   }
}
