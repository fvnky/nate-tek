import { isString, pick } from 'lodash';
import { InvalidParameterFormat, TodoNotFound } from '../errors';
import { Store } from '../factory';

import { TodoSchema } from '../schemas';

import {
   generateId,
   timestamp,
   parseUpdate,
   parseData,
   parseDoc,
   isNotFound
} from '../utils';

// Typescript
import { IDatabase, ICouchDB, IDbDocument, ITodo } from '../../typescript';

// Constant
import { FIND_BY_ID, CREATE, UPDATE, REMOVE } from '../../constants/models';
import {
   NATE_TODO,
   NAME_TODO,
   DUE_DATE,
   IS_DONE
} from '../../constants/models/todo';
import {
   ADD_TODO,
   REMOVE_TODO,
   FETCH_TODO,
   UPDATE_TODO
} from '../../reducers/todos';

// ================================================ //
// ==================== Create ==================== //
// ================================================ //

function create(this: IDatabase, data: ITodo) {
   return new Promise((ok, ko) => {
      const store: any = new Store();
      const mandatory = [NAME_TODO, DUE_DATE, IS_DONE];
      parseData(data, mandatory)
         .then((parsed: ITodo) => {
            parsed._id = generateId();
            parsed.createdAt = timestamp();
            parsed.updatedAt = parsed.createdAt;
            parsed.content = '';
            parsed.collection = NATE_TODO;
            return this._main.put(parsed);
         })
         .then(res => store.add('nTodoId', res.id))
         .then((id: string) => this._main.get(id))
         .then(nTodo => parseDoc(TodoSchema, nTodo))
         .then((nTodo: ITodo) => {
            this.dispatch({ type: ADD_TODO, payload: nTodo });
            return nTodo;
         })
         .then(ok)
         .catch(
            err =>
               new Promise((next, spread) => {
                  const { nTodoId } = store;
                  if (!nTodoId) return spread(err);
                  this._main
                     .get(nTodoId)
                     .then(doc => this._main.remove(doc._id, doc._rev))
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .catch(ko);
   });
}

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll(this: IDatabase): Promise<ITodo | ITodo[]> {
   return new Promise((ok, ko) => {
      this._main
         .find({ selector: { collection: NATE_TODO } })
         .then(res => {
            this.dispatch({ type: FETCH_TODO, payload: res.docs });
            const result: any = res.docs;
            ok(result);
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

function update(this: IDatabase, doc: ITodo) {
   return new Promise((ok, ko) => {
      const store: any = new Store();
      const fields = [];

      parseDoc(TodoSchema, doc)
         .then(parsed => this._main.get(parsed._id))
         .then(old => parseUpdate(old, doc, fields))
         .then(nTodo => store.add('nTodo', nTodo))
         .then(nTodo => {
            nTodo.updatedAt = timestamp();
            return this._main.put(nTodo);
         })
         .then(res => this._main.get(res.id))
         .then(nTodo => {
            this.dispatch({ type: UPDATE_TODO, payload: nTodo });
            return nTodo;
         })
         .then(nTodo => ok(nTodo))
         .catch(err => {
            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Delete ==================== //
// ================================================ //

function remove(data: ITodo) {
   return new Promise((ok, ko) => {
      parseData(pick(data, ['_id']), ['_id']) // On peut pas simplement faire data._id ?
         .then(parsed => this._main.get(parsed._id))
         .then(nTodo => {
            if (nTodo.collection !== NATE_TODO)
               throw new TodoNotFound(
                  `The document's id '${nTodo._id}' does not reference a todo`
               );
            return nTodo;
         })
         .then(({ _id, _rev }) => {
            const promises = [
               this._main.remove(_id, _rev),
               this._cloud.remove(_id, _rev)
            ];

            return Promise.all(promises);
         })
         .then(() => this.dispatch({ type: REMOVE_TODO, payload: data }))
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               throw new TodoNotFound(
                  `The nTodo '${data._id}' could not be found`
               );
            throw err;
         })
         .catch(ko);
   });
}

export default function() {
   return {
      [CREATE]: create.bind(this),
      findAll: findAll.bind(this),
      // [FIND_BY_ID]: findById.bind(this),
      [UPDATE]: update.bind(this),
      [REMOVE]: remove.bind(this)
   };
}
