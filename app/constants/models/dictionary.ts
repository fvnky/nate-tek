// Collection
export const DICTIONARY = 'dictionary';

// Attributes
export const DEFINITION_DICTIONARY = 'definition';
export const NAME_DICTIONARY = 'name';
export const SUBJECT_DICTIONARY = 'subject_id';
