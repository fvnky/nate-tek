import { QUESTION } from '../quiz';
import { SCHEMA_ID } from './schema';

// Name
export const QUESTION_ANSWER = 'question_answer';

// Attributes
export const QUESTIONS_Q_A = QUESTION;
export const ANSWER_Q_A = 'answer';

// Schema
export const SCHEMA_QUESTION_ANSWER = {
   [QUESTIONS_Q_A]: { type: 'string' },
   [ANSWER_Q_A]: { type: 'string' },
   [SCHEMA_ID]: { type: 'string' }
};
