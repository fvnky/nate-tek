import styles from './CalendarHandleEvent.less';

// Constant
// import { MODAL_CREATE_SCHEDULE, SCHEDULE, } from '../../../constants/modal';
import {
   CREATE,
   UPDATE,
   SCHEDULES_DB,
   SCHEDULE
} from '../../../constants/models';
import {
   TITLE_SCHEDULE,
   DATE_SCHEDULE,
   CLASSNAME_SCHEDULE,
   ROOM_SCHEDULE,
   NOTE_SCHEDULE,
   START_SCHEDULE,
   END_SCHEDULE,
   ID_SUBJECT_SCHEDULE
} from '../../../constants/models/schedule';
// Typescript
import { ICalendar } from '../CalendarPage';
import { IArgDateClick, ISchedule } from '../../../typescript';

interface IPropsArgEvent extends IArgDateClick, ISchedule {}

const initEvent = (arg: any) => {
   // console.log("Arg :", arg);
   const {
      date,
      room = '',
      start = arg.date.toISOString(),
      end = arg.date.toISOString(),
      note = '',
      title = 'new event',
      id_subject = ''
   } = arg;
   // console.log("End :", end);
   const obj = {
      ...arg,
      [DATE_SCHEDULE]: date.toISOString(),
      [TITLE_SCHEDULE]: title,
      [CLASSNAME_SCHEDULE]: styles.containerScheduleEvent,
      [ROOM_SCHEDULE]: room,
      [NOTE_SCHEDULE]: note,
      [START_SCHEDULE]: start,
      end: end,
      [ID_SUBJECT_SCHEDULE]: id_subject
   };
   obj.end = arg.end ? arg.end.toISOString() : arg.date.toISOString();
   obj.start = arg.start ? arg.start.toISOString() : arg.date.toISOString();
   // if (obj.start instanceof Date)
   //     obj.start = obj[START_SCHEDULE].toISOString();
   // console.log("OBJ.End :", obj.end);
   return obj;
};

const initEventFixe = (arg: any) => {
   // console.log("Arg :", arg);
   const {
      // date,
      room = '',
      start = '',
      end = arg.start.toISOString(),
      note = '',
      title = 'new event',
      id_subject = ''
   } = arg;
   // console.log("End :", end);
   const obj = {
      ...arg,
      [DATE_SCHEDULE]: start.toISOString(),
      [TITLE_SCHEDULE]: title,
      [CLASSNAME_SCHEDULE]: styles.containerScheduleEvent,
      [ROOM_SCHEDULE]: room,
      [NOTE_SCHEDULE]: note,
      [START_SCHEDULE]: start,
      end: end,
      [ID_SUBJECT_SCHEDULE]: id_subject
   };
   obj.end = arg.end ? arg.end.toISOString() : arg.start.toISOString();
   obj.start = arg.start ? arg.start.toISOString() : arg.start.toISOString();
   // if (obj.start instanceof Date)
   //     obj.start = obj[START_SCHEDULE].toISOString();
   // console.log("OBJ.End :", obj.end);
   return obj;
};

export const handleReceive = props => arg => {
   console.log(arg);
   const { db } = props;

   const event = initEventFixe(arg.event);
   const obj = {
      ...event,
      ...arg.event.extendedProps
   };
   db[SCHEDULES_DB].update(obj)
      .then(() => {})
      .catch(err => console.log(err));
};

export const handleDrop = (props: ICalendar) => arg => {
   console.warn(arg);
   const { db } = props;
   const event = initEventFixe(arg.event);
   const obj = {
      ...event,
      ...arg.event.extendedProps
   };
   console.warn(obj);
   db[SCHEDULES_DB].update(obj)
      .then(() => {})
      .catch(err => console.log(err));
};

export const handleDateToggleModal = (props: ICalendar) => (
   arg: IArgDateClick
) => {
   const { toggleModal } = props;

   // console.log('handleDateToggle Arg: ', arg);
   // console.log(arg.date instanceof Date);
   // console.log('type', typeof arg.date);
   toggleModal('ModalCreateSchedule', {
      arg: initEvent(arg),
      collection: SCHEDULE,
      method: CREATE
   });
};

export const handleEventToggleModal = (props: ICalendar) => arg => {
   const { toggleModal } = props;

   // console.log('handleEventToggle Arg: ', arg);
   const params = {
      classNames: arg.event.classNames,
      start: arg.event.start,
      end: arg.event.end,
      title: arg.event.title,
      date: arg.event.start,
      ...arg.event.extendedProps
   };
   // console.warn('handleEventToggle Params: ', params);
   toggleModal('ModalCreateSchedule', {
      arg: initEvent(params),
      collection: SCHEDULE,
      method: UPDATE
   });
};

// export const useDisplaySchedulesEvent = () => {
//    return [
//       { title: 'event 1', date: '2019-10-30', id: 1 },
//       {
//          title: 'event 2',
//          date: '2019-11-01',
//          classNames: [styles.containerScheduleEvent],
//          id: 2
//       },
//       {
//          title: 'Meeting',
//          start: '2019-10-12T14:30:00',
//          extendedProps: {
//             status: 'done'
//          },
//          id: 3
//       },
//       {
//          title: 'Birthday Party',
//          date: '2019-10-13T14:30:00',
//          backgroundColor: 'green',
//          borderColor: 'black',
//          borderRadius: '0px',
//          id: 4
//       },
//       {
//          title: 'Birthday Party',
//          date: 'Wed Nov 20 2019 00:00:00 GMT+0100 (CET)',
//          backgroundColor: 'green',
//          borderColor: 'black',
//          borderRadius: '0px',
//          id: 4
//       }
//    ];
// };
