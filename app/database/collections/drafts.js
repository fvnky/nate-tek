import { DRAFT, DRAFT_ID, COURSE } from '../constants';

import {
   timestamp,
   parseData
} from '../utils';

import {
   ADD_DRAFT,
   FETCH_DRAFT
} from '../../reducers/drafts';


// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
   return new Promise((ok, ko) => {
      this._main
         .find({ selector: { 'parent.collection': DRAFT } })
         .then(res => {
            this.dispatch({ type: FETCH_DRAFT, payload: res.docs });
            ok(res.docs);
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

function remove(doc) {
   console.log("---doc", doc)
   return new Promise((ok, ko) => {
      const mandatory = ["_id"];
      const optionals = [];
      parseData(doc, mandatory, optionals)
      .then((parsed) => this._main.get(parsed._id))
      .then((draft) => {
         if (draft.collection === COURSE)
            return this.courses.remove(draft);
      })
      .then(ok)
      .catch(ko)
   })
}

function addCourse(doc) {
   return new Promise(resolve => {
      const mandatory = [];
      const optionals = ['name'];

      parseData(doc, mandatory, optionals)
         .then(parsed => {
            parsed.name = parsed.name || `Untitled-${timestamp()}`;
            parsed.parent = { id: DRAFT_ID, collection: DRAFT };
            return this.courses.create(parsed);
         })
         .then(draft => {
            this.dispatch({ type: ADD_DRAFT, payload: draft });
            return draft;
         })
         .then(draft => resolve(draft))
         .catch(err => {
            throw err;
         })
         .catch(err => {
            console.log(err);
            resolve(null);
         });
   });
}


export default function() {
   return {
      findAll: findAll.bind(this),
      addCourse: addCourse.bind(this),
      remove: remove.bind(this)
   };
}
