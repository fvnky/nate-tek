import * as React from 'react';
import cx from 'classnames';
import { CSSTransition } from 'react-transition-group';

import withTheme from '../withTheme';

import IconArrowDown from '../../assets/icons/IconArrowDown';
import IconPlus from '../../assets/icons/IconPlus';
// Style
import styles from './NateSelect.less';

interface IProps {
   value?: any;
   onChange?: (value: any) => void;
   onInputChangeCb?: (inputValue: string) => void;
   options: Array<{ value: any; label: string }>;
   theme: string;
   className?: string;
   creatable?: Boolean;
   createPrefix?: string;
   onCreateCb?: (label: string) => void;
   width?: number;
   input?: any;
}

const NateSelect: React.FC<IProps> = ({
   value,
   onChange,
   options,
   theme,
   className,
   creatable = false,
   createPrefix = 'Create',
   onCreateCb,
   onInputChangeCb,
   width,
   input
}) => {
   const [inputRef, setInputRef] = React.useState(null);
   const [inputValue, setInputValue] = React.useState(value ? value : '');
   const [node, setNode] = React.useState(null);
   const [opened, setOpened] = React.useState(false);
   const [inputFocusedOrHovered, setInputFocusedOrHovered] = React.useState(
      false
   );

   React.useEffect(() => {
      document.addEventListener('click', handleClick, false);
      return () => {
         document.removeEventListener('click', handleClick, false);
      };
   });

   // React.useEffect(() => {
   //    if (opened) {
   //       const el = document.getElementById("nate-select-" + getActiveValue().value);
   //       if (el)
   //          el.scrollIntoView();
   //    }
   // }, [opened])

   React.useEffect(() => {
      setInputValue(value ? value : '');
   }, [value]);

   // React.useEffect(() => {
   //    console.log("inputValue: ", inputValue)
   //    console.log("options changed: ", options)
   //    console.log("filtered options: ", filteredOptions)
   //    // const newActiveValue = getActiveValue();
   //    setFilteredOptions(options);
   // }, [options])

   const handleClick = e => {
      if (node.contains(e.target)) return;
      setOpened(false);
   };

   const onSelectClick = () => {
      const isOpened = opened;
      setOpened(!opened);
      // if (!isOpened) {
      //    console.log("nate-select-"  + getActiveValue().value)
      //    const el = document.getElementById("nate-select-" + getActiveValue().value);
      //    console.log("el", el)
      //    if (el)
      //       el.scrollIntoView();
      // }
   };

   const handleOptionClick = (value: any) => {
      input.onChange({ target: { value } });
      onChange(value);
      setOpened(false);
   };

   const onInputMouseEnter = () => {
      setInputFocusedOrHovered(true);
   };

   const onInputMouseLeave = () => {
      setInputFocusedOrHovered(false);
   };

   const renderDropdownItems = () => {
      if (options) {
         const values = options.map(option => {
            const active = value === option.value;
            return (
               <div
                  id={'nate-select-' + option.value}
                  key={option.value}
                  role="button"
                  className={cx(
                     theme,
                     styles.option,
                     active ? styles.optionActive : ''
                  )}
                  onClick={() => handleOptionClick(option.value)}
               >
                  {option.label}
               </div>
            );
         });
         if (!options.find(o => o.label === inputValue) && creatable) {
            values.push(
               <div
                  id={'nate-select-create'}
                  key={'create'}
                  role="button"
                  className={cx(theme, styles.option)}
                  onMouseDown={() => onCreateCb(inputValue)}
               >
                  <div>
                     <IconPlus className={cx(theme, styles.createPlusIcon)} />
                  </div>
                  {createPrefix + ' ' + inputValue}
               </div>
            );
         }
         return values;
      }
      return null;
   };

   const onInputFocus = e => {
      // setFilteredOptions(options.filter(o => o.label.includes(inputValue)))
      input.onFocus(e);
   };

   const onInputBlur = e => {
      input.onBlur(e);
      // setInputValue(getActiveValue().label);
   };

   const onInputChange = event => {
      // console.warn("change", event.target.value)
      input.onChange(event);
      if (!opened) setOpened(true);
      setInputValue(event.target.value);
      if (onInputChangeCb) {
         onInputChangeCb(event.target.value);
      }
   };

   const renderDropdown = () => {
      const style = {
         top: 0,
         left: 0,
         width: 100,
         zIndex: 10
      };

      if (inputRef) {
         style.top = inputRef.offsetTop + inputRef.offsetHeight;
         style.left = inputRef.offsetLeft;
         style.width = width ? width + 'px' : inputRef.offsetWidth;
      }

      return (
         <CSSTransition
            in={opened}
            timeout={500}
            classNames={{
               appear: '',
               appearActive: '',
               enter: styles.enter,
               enterActive: styles.enterActive,
               enterDone: '',
               exit: styles.exit,
               exitActive: styles.exitActive,
               exitDone: styles.exitDone
            }}
            unmountOnExit
         >
            <div className={cx(theme, styles.dropDownContainer)} style={style}>
               {renderDropdownItems()}
            </div>
         </CSSTransition>
      );
   };

   // console.log(inputValue)
   return (
      <div ref={n => setNode(n)} className={cx(styles.node)}>
         <div
            role="button"
            className={cx(theme, styles.input, className)}
            onClick={onSelectClick}
            ref={i => setInputRef(i)}
            onMouseLeave={onInputMouseLeave}
            onMouseEnter={onInputMouseEnter}
         >
            <input
               className={cx(theme, styles.searchableInput)}
               value={inputValue}
               onChange={onInputChange}
               onFocus={onInputFocus}
               onBlur={onInputBlur}
            />
            <div>
               <IconArrowDown
                  className={cx(
                     theme,
                     styles.iconArrowDown,
                     inputFocusedOrHovered || opened
                        ? styles.iconArrowDownActive
                        : '',
                     opened ? styles.iconUpsideDown : ''
                  )}
               />
            </div>
         </div>
         {renderDropdown()}
      </div>
   );
};

export default withTheme(NateSelect);
