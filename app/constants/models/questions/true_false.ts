import { QUESTION } from '../quiz';
import { SCHEMA_ID } from './schema';

// Name
export const TRUE_FALSE = 'TrueOrFalse';

// Attributes
export const QUESTION_T_F = QUESTION;
export const ANSWER_T_F = 'answer';

// Schema
export const SCHEMA_TRUE_FALSE = {
   [QUESTION_T_F]: { type: 'string' },
   [ANSWER_T_F]: { type: 'boolean' },
   [SCHEMA_ID]: { type: 'string' }
};
