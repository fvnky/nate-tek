import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { History } from 'history';
// Component
import routes from '../../constants/routes.json';

// Style
import styles from './BreadCrumbs.less';

// Icon
import { iconsSubject } from '../../assets/icons/Subject/IconsSubject';

// typescript
import { IDbDocument, IFolder, ISubject, ILanguage } from '../../typescript';

interface IProps extends RouteComponentProps {
   router: any;
   // params: { [key: string]: string };
   subjects: ISubject[];
   folders: IFolder[];
   lang: ILanguage;
   history: History;
}

interface IState {
   links: IDbDocument[];
}

export default class BreadCrumbs extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);
      this.state = {
         links: []
      };
   }

   componentDidUpdate(prevProps: IProps) {
      const { router, folders } = this.props;
      const {
         location: { pathname }
      } = router;
      if (
         pathname !== prevProps.router.location.pathname ||
         prevProps.folders !== folders
      ) {
         this.refreshBreadCrumbs();
      }
   }

   refreshBreadCrumbs = () => {
      const { router } = this.props;
      const {
         location: { pathname }
      } = router;
      if (pathname) {
         const splitPath = pathname.split('/');
         if (splitPath.length > 0) {
            const id = splitPath[splitPath.length - 1];
            this.updateLinks(id, []);
         }
      }
   };

   getSubjectIDFromProps = (): string => {
      const { router } = this.props;
      const {
         location: { pathname }
      } = router;
      if (pathname) {
         const splitPath: string[] = pathname.split('/');
         if (splitPath.length > 0) {
            const subjects = `${routes.RESOURCES}/${splitPath[2]}`;
            if (subjects === routes.SUBJECTS && splitPath.length > 1) {
               const id: string = splitPath[3];
               return id;
            }
         }
      }
      return null;
   };

   compareArray = (tab1: [], tab2: []) => {
      for (let cnt = 0; cnt < tab1.length; cnt += 1) {
         if (cnt > tab2.length) return false;
         if (tab1[cnt] !== tab2[cnt]) return false;
      }
      return true;
   };

   addLink = (docs: IDbDocument, tabs: IDbDocument[]) => {
      const { parent } = docs;
      tabs.push(docs);
      if (parent) this.updateLinks(parent.id, tabs);
      else this.setState({ links: tabs.reverse() });
   };

   updateLinks = (id: string, tabs: IDbDocument[]) => {
      const { folders, subjects } = this.props;

      const folder = folders.find((x: IFolder) => x._id === id);
      const subject = subjects.find((x: ISubject) => x._id === id);

      if (folder) this.addLink(folder, tabs);
      if (subject) this.addLink(subject, tabs);
   };

   getActiveTab = (): string => {
      const { router, lang } = this.props;
      const {
         location: { pathname }
      } = router;

      if (pathname) {
         const splitPath: string[] = pathname.split('/');
         if (splitPath.length > 0) {
            if (`/${splitPath[1]}` === routes.RESOURCES)
               return lang[splitPath[2]];
            return lang[splitPath[1]];
         }
      }
   };

   onBreadCrumbClick = (link: string) => () => {
      const { history } = this.props;

      history.push(link);
   };

   renderLink = () => {
      const { links } = this.state;
      const subjectID = this.getSubjectIDFromProps();
      if (!subjectID) return null;

      return links.map((item: IDbDocument) => {
         const { _id, collection, name, icon } = item;
         let link = `${routes.SUBJECTS}/${subjectID}`;
         let ICON = iconsSubject[icon];

         if (collection === 'folder') link += `${routes.FOLDERS}/${_id}`;
         else
            ICON = (
               <div className={styles.alignIcon}>
                  <ICON.type className={styles.iconLink} />
               </div>
            );

         return (
            <React.Fragment key={_id}>
               <div className={styles.slash}>/</div>
               <div
                  role="button"
                  onClick={this.onBreadCrumbClick(link)}
                  className={styles.containerLink}
               >
                  {ICON}
                  <div className={styles.name}>{name}</div>
               </div>
            </React.Fragment>
         );
      });
   };

   render() {
      return (
         <div className={styles.containerLinks}>
            <div
               role="button"
               className={styles.containerLink}
               onClick={this.onBreadCrumbClick(routes.SUBJECTS)}
            >
               <div className={styles.name}>{this.getActiveTab()}</div>
            </div>
            {this.renderLink()}
         </div>
      );
   }
}
