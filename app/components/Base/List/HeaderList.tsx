import * as React from 'react';
import * as _ from 'lodash';
import cx from 'classnames';

// Component
import List from './List';

// Icon
import IconArrowDown from '../../../assets/icons/IconArrowDown';

// Style
import stylesList from './List.less';
import stylesElement from './ElementsList.less';
import styles from './HeaderList.less';

// Typescript
import {
   IDocumentHeader,
   IListItem,
   ILanguage,
   ToggleModal
} from '../../../typescript';

// Constant
export const TIME_HEADER_LIST = 'timeHeader';

export function BuildHeader(
   props: IDocumentHeader,
   setColumnOrder: any
): React.ReactNode {
   const { text, display, lang, theme } = props;
   const { className = stylesElement[text] } = props;
   const [order, setOrder] = React.useState({ column: text, order: 'desc' });

   const orderList = () => {
      setColumnOrder(order);
      const newOrder = order.order === 'asc' ? 'desc' : 'asc';
      setOrder({ column: text, order: newOrder });
   };

   return (
      <div
         key={className}
         role="button"
         className={cx(theme, styles.columnHeader, className)}
         onClick={orderList}
      >
         {display && lang[text]}
         {display && (
            <IconArrowDown className={cx(theme, styles.iconArrowDown)} />
         )}
      </div>
   );
}

interface IPropsHeader {
   header: any;
   setColumnOrder: (key: any) => void;
   lang: ILanguage;
   theme: string;
}

export const BuilderHeaders = (props: IPropsHeader) => {
   const { header, setColumnOrder, lang, theme } = props;
   const line = [];

   Object.keys(header).forEach((element: string) => {
      header[element].lang = lang;
      header[element].theme = theme;
      line.push(BuildHeader(header[element], setColumnOrder));
   });
   return <div className={stylesElement.columnsContainer}>{line}</div>;
};

interface IHeaderProps {
   header: any;
   items: IListItem[][];
   lang: ILanguage;
   toggleModal: ToggleModal;
   theme: string;
   sort?: { column: string; order: string };
}

export const HeaderList = (props: IHeaderProps) => {
   const { items, sort = { column: 'name', order: 'asc' } } = props;
   const [list, setList] = React.useState(items);
   const [columnOrder, setColumnOrder] = React.useState(sort);

   React.useEffect(() => {
      const { column, order } = columnOrder;
      const niqueLes: any = order;
      const sortedItems: IListItem[][] = items.map((item: any) =>
         _.orderBy(item, (e: any) => e[column][column], niqueLes)
      );
      setList(sortedItems);
   }, [columnOrder, items]);

   // console.log("HeaderList :", list);

   return (
      <>
         {/* <div className={cx(stylesList.alignContent, styles.alignContent)}>
            <BuilderHeaders setColumnOrder={setColumnOrder} {...props} />
         </div> */}
         <div
            id="childrenList"
            className={styles.childrenList}
            style={{
               display: 'flex',
               flexDirection: 'column',
               flex: '1 1 auto'
            }}
         >
            <List list={list} {...props} />
         </div>
      </>
   );
};
