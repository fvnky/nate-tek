import { SHOW_TOAST } from '../actions/toast';

import { ToastOptions } from '../typescript';

export function toast(
   state = null,
   action: { type: string; payload: ToastOptions }
) {
   switch (action.type) {
      case SHOW_TOAST:
         return action.payload;
      default:
         return state;
   }
}
