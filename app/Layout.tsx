import * as React from 'react';

// Style
import styles from './Layout.less';

interface IProps {
   children: React.ReactNode | React.ReactNode[];
}

export default class Layout extends React.Component<IProps> {
   render() {
      const { children } = this.props;

      return children;//  <div className={styles.layout}>{children}</div>;
   }
}
