import * as React from "react";

const Underline = props => (
   <svg {...props} viewBox="0 0 18 18">
      <path
         d="M9,14c-2.8,0-5-2.2-5-5V3c0-0.6,0.4-1,1-1s1,0.4,1,1v6c0,1.6,1.4,3,3,3c1.6,0,3-1.4,3-3V3c0-0.6,0.4-1,1-1s1,0.4,1,1v6
	C14,11.8,11.8,14,9,14z"
      />
      <path d="M3.5,15h11c0.3,0,0.5,0.2,0.5,0.5l0,0c0,0.3-0.2,0.5-0.5,0.5h-11C3.2,16,3,15.8,3,15.5l0,0C3,15.2,3.2,15,3.5,15z" />
   </svg>
);

export default Underline;
