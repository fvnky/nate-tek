// Constant
import { QUIZ } from '../constants';
import {
   SCHEMA_QUESTIONS,
   FORM,
   TYPE_FORM,
   SCORE,
   SCHEMA_ID
} from '../../constants/models';

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      name: { type: String, required: true },
      parent: {
         collection: { type: String, required: true },
         id: { type: String, required: true }
      },
      // children: [{answer: Boolean, type: String, question: String}],
      [SCHEMA_QUESTIONS]: [
         {
            [FORM]: { type: Object, required: true },
            [TYPE_FORM]: { type: String, required: true },
            [SCHEMA_ID]: { type: String, required: true }
         }
      ],
      [SCORE]: { type: Number, required: true },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      collection: QUIZ
   }),
   fields: [
      '_id',
      '_rev',
      'name',
      'parent.collection',
      'parent.id',
      SCHEMA_QUESTIONS,
      SCORE,
      'createdAt',
      'updatedAt',
      'collection'
   ]
};
