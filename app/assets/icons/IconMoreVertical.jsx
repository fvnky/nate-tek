import * as React from "react";

const IconMoreVertical = props => (
   <svg viewBox="0 0 16 16" {...props}>
      <circle cx="8" cy="2" r="2" />
      <circle cx="8" cy="8" r="2" />
      <circle cx="8" cy="14" r="2" />
   </svg>
);

export default IconMoreVertical;
