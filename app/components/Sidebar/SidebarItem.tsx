import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { History } from 'history';
import cx from 'classnames';

// Style
import styles from './SidebarItem.less';

// Typescript
import { Theme } from '../../typescript';

interface IProps extends RouteComponentProps {
   name: string;
   link: string;
   icon: { outline: any; fill: any };
   active: boolean;
   history: History;
   marginTop: number;
   theme: string;
   collapse: boolean;
}

class SidebarItem extends React.Component<IProps> {
   handleOnClickLink = () => {
      const { link, history } = this.props;

      history.push(link);
   };

   render() {
      const { name, icon, collapse } = this.props;
      const { active, marginTop, theme } = this.props;
      const className = `${cx(theme, styles.linkButton)} ${
         active ? styles.selected : cx(theme, styles.unselected)
      }`;
      const hidden = collapse ? 0 : 1;
      const visibility = collapse ? 'hidden' : 'visible';
      const Icon = active ? icon.fill : icon.outline;

      return (
         <div
            role="button"
            onClick={this.handleOnClickLink}
            className={className}
            style={{ marginTop: `${marginTop}px`, bottom: name === "Settings" ? "15px" : '' }}
         >
            <div>
               <Icon.type className={styles.linkButtonIcon} />
            </div>
            <span
               style={{ opacity: hidden, visibility }}
               className={styles.linkButtonName}
            >
               {name}
            </span>
         </div>
      );
   }
}

export default withRouter(SidebarItem);
