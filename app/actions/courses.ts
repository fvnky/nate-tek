import { ICourse } from '../typescript';
import { Dispatch, GetState } from '../reducers/types';
import NateControlledSearchableSelect from '../components/Base/NateControlledSearchableSelect';

function ExceptionCourse(error: any) {
   return error;
}

export function loadCourses() {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.courses.findAll()
         .then(() => {})
         .catch((err: any) => console.error(err.stack));
   };
}

export function createCourse(
   newCourse: {
      name: string;
      parent: { id: string; collection: string };
      coverImage?: {
         full: string;
         raw: string;
         regular: string;
         small: string;
         thumb: string;
      };
   },
   content: Object = null
) {
   return (dispatch: Dispatch, getState: GetState) => {
      return getState()
         .database.courses.create(newCourse, content)
         .then(() => {})
         .catch((err: any) => {
            console.error(err);

            console.error(err.stack);
            throw ExceptionCourse(err);
         });
   };
}

export function updateCourse(newCourse: ICourse, content?: Object) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.courses.update(newCourse, content)
         .then(() => {})
         .catch((err: any) => {
            console.error(err.stack);
            throw ExceptionCourse(err);
         });
}

export function removeCourse(toRemove: ICourse) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.courses.remove(toRemove)
         .then(() => {})
         .catch((err: any) => {
            console.error(err.stack);
            throw ExceptionCourse(err);
         });
}
