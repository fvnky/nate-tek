import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import cx from 'classnames';
import { JSDOM } from 'jsdom';
import fetch from 'node-fetch';
import https from 'https';

import Gallery from './Gallery';
import NateFileUploader from '../../../../Base/NateFileUploader';
import IconSearch from '../../../../../assets/icons/IconSearch';
import IconUpload from '../../../../../assets/icons/IconUpload';
import AddImage from '../../../../../assets/icons/Editor/AddImage';

import styles from './InsertImagePopup.less';

const opts = {
   headers: {
      'user-agent':
         'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
   }
};

function getBase64(file) {
   return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
         // console.log("blob1", reader.result)
         resolve(reader.result);
      };
      reader.onerror = error => reject(error);
   });
}

function compress(e) {
   return new Promise((resolve, reject) => {
      console.log('Original file: ', e.target.files[0]);

      const originalFile = e.target.files[0];
      const fileName = e.target.files[0].name;
      e.persist();

      const fileType = e.target.files[0].type;
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      e.persist();

      reader.onload = event => {
         const img = new Image();
         img.src = reader.result as string;
         (img.onload = () => {
            const elem = document.createElement('canvas');

            console.log('img.width: ', img.width);
            e.persist();
            if (img.width <= 794) {
               console.log('Image is smaller = no compression ');
               resolve(getBase64(e.target.files[0]));
               return;
            }
            const width = img.width > 794 ? 794 : img.width;
            const scaleFactor = img.width > 794 ? 794 / img.width : 1;
            const height = img.height * scaleFactor;

            elem.width = width;
            elem.height = height;
            const ctx = elem.getContext('2d');
            // img.width and img.height will contain the original dimensions
            ctx.drawImage(img, 0, 0, width, height);
            ctx.canvas.toBlob(
               blob => {
                  const file = new File([blob], fileName, {
                     type: fileType,
                     lastModified: Date.now()
                  });
                  console.log('After compression: ', file);
                  if (file.size < originalFile.size) {
                     console.log(
                        'Compression gains: ',
                        originalFile.size - file.size
                     );
                     console.log('using resized image ');
                     resolve(getBase64(file));
                  } else {
                     console.log(
                        'Compression lost: ',
                        file.size - originalFile.size
                     );
                     console.log('using original image instead');
                     resolve(getBase64(originalFile));
                  }
               },
               fileType,
               1
            );
         }),
            (reader.onerror = error => reject(error));
      };
   });
}

interface IProps {
   theme: string;
   show: boolean;
   editor: any;
   closeMenu: () => void;
}

const InsertMenu: React.FC<IProps> = ({ closeMenu, show, editor, theme }) => {
   const [inputValue, setInputValue] = useState('');
   const [thumbnails, setThumbnails] = useState([]);

   const onInsertImage = e => {
      compress(e)
         .then(data => editor.insertImage(data))
         .catch(err => console.error(err));
      // const file = e.target.files[0];
      // getBase64(file)
      //    .then(data => editor.insertImage(data))
      //    .catch(err => console.error(err));
   };

   const onKeyDown = e => {
      if (e.key === 'Enter' && inputValue !== '') {
         console.warn('searched ', inputValue);
         getThumnails(inputValue);
      }
   };

   const onInputChange = e => {
      setInputValue(e.target.value);
   };

   const getThumnails = keyword => {
      let response_handler = function(response) {
         let body = '';
         response.on('data', function(d) {
            body += d;
         });
         response.on('end', function() {
            const bodyObj = JSON.parse(body);
            const value = bodyObj.value;
            const _images = [];

            value.forEach(v => {
               const _image = {
                  thumbnail: v.thumbnailUrl,
                  original: v.thumbnailUrl
               };
               _images.push(_image);
            });
            setThumbnails(_images);
         });
      };

      let subscriptionKey = '10364a76c5b344eb96b0c5415afe2bd2';
      let host = 'api.cognitive.microsoft.com';
      let path = '/bing/v7.0/images/search';
      let term = keyword;

      let request_params = {
         method: 'GET',
         hostname: host,
         path: path + '?q=' + encodeURIComponent(term),
         headers: {
            'Ocp-Apim-Subscription-Key': subscriptionKey
         }
      };

      let req = https.request(request_params, response_handler);
      req.end();

      // fetch(
      //    `https://www.google.com/search?q=${keyword}&espv=2&biw=1366&bih=667&site=webhp&source=lnms&tbm=isch&sa=X&ei=XosDVaCXD8TasATItgE&ved=0CAcQ_AUoAg`,
      //    opts
      // )
      //    .then(res => {
      //       res.text()
      //          .then(body => {
      //             const dom = new JSDOM(body, { includeNodeLocations: true });
      //             const { document } = dom.window;
      //             // const { bodyEl } = document; // implicitly created
      //             // const pEl = document.querySelector('p');
      //             // const textNode = pEl.firstChild;
      //             // const imgEl = document.querySelector('img');

      //             //  console.log(body);
      //             const _images = [];
      //             const temp = document.getElementsByClassName(
      //                'rg_meta notranslate'
      //             );
      //             console.warn("document", document)
      //             for (let i = 0; i < 30; i += 1) {
      //                const j = JSON.parse(temp[i].innerHTML);
      //                /*               console.log('Image ' + i + ' Thumbnail URL:');
      //         console.log(j.tu);
      //         console.log('Image ' + i + ' URL:');
      //         console.log(j.ou); */
      //                const _image = {
      //                   thumbnail: j.tu,
      //                   original: j.ou
      //                };
      //                _images.push(_image);
      //             }
      //             setThumbnails(_images);
      //             // this.setState({
      //             //    images: _images
      //             // });
      //             // console.warn(_images)
      //             return body;
      //          })
      //          .catch(err => console.error(err));
      //       return res;
      //    })
      //    .catch(err => console.error(err));
   };

   return (
      <CSSTransition
         in={show}
         appear={true}
         timeout={300}
         classNames={{
            enter: styles.enter,
            enterActive: styles.enterActive,
            exit: styles.exit,
            exitActive: styles.exitActive
         }}
         unmountOnExit
      >
         <div className={styles.insertImagePopup}>
            {/* // onMouseDown={(e) => {e.preventDefault(); e.stopPropagation();}}> */}
            <div className={styles.languette} />
            <div className={styles.languette2} />
            <div className={styles.popup}>
               <div className={styles.header}>
                  <div className={cx(theme, styles.upload)}>
                     <NateFileUploader
                        onChange={onInsertImage}
                        accept="image/x-png,image/jpeg"
                     >
                        <IconUpload className={styles.iconUpload} />
                        <div className={styles.uploadLabel}>Upload</div>
                     </NateFileUploader>
                  </div>
                  <span className={cx(theme, styles.or)}>or</span>
                  <IconSearch className={cx(theme, styles.iconSearch)} />
                  <input
                     autoFocus={true}
                     onKeyDown={onKeyDown}
                     onChange={onInputChange}
                     value={inputValue}
                     className={cx(theme, styles.searchInput)}
                     placeholder="Search the web"
                  />
               </div>
               <Gallery
                  theme={theme}
                  thumbnails={thumbnails}
                  editor={editor}
                  closeMenu={closeMenu}
               />
            </div>
         </div>
      </CSSTransition>
   );
};

export default InsertMenu;
