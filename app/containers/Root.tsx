
import * as React from "react";

import { ConnectedRouter } from "connected-react-router";
import { Provider } from "react-redux";

// import { Store } from "../reducers/types";

import { INIT_DB } from "../reducers/database";
import Routes from "../Routes";

export default class Root extends React.Component<any> {
   render() {
      const { store, history } = this.props;

      store.dispatch({ type: INIT_DB, payload: store });

      return (
         <Provider store={store}>
            <ConnectedRouter history={history}>
               <Routes />
            </ConnectedRouter>
         </Provider>
      );
   }
}
