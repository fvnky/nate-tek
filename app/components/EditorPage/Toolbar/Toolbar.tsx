import * as React from 'react';
import cx from 'classnames';
import FormatButton from '../FormatButtons/FormatButton';
import HeadingButton from '../FormatButtons/HeadingButton';
import HighlightButton from '../FormatButtons/HighlightButton';

import Bold from '../../../assets/icons/Editor/Bold';
import Italic from '../../../assets/icons/Editor/Italic';
import Underline from '../../../assets/icons/Editor/Underline';
import Superscript from '../../../assets/icons/Editor/Superscript';
import UnorderedList from '../../../assets/icons/Editor/UnorderedList';
import OrderedList from '../../../assets/icons/Editor/OrderedList';
import Indent from '../../../assets/icons/Editor/Indent';
import Outdent from '../../../assets/icons/Editor/Outdent';

import styles from './Toolbar.less';

interface IProps {
   editor: any;
   theme: string;
   editorRightPanel: boolean;
   toggleEditorRightPanel: () => void;
}

export default class Toolbar extends React.Component<IProps> {
   constructor(props: IProps) {
      super(props);
      this.state = {};
   }

   onClickHighlight = type => event => {
      event.preventDefault();
      event.stopPropagation();
      const { editor } = this.props;

      editor.setHighlight(type);
   };

   onClickPdf = event => {
      const { editor } = this.props;
      event.preventDefault();
      const file = event.target.files[0];
      console.log(file);
      editor.insertPdf(file.name, file.path);
   };

   render() {
      const {
         editor,
         editorRightPanel,
         theme,
         toggleEditorRightPanel
      } = this.props;
      const visibility = !editorRightPanel ? styles.visible : styles.hidden;
      if (!editor) return <></>;
      return (
         <div className={cx(theme, styles.toolbarWrapper)}>
            <div className={`${cx(theme, styles.layoutToolbar, visibility)}`}>
               <div className={styles.container}>
                  <HighlightButton
                     theme={theme}
                     type={1}
                     callback={editor.setHighlight}
                     isActive={editor.isHighlightOfType}
                  />
                  <HighlightButton
                     theme={theme}
                     type={2}
                     callback={editor.setHighlight}
                     isActive={editor.isHighlightOfType}
                  />
                  <HighlightButton
                     theme={theme}
                     type={3}
                     callback={editor.setHighlight}
                     isActive={editor.isHighlightOfType}
                  />
                  <HighlightButton
                     theme={theme}
                     type={4}
                     callback={editor.setHighlight}
                     isActive={editor.isHighlightOfType}
                  />
                  <HighlightButton
                     theme={theme}
                     type={5}
                     callback={editor.setHighlight}
                     isActive={editor.isHighlightOfType}
                  />
                  <div className={cx(theme, styles.separator)} />
                  <HeadingButton
                     theme={theme}
                     heading={1}
                     callback={editor.toggleHeading}
                     isActive={editor.isHeading}
                  />
                  <HeadingButton
                     theme={theme}
                     heading={2}
                     callback={editor.toggleHeading}
                     isActive={editor.isHeading}
                  />
                  <HeadingButton
                     theme={theme}
                     heading={3}
                     callback={editor.toggleHeading}
                     isActive={editor.isHeading}
                  />
                  <HeadingButton
                     theme={theme}
                     heading={4}
                     callback={editor.toggleHeading}
                     isActive={editor.isHeading}
                  />
                  <div className={cx(theme, styles.separator)} />
                  <FormatButton
                     theme={theme}
                     icon={<Bold />}
                     callback={editor.toggleBold}
                     isActive={editor.isBold}
                  />
                  <FormatButton
                     theme={theme}
                     icon={<Italic />}
                     callback={editor.toggleItalic}
                     isActive={editor.isItalic}
                  />
                  <FormatButton
                     theme={theme}
                     icon={<Underline />}
                     callback={editor.toggleUnderlined}
                     isActive={editor.isUnderlined}
                  />
                  <FormatButton
                     theme={theme}
                     icon={<Superscript />}
                     callback={editor.toggleSuperscript}
                     isActive={editor.isSuperscript}
                  />
                  <div className={cx(theme, styles.separator)} />
                  <FormatButton
                     theme={theme}
                     icon={<UnorderedList />}
                     callback={() => editor.setList('unordered-list')}
                     isActive={editor.isUnorderedList}
                  />
                  {/* <FormatButton
                     theme={theme}
                     icon={<OrderedList />}
                     callback={() => editor.setList('ordered-list')}
                     isActive={editor.isOrderedList}
                  /> */}
                  <div className={cx(theme, styles.separator)} />
                  <FormatButton
                     theme={theme}
                     icon={<Indent />}
                     callback={editor.indentList}
                  />
                  <FormatButton
                     theme={theme}
                     icon={<Outdent />}
                     callback={editor.outdentList}
                  />
               </div>
            </div>
         </div>
      );
   }
}
