import React, { Component } from 'react';
import { Document, Page } from 'react-pdf/dist/entry.webpack';

import IconClose from '../../../../../assets/icons/IconClose';

import styles from './PdfViewer.less';

interface IProps {
   file: string;
   close: () => void;
}

interface IState {
   numPages: number;
   pageNumber: number;
}

export default class MyApp extends Component<IProps, IState> {
   state: IState = {
      numPages: null,
      pageNumber: 1
   };

   onDocumentLoadSuccess = ({ numPages }) => {
      this.setState({ numPages });
   };

   decreasePageNumber = () => {
      const { pageNumber, numPages } = this.state;

      this.setState({ pageNumber: pageNumber - 1 < 1 ? pageNumber : pageNumber - 1 });

   }


   increasePageNumber = () => {
      const { pageNumber, numPages } = this.state;

      this.setState({ pageNumber: pageNumber + 1 > numPages ? pageNumber : pageNumber + 1 });

   }

   render() {
      const { file, close } = this.props;
      const { pageNumber, numPages } = this.state;

      return (
         <div className={styles.layout}>
            <div>
               <IconClose className={styles.iconClose} onClick={close} />
               <Document file={file} onLoadSuccess={this.onDocumentLoadSuccess}>
                  <Page pageNumber={pageNumber} className={styles.page} />
               </Document>
               <div className={styles.under}>
               <p>
                  Page {pageNumber} of {numPages}
               </p>
               <div className={styles.navButtonsContainer}>
                  <div className={styles.navButton} onClick={this.decreasePageNumber}>previous</div>
                  <div className={styles.divider}>/</div>
                  <div className={styles.navButton} onClick={this.increasePageNumber}>next</div>
               </div>

               </div>


            </div>
         </div>
      );
   }
}
