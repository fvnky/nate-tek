export interface IModal {
   currentModal: string;
   propsModal: any;
}

export type ToggleModal = (modalName: string, modalProps: any) => void;
