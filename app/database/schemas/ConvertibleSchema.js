import { CONVERTIBLE } from '../constants';

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      name: { type: String, required: false },
      type: { type: String, required: true },
      content: { type: Object, required: false },
      folderId: { type: String, required: false },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      collection: CONVERTIBLE
   }),
   fields: [
      '_id',
      '_rev',
      'name',
      'type',
      'content',
      'folderId',
      'createdAt',
      'updatedAt',
      'collection'
   ]
};
