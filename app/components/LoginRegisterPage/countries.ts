import countriesFR from "./countries_FR";
import countriesEN from "./countries_EN";
import countriesDE from "./countries_DE";

function createCountryArray(countries: {}) {
   const arr = [];
   Object.keys(countries).map((key) => arr.push({value: key, label: countries[key]}))
   return arr;
}

export function getCountries(lang: string) {
   switch (lang) {
      case "EN":
         return createCountryArray(countriesEN);
      case "FR":
         return createCountryArray(countriesFR);
      case "DE":
         return createCountryArray(countriesDE);
      default:
         return null;
   }
}
