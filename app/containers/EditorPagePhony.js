import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import EditorPagePhony from '../components/EditorPage/EditorPagePhony';

// eslint-disable-next-line no-unused-vars
function mapStateToProps(state) {
   return {

   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {

      },
      dispatch
   );
}

export default connect(mapStateToProps, mapDispatchToProps)(EditorPagePhony);
