import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { History } from 'history';

import routes from '../../constants/routes.json';

// Component
import BreadCrumbs from './BreadCrumbs';

// Icon
import IconBack from '../../assets/icons/IconBack';
import IconSearch from '../../assets/icons/IconSearch';

// Style
import styles from './Topbar.less';

// Typescript
import {
   IMatch,
   ILanguage,
   ISubject,
   IFolder,
   ToggleModal
} from '../../typescript';

interface IProps extends RouteComponentProps {
   history: History;
   match: IMatch;
   lang: ILanguage;
   subjects: ISubject[];
   folders: IFolder[];
   router: any;
   toggleModal: ToggleModal;
}

class TopBar extends React.Component<IProps> {
   handlePushBack = () => {
      const { history } = this.props;

      history.goBack();
   };

   handlePushForward = () => {
      const { history } = this.props;

      history.goForward();
   };

   handleModalSearch = () => {
      const { toggleModal } = this.props;

      toggleModal('ModalSearch', {});
   };

   show() {
      const { router } = this.props;
      const {
         location: { pathname }
      } = router;
      if (pathname) {
         const splitPath: string[] = pathname.split('/');

         if (splitPath.length > 0) {
            if (`/${splitPath[1]}` === routes.EDITOR_LINK) return false;
         }
      }
      return true;
   }

   render() {
      if (!this.show()) return <></>;
      return (
         <div className={styles.layoutTopbar}>
            <div className={styles.layoutHistory}>
               <div className={styles.topBarButton}>
                  <IconBack
                     className={styles.iconBack}
                     onClick={this.handlePushBack}
                  />
               </div>
               <div className={styles.topBarButton}>
                  <IconBack
                     className={styles.iconForward}
                     onClick={this.handlePushForward}
                  />
               </div>
               <div className={styles.breadCrumbs}>
                  <BreadCrumbs {...this.props} />
               </div>
            </div>
            <div className={styles.topBarButton}>
               <IconSearch
                  className={styles.iconSearch}
                  onClick={this.handleModalSearch}
               />
            </div>
         </div>
      );
   }
}

export default withRouter(TopBar);
