import Html from "slate-html-serializer";
import { getEventTransfer } from "slate-react";

import {BOLD, ITALIC, UNDERLINED} from './basicFormatting';
import {SUPERSCRIPT} from './superScript';
import { DEFAULT_NODE } from "../constants";
import {HEADING} from './heading/heading';

const HEADINGS = {
   h1: 1,
   h2: 2,
   h3: 3,
   h4: 4
}

const BLOCK_TAGS = {
   p: DEFAULT_NODE,
};

const MARK_TAGS = {
   b: BOLD,
   strong: BOLD,
   em: ITALIC,
   i: ITALIC,
   u: UNDERLINED,
   sup: SUPERSCRIPT
};

const RULES = [
   {
      deserialize(el, next) {
         const level = HEADINGS[el.tagName.toLowerCase()];

         if (level) {
            return {
               object: "block",
               type: HEADING,
               data: {level},
               nodes: next(el.childNodes)
            };
         }
      }
   },
   {
      deserialize(el, next) {
         const block = BLOCK_TAGS[el.tagName.toLowerCase()];

         if (block) {
            return {
               object: "block",
               type: block,
               nodes: next(el.childNodes)
            };
         }
      }
   },
   {
      deserialize(el, next) {
         const mark = MARK_TAGS[el.tagName.toLowerCase()];
         if (mark) {
            return {
               object: "mark",
               type: mark,
               nodes: next(el.childNodes)
            };
         }
      }
   },
   {
      // Special case for code blocks, which need to grab the nested childNodes.
      deserialize(el, next) {
         if (el.tagName.toLowerCase() === "pre") {
            const code = el.childNodes[0];
            const childNodes = code && code.tagName.toLowerCase() === "code" ? code.childNodes : el.childNodes;

            return {
               object: "block",
               type: "code",
               nodes: next(childNodes)
            };
         }
      }
   },
   {
      // Special case for images, to grab their src.
      deserialize(el, next) {
         if (el.tagName.toLowerCase() === "img") {
            return {
               object: "block",
               type: "image",
               nodes: next(el.childNodes),
               data: {
                  src: el.getAttribute("src")
               }
            };
         }
      }
   },
   {
      // Special case for links, to grab their href.
      deserialize(el, next) {
         if (el.tagName.toLowerCase() === "a") {
            return {
               object: "inline",
               type: "link",
               nodes: next(el.childNodes),
               data: {
                  href: el.getAttribute("href")
               }
            };
         }
      }
   }
];

const serializer = new Html({ rules: RULES });

export default function htmlPaste(options = {}) {
   return {
      onPaste(event, editor, next) {
         const transfer = getEventTransfer(event);

         console.log(transfer);

         if (transfer.type !== "html") return next();
         console.log("in ON PASTE HTML");

         const { document } = serializer.deserialize(transfer.html);
         editor.insertFragment(document);
      }
   };
}
