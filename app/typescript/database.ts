import {
   IDbDocument,
   ISubject,
   IFolder,
   ICourse,
   IUser,
   IQuiz
} from './collections';
import { Dispatch } from '../reducers/types';

export interface IDatabase {
   dispatch: Dispatch;
   subjects: IDatabaseSubjects;
   folders: IDatabaseFolders;
   courses: IDatabaseCourses;
   quizs: IDatabaseQuizs;
   _main: PouchDB.Database;
   // _main: {
   //    get: GetId;
   //    put: any; // (doc: IDbDocument) => Promise<IDbDocument>;
   //    remove:
   // };
   init: any;
   request: any;
}

export type IDatabaseSubjects = {
   findAll: FindAll;
   findById: FindSubjectById;
   create: CreateSubject;
   update: UpdateSubject;
   remove: RemoveSubject;
};

export type IDatabaseFolders = {
   findById: FindFolderById;
   create: CreateFolder;
   update: UpdateFolder;
   remove: RemoveFolder;
};

export type IDatabaseCourses = {
   findById: FindCourseById;
   create: CreateCourse;
   update: UpdateCourse;
   remove: RemoveCourse;
};

export type IDatabaseQuizs = {
   findById: FindQuizById;
   create: CreateQuiz;
   remove: RemoveQuiz;
};

// _main
export type GetId = (id: string) => Promise<IDbDocument>;
// export type remove =

// subjects
export type FindAll = () => Promise<ISubject[]>;
export type FindSubjectById = (id: string) => Promise<ISubject>;
export type CreateSubject = (newSubject: {
   name: string;
   icon: string | null;
}) => Promise<ISubject>;
export type UpdateSubject = (toUpdate: ISubject) => Promise<ISubject>;
export type RemoveSubject = (toRemove: ISubject) => Promise<ISubject>;

// folders
export type FindFolderById = (id: string) => Promise<IFolder>;
export type CreateFolder = (newFolder: {
   name: string;
   parent: { id: string; collection: string };
}) => Promise<IFolder>;
export type UpdateFolder = (toUpdate: IFolder) => Promise<IFolder>;
export type RemoveFolder = (toRemove: IFolder) => Promise<IFolder>;

// Quiz
export type CreateQuiz = (newQuiz: {
   name: string;
   parent: { id: string; collection: string };
}) => Promise<IQuiz>;

// courses
export type FindCourseById = (id: string) => Promise<ICourse>;
export type CreateCourse = (
   newCourse: {
      name: string;
      parent: { id: string; collection: string };
   },
   content?: Object
) => Promise<ICourse>;
export type UpdateCourse = (
   toUpdate: ICourse,
   content?: Object
) => Promise<ICourse>;
export type RemoveCourse = (toRemove: ICourse) => Promise<ICourse>;

// user
export type UpdateUser = (toUpdate: IUser) => Promise<IUser>;

// Question
export type UpdateQuiz = (toUpdate: IQuiz) => Promise<IQuiz>;
export type RemoveQuiz = (toRemove: IQuiz) => Promise<IQuiz>;
export type FindQuizById = (id: string) => Promise<IQuiz>;

/*
export interface INewFolder {
   name: string;
   parent: IParent;
}
*/

// export type CreateCourse = (newCourse: {
//    name: string,
//    folderId: string
// }) => Promise<ICourse>;
// export type CreateFolder = (newFolder: INewFolder) => Promise<IFolder>;
