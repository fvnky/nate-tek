import React from 'react';
import { RouteComponentProps, withRouter, match } from 'react-router';
import { push } from 'connected-react-router';
import { History } from 'history';

import { lastUpdateRecursive } from '../../utils';

import IconMore from '../../../assets/icons/IconMore';

import PopUpConstructor, {
   POPUP_TRIGGER_LIST
} from '../PopUp/PopUpConstructor';

// Constant
import {
   DRAFT,
   SUBJECT,
   FOLDER,
   COURSE,
   QUIZ
} from '../../../database/constants/index';
import {
   NAME_LIST,
   ICON_LIST,
   CREATE_LIST,
   UPDATE_LIST,
   LAST_UPDATED_LIST,
   TIME_HEADER_LIST
} from './ElementsList';
import {
   ONCLICK_LIST,
   DROPPABLE_LIST,
   DRAGGABLE_LIST,
   POPUP_LIST
} from './ActionsList';
import routes from '../../../constants/routes.json';

// Icon
import IconFolder from '../../../assets/icons/IconFolder';
import IconCourse from '../../../assets/icons/IconCourse';
import IconDelete from '../../../assets/icons/IconDelete';
import { iconsSubject } from '../../../assets/icons/Subject/IconsSubject';

// Typescipt
import { IDbDocument, IListItem } from '../../../typescript';

// Styles
import styles from './Descriptor.less';

// Export
export {
   NAME_LIST,
   ICON_LIST,
   CREATE_LIST,
   UPDATE_LIST,
   LAST_UPDATED_LIST
} from './ElementsList';
export {
   ONCLICK_LIST,
   DROPPABLE_LIST,
   DRAGGABLE_LIST,
   POPUP_LIST
} from './ActionsList';
export { TIME_HEADER_LIST } from './HeaderList';
export {
   EDIT_POPUP,
   DELETE_POPUP,
   POPUP_TRIGGER_LIST,
   MOVE_POPUP
} from '../PopUp/PopUpConstructor';

export function fillName(doc: IDbDocument) {
   const { name } = doc;

   return { [NAME_LIST]: name };
}

export function fillLastUpdated(
   doc: IDbDocument,
   collections?: (IDbDocument[])[]
) {
   const lastUpdatedDoc = lastUpdateRecursive(doc, collections);
   const { updatedAt, createdAt } = lastUpdatedDoc;
   if (updatedAt) return { [LAST_UPDATED_LIST]: updatedAt };
   return { [LAST_UPDATED_LIST]: createdAt };
}

export function fillIcon(doc: IDbDocument) {
   const { collection } = doc;

   switch (collection) {
      case SUBJECT:
         return { [ICON_LIST]: doc.icon };
      case FOLDER:
         return { [ICON_LIST]: 'IconFolder' };
      case COURSE:
         return { [ICON_LIST]: 'IconCourse' };
      case QUIZ:
         return { [ICON_LIST]: 'IconQuiz' };
      default:
         return { [ICON_LIST]: 'IconCourse' };
   }
}

export function fillDroppable(doc: IDbDocument) {
   const { _id } = doc;

   return { parentId: _id, key: 'id' };
}

export function fillDraggable(doc: IDbDocument) {
   const { _id } = doc;

   return { id: _id };
}

export function fillPopup(doc: IDbDocument, popup: string[]) {
   return {
      [POPUP_LIST]: <PopUpConstructor doc={doc} popup={popup} />,
      [POPUP_TRIGGER_LIST]: <IconMore className={styles.iconMore} />
   };
}

export const routesOnClick = {
   [FOLDER]: `${routes.RESOURCES}${routes.FOLDERS}/`,
   [SUBJECT]: `${routes.SUBJECTS}/`,
   [COURSE]: `${routes.DOC_VIEWER_LINK}/`,
   [DRAFT]: `${routes.EDITOR_LINK}/`,
   [QUIZ]: `${routes.QUIZ_LINK}/`
};

export function fillOnClick(doc: IDbDocument, history: History) {
   const { _id, collection } = doc;

   const handleOnClick = (link: string) => (
      e: React.MouseEvent<HTMLDivElement, MouseEvent>
   ) => {
      e.stopPropagation();
      // console.log("LINK :", link);
      history.push(link);
   };

   return {
      [ONCLICK_LIST]: handleOnClick(`${routesOnClick[collection]}${_id}`)
   };
}

export function fillTimeHeaderList(time: string) {
   return [
      {
         id: time,
         [TIME_HEADER_LIST]: { [TIME_HEADER_LIST]: time },
         className: styles.alignTimeHeader
      }
   ];
}

export const fillField = {
   [DRAGGABLE_LIST]: fillDraggable,
   [DROPPABLE_LIST]: fillDroppable,
   [ONCLICK_LIST]: fillOnClick,
   [POPUP_LIST]: fillPopup,
   [NAME_LIST]: fillName,
   [ICON_LIST]: fillIcon,
   [LAST_UPDATED_LIST]: fillLastUpdated
};

export function fillDocument(
   doc: IDbDocument,
   history: History,
   popup: string[],
   fields: string[],
   collections?: (IDbDocument[])[]
): IListItem {
   const descriptor = { id: '' };

   descriptor.id = doc._id;
   fields.forEach((field: string) => {
      if (fillField[field]) {
         if (field === POPUP_LIST)
            descriptor[field] = fillField[field](doc, popup);
         else if (field === ONCLICK_LIST)
            descriptor[field] = fillField[field](doc, history);
         else if (field === LAST_UPDATED_LIST)
            descriptor[field] = fillField[field](doc, collections);
         else descriptor[field] = fillField[field](doc);
      }
   });
   return descriptor as IListItem;
}

export function fillHeaderColumn(field: string) {
   return { text: field, display: true };
}

export function fillHeader(fields: string[]) {
   const descriptor = {};

   fields.forEach((field: string) => {
      descriptor[field] = fillHeaderColumn(field);
   });
   return descriptor;
}
