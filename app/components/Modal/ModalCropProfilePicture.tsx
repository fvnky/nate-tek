import * as React from 'react';
import cx from 'classnames';
import AvatarEditor from 'react-avatar-editor';

// Component
import Modal from '../Base/Modal';
import SliderInput from '../Base/SliderInput';

// Utils

// Style
import stylesErr from './ModalError.less';
import styles from './ModalCropProfilePicture.less';

// typescript
import { ILanguage } from '../../typescript';

interface IProps {
   profilePicUrl: string;
   setCroppedProfilePicUrl: (url: string) => void;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   scale: number;
}

export default class ModalCropProfilePicture extends React.Component<
   IProps,
   IState
> {
   editor: any;

   state = {
      scale: 1
   };

   handleOk = () => {
      const { handleDisplayModal, setCroppedProfilePicUrl } = this.props;
      setCroppedProfilePicUrl(this.editor.getImageScaledToCanvas().toDataURL());
      handleDisplayModal();
   };

   handleScale = e => {
      const scale = parseFloat(e.target.value);
      this.setState({ scale });
   };

   setEditorRef = editor => {
      if (editor) this.editor = editor;
   };

   renderCropper = () => {
      const { theme, profilePicUrl } = this.props;
      const { scale } = this.state;
      return (
         <div className={styles.layout}>
            <AvatarEditor
               ref={this.setEditorRef}
               className={styles.picture}
               image={profilePicUrl}
               width={250}
               height={250}
               border={0}
               borderRadius={999}
               scale={scale}
            />
            <div className={styles.sliderContainer}>
               <SliderInput
                  onChange={this.handleScale}
                  min="1"
                  max="2"
                  step="0.01"
                  defaultValue="1"
               />
            </div>
         </div>
      );
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;

      return (
         <Modal
            theme={theme}
            title={lang.crop}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.crop}
            cancelText={lang.cancel}
         >
            {this.renderCropper()}
         </Modal>
      );
   }
}
