import React from "react";
import { isKeyHotkey } from "is-hotkey";

export const BOLD = "bold";
export const ITALIC = "italic";
export const UNDERLINED = "underlined";

export default function basicFormatting(options = {}) {
   return {
      commands: {
         toggleBold(editor) {
            editor.toggleMark(BOLD);
         },
         toggleItalic(editor) {
            editor.toggleMark(ITALIC);
         },
         toggleUnderlined(editor) {
            editor.toggleMark(UNDERLINED);
         }
      },
      queries: {
         isBold(editor) {
            return editor.value.activeMarks.some(mark => mark.type === BOLD)
         },
         isItalic(editor) {
            return editor.value.activeMarks.some(mark => mark.type === ITALIC)
         },
         isUnderlined(editor) {
            return editor.value.activeMarks.some(mark => mark.type === UNDERLINED)
         }
      },
      onKeyDown(event, editor, next) {
         let mark;

         if (isKeyHotkey("mod+b")(event)) {
            mark = BOLD;
         } else if (isKeyHotkey("mod+i")(event)) {
            mark = ITALIC;
         } else if (isKeyHotkey("mod+u")(event)) {
            mark = UNDERLINED;
         } else {
            return next();
         }

         event.preventDefault();
         editor.toggleMark(mark);
      },
      renderMark(props, editor, next) {
         const { children, mark, attributes } = props;

         switch (mark.type) {
            case BOLD:
               return <strong {...attributes}>{children}</strong>;
            case ITALIC:
               return <em {...attributes}>{children}</em>;
            case UNDERLINED:
               return <u {...attributes}>{children}</u>;
            default:
               return next();
         }
      }
   };
}
