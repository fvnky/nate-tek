import { CustomError } from '../factory';

export class DraftNotFound extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The draft could not be found';
   }
}

/*
export class TypeNotFound extends CustomError {
  constructor(message) {
    super(message)
    this.message = message || 'The type could not be found'
  }
}
*/

export class NoDraft extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'No draft could be found';
   }
}
