import * as React from 'react';
import cx from 'classnames';

// Component
import DisplayIcons from '../Base/DisplayIcons';
import Modal from '../Base/Modal';

// Utils
import { capitalize, errorModal } from '../utils';

// Icon
import { iconsSubject } from '../../assets/icons/Subject/IconsSubject';
import IconQuestionMark from '../../assets/icons/Subject/IconQuestionMark';

// Style
import styles from './ModalCreateSubject.less';
import stylesErr from './ModalError.less';

// typescript
import { CreateSubject, ILanguage } from '../../typescript';

interface IProps {
   handleDisplayModal: () => void;
   createSubject: CreateSubject;
   lang: ILanguage;
   theme: string;
}

interface IState {
   selectIcon: string;
   message: string;
   title: string;
}

export default class ModalCreateSubject extends React.Component<
   IProps,
   IState
> {
   input: React.RefObject<HTMLInputElement> = React.createRef<
      HTMLInputElement
   >();

   constructor(props: IProps) {
      super(props);
      this.state = {
         selectIcon: 'IconQuestionMark',
         message: '',
         title: ''
      };
   }

   componentDidMount() {
      const input = this.input.current;

      if (input) input.focus();
   }

   handleOnClickIcon = (id: string) => {
      this.setState({ selectIcon: id });
   };

   handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { theme } = this.props;
      const { target } = event;
      const { value } = target;
      const input = this.input.current;

      if (input) {
         if (input.className !== styles.inputNameModal) {
            input.className = cx(theme, styles.inputNameModal);
            this.setState({ message: '' });
         }
      }
      this.setState({ title: capitalize(value) });
   };

   handleOk = () => {
      const { selectIcon, title } = this.state;
      const { handleDisplayModal, createSubject, theme } = this.props;
      const input = this.input.current;
      const subject = { name: title, icon: selectIcon };

      createSubject(subject)
         .then(() => handleDisplayModal())
         .catch((err: any) => {
            this.setState({ message: err.message });
            if (input) {
               input.className = `${stylesErr.inputErrorColor} ${cx(
                  theme,
                  styles.inputNameModal
               )}`;
               input.focus();
               input.setSelectionRange(0, input.selectionEnd);
            }
         });
   };

   renderInputName = () => {
      const { message, title } = this.state;
      const { lang, theme } = this.props;

      return (
         <div className={styles.inputNameContainer}>
            <div className={stylesErr.errorMessage}>{message}</div>
            <input
               ref={this.input}
               type="text"
               value={title}
               onChange={this.handleChange}
               className={cx(theme, styles.inputNameModal)}
               placeholder={lang.enterTitle}
            />
         </div>
      );
   };

   renderSelectIcon = () => {
      const { selectIcon } = this.state;
      const { theme } = this.props;
      const ICON = iconsSubject[selectIcon];

      if (!ICON) return errorModal(this.constructor.name, selectIcon);
      return (
         <div className={`${cx(theme, styles.selectedIcon)}`}>
            <ICON />
         </div>
      );
   };

   renderIcons = () => {
      const { theme } = this.props;
      return (
         <div>
            <div className={cx(theme, styles.gradient)} />
            <div className={styles.containerIcons}>
               <DisplayIcons
                  styles={cx(theme, styles.iconsDisplay)}
                  onClick={this.handleOnClickIcon}
                  icons={iconsSubject}
                  activeStyle={cx(theme, styles.iconsDisplaySelected)}
               />
            </div>
         </div>
      );
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;

      return (
         <Modal
            theme={theme}
            title={lang.newSubject}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.create}
            cancelText={lang.cancel}
         >
            <div className={styles.layout}>
               {/* <div className={styles.breather}/> */}
               {this.renderIcons()}
               <div className={styles.iconAndNameLayout}>
                  {this.renderSelectIcon()}
                  {this.renderInputName()}
               </div>
            </div>
         </Modal>
      );
   }
}
