import _ from 'lodash';
import { PROFILE_PICTURE } from '../database/collections/user';

export function userHasProfilePicture(user) {
   const { _attachments } = user;

   if (!_attachments || !_.hasIn(_attachments, PROFILE_PICTURE)) return false;
   return true;
}

export function getProfilePictureUrl(user) {
   const { _attachments } = user;

   if (!userHasProfilePicture(user)) return;

   const picture = _.get(_attachments, PROFILE_PICTURE);
   return `data:${picture.content_type};base64,${picture.data}`;
}
