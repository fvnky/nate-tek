import * as React from 'react';

// Component
import Modal from '../Base/Modal';

// typescript
import { ICourse, IDatabase, RemoveCourse, ILanguage } from '../../typescript';

interface IProps {
   id: string;
   db: IDatabase;
   removeCourse: RemoveCourse;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   course: ICourse | null;
   title: string;
}

export default class ModalDeleteFolder extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);
      this.state = {
         course: null,
         title: ''
      };
   }

   componentDidMount() {
      const { id, db } = this.props;

      db.courses
         .findById(id)
         .then((course: ICourse) =>
            this.setState({ course, title: course.name })
         )
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   }

   handleOk = () => {
      const { handleDisplayModal, removeCourse } = this.props;
      const { course } = this.state;

      if (!course) return;
      removeCourse(course)
         .then(() => handleDisplayModal())
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;
      const { title } = this.state;

      return (
         <Modal
            theme={theme}
            titleLess={true}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.delete}
            cancelText={lang.cancel}
         >
            {lang.deleteItem}
            {title} ?
         </Modal>
      );
   }
}
