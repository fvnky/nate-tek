import React from 'react'
import HoverMenuComponent from "./HoverMenuComponent";

export default function hoverMenu(options = {}) {
  return {
    renderEditor(props, editor, next) {
      const children = next();
      return (
        <>
            <>{children}</>
            <HoverMenuComponent editor={editor} value={editor.value}/>
         </>
      )
    },
  }
}
