export default class Store {
   add(key, value) {
      this[key] = value;
      return Promise.resolve(value);
   }
}
