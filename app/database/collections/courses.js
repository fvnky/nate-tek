import _ from 'lodash';
import { Base64 } from 'js-base64';

// import { Value } from 'slate';
import initialValue from '../../constants/initialValue';
// import initialValue from '../../constants/initialValue.json';
import { Store } from '../factory';
import { CourseSchema } from '../schemas';
import { COURSE, FOLDER, SUBJECT, DRAFT } from '../constants';
// import { UPDATE_DRAFT } from '../../reducers/drafts';

import {
   ADD_COURSE,
   FETCH_COURSE,
   UPDATE_COURSE,
   REMOVE_COURSE
} from '../../reducers/courses';

import {
   generateId,
   timestamp,
   parseUpdate,
   parseData,
   parseDoc,
   isNotFound
} from '../utils';

import { InvalidParameterFormat, CourseNotFound } from '../errors';

// ================================================ //
// ==================== Create ==================== //
// ================================================ //

function create(data, content = null) {
   return new Promise((ok, ko) => {
      const store = new Store();
      const mandatory = ['name', 'parent.collection', 'parent.id'];
      const optionals = ['textContent', 'coverImage'];
      console.log('before anything', data);
      parseData(data, mandatory, optionals)
         .then(parsed => {
            console.log('bafter parse data', JSON.stringify(parsed));
            parsed._id = generateId();
            // parsed.content = parsed.content || initialValue.toJSON();
            console.log('§§§§§§§§§§ content', content);
            parsed._attachments = {
               'value.json': {
                  content_type: 'application/json',
                  data:
                     content !== null
                        ? Base64.encode(JSON.stringify(content))
                        : Base64.encode(JSON.stringify(initialValue))
               }
            };
            parsed.coverImage = parsed.coverImage || {
               full: '',
               raw: '',
               regular: '',
               small: '',
               thumb: ''
            };
            parsed.likes = [];
            parsed.comments = [];
            parsed.textContent = parsed.textContent || ''; // Here get plain text from content
            parsed.createdAt = timestamp();
            parsed.updatedAt = parsed.createdAt;
            parsed.collection = COURSE;
            return this._main.put(parsed);
         })
         .then(res => store.add('courseId', res.id))
         .then(id => this._main.get(id))
         .then(course => parseDoc(CourseSchema, course))
         // .then(course => this._main.put(course)) // Pourquoi le mettre une deuxième fois et le get une deuxième fois ?
         // .then(res => this._main.get(res.id))
         .then(course => {
            this.dispatch({ type: ADD_COURSE, payload: course });
            return course;
         })
         .then(ok)
         .catch(
            err =>
               new Promise((next, spread) => {
                  if (!store.courseId) return spread(err);

                  this._main
                     .get(store.courseId)
                     .then(doc => this._main.remove(doc._id, doc._rev))
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .catch(ko);
   });
}

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
   return new Promise((ok, ko) => {
      this._main
         .find({ selector: { collection: COURSE } })
         .then(res => {
            // console.warn("Queries Courses :", res.docs);
            this.dispatch({ type: FETCH_COURSE, payload: res.docs });
            ok(res.docs);
         })
         .catch(ko);
   });
}

function findById(id) {
   return new Promise((ok, ko) => {
      if (!_.isString(id))
         return ko(
            new InvalidParameterFormat(`The id parameter is not a String`)
         );

      this._main
         .get(id)
         .then(doc => {
            if (doc.collection !== COURSE)
               throw new CourseNotFound(
                  `The document's id '${doc._id}' does not reference a course`
               );

            this.dispatch({ type: UPDATE_COURSE, payload: doc }); // A t'on vraiment besoin de dispatch ici ?
            ok(doc);
         })
         .catch(err => {
            if (isNotFound(err))
               throw new CourseNotFound(
                  `The course '${id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

function getCourseValue(id) {
   return new Promise((ok, ko) => {
      if (!_.isString(id))
         return ko(
            new InvalidParameterFormat(`The id parameter is not a String`)
         );

      this._main
         .get(id, { attachments: true })
         .then(doc => {
            const base64 = doc._attachments['value.json'].data;
            const value = JSON.parse(Base64.decode(base64));
            // ok(Value.fromJSON(value));
            ok(value);
         })
         .catch(err => {
            if (isNotFound(err))
               throw new CourseNotFound(
                  `The course '${id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

function update(doc, content = null) {
   return new Promise((ok, ko) => {
      console.log('$$$$ GOT', doc);
      const store = new Store();
      const fields = ['folderId'];

      parseDoc(CourseSchema, doc)
         .then(parsed => this._main.get(parsed._id))
         .then(old => {
            // console.warn("before", old)
            if (old._attachments) store.add('attachments', old._attachments);
            return parseUpdate(old, doc, fields);
         })
         .then(course => store.add('course', course))
         .then(course => {
            if (
               course.parent.collection !== SUBJECT &&
               course.parent.collection !== FOLDER &&
               course.parent.collection !== DRAFT
            )
               throw new InvalidParameterFormat(
                  `The parent collection to update ${course.id} are not compatible`
               );
         })
         // .then(course => this._main.get(course.parent.id))
         // .then(parent => store.add("parent", parent))
         .then(() => {
            const { course } = store;
            // const { course, parent } = store;
            // if (parent.collection !== SUBJECT && parent.collection !== FOLDER && parent.collection !== DRAFT)
            //    throw new InvalidParameterFormat(`The parent collection to update ${course.id} are not compatible`);
            course.updatedAt = timestamp();
            if (content !== null) {
               course._attachments = {
                  'value.json': {
                     content_type: 'application/json',
                     data: Base64.encode(JSON.stringify(content))
                  }
               };
            } else if (store.attachments) {
               course._attachments = store.attachments;
            }
            // console.log("in database collection textcontent : ", course.textContent)
            course.textContent = course.textContent || ''; // Here get plain text from content

            return this._main.put(course);
         })
         .then(res => this._main.get(res.id))
         .then(course => {
            // console.warn("after", course)
            this.dispatch({ type: UPDATE_COURSE, payload: course });
            return course;
         })
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               if (!store.course)
                  throw new CourseNotFound(
                     `The course '${doc._id}' could not be found`
                  );
            // if (!store.parent) throw new CourseNotFound(`The parent '${doc.parent.id}' could not be found`);
            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Delete ==================== //
// ================================================ //

function remove(data) {
   console.log('Remove Selector');
   return new Promise((ok, ko) => {
      parseData(_.pick(data, ['_id']), ['_id']) // On peut pas simplement faire data._id ?
         .then(parsed => this._main.get(parsed._id))
         .then(course => {
            if (course.collection !== COURSE)
               throw new CourseNotFound(
                  `The document's id '${course._id}' does not reference a course`
               );
            return course;
         })
         .then(({ _id, _rev }) => {
            const promises = [
               this._main.remove(_id, _rev),
               this._cloud.remove(_id, _rev)
            ];

            return Promise.all(promises);
         })
         .then(() => this.dispatch({ type: REMOVE_COURSE, payload: data }))
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               throw new CourseNotFound(
                  `The course '${data._id}' could not be found`
               );
            throw err;
         })
         .catch(ko);
   });
}

export default function() {
   return {
      create: create.bind(this),
      findAll: findAll.bind(this),
      findById: findById.bind(this),
      getCourseValue: getCourseValue.bind(this),
      update: update.bind(this),
      remove: remove.bind(this)
   };
}
