import * as React from 'react';

const SvgNounBuilding1009876 = props => (
   <svg viewBox="0 0 52 65" {...props}>
      <path d="M26.007 50.104H3V1.896l23.007 4.38v43.828zM5 48.104h19.007V7.932L5 4.312v43.792z" />
      <path d="M49 50.104H24.007V11.81L49 18.332v31.772zm-22.993-2H47V19.876l-20.993-5.478v33.706z" />
      <path d="M2 48.104h48v2H2z" />
      <path d="M20.409 50.104L8.35 50.096V33.104H20.41v17zM10.35 48.097l8.058.007v-13H10.35v12.993zM42.028 48.104h-2v-13H33v12.993h-2V33.104h11.028z" />
      <path d="M32 33.104h9.028v2H32zM9.039 12.104h12v2h-12zM9.07 18.104h12v2h-12zM8.976 24.104h12v2h-12zM30.976 22.104h12v2h-12zM31 28.104h12.025v2H31z" />
   </svg>
);

export default SvgNounBuilding1009876;
