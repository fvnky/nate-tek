import { FOLDER } from '../constants';

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      name: { type: String, required: true },
      parent: {
         collection: { type: String, required: true },
         id: { type: String, required: true }
      },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      collection: FOLDER
   }),
   fields: [
      '_id',
      '_rev',
      'name',
      'parent.collection',
      'parent.id',
      'createdAt',
      'updatedAt',
      'collection'
   ]
};
