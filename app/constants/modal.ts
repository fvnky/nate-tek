import { FOLDER, SUBJECT, COURSE, QUIZ } from '../database/constants/index';

export { modalDictionary } from '../components/Modal/ModalManager';

export const MODAL = 'Modal';

export const CREATE = 'Create';
export const DELETE = 'Delete';
export const UPDATE = 'Update';
export const MOVE = 'Move';
export const SCHEDULE = 'Schedule';

export const MODAL_MOVE_RESOURCE = 'ModalMoveResource';
export const MODAL_CREATE_COURSE = `${MODAL}${CREATE}${COURSE}`;
export const MODAL_CREATE_FOLDER = MODAL + CREATE + FOLDER;
export const MODAL_CREATE_SUBJECT = MODAL + CREATE + SUBJECT;
export const MODAL_DELETE_COURSE = MODAL + DELETE + COURSE;
export const MODAL_DELETE_FOLDER = MODAL + DELETE + FOLDER;
export const MODAL_DELETE_SUBJECT = MODAL + DELETE + SUBJECT;
export const MODAL_DELETE_QUIZ = MODAL + DELETE + QUIZ;
export const MODAL_UPDATE_COURSE = MODAL + UPDATE + COURSE;
export const MODAL_UPDATE_FOLDER = MODAL + UPDATE + FOLDER;
export const MODAL_UPDATE_SUBJECT = MODAL + UPDATE + SUBJECT;
export const MODAL_UPDATE_QUIZ = MODAL + UPDATE + QUIZ;

export const MODAL_MOVE_COURSE = MODAL + MOVE + COURSE;
export const MODAL_MOVE_FOLDER = MODAL + MOVE + FOLDER;

export const MODAL_CREATE_SCHEDULE = MODAL + CREATE + SCHEDULE;
