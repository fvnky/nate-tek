import React from 'react';

// Component
import {
   QuizLayout,
   QuizHeader,
   QuizContent,
   QuizFooter,
   QuizFooterButton
} from './QuizLayout';
import {
   useSwitchQuizPage,
   useFindQuiz,
   useFindQuizParam
} from './useFetchQuiz';
import { upper } from '../utils';
import { displayScore } from './QuizGame';

// SVG
import IconArrowDown from '../../assets/icons/IconArrowDown';

// Constant
import { QUESTIONS, FORM, QUESTION, SCORE } from '../../constants/models';
import routes from '../../constants/routes.json';

// Style
import styles from './QuizHome.less';

// Typescript
import { IQuizPage } from '../../typescript';

function getQuestionsLenght(props: IQuizHome) {
   const questions = useFindQuizParam({ param: QUESTIONS, ...props });

   if (!questions) return 0;
   return questions.length;
}

export function getSubjectName(props: IQuizHome) {
   const { folders, subjects } = props;
   const quiz = useFindQuiz(props);

   if (!quiz) return null;
   let id = quiz.parent.id;
   while (!subjects.find(subject => subject._id === id)) {
      const folder = folders.find(folder => folder._id === id);
      if (!folder) return null;
      id = folder.parent.id;
   }
   const subject = subjects.find(subject => subject._id === id);

   if (!subject) return null;
   return subject.name;
}

interface IQuizHomeInfo {
   title: string;
   content: any;
}

const QuizHomeInfo = (props: IQuizHomeInfo) => {
   const { title, content } = props;

   return (
      <div className={styles.elementQuizInfo}>
         <div>{title}</div>
         <div className={styles.elementInfo}>{content}</div>
      </div>
   );
};

interface IQuizQuestion extends IQuizHome {}

const QuizQuestions = (props: IQuizQuestion) => {
   const { lang } = props;
   const [displayQuestion, setDisplayQuestion] = React.useState(false);
   const questions = useFindQuizParam({ param: QUESTIONS, ...props });
   const displayArrow = displayQuestion ? styles.arrowUp : styles.arrowDown;

   if (!questions) return null;

   const handleOnClick = () => setDisplayQuestion(!displayQuestion);

   return (
      <div className={styles.layoutQuestion}>
         <div
            role="button"
            className={styles.buttonDisplayQuestion}
            onClick={handleOnClick}
         >
            <div className={styles.textDisplayQuestion}>
               {lang.seeQuestions}
            </div>
            <IconArrowDown className={`${styles.arrow} ${displayArrow}`} />
         </div>
         {displayQuestion && (
            <div className={styles.containerQuizQuestions}>
               {questions.map((question: any, index: number) => {
                  const {
                     [FORM]: { [QUESTION]: question_name }
                  } = question;

                  return (
                     <div key={index} className={styles.contentQuizQuestions}>
                        <div className={styles.indexQuizQuestions}>
                           {index + 1}.
                        </div>
                        <div className={styles.nameQuizQuestions}>
                           {question_name}
                        </div>
                     </div>
                  );
               })}
            </div>
         )}
      </div>
   );
};

interface IQuizHome extends IQuizPage {
   // setView: React.Dispatch<React.SetStateAction<string>>;
}

const QuizHome = (props: IQuizHome) => {
   const { lang } = props;
   const quizScore = useFindQuizParam({ param: SCORE, ...props });
   const subjectName = getSubjectName(props);
   const questionLenght = getQuestionsLenght(props);

   const handleOnClick = (view: string) => () =>
      useSwitchQuizPage({ page: view, ...props });

   return (
      <QuizLayout>
         <QuizHeader title={lang.quiz} {...props} />
         <div className={styles.containerQuizInfo}>
            <div className={styles.widthInfo}>
               <QuizHomeInfo title={lang.subject} content={subjectName} />
               <QuizHomeInfo
                  title={lang.score}
                  content={displayScore(quizScore)}
               />
               <QuizHomeInfo title={lang.questions} content={questionLenght} />
            </div>
         </div>
         <QuizContent>
            <QuizQuestions {...props} />
         </QuizContent>
         <QuizFooter>
            <QuizFooterButton
               text={upper(lang.start)}
               style={styles.quizStart}
               handleOnClick={handleOnClick(routes.QUIZ_GAME_LINK)}
            />
            <QuizFooterButton
               text={upper(lang.edit)}
               style={styles.quizEdit}
               handleOnClick={handleOnClick(routes.QUIZ_EDITOR_LINK)}
            />
         </QuizFooter>
      </QuizLayout>
   );
};

export default QuizHome;
