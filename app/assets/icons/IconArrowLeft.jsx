import * as React from 'react';

const IconArrowLeft = props => (
   <svg viewBox="0 0 100 100" {...props}>
      <g transform="translate(0,-952.36218)">
         <path d="m 66.8125,962.34375 a 3.0003,3.0003 0 0 0 -2.03125,1 l -34,37.00005 a 3.0003,3.0003 0 0 0 0,4.0624 l 34,37 a 3.0080933,3.0080933 0 1 0 4.4375,-4.0624 l -32.125,-34.9688 32.125,-34.96875 a 3.0003,3.0003 0 0 0 -2.40625,-5.0625 z" />
      </g>
   </svg>
);

export default IconArrowLeft;
