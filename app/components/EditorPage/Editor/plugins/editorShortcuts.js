import { isKeyHotkey } from "is-hotkey";

export default function editorShortcuts(options = {}) {
   return {
      onKeyDown(event, editor, next) {
         if (isKeyHotkey("mod+s")(event)) {
            options.nateEditor.saveCurrentCourse()
         }
         return next();
      }
   };
}
