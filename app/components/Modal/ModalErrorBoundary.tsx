import * as React from 'react';

// Component
import Modal from '../Base/Modal';

// typescript
import { ToggleModal, ILanguage } from '../../typescript';

// Constant
import { NONE } from '../../reducers/modals';

interface IProps {
   children: React.ReactNode;
   toggleModal: ToggleModal;
   lang: ILanguage;
   theme: string;
}

interface IState {
   error: any;
   errorInfo: any;
}

export default class ErrorBoundary extends React.Component<IProps, IState> {
   input: React.RefObject<HTMLInputElement> = React.createRef<
      HTMLInputElement
   >();

   constructor(props: IProps) {
      super(props);
      this.state = {
         error: null,
         errorInfo: null
      };
   }

   componentDidCatch(error: any, errorInfo: any) {
      this.setState({
         error,
         errorInfo
      });
   }

   handleToggleModal = () => {
      const { toggleModal } = this.props;

      toggleModal(NONE, {});
   };

   render() {
      const { errorInfo, error } = this.state;
      const { children, theme } = this.props;

      if (errorInfo) {
         return (
            <Modal
               theme={theme}
               onCancel={this.handleToggleModal}
               onOk={this.handleToggleModal}
            >
               <div>
                  <h2>Something went wrong.</h2>
                  <details style={{ whiteSpace: 'pre-wrap' }}>
                     {error && error.toString()}
                     <br />
                     {errorInfo.componentStack}
                  </details>
               </div>
            </Modal>
         );
      }
      return children;
   }
}
