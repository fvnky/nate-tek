import _ from 'lodash';

import { LOAD_LANG } from '../../reducers/language';
import { USER, PROFILE } from '../constants';
import { sync } from '../utils';

import { UPDATE_PROFILE } from '../../reducers/user';

export const PROFILE_PICTURE = 'profile_picture';

// ============================================== //
// ==================== Auth ==================== //
// ============================================== //

function verify(data) {
   console.log(data);
   return new Promise((next, stop) => {
      this.request
         .post('/auth/verify', { mail: data.name })
         .then(res => next(res.data))
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function signUp(data) {
   return new Promise((next, stop) => {
      const env = {};

      this.request
         .post('/auth/register', data)
         .then(res => {
            env.profile = res.data.user;
            env.token = res.data.token;
         })
         .then(
            () =>
               (this.request.defaults.headers.common.Authorization = `JWT ${
                  env.token
               }`)
         )
         .then(
            () =>
               new Promise(done => {
                  this._session
                     .get(USER)
                     .then(user => this._session.remove(USER, user._rev))
                     .then(() => done())
                     .catch(() => done());
               })
         )
         .then(() =>
            this._session.put({
               _id: USER,
               profile: env.profile,
               token: env.token
            })
         )
         .then(() =>
            this.dispatch({ type: LOAD_LANG, payload: env.profile.lang })
         )
         .then(() =>
            this.dispatch({ type: UPDATE_PROFILE, payload: env.profile })
         )
         .then(() => this.connect(env.profile._id, env.token))
         .then(() => this.fetch())
         .then(() => this.refresh())
         .then(() => this.sync())
         .then(() => next())
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function sendValidation(mail) {
   console.log("step 1.1")
   return new Promise((next, stop) => {
      this.request
         .post(`/auth/validation`, { mail })
         .then(() => next())
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function logIn(mail, code) {
   return new Promise((next, stop) => {
      const env = {};
      console.log('LOG IN');

      this.request
         .post('/auth/login', { mail, code })
         .then(res => {
            env.profile = res.data.user;
            env.token = res.data.token;
         })
         .then(
            () =>
               (this.request.defaults.headers.common.Authorization = `JWT ${
                  env.token
               }`)
         )
         .then(
            () =>
               new Promise(done => {
                  this._session
                     .get(USER)
                     .then(user => this._session.remove(USER, user._rev))
                     .then(() => done())
                     .catch(() => done());
               })
         )
         .then(() =>
            this._session.put({
               _id: USER,
               profile: env.profile,
               token: env.token
            })
         )
         .then(() =>
            this.dispatch({ type: LOAD_LANG, payload: env.profile.lang })
         )
         .then(() =>
            this.dispatch({ type: UPDATE_PROFILE, payload: env.profile })
         )
         .then(() => this.connect(env.profile._id, env.token))
         .then(() => this.fetch())
         .then(() => this.refresh())
         .then(() => this.sync())
         .then(() => next())
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function logOut() {
   if (this._sync) {
      this._sync.cancel();
      this._sync = null;
   }

   return new Promise((next, stop) => {
      this._main
         .allDocs({ include_docs: true, attachments: true })
         .then(res => {
            const task = function(item, index, cb) {
               this._main
                  .remove(item.doc._id, item.doc._rev)
                  .then(() => cb())
                  .catch(() => cb());
            };

            return sync(res.rows, task.bind(this));
         })
         .then(
            () =>
               new Promise(done => {
                  this._session
                     .get(USER)
                     .then(user => this._session.remove(USER, user._rev))
                     .then(() => done())
                     .catch(() => done());
               })
         )
         .then(() => this.dispatch({ type: UPDATE_PROFILE, payload: {} }))
         .then(() => this.refresh())
         .then(() => next())
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

// ============================================== //
// ==================== Load ==================== //
// ============================================== //

function loadUser() {
   return new Promise((next, stop) => {
      let env = {};
      // console.log("LOAD");

      this._session
         .get(USER)
         .then(res => (env = res))
         .then(() => {
            this.dispatch({ type: LOAD_LANG, payload: env.profile.lang });
            this.dispatch({ type: UPDATE_PROFILE, payload: env.profile });
            return this.refresh();
         })
         .then(() => this.refresh())
         .then(() => this.request.put('/auth/refresh', { token: env.token }))
         .then(res => {
            env.profile = res.data.user;
            env.token = res.data.token;

            this.dispatch({ type: LOAD_LANG, payload: env.profile.lang });
            this.dispatch({ type: UPDATE_PROFILE, payload: env.profile });
            this.request.defaults.headers.common.Authorization = `JWT ${
               env.token
            }`;
         })
         .then(() =>
            this._session.put({
               _id: USER,
               _rev: env._rev,
               profile: env.profile,
               token: env.token
            })
         )
         .then(() => this.connect(env.profile._id, env.token))
         .then(() => this.fetch())
         .then(() => this.refresh())
         .then(() => this.sync())
         .then(() => next())
         .catch(err => {
            const error =
               err.response && err.response.data ? err.response.data : err;

            if (error === 'InvalidToken')
               this.logOut()
                  .then(() => next())
                  .catch(stop);
            else if (error.status === 404) next();
            else stop(error);
         });
   });
}

function createUniversity(name) {
   return new Promise((next, stop) => {
      this.request
         .post(`/universities`, { name })
         .then(res => next(res.data))
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function getUniversities(query) {
   return new Promise((next, stop) => {
      this.request
         .get(`/universities/${query}`)
         .then(res => next(res.data))
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function createCurriculum(name) {
   return new Promise((next, stop) => {
      this.request
         .post(`/curriculums`, { name })
         .then(res => next(res.data))
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function getCurriculums(query) {
   return new Promise((next, stop) => {
      this.request
         .get(`/curriculums/${query}`)
         .then(res => next(res.data))
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

function updateUser(id, data) {
   return new Promise((next, stop) => {
      console.log('received data', data);
      this.request
         .put(`/users/${id}`, data)
         .then(res => {
            console.log('Got in return  res', res);

            this.dispatch({ type: LOAD_LANG, payload: res.data.lang });
            this.dispatch({ type: UPDATE_PROFILE, payload: res.data });

            next(res.data);
         })
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function updateProfile(data) {
   return new Promise((next, stop) => {
      const env = {};

      this._session
         .get(USER)
         .then(user => (env.user = user))
         .then(() => this.request.post(`/users/${env.user.profile._id}`, data))
         .then(user => (env.profile = user))
         .then(() => this._session.remove(USER, env.user._rev))
         .then(() => this._session.put({ _id: USER, profile: env.profile }))
         .then(() =>
            this.dispatch({ type: LOAD_LANG, payload: env.profile.lang })
         )
         .then(() =>
            this.dispatch({ type: UPDATE_PROFILE, payload: env.profile })
         )
         .then(() => next())
         .catch(err => stop(err.response.data));
   });
}

function updateProfilePicture(data) {
   return new Promise((ok, ko) => {
      this._main
         .get(PROFILE)
         .then(profile =>
            this._main.putAttachment(
               PROFILE,
               PROFILE_PICTURE,
               profile._rev,
               data
            )
         )
         .then(() => setTimeout(() => {}, 1000))
         .then(() => this._main.get(PROFILE, { attachments: true }))
         .then(ok)
         .catch(ko);
   });
}

// ================================================ //
// ==================== Delete ==================== //
// ================================================ //

function removeProfilePicture() {
   const env = {};

   return new Promise((ok, ko) => {
      this._main
         .get(PROFILE)
         .then(profile =>
            this._main.removeAttachment(PROFILE, PROFILE_PICTURE, profile._rev)
         )
         .then(() => this._session.get(USER))
         .then(user => (env.user = user))
         .then(() => this._main.get(PROFILE, { attachments: true }))
         .then(profile => (env.user = _.merge(env.user, profile)))
         .then(profile => {
            this.dispatch({ type: UPDATE_PROFILE, payload: env.user });
            return profile;
         })
         .then(ok)
         .catch(ko);
   });
}

export default function() {
   return {
      verify: verify.bind(this),
      logIn: logIn.bind(this),
      logOut: logOut.bind(this),
      signUp: signUp.bind(this),
      createCurriculum: createCurriculum.bind(this),
      getCurriculums: getCurriculums.bind(this),
      createUniversity: createUniversity.bind(this),
      getUniversities: getUniversities.bind(this),
      sendValidation: sendValidation.bind(this),
      loadUser: loadUser.bind(this),
      updateUser: updateUser.bind(this),
      updateProfile: updateProfile.bind(this),
      updateProfilePicture: updateProfilePicture.bind(this),
      removeProfilePicture: removeProfilePicture.bind(this)
   };
}
