import _ from 'lodash';
// import { Action } from './types';
import { IDictionary } from '../typescript';
import { IActionCourse, IActionQuiz, IActionDictionary } from './types';

export const ADD_DICTIONARY = 'add_dictionary';
export const FETCH_DICTIONARY = 'fetch_dictionary';
export const UPDATE_DICTIONARY = 'update_dictionary';
export const REMOVE_DICTIONARY = 'remove_dictionary';
// export const FETCH_COURSE = "fetch_course";
// export const UPDATE_COURSE = "update_course";
// export const REMOVE_COURSE = "remove_course";

export function dictionary(
   state: IDictionary[] = [],
   action: IActionDictionary
) {
   switch (action.type) {
      case ADD_DICTIONARY:
         return _.union(state, [action.payload]);
      case FETCH_DICTIONARY: {
         // console.log("HELLO");
         return action.payload;
      }
      case UPDATE_DICTIONARY: {
         const ref = _.clone(state);
         const quiz = _.find(ref, { _id: action.payload._id });

         _.assign(quiz, action.payload);
         return ref;
      }
      case REMOVE_DICTIONARY: {
         const ref = _.clone(state);

         _.remove(ref, { _id: action.payload._id });
         return ref;
      }

      // case UPDATE_COURSE: {
      //    const ref = _.clone(state);
      //    const subject = _.find(ref, { _id: action.payload._id });

      //    _.assign(subject, action.payload);
      //    return ref;
      // }
      // case REMOVE_COURSE: {
      //    const ref = _.clone(state);

      //    _.remove(ref, { _id: action.payload._id });
      //    return ref;
      // }
      default: {
         // const ref = _.clone(state);

         return state;
      }
   }
}
