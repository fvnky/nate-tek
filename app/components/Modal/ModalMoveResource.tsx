import React, { useEffect } from 'react';
import cx from 'classnames';

// Component
import Modal from '../Base/Modal';
import IconCourse from '../../assets/icons/IconCourse';
import IconFolder from '../../assets/icons/IconFolder';
import IconArrowLeft from '../../assets/icons/IconBack';
import IconArrowRight from '../../assets/icons/IconArrowRight';
import { iconsSubject } from '../../assets/icons/Subject/IconsSubject';

// Constant
import {
   SUBJECT,
   FOLDER,
   COURSE,
   DRAFT_ID,
   DRAFT,
   QUIZ
} from '../../database/constants/index';

// typescript
import {
   IDatabase,
   UpdateCourse,
   UpdateFolder,
   ILanguage,
   IDbDocument,
   ISubject,
   ICourse,
   IFolder,
   ToastOptions,
   IQuiz,
   CreateCourse
} from '../../typescript';

import styles from './ModalMoveResource.less';
import { CourseAlreadyExists } from '../../database/errors';

interface IProps {
   createCourse: CreateCourse;
   showToast: (options: ToastOptions) => void;
   subjects: ISubject[];
   folders: IFolder[];
   resource: IFolder | ICourse | IQuiz;
   fromLibrary?: boolean;
   course?: any;
   collection: string;
   db: IDatabase;
   updateCourse: UpdateCourse;
   updateFolder: UpdateFolder;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
   c;
}

const getParentFolder = (
   resource: IDbDocument,
   folders: IFolder[],
   subjects: ISubject[]
) => {
   if (resource && resource.parent)
      return resource.parent.collection === FOLDER
         ? folders.find(f => f._id === resource.parent.id)
         : subjects.find(s => s._id === resource.parent.id);
   else return null;
};

const ModalMoveResource: React.FC<IProps> = ({
   handleDisplayModal,
   theme,
   lang,
   resource,
   folders,
   subjects,
   db,
   showToast,
   fromLibrary,
   course,
   createCourse
}) => {
   const [currentFolder, setCurrentFolder] = React.useState(
      getParentFolder(resource, folders, subjects)
   );
   const [hoveredFolder, setHoveredFolder] = React.useState('');
   const [selectedFolder, setselectedFolder] = React.useState('');
   const [isDraftsSelected, setIsDraftsSelected] = React.useState(false);

   const handleOk = () => {
      if (fromLibrary) {
         console.warn('COURSE', course);
         let newDocument: {
            name: string;
            parent: { id: string; collection: string };
            coverImage?: {
               full: string;
               raw: string;
               regular: string;
               small: string;
               thumb: string;
            };
            textContent?: string;
         } = {
            name: course.name,
            parent: { id: null, collection: null },
            coverImage: course.coverImage,
            textContent: course.textContent
         };
         if (isDraftsSelected) {
            newDocument.parent.id = DRAFT_ID;
            newDocument.parent.collection = DRAFT;
         } else {
            newDocument.parent.id =
               selectedFolder === '' ? currentFolder._id : selectedFolder;
            newDocument.parent.collection = !currentFolder ? SUBJECT : FOLDER;
         }
         // console.log("After +++", JSON.stringify(newDocument,null, 3) )
         // console.log("course.attachment", course.attachment )
         // console.log("JSON.stringify(course.attachment", JSON.parse(course.attachment) )
         createCourse(newDocument, JSON.parse(course.attachment))
            .then(() => handleDisplayModal())
            .then(() => showToast({ text: 'Saved', iconName: 'check' }))
            .catch(err => console.error(err));
         return;
      }
      const updatedResource = resource;
      if (isDraftsSelected) {
         updatedResource.parent.id = DRAFT_ID;
         updatedResource.parent.collection = DRAFT;
      } else {
         updatedResource.parent.id =
            selectedFolder === '' ? currentFolder._id : selectedFolder;
         updatedResource.parent.collection = !currentFolder ? SUBJECT : FOLDER;
      }
      switch (resource.collection) {
         case FOLDER: {
            db.folders
               .update(updatedResource as IFolder)
               .then(() => handleDisplayModal())
               .then(() => showToast({ text: 'Moved', iconName: 'check' }))
               .catch(err => console.error(err));
         }
         case COURSE: {
            db.courses
               .update(updatedResource as ICourse)
               .then(() => handleDisplayModal())
               .then(() => showToast({ text: 'Moved', iconName: 'check' }))
               .catch(err => console.error(err));
         }
         case QUIZ: {
            db.quizs
               .update(updatedResource as IQuiz)
               .then(() => handleDisplayModal())
               .then(() => showToast({ text: 'Moved', iconName: 'check' }))
               .catch(err => console.error(err));
         }
         default: {
            handleDisplayModal();
         }
      }
   };

   const changeIsDraftSelected = value => e => {
      if (value === false) {
         setselectedFolder('');
      }
      setIsDraftsSelected(value);
   };

   const changeSelectedFolder = id => e => {
      if (isDraftsSelected) {
         setIsDraftsSelected(false);
      }
      if (selectedFolder !== id) setselectedFolder(id);
   };

   const changeHoveredFolder = id => e => {
      if (hoveredFolder !== id) {
         setHoveredFolder(id);
      }
   };

   const onClickOutside = e => {
      if (hoveredFolder === '') {
         if (isDraftsSelected) setIsDraftsSelected(false);
         setselectedFolder('');
      }
   };

   const changeCurrentFolder = (folder: IFolder | ISubject | null) => e => {
      setselectedFolder('');
      setCurrentFolder(folder);
   };

   const renderList = () => {
      if (currentFolder) {
         const childFolders = folders.filter(
            f => f.parent.id === currentFolder._id && f._id !== resource._id
         );
         if (childFolders.length === 0)
            return (
               <div className={cx(theme, styles.empty)}>{lang.emptyFolder}</div>
            );
         return childFolders.map(f => {
            const hovered = hoveredFolder === f._id;
            const selected = selectedFolder === f._id;
            return (
               <div
                  key={f._id}
                  className={cx(
                     theme,
                     styles.listItem,
                     hovered ? styles.listItemHovered : '',
                     selected ? styles.listItemSelected : ''
                  )}
                  onClick={changeSelectedFolder(f._id)}
                  onMouseMove={changeHoveredFolder(f._id)}
                  onMouseLeave={changeHoveredFolder('')}
                  onDoubleClick={changeCurrentFolder(f)}
               >
                  <IconFolder className={styles.listIcon} />
                  <div className={styles.listItemName}>{f.name}</div>
                  {hovered && (
                     <IconArrowRight
                        className={cx(theme, styles.arrowRight)}
                        onClick={changeCurrentFolder(f)}
                     />
                  )}
               </div>
            );
         });
      } else {
         if (subjects.length === 0)
            return (
               <div className={cx(theme, styles.empty)}>
                  {lang.emptySubjects}
               </div>
            );
         return subjects.map(s => {
            const Icon = iconsSubject[s.icon];
            const hovered = hoveredFolder === s._id;
            const selected = selectedFolder === s._id;
            return (
               <div
                  key={s._id}
                  className={cx(
                     theme,
                     styles.subjectItem,
                     hovered ? styles.listItemHovered : '',
                     selected ? styles.listItemSelected : ''
                  )}
                  onClick={changeSelectedFolder(s._id)}
                  onMouseMove={changeHoveredFolder(s._id)}
                  onMouseLeave={changeHoveredFolder('')}
                  onDoubleClick={changeCurrentFolder(s)}
               >
                  <Icon className={styles.subjectIcon} />
                  <div className={styles.subjectName}>{s.name}</div>
                  {hovered && (
                     <IconArrowRight
                        className={cx(theme, styles.arrowRight)}
                        onClick={changeCurrentFolder(s)}
                     />
                  )}
               </div>
            );
         });
      }
   };

   return (
      <Modal
         theme={theme}
         title={fromLibrary ? 'Save' : 'Move'}
         onCancel={handleDisplayModal}
         onOk={handleOk}
         okText={
            selectedFolder || isDraftsSelected
               ? fromLibrary
                  ? 'Save here'
                  : 'Move'
               : fromLibrary
               ? 'Save here'
               : 'Move here'
         }
         cancelText={lang.cancel}
         okDisabled={!currentFolder && selectedFolder === ''}
      >
         <div className={styles.layout} onClick={onClickOutside}>
            <div className={styles.title}>
               <IconCourse className={cx(theme, styles.icon)} />
               <div className={cx(theme, styles.name)}>
                  {resource && resource.name}
               </div>
            </div>

            <div className={styles.parent}>
               {currentFolder && (
                  <IconArrowLeft
                     className={cx(theme, styles.arrowLeft)}
                     onClick={changeCurrentFolder(
                        getParentFolder(currentFolder, folders, subjects)
                     )}
                  />
               )}
               <div className={cx(theme, styles.parentName)}>
                  {currentFolder ? currentFolder.name : 'Subjects'}
               </div>
            </div>
            <div className={styles.list}>{renderList()}</div>
            <div className={cx(theme, styles.divider)} />
            <div
               className={cx(
                  theme,
                  styles.drafts,
                  hoveredFolder === DRAFT_ID ? styles.listItemHovered : '',
                  isDraftsSelected ? styles.listItemSelected : ''
               )}
               onMouseMove={changeHoveredFolder(DRAFT_ID)}
               onMouseLeave={changeHoveredFolder('')}
               onClick={changeSelectedFolder(changeIsDraftSelected(true))}
            >
               Drafts
            </div>
         </div>
      </Modal>
   );
};

export default ModalMoveResource;
