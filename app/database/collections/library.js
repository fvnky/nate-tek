import _ from 'lodash';

import { sync } from '../utils';

function get(userId = null) {
   return new Promise((next, stop) => {
      this.request
         .post('/library', { userId })
         .then(res => next(res.data))
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function course(userId, courseId) {
   return new Promise((next, stop) => {
      this.request
         .post('/library/course', { userId, courseId })
         .then(res => next(res.data))
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function like(userId, courseId) {
   return new Promise((next, stop) => {
      this.request
         .post('/library/like', { userId, courseId })
         .then(res => next(res.data))
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

function comment(userId, courseId, message) {
   return new Promise((next, stop) => {
      this.request
         .post('/library/comment', { userId, courseId, message })
         .then(res => next(res.data))
         .catch(err =>
            stop(err.response && err.response.data ? err.response.data : err)
         );
   });
}

export default function() {
   return {
      get: get.bind(this),
      course: course.bind(this),
      like: like.bind(this),
      comment: comment.bind(this)
   };
}
