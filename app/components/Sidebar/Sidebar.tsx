import * as React from 'react';
import cx from 'classnames';

// Component
import IconDoubleArrowLeft from '../../assets/icons/IconDoubleArrowLeft';
import IconSearch from '../../assets/icons/IconSearch';
import SidebarList from './SidebarList';

// Utils
// import { getProfilePictureUrl, userHasProfilePicture } from '../../utils/utils';

// Constant
import {
   SELECTED_SIDEBAR_ITEM,
   WIDTH_COLLAPSE_SIDEBAR,
   WIDTH_UNCOLLAPSE_SIDEBAR,
   DOUBLE_ARROW
} from '../../constants/transition';

// Style
import styles from './Sidebar.less';

// Typescript
import { ISidebar, IUser, ILanguage, ToggleModal } from '../../typescript';

interface IProps {
   sidebar: ISidebar;
   setCollapse: (collapse: boolean) => void;
   user: IUser;
   lang: ILanguage;
   router: any;
   theme: string;
   toggleModal: ToggleModal;
}

interface IState {
   timer: any;
}

export default class Sidebar extends React.Component<IProps, IState> {
   timeout: any;

   componentDidMount() {
      this.setActiveElementWidth();
      this.forceUpdate();
   }

   componentDidUpdate() {
      this.setActiveElementWidth();
      this.setArrowDirection();
   }

   setActiveElementWidth = () => {
      const elem = document.getElementById(SELECTED_SIDEBAR_ITEM);
      if (!elem) return;

      const { sidebar } = this.props;
      elem.style.width = sidebar.collapse
         ? `${WIDTH_COLLAPSE_SIDEBAR}px`
         : `${WIDTH_UNCOLLAPSE_SIDEBAR}px`;
   };

   setArrowDirection = () => {
      const arrow = document.getElementById(DOUBLE_ARROW);
      if (!arrow) return;
      const { sidebar } = this.props;
      arrow.style.transform = sidebar.collapse ? 'rotate(180deg)' : '';
   };

   toggleCollapse = () => {
      const { sidebar, setCollapse } = this.props;
      setCollapse(!sidebar.collapse);
   };

   handleModalSearch = () => {
      const { toggleModal } = this.props;

      toggleModal('ModalSearch', {});
   };

   handleMouseEnter = () => {
      const { setCollapse } = this.props;
      this.timeout = setTimeout(() => {
         setCollapse(false);
      }, 1200);
   };

   handleMouseMove = () => {
      const { setCollapse } = this.props;

      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
         setCollapse(false);
      }, 1200);
   };

   handleMouseLeave = () => {
      const { setCollapse } = this.props;
      clearTimeout(this.timeout);
      setCollapse(true);
   };

   render() {
      const { theme, sidebar } = this.props;
      const sidebarWidth = sidebar.collapse
         ? WIDTH_COLLAPSE_SIDEBAR
         : WIDTH_UNCOLLAPSE_SIDEBAR;

      return (
         <div
            className={cx(
               theme,
               styles.layout,
               sidebar.collapse ? '' : styles.expanded
            )}
            // onMouseEnter={this.handleMouseEnter}
            // onMouseLeave={this.handleMouseLeave}
            // onMouseMove={this.handleMouseMove}
         >
            <div className={cx(theme, styles.sidebar)}>
               <div className={styles.search}>
                  <IconSearch
                     className={cx(theme, styles.iconSearch)}
                     onClick={this.handleModalSearch}
                  />
               </div>
               <div className={styles.linksContainer}>
                  <SidebarList collapse={sidebar.collapse} {...this.props} />
               </div>
            </div>
         </div>
      );
   }
}
