import React from 'react';

import styles from './NateTimePicker.less';

const minuteSchedule = [
   '00',
   '05',
   '10',
   '15',
   '20',
   '25',
   '30',
   '35',
   '40',
   '45',
   '50',
   '55'
];

const hourSchedule = [
   '00',
   '01',
   '02',
   '03',
   '04',
   '05',
   '06',
   '07',
   '08',
   '09',
   '10',
   '11',
   '12',
   '13',
   '14',
   '15',
   '16',
   '17',
   '18',
   '19',
   '20',
   '21',
   '22',
   '23'
];

const CellTimeHourPicker = props => {
   const { time, setTime, timer, minHour } = props;

   const handleOnClick = currentTime => () => {
      setTime(parseInt(currentTime));
   };

   return timer.map(currentTime => {
      // console.log("time :", time);
      // console.log("currenTime: ", currentTime);
      const style =
         parseInt(time) === parseInt(currentTime)
            ? styles.boxTimeSelected
            : styles.boxTime;
      // console.log("MinMin :", minMin);
      // const min = parseInt(minHour, 10);
      // const current = parseInt(currentTime, 10);
      // console.log("Min :", min);
      // console.log("Current :", current);
      if (parseInt(currentTime, 10) < parseInt(minHour, 10)) return null;

      return (
         <div
            role="button"
            key={currentTime + minHour}
            className={style}
            onClick={handleOnClick(currentTime)}
         >
            <div>{currentTime}</div>
         </div>
      );
   });
};

const CellTimeMinutePicker = props => {
   const { time, setTime, timer, minHour, hour } = props;

   const handleOnClick = currentTime => () => {
      setTime(parseInt(currentTime));
   };

   // console.log("MinMin :", minMin);
   // console.log("Min :", min);
   return timer.map(currentTime => {
      // console.log("time :", time);
      // console.log("currenTime: ", currentTime);
      const style =
         parseInt(time) === parseInt(currentTime)
            ? styles.boxTimeSelected
            : styles.boxTime;
      // const min = parseInt(minMin, 10);
      // const current = parseInt(currentTime, 10);
      // console.log("Current :", current);
      if (hour === minHour && parseInt(minHour) > parseInt(currentTime))
         return null;
      // if (current < minMin) return null;

      return (
         <div
            role="button"
            key={minHour + currentTime}
            className={style}
            onClick={handleOnClick(currentTime)}
         >
            <div>{currentTime}</div>
         </div>
      );
   });
};

const getZero = value => {
   if (value < 10) return '0';
   return '';
};

export default function NateTimePicker({
   onChange,
   minutes,
   hours,
   minHour,
   minMin
}) {
   const [minute, setMinute] = React.useState(minutes || '00');
   const [hour, setHour] = React.useState(hours || '00');
   // const minHour = "10";
   // const minMin = "10";

   React.useEffect(() => {
      onChange(minute, hour);
   }, [hour, minute]);

   React.useEffect(() => {
      setHour(hours);
   }, [hours]);

   React.useEffect(() => {
      setMinute(minutes);
   }, [minutes]);

   // console.log('Hour: ', hour);
   // console.log('minute: ', minute);

   return (
      <div className={styles.layerTimePicker}>
         <div className={styles.displayTime}>
            {getZero(hour)}
            {hour}
            <span className={styles.spaceDisplayTime}>:</span>
            {getZero(minute)}
            {minute}
         </div>
         <div className={styles.sizeTimePicker}>
            <div className={styles.hourTimePicker}>
               <CellTimeHourPicker
                  time={hour}
                  setTime={setHour}
                  timer={hourSchedule}
                  minHour={minHour}
               />
            </div>
            <div className={styles.minuteTimePicker}>
               <CellTimeMinutePicker
                  time={minute}
                  setTime={setMinute}
                  timer={minuteSchedule}
                  minHour={minHour}
                  minMin={minMin}
                  hour={hour}
               />
            </div>
         </div>
      </div>
   );
}
