import _ from 'lodash';

import base from '../course.json';
import { Store, Enumeration } from '../factory';
import { ConvertibleSchema } from '../schemas';
import { CONVERTIBLE, DRAFT } from '../constants';

import {
   generateId,
   timestamp,
   parseUpdate,
   parseData,
   parseDoc,
   isNotFound
} from '../utils';

import {
   InvalidParameterFormat,
   InvalidDocumentFormat,
   ConvertibleNotFound,
   TypeNotFound,
   NoConvertibles
} from '../errors';

// TODO: method move

// ================================================ //
// ==================== Create ==================== //
// ================================================ //

function create(data) {
   return new Promise((ok, ko) => {
      const store = new Store();
      const mandatory = ['type'];
      const optionals = ['name', 'content', 'folderId'];

      parseData(data, mandatory, optionals)
         .then(parsed => {
            parsed._id = generateId();
            parsed.name = parsed.name || 'Untitled';
            parsed.content = parsed.content || base;
            parsed.folderId = parsed.folderId || '';
            parsed.createdAt = timestamp();
            parsed.updatedAt = 0;
            parsed.collection = CONVERTIBLE;
            return this._main.put(parsed);
         })
         .then(res => store.add('convertibleId', res.id))
         .then(id => this._main.get(id))
         .then(convertible => parseDoc(ConvertibleSchema, convertible))
         .then(convertible => parseType(convertible))
         .then(convertible => this._main.put(convertible))
         .then(res => this._main.get(res.id))
         .then(
            convertible =>
               new Promise((next, stop) => {
                  if (_.isEmpty(convertible.folderId)) return next();

                  this.folders
                     .findById(convertible.folderId)
                     .then(folder => {
                        folder.children.push({
                           collection: CONVERTIBLE,
                           id: convertible._id
                        });
                        return this._main.put(folder);
                     })
                     .then(folder => store.add('folder', folder))
                     .then(() => next())
                     .catch(stop);
               })
         )
         .then(() => this._main.get(store.convertibleId))
         .then(ok)
         .catch(
            err =>
               new Promise((next, spread) => {
                  if (!store.convertibleId) return spread(err);

                  this._main
                     .get(store.convertibleId)
                     .then(doc => this._main.remove(doc._id, doc._rev))
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .catch(
            err =>
               new Promise((next, spread) => {
                  if (!store.folder) return spread(err);

                  this._main
                     .get(store.folder._id)
                     .then(doc => {
                        _.remove(doc.children, { id: store.convertibleId });
                        return this._main.put(doc);
                     })
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .catch(ko);
   });
}

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
   return new Promise((ok, ko) => {
      this._main
         .find({ selector: { collection: CONVERTIBLE } })
         .then(res => {
            if (_.isEmpty(res.docs)) throw new NoConvertibles();

            ok(res.docs);
         })
         .catch(ko);
   });
}

function findById(id) {
   return new Promise((ok, ko) => {
      if (!_.isString(id))
         throw new InvalidParameterFormat(`The id parameter is not a String`);

      this._main
         .get(id)
         .then(doc => {
            if (doc.collection !== CONVERTIBLE)
               throw new ConvertibleNotFound(
                  `The document's id '${
                     doc._id
                  }' does not reference a convertible`
               );

            ok(doc);
         })
         .catch(err => {
            if (isNotFound(err))
               throw new ConvertibleNotFound(
                  `The convertible '${id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

function findByType(type) {
   return new Promise((ok, ko) => {
      parseType({ type })
         .then(() => this._main.find({ selector: { collection: CONVERTIBLE } }))
         .then(res => {
            if (_.isEmpty(res.docs)) throw new NoConvertibles();

            ok(
               _.compact(
                  _.map(res.docs, c => (c.type === type ? c : undefined))
               )
            );
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

// TODO: check if folderId is required when updating type (when other types will be set)
function update(doc) {
   return new Promise((ok, ko) => {
      const fields = ['folderId'];

      parseDoc(ConvertibleSchema, doc)
         .then(parsed => this._main.get(parsed._id))
         .then(old => parseUpdate(old, doc, fields))
         .then(convertible => parseType(convertible))
         .then(convertible => {
            convertible.updatedAt = timestamp();
            return this._main.put(convertible);
         })
         .then(res => this._main.get(res.id))
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               throw new ConvertibleNotFound(
                  `The convertible '${doc._id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Delete ==================== //
// ================================================ //

function remove(data) {
   return new Promise((ok, ko) => {
      const store = new Store();

      parseData(_.pick(data, ['_id']), ['_id'])
         .then(parsed => this._main.get(parsed._id))
         .then(convertible => store.add('convertible', convertible))
         .then(
            convertible =>
               new Promise((next, stop) => {
                  if (_.isEmpty(convertible.folderId)) return next();

                  this.folders
                     .findById(convertible.folderId)
                     .then(folder => {
                        _.remove(folder.children, { id: convertible._id });
                        return this._main.put(folder);
                     })
                     .then(() => next())
                     .catch(stop);
               })
         )
         .then(() =>
            this._main.remove(store.convertible._id, store.convertible._rev)
         )
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               throw new ConvertibleNotFound(
                  `The convertible '${data._id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

// ================================================== //
// ==================== Specific ==================== //
// ================================================== //

// TODO: move this to method
function transform(doc, data) {
   return new Promise((ok, ko) => {
      if (!_.isObject(doc))
         throw new InvalidDocumentFormat(`The document is not an Object`);

      if (doc.collection !== CONVERTIBLE)
         throw new ConvertibleNotFound(
            `The document's id '${doc._id}' does not reference a convertible`
         );

      const store = new Store();
      const mandatory = ['folderId'];
      const optionals = ['name'];

      parseDoc(ConvertibleSchema, doc)
         .then(() => parseData(data, mandatory, optionals))
         .then(parsed =>
            this.courses.create({
               name: parsed.name || doc.name,
               folderId: parsed.folderId,
               content: doc.content
            })
         )
         .then(course => {
            course.createdAt = doc.createdAt;
            course.updatedAt = timestamp();
            return this._main.put(course);
         })
         .then(res => store.add('courseId', res.id))
         .then(() => this.convertibles.remove(doc))
         .then(() => this._main.get(store.courseId))
         .then(ok)
         .catch(
            err =>
               new Promise((next, spread) => {
                  if (!store.courseId) return spread(err);

                  this.courses
                     .remove({ _id: store.courseId })
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .catch(ko);
   });
}

export const types = new Enumeration([DRAFT]);

export default function() {
   return {
      create: create.bind(this),
      findAll: findAll.bind(this),
      findById: findById.bind(this),
      findByType: findByType.bind(this),
      update: update.bind(this),
      remove: remove.bind(this),
      transform: transform.bind(this)
   };
}

// =============================================== //
// ==================== Utils ==================== //
// =============================================== //

function parseType(doc) {
   return new Promise(next => {
      if (!_.isObject(doc))
         throw new InvalidDocumentFormat(`The document is not an Object`);

      if (!_.isString(doc.type))
         throw new InvalidParameterFormat(
            `The document's type is not a String`
         );

      if (!types.includes(doc.type))
         throw new TypeNotFound(
            `The type '${
               doc.type
            }' could not be found. Check the available types in the type Enumeration in convertibles.`
         );

      next(doc);
   });
}
