import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SettingsPage from '../components/SettingsPage/SettingsPage';
import * as UserAction from '../actions/user';
import * as ModalAction from '../actions/modal';
import * as ThemeActions from '../actions/theme';
import * as LanguageActions from '../actions/language';

function mapStateToProps(state) {
   return {
      user: state.user,
      lang: state.language,
      theme: state.theme,
      db: state.database
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      { ...UserAction, ...ModalAction, ...ThemeActions, ...LanguageActions },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(SettingsPage);
