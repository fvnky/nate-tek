export interface IStyles {
   [key: string]: string;
}

export type Theme = (styles: IStyles, className: string) => string;

export type ThemeName = string;

export type SetThemeName = (themeName: string) => void;
