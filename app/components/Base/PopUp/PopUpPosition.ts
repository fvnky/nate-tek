// Constant
import {
   RIGHT,
   LEFT,
   TOP,
   TOP_RIGHT,
   TOP_LEFT,
   BOTTOM,
   BOTTOM_RIGHT,
   BOTTOM_LEFT,
   MIDDLE
} from '../../../constants/PopUp';

function leftPosition(trigger: ClientRect, popup: ClientRect, pos: string) {
   switch (pos) {
      case RIGHT:
      case TOP_RIGHT:
      case BOTTOM_RIGHT:
         return trigger.left;
      case TOP:
      case BOTTOM:
         return trigger.left - trigger.width / 2;
      case BOTTOM_LEFT:
      case LEFT:
      case TOP_LEFT:
         return trigger.left;
      case MIDDLE:
         return trigger.left + trigger.width / 2;
      default:
         return 0;
   }
}

function topPosition(trigger: ClientRect, popup: ClientRect, pos: string) {
   switch (pos) {
      case RIGHT:
      case LEFT:
         return trigger.top - popup.height / 2 + trigger.height / 2;
      case TOP:
      case TOP_RIGHT:
      case TOP_LEFT:
         return trigger.top - popup.height;
      case BOTTOM:
      case BOTTOM_RIGHT:
      case BOTTOM_LEFT:
         return trigger.top + trigger.height;
      case MIDDLE:
         return trigger.top + trigger.height / 2;
      default:
         return 0;
   }
}

function checkBorderLeft(
   placement: string,
   left: number,
   popup: ClientRect,
   triggerPopup: ClientRect
) {
   if (placement !== (LEFT && RIGHT)) {
      if (window.innerWidth < popup.width + left)
         left = window.innerWidth - popup.width;
      if (left < 0) left = 0;
   } else if (placement === RIGHT && window.innerWidth < popup.width + left)
      left = window.innerWidth - popup.width - triggerPopup.width;
   else if (left <= 0) left = triggerPopup.width;
   return left;
}

function checkBorderTop(
   placement: string,
   top: number,
   popup: ClientRect,
   triggerPopup: ClientRect
) {
   if (placement !== (TOP && BOTTOM)) {
      if (window.innerHeight <= top) top = window.innerHeight - popup.height;
      if (top < 0) top = 0;
   } else if (placement === BOTTOM && top >= window.innerHeight)
      top = window.innerHeight - popup.height - triggerPopup.height;
   else if (top <= 0) top = triggerPopup.height;
   return top;
}

function leftRelative(placement: string, triggerPopup: ClientRect, popup: ClientRect) {
   switch (placement) {
      case MIDDLE:
         return { left: triggerPopup.width / 2, top: - (triggerPopup.height / 2) };
      case BOTTOM_LEFT: {
         // console.log("BOTTOM_LEFT");
         return { left: -popup.width  + (triggerPopup.width / 2), top: 0} //- (triggerPopup.height / 2)};
      }
      default:
         return { left: 0, top: 0 };
   }
}

function renderPosition(placement: string, triggerPopup: any, popup: any) {
   triggerPopup = triggerPopup.getBoundingClientRect();
   // console.log(triggerPopup);
   popup = popup.getBoundingClientRect();
   // let left = leftPosition(triggerPopup, popup, placement);
   // let top = topPosition(triggerPopup, popup, placement);
   // left = checkBorderLeft(placement, left, popup, triggerPopup);
   // top = checkBorderTop(placement, top, popup, triggerPopup);
   // console.log(left, top)
   return  leftRelative(placement, triggerPopup, popup) ;
}

const setPosition = (placement: string, triggerPopup: any, popup: any) => {
   // const { placement } = this.props;
   // const triggerPopup = this.triggerPopup.current;
   // const popup = this.popup.current;
   if (popup && triggerPopup) {
      const pos = renderPosition(placement, triggerPopup, popup);
      // console.log("POS :", pos);
      popup.style.left = `${pos.left}px`;
      popup.style.top = `${pos.top}px`;
   }
};

function getTopLeftPosition(
   element: HTMLElement
): { left: number; top: number } {
   const pos = element.getBoundingClientRect();
   const left = pos.left;
   const top = pos.top;

   return { left, top };
}

export {
   setPosition,
   renderPosition,
   checkBorderTop,
   checkBorderLeft,
   topPosition,
   leftPosition,
   getTopLeftPosition
};
