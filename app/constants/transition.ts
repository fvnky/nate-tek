// Sidebar BEGIN
export const SELECTED_SIDEBAR_ITEM = 'SelectedSidebarItem';
export const DOUBLE_ARROW = 'DoubleArrow';
export const WIDTH_COLLAPSE_SIDEBAR = 68;
export const WIDTH_UNCOLLAPSE_SIDEBAR = 220;
// Sidebar END
