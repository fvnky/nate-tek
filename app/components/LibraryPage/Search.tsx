import React, { useEffect, useState } from 'react';
import Highlighter from 'react-highlight-words';
import cx from 'classnames';

// Utils
import { capitalize } from '../utils';

import routes from '../../constants/routes.json';

// Typescript
import { IMatch, ILanguage, IDatabase, ICourse } from '../../typescript';

import styles from './Search.less';
import IconSearch from '../../assets/icons/IconSearch';

interface IProps {
   setDoc: any;
   theme: string;
   match: IMatch;
   history: any;
   lang: ILanguage;
   db: IDatabase;
   all: any;
}

const Search: React.FC<IProps> = ({
   theme,
   lang,
   match,
   history,
   db,
   setDoc,
   all
}) => {
   // const [all, setAll] = React.useState([]);
   const [keyword, setKeyword] = React.useState('');

   // React.useEffect(() => {
   //    db.library
   //       .get()
   //       .then(({ docs }) => setAll(docs))
   //       .catch(err => {
   //          console.error(err);
   //       });
   // }, []);

   const onKeywordChange = e => {
      setKeyword(e.target.value);
   };

   const setDocToViewer = course => () => {
      db.library
         .course(course.user._id, course._id)
         .then(course => setDoc(course))
         .catch(err => {
            console.error(err);
         });
   };

   const results = [
      {
         match: 'name',
         results: all.filter(doc => {
            return doc.name.toLowerCase().includes(keyword.toLowerCase());
         })
      },
      {
         match: 'textContent',
         results: all
            .filter(doc => {
               return !doc.name.toLowerCase().includes(keyword.toLowerCase());
            })
            .filter(doc => {
               return doc.textContent
                  .toLowerCase()
                  .includes(keyword.toLowerCase());
            })
      }
   ];

   // console.log(keyword.split(/\s+/))
   if (!navigator.onLine) {
      return (
         <div className={styles.offline}>
            It seems you're not connected to the internet.
         </div>
      );
   }

   return (
      <div className={cx(theme, styles.layout)}>
         <div className={styles.inputLine}>
            <div>
               <IconSearch className={cx(theme, styles.icon)} />
            </div>
            <input
               placeholder="Rechercher "
               value={keyword}
               onChange={onKeywordChange}
               className={cx(theme, styles.searchInput)}
            />
         </div>

         <div className={cx(theme, styles.resultsContainer)}>
            {results.map(({ match, results }) => {
               return results.map(course => (
                  <div
                     className={cx(styles.row, theme)}
                     key={course._id}
                     onClick={setDocToViewer(course)}
                  >
                     <div className={styles.main}>
                        {/* <span className={cx(styles.subject, theme)}>
                           {course.user.curriculum}
                        </span> */}
                        <Highlighter
                           className={cx(styles.name, theme)}
                           highlightClassName={cx(styles.highlight, theme)}
                           searchWords={[keyword]}
                           autoEscape={true}
                           textToHighlight={course.name}
                        />
                        <span className={cx(styles.curriculum, theme)}>
                           {course.user.curriculum}
                        </span>
                        <span className={cx(styles.curriculum, theme)}>
                           ★ 1
                        </span>
                     </div>
                     {/* {match === "textContent" && <div className={styles.detail}>

                           <Highlighter
                              className={cx(styles.name, theme)}
                              highlightClassName={cx(styles.highlight, theme)}
                              searchWords={[keyword]}
                              autoEscape={true}
                              textToHighlight={course.textContent}
                           />

                        </div>} */}
                  </div>
               ));
            })}
         </div>
      </div>
   );
};

export default Search;
