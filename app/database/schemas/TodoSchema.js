import { NATE_TODO } from '../../constants/models/todo';

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      name: { type: String, required: true },
      content: { type: String, required: false },
      dueAt: { type: Number, required: true },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      status: { type: Boolean, required: false },
      collection: NATE_TODO
   }),
   fields: [
      '_id',
      '_rev',
      'name',
      'content',
      'dueAt',
      'createdAt',
      'updatedAt',
      'status',
      'collection'
   ]
};
