import * as React from 'react';
import cx from 'classnames';
import { RouteComponentProps } from 'react-router';

// Component
import SubjectPage from '../SubjectPage/SubjectsPage';
import FoldersPage from '../FolderPage/FoldersPage';
import DroppableItem from '../Base/DroppableItem';

// Utils
import { getTreeArray } from './getTreeArray';

// Constant
import routes from '../../constants/routes.json';
import { FOLDER, SUBJECT } from '../../database/constants';

// Style
import styles from './BrowserPage.less';

// typescript
import {
   ISubject,
   IFolder,
   ICourse,
   IMatch,
   ILanguage,
   ToggleModal,
   IQuiz,
   CreateCourse
} from '../../typescript';

interface IProps extends RouteComponentProps {
   subjects: ISubject[];
   folders: IFolder[];
   courses: ICourse[];
   quizs: IQuiz[];
   match: IMatch;
   lang: ILanguage;
   toggleModal: ToggleModal;
   theme: string;
   router: any;
   resourcesNavigation: { lastTab: string; lastBrowserPath: string };
   setLastActiveNavigationTab: (tab: string) => void;
   setLastActiveBrowserPath: (path: string) => void;
   createCourse : CreateCourse;
   droppedFiles? : any;

}

interface IState {
   tree: any[];
}

export default class BrowserPage extends React.Component<IProps, IState> {
   state: IState = {
      tree: []
   };

   static getDerivedStateFromProps(props: IProps) {
      const { history, folders, subjects, lang } = props;
      return {
         tree: getTreeArray(history.location.pathname, folders, subjects, lang)
      };
   }

   componentDidUpdate(prevProps: IProps, prevState: IState) {
      const { match } = this.props;

      const prevID = prevProps.match.params.FolderID
         ? prevProps.match.params.FolderID
         : prevProps.match.params.SubjectID;

      const { tree } = this.state;
      if (tree.length > 1 && prevID === tree[tree.length - 2]._id) {
         const title = document.getElementById(`title`);
         const lastParent = document.getElementById(`tree-${tree.length - 2}`);
         // setTimeout(() => {
         //    this.prepareInAnimation(title, lastParent);
         // }, 0);
         this.prepareInAnimation(title, lastParent);
         return;
      }
      // eslint-disable-next-line react/destructuring-assignment
      const curID = this.props.match.params.FolderID
         ? match.params.FolderID
         : match.params.SubjectID;
      // eslint-disable-next-line react/destructuring-assignment
      const foundIndex = prevState.tree.findIndex(
         (elem: any) => elem._id === curID
      );
      // eslint-disable-next-line react/destructuring-assignment
      if (
         prevState.tree.length > 1 &&
         foundIndex !== -1 &&
         foundIndex !== prevState.tree.length - 1
      ) {
         this.prepareOutAnimation(prevState.tree.length - foundIndex - 2);
      }
   }

   prepareOutAnimation = (offset: number) => {
      const { theme } = this.props;
      const title = document.getElementById(`title`);
      title.className = cx(theme, styles.parent);
      title.style.marginBottom = `${41 + offset * 22}px`;
      setTimeout(() => {
         this.triggerOutAnimation();
      }, 0);
   };

   triggerOutAnimation = () => {
      const { theme } = this.props;
      const title = document.getElementById(`title`);
      title.className = cx(theme, styles.title, styles.transition);
      title.style.marginBottom = '';
   };

   prepareInAnimation = (title: any, lastParent: any) => {
      const { theme } = this.props;
      title.className = cx(theme, styles.title, styles.titleHiddenBottom);
      lastParent.className = cx(theme, styles.title, styles.transition);
      setTimeout(() => {
         this.triggerInAnimation(title, lastParent);
      }, 0);
   };

   triggerInAnimation = (title: any, lastParent: any) => {
      const { theme } = this.props;
      title.className = cx(theme, styles.title, styles.transition);
      lastParent.className = cx(theme, styles.parent, styles.transition);
      setTimeout(() => {
         this.restoreDefaults(title, lastParent);
      }, 400);
   };

   restoreDefaults = (title: any, lastParent: any) => {
      const { theme } = this.props;
      title.className = cx(theme, styles.title);
      lastParent.className = cx(theme, styles.parent);
   };

   handleParentClick = (link: string) => () => {
      const { history } = this.props;
      history.push(link);
   };

   renderTree = () => {
      const { theme } = this.props;
      const { tree } = this.state;
      return tree.map((elem: any, index: number) => {
         if (elem) {
            const last = index === tree.length - 1 ? true : false;
            return (
               <DroppableItem
                  key={elem._id ? elem._id : 'subjects'}
                  data={{ parentId: elem._id, key: 'id' }}
               >
                  <div
                     role="button"
                     id={last ? `title` : `tree-${index}`}
                     className={cx(theme, last ? styles.title : styles.parent)}
                     style={{ opacity: elem.opacity }}
                     onClick={this.handleParentClick(elem.link)}
                  >
                     {elem.name}
                  </div>
               </DroppableItem>
            );
         }
         return <></>;
      });
   };

   renderPage = () => {
      const {
         match: {
            path,
            params: { SubjectID, FolderID }
         },
         droppedFiles
      } = this.props;
      const id = FolderID ? FolderID : SubjectID;
      const collection = FolderID ? FOLDER : SUBJECT;

      switch (path) {
         case routes.SUBJECTS:
            return <SubjectPage {...this.props} />;
         case routes.SUBJECT:
            return (
               <FoldersPage id={id} collection={collection} {...this.props} droppedFiles={droppedFiles} />
            );
         case routes.FOLDER:
            return (
               <FoldersPage id={id} collection={collection} {...this.props} droppedFiles={droppedFiles}/>
            );
         default:
            return <>NOTHING HERE -- BROWSER</>;
      }
   };

   render() {
      return (
         <>
            <div className={styles.headerContainer}>
               {this.renderTree()}
               {/* <div id="title" className={cx(theme, styles.title)}>
                     {name}
                  </div> */}
            </div>
            {this.renderPage()}
         </>
      );
   }
}
