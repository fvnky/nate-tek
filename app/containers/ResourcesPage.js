import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ResourcesPage from '../components/ResourcesPage/ResourcesPage.tsx';
import * as UserActions from '../actions/user';
import * as SubjectsActions from '../actions/subjects';
import * as FoldersActions from '../actions/folders';
import * as CoursesActions from '../actions/courses';
import * as ModalActions from '../actions/modal';
import * as ResourcesNavigationActions from '../actions/resourcesNavigation';
import * as QuizsActions from '../actions/quizs';

function mapStateToProps(state) {
   return {
      db: state.database,
      folders: state.folders,
      courses: state.courses,
      subjects: state.subjects,
      lang: state.language,
      theme: state.theme,
      router: state.router,
      resourcesNavigation: state.resourcesNavigation,
      quizs: state.quizs
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...UserActions,
         ...SubjectsActions,
         ...FoldersActions,
         ...CoursesActions,
         ...ModalActions,
         ...ResourcesNavigationActions,
         ...QuizsActions
      },
      dispatch
   );
}

export default withRouter(
   connect(
      mapStateToProps,
      mapDispatchToProps
   )(ResourcesPage)
);
