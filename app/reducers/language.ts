// import type { Action } from './types';

import { loadLanguage } from '../language/loadLanguage';
import { ILanguage } from '../typescript';
import { IActionLanguage } from './types';

export const LOAD_LANG = 'LOAD_LANG';

export function language(
   state: ILanguage = loadLanguage('en'),
   action: IActionLanguage
) {
   switch (action.type) {
      case LOAD_LANG:
         return loadLanguage(action.payload);
      default:
         return state;
   }
}
