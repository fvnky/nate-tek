// Constant
import { generateId } from '../../../database/utils';
import {
   QUESTION,
   FORM,
   TYPE_FORM,
   SCHEMA_ID
} from '../../../constants/models';
import {
   ANSWER_QCM,
   QCM,
   PROPOSITION_QCM
} from '../../../constants/models/questions/qcm';
import {
   ANSWER_Q_A,
   QUESTION_ANSWER
} from '../../../constants/models/questions/question_answer';
import {
   ANSWER_T_F,
   TRUE_FALSE
} from '../../../constants/models/questions/true_false';

function TrueOrFalse() {
   return {
      [FORM]: {
         [QUESTION]: '',
         [ANSWER_T_F]: false
      },
      [TYPE_FORM]: TRUE_FALSE,
      [SCHEMA_ID]: generateId()
   };
}

function Qcm() {
   return {
      [FORM]: {
         [QUESTION]: '',
         [ANSWER_QCM]: '',
         [PROPOSITION_QCM]: {}
      },
      [TYPE_FORM]: QCM,
      [SCHEMA_ID]: generateId()
   };
}

function QuestionAnswer() {
   return {
      [FORM]: {
         [QUESTION]: '',
         [ANSWER_Q_A]: ''
      },
      [TYPE_FORM]: QUESTION_ANSWER,
      [SCHEMA_ID]: generateId()
   };
}

export default {
   [TRUE_FALSE]: TrueOrFalse,
   [QCM]: Qcm,
   [QUESTION_ANSWER]: QuestionAnswer
};
