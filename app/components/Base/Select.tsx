import * as React from "react";
import cx from "classnames";
import Select from "react-select";
import withTheme from '../withTheme'

// Style
import styles from "./Select.less";

interface IProps {
   placeholder?: string;
   options: Array<{value: string, label: string}>;
   theme: string;
   border? : boolean;
}

interface IState {}

// eslint-disable-next-line typescript/class-name-casing
class _Select extends React.Component<IProps, IState> {
   render() {
      const { theme, options, placeholder, border = true  } = this.props;

      return (
            <Select
            {...this.props}
            className={cx(styles.select, !border ? styles.withoutBorder : "")}
            options={options}
            placeholder={placeholder}
            classNamePrefix="react-select"
         />
      );
   }
}

export default withTheme(_Select);
