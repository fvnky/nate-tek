import { GetState, Dispatch } from '../reducers/types';
import { IFolder } from '../typescript';

function ExceptionFolder(error: any) {
   return error;
}

export function loadFolders() {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.folders.findAll()
         .then(() => {})
         .catch((err: any) => console.error(err.stack));
   };
}

export function createFolder(newFolder: {
   name: string;
   parent: { id: string; collection: string };
}) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.folders.create(newFolder)
         .then(() => {})
         .catch((err: any) => {
            console.log(err.stack);
            throw ExceptionFolder(err);
         });
}

export function updateFolder(newFolder: IFolder) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.folders.update(newFolder)
         .then(() => {})
         .catch((err: any) => {
            console.log(err.stack);
            throw ExceptionFolder(err);
         });
}

export function removeFolder(toRemove: IFolder) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.folders.remove(toRemove)
         .then(() => {})
         .catch((err: any) => {
            console.log(err.stack);
            throw ExceptionFolder(err);
         });
}
