import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BetaFeedbackPage from '../components/BetaFeedbackPage/BetaFeedbackPage';
import * as UserActions from '../actions/user';

function mapStateToProps(state) {
   return {
      db: state.database
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...UserActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(BetaFeedbackPage);
