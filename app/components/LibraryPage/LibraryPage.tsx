import * as React from 'react';
import cx from 'classnames';
import { Route, Switch } from 'react-router';

import { RouteComponentProps } from 'react-router';
import LogoNate from '../../assets/icons/LogoNate';
import LogoNateOutline from '../../assets/icons/LogoNateOutline';
// const LogoNate2 = require("../../assets/n.svg");

// constant
import routes from '../../constants/routes.json';

import styles from './LibraryPage.less';

// typescript
import {
   ISubject,
   IFolder,
   ICourse,
   IMatch,
   ILanguage,
   ToggleModal,
   IDatabase
} from '../../typescript';
import Sections from './Sections';
import Search from './Search';
import { string } from 'prop-types';
import DocViewer from './DocViewer';
import CourseCard, { CourseContainer } from '../CourseCard/CourseCard';
import Spinner from '../Base/Spinner';

interface IProps extends RouteComponentProps {
   subjects: ISubject[];
   folders: IFolder[];
   courses: ICourse[];
   match: IMatch;
   lang: ILanguage;
   toggleModal: ToggleModal;
   theme: string;
   router: any;
   createDraftCourse: any;
   loadSubjects: any;
   db: IDatabase;
}

interface IState {
   all: any;
   doc: any;
   loading: boolean;
}

export default class ResourcesPage extends React.Component<IProps, IState> {
   constructor(props) {
      super(props);
      this.state = {
         all: [],
         doc: null,
         loading: false
      };
   }

   componentDidMount() {
      this.loadAll();
   }

   loadAll = () => {
      const { db } = this.props;
      this.setState({ loading: true });
      db.library
         .get()
         .then(({ docs }) => this.setState({ all: docs, loading: false }))
         .catch(err => {
            this.setState({ loading: false });
            console.error(err);
         });
   };

   onClickCourse = c => {
      const { db } = this.props;
      db.library
         .course(c.user._id, c._id)
         .then(doc => {
            console.warn('got course from library:', doc);
            this.setDoc(doc);
         })
         .catch(err => {
            console.error(err);
         });
   };

   setDoc = doc => {
      this.setState({ doc });
   };

   renderLoader = () => {
      if (this.state.loading)
         return <Spinner className={styles.spinnerContainer} />;
      else return null;
   };

   renderCourses = () => {
      const { theme, toggleModal, history } = this.props;
      const { all } = this.state;

      if (!this.state.loading && !this.state.doc)
         return (
            <CourseContainer>
               {all
                  .filter(c => c.coverImage && c.coverImage.full)
                  .map(c => (
                     <CourseCard
                        course={c}
                        history={history}
                        toggleModal={toggleModal}
                        onClickCourse={this.onClickCourse}
                     />
                  ))}
            </CourseContainer>
         );
      else return null;
   };

   render() {
      const { theme, toggleModal, history } = this.props;
      const { all } = this.state;
      console.log(all);
      if (this.state.doc)
         return (
            <DocViewer
               {...this.props}
               doc={this.state.doc}
               setDoc={this.setDoc}
               toggleModal={toggleModal}
            />
         );
      return (
         <div className={cx(theme, styles.layout)}>
            <div className={cx(theme, styles.title)} onClick={this.loadAll}>
               <LogoNateOutline className={cx(theme, styles.logo)} />
               <div className={cx(theme, styles.library)}>Library</div>
            </div>
            <div className={styles.content}>
               <Sections {...this.props} />
               {this.renderLoader()}
               {this.renderCourses()}
            </div>
         </div>
      );
   }
}
