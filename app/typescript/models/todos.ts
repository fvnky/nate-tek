import { CREATE, FIND_BY_ID, UPDATE, REMOVE } from '../../constants/models';

export interface ITodo {
   _id: string;
   _rev: string;
   content: string;
   dueAt: number;
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   status?: boolean;
   name: string;
   is_done: true;
}

export type UpdateTodo = (toUpdate: ITodo) => Promise<ITodo>;
export type RemoveTodo = (toRemove: ITodo) => Promise<ITodo>;
export type FindTodoById = (id: string) => Promise<ITodo>;
export type CreateTodo = (newTodo: {}) => Promise<ITodo>;

export type IDatabaseTodo = {
   [FIND_BY_ID]: FindTodoById;
   [CREATE]: CreateTodo;
   [UPDATE]: UpdateTodo;
   [REMOVE]: RemoveTodo;
};
