
const monthNamesEn = [
   "January", "February", "March",
   "April", "May", "June", "July",
   "August", "September", "October",
   "November", "December"
];

const monthNamesFr = [
   "Janvier", "Février", "Mars",
   "Avril", "Mai", "Juin", "Juillet",
   "Aout", "Septembre", "Octobre",
   "Novembre", "Décember"
];

export const dateCalendarEvent = (event) => {
   // const { event } = props;
   // if (!event) return "";
   const { date } = event;
   const time = new Date(date);

   const day = time.getDate();
   const monthIndex = time.getMonth();
   const year = time.getFullYear();

   const res = "" + day + " " + monthNamesEn[monthIndex] + " " + year + "";
   // console.log("res :", res);
   return res;
}


export const getName = (array: any[], str) => {
   let search = '';
   array.forEach(elem => {
      const { name, _id } = elem;
      if (_id === str) search = name;
   });
   return search;
};

export const getSubjectId = (id, subjects) => {
   if (!id || id === '') return subjects[0]._id || '';
   return id;
};
