import * as React from "react";

const IconSettings = props => (
   <svg viewBox="0 0 100 100" {...props}>
      <g>
         <path
            d="M50,37.4c-7.2,0-13,5.8-13,13s5.8,13,13,13s13-5.8,13-13S57.2,37.4,50,37.4z M50,59.4c-5,0-9-4-9-9s4-9,9-9s9,4,9,9
		S55,59.4,50,59.4z"
         />
         <path
            d="M83.1,42.3L77.9,41c-0.4-1.2-0.9-2.4-1.4-3.5l2.7-4.6c0.1-0.1,0.2-0.3,0.2-0.4c1.1-1.7,0.9-4.1-0.6-5.6l-5.3-5.3
		c-0.8-0.8-1.8-1.2-2.9-1.2c-0.9,0-1.9,0.3-3,0.9L63,24c-1.1-0.6-2.3-1-3.5-1.5l-1.3-5.3c0-0.1-0.1-0.2-0.1-0.3
		c-0.4-2.1-2.2-3.6-4.4-3.6h-7.5c-2.1,0-3.7,1.4-4.4,3.9l-1.3,5.2c-1.2,0.4-2.4,0.9-3.5,1.4L32.3,21c-0.1-0.1-0.2-0.1-0.3-0.2
		c-0.7-0.5-1.6-0.7-2.4-0.7c-1.2,0-2.3,0.4-3.2,1.3l-5.3,5.3c-1.5,1.5-1.6,3.6-0.3,5.9l2.7,4.6c-0.6,1.1-1,2.3-1.5,3.5l-5,1.5
		l-0.2,0.1c-0.1,0-0.1,0-0.2,0.1c-2.1,0.4-3.5,2.2-3.5,4.4v7.5c0,2.1,1.5,3.7,3.9,4.4l5.2,1.3c0.4,1.2,0.9,2.3,1.4,3.5l-2.8,4.7
		l-0.1,0.3c-1.2,1.8-0.9,4.1,0.6,5.6l5.3,5.3c0.8,0.8,1.8,1.2,2.9,1.2c0.9,0,1.9-0.3,3-0.9l4.6-2.7c1.1,0.6,2.3,1,3.5,1.5l1.3,5.2
		l0.1,0.4c0.4,2.1,2.2,3.6,4.4,3.6h7.5c2.1,0,3.7-1.4,4.4-3.9l1.3-5.2c1.2-0.4,2.4-0.9,3.5-1.4l4.7,2.8c0.1,0.1,0.2,0.1,0.3,0.2
		c0.7,0.5,1.6,0.7,2.4,0.7c1.2,0,2.3-0.5,3.2-1.3l5.3-5.3c0.7-0.7,2.1-2.7,0.3-5.9l-2.9-5c0.6-1.1,1-2.3,1.5-3.5l5.3-1.3
		c0.1,0,0.2-0.1,0.3-0.1c2.1-0.4,3.5-2.2,3.5-4.4v-7.5C87,44.6,85.6,43,83.1,42.3z M83,54.1c0,0.2-0.1,0.4-0.4,0.4h-0.2l-0.2,0.1
		h-0.1l-5.2,1.3l-2.1,0.5l-0.7,2c-0.4,1-0.8,2-1.3,3l-1,2l1.1,1.9l2.7,4.6c0.3,0.6,0.4,1,0.3,1.1l-5.2,5.4c-0.1,0.1-0.2,0.1-0.3,0.1
		c-0.1,0-0.2,0-0.3-0.1l-0.2-0.1l-0.2-0.1h-0.1l0,0L65,73.4l-1.9-1.1l-1.9,1c-1,0.5-2,0.9-3,1.2l-2.1,0.7l-0.5,2.1l-1.4,5.1
		c-0.2,0.7-0.5,0.9-0.5,1h-7.5c-0.2,0-0.4-0.1-0.5-0.4v-0.2l-0.1-0.2l0-0.2l-1.3-5.1l-0.5-2.1l-2-0.7c-1-0.4-2-0.8-3-1.3l-1.9-1
		L35,73.3L30.4,76c-0.6,0.3-0.9,0.3-1,0.3l0,0l0,0l0,0L24,71.1c-0.2-0.2-0.2-0.4-0.1-0.6l0.1-0.2l0.1-0.2l0.1-0.2l2.8-4.5l1.1-1.9
		l-0.9-1.9c-0.5-1-0.9-2-1.2-3l-0.7-2.1L23.2,56L18,54.6c-0.7-0.2-0.9-0.4-1-0.5v-7.5c0-0.2,0.1-0.4,0.4-0.4h0.2l0.2-0.1h0.1H18
		l5.1-1.3l2.1-0.5l0.7-2c0.4-1,0.8-2,1.3-3l1-1.9l-1.1-1.9l-2.7-4.6c-0.3-0.6-0.3-1-0.3-1.1l5.3-5.3c0.1-0.1,0.2-0.1,0.3-0.1
		c0.1,0,0.2,0,0.2,0.1l0.2,0.1l0.2,0.1c0,0,0.1,0,0.1,0.1l4.6,2.6l1.9,1.1l1.9-1c1-0.5,2-0.9,3-1.2l2.1-0.7l0.5-2.1l1.3-5.1
		c0.2-0.7,0.5-0.9,0.5-1h7.5c0.2,0,0.4,0.1,0.5,0.4V18l0.1,0.2v0.1l1.3,5.2l0.5,2.1l2,0.7c1,0.4,2,0.8,3,1.3l2,1l1.9-1.1l4.6-2.7
		c0.6-0.3,0.9-0.3,1-0.3l0,0l0,0l0,0l5.3,5.3c0.1,0.1,0.2,0.4,0.1,0.6l-0.1,0.2l-0.1,0.2l0,0v0.1V31L73,35.4l-1.1,1.9l1,1.9
		c0.5,1,0.9,2,1.2,3l0.7,2.1l2.1,0.5l5.2,1.3c0.7,0.2,0.9,0.4,1,0.5L83,54.1L83,54.1z"
         />
      </g>
   </svg>
);

export default IconSettings;
