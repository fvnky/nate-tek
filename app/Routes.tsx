import * as React from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router';
import { connect } from 'react-redux';
import routes from './constants/routes.json';
import App from './containers/App';
import BetaFeedbackPage from './containers/BetaFeedbackPage';
import Home from './containers/Home';
import Spinner from './components/Base/Spinner';
import LibraryPage from './containers/LibraryPage';
import { PageNotFound } from './components/PageNotFound';
import DevPage from './containers/DevPage';
import EditorPagePhony from './containers/EditorPagePhony';
import EditorPage from './containers/EditorPage';
import Toast from './containers/Toast';
import ModalManager from './containers/ModalManager';
import SettingsPage from './containers/SettingsPage';
import ResourcesPage from './containers/ResourcesPage';
import ProfilePage from './containers/ProfilePage';
import LoginRegisterPage from './containers/LoginRegisterPage';
import QuizPage from './containers/QuizPage';
import Layout from './Layout';
import Content from './Content';
import Sidebar from './containers/Sidebar';
import CalendarPage from './containers/CalendarPage';

function _ProtectedRoutes({ children, ...rest }) {
   // console.warn("user", rest.user)

   if (!rest.user._id) {
      console.log('======= wil redirect');
      return <Redirect to={{ pathname: routes.LOGIN }} />;
   }
   return <React.Fragment>{children}</React.Fragment>;
}

function mapStateToProps(state) {
   return {
      user: state.user
   };
}

const ProtectedRoutes = connect(mapStateToProps)(_ProtectedRoutes);

// const _ProtectedRoute = ({ component: Component , ...rest })  => {
//    return (
//       <Route
//       {...rest}
//       render={props => {
//          if (rest.user._id) {
//             return <Component {...props} />
//          } else {
//             return (<Redirect to={{ pathname: routes.LOGIN }} />)
//          }
//       }}
//       />
//    )
// }

// function mapStateToProps(state) {
//    return {
//       user: state.user
//    };
// }

// const ProtectedRoutes = connect(mapStateToProps)(_ProtectedRoute)

export default () => (
   <App>
      <ModalManager />
      {/* <Spinner /> */}

      <Route exact={true} path={routes.LOGIN} component={LoginRegisterPage} />

      {/* <Route exact={true} path={routes.REGISTER} component={LoginRegisterPage} /> */}
      <ProtectedRoutes>
         <Toast />
         <Layout>
            <Sidebar />
            <Content>
               <EditorPage />
               <Switch>
{/*                   <Route
                     exact={true}
                     path={routes.SUBJECTS}
                     component={CalendarPage}
                  /> */}
                  <Route
                     exact={true}
                     path={routes.SUBJECTS}
                     component={ResourcesPage}
                  />
                  <Route path={routes.LIBRARY} component={LibraryPage} />
                  {/* <Route
                     exact={true}
                     path={routes.LIBRARY_SEARCH}
                     component={LibraryPage}
                  />
                  <Route
                     exact={true}
                     path={routes.LIBRARY_RECOMMENDED}
                     component={LibraryPage}
                  /> */}
                  {/* <Route exact={true} path={routes.SUBJECTS} component={Home} /> */}
                  <Route path={routes.QUIZ} component={QuizPage} />
                  <Route
                     exact={true}
                     path={routes.RESOURCES}
                     component={ResourcesPage}
                  />
                  <Route
                     exact={true}
                     path={routes.DOC_VIEWER}
                     component={ResourcesPage}
                  />
                  <Route
                     exact={true}
                     path={routes.SUBJECT}
                     component={ResourcesPage}
                  />
                  <Route
                     exact={true}
                     path={routes.SUBJECTS}
                     component={ResourcesPage}
                  />
                  <Route
                     exact={true}
                     path={routes.FOLDER}
                     component={ResourcesPage}
                  />
                  <Route
                     exact={true}
                     path={routes.RECENT}
                     component={ResourcesPage}
                  />
                  <Route
                     exact={true}
                     path={routes.DRAFTS}
                     component={ResourcesPage}
                  />
                  <Route
                     exact={true}
                     path={routes.EDITOR}
                     component={EditorPagePhony}
                  />
                  <Route
                     exact={true}
                     path={routes.EDITOR_LINK}
                     component={EditorPagePhony}
                  />
                  <Route path={routes.PROFILE} component={ProfilePage} />
                  <Route
                     exact={true}
                     path={routes.SETTINGS}
                     component={SettingsPage}
                  />
                  <Route
                     exact={true}
                     path={routes.CALENDAR}
                     component={CalendarPage}
                  />
                  <Route
                     exact={true}
                     path={routes.BETA_FEEDBACK}
                     component={BetaFeedbackPage}
                  />
                  <Route exact={true} path={routes.DEV} component={DevPage} />
                  <Route component={PageNotFound} />
               </Switch>
            </Content>
         </Layout>
      </ProtectedRoutes>
   </App>
);
