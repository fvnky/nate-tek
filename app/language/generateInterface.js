/* eslint-disable */
// @ts-ignore
const TEXT = require('./text.json');

const fs = require('fs');

const file = './app/typescript/language.ts';

function interfaceGenerator() {
   let str = 'export interface ILanguage {\n';
   Object.keys(TEXT).forEach(element => {
      str += `${element}: string;\n`;
   });
   str += '}\n';
   return str;
}

fs.writeFile(file, interfaceGenerator(), function(err) {
   if (err) {console.log(err); throw err};
   console.log('Create New interface ILanguage !');
});
