import React from "react";
import { isKeyHotkey } from "is-hotkey";

import { getHighestSelectedBlocks } from '../utils';

import { DEFAULT_NODE } from "../../constants";

export const ORDERED_LIST = "ordered-list";
export const UNORDERED_LIST = "unordered-list";
export const LIST_ITEM = "list-item";

function getType(listType, indent) {
   if (listType === UNORDERED_LIST) {
      if (indent % 3 === 0)
         return ("disc")
      if (indent % 3 === 1)
         return ("circle")
      if (indent % 3 === 2)
         return ("square")
      return ("disc")
   }

   if (listType === ORDERED_LIST) {
      if (indent % 2 === 0)
         return ("1")
      if (indent % 2 === 1)
         return ("a")
      return ("1")
   }

}

export default function list(options = {}) {
   return {
      commands: {
         setList(editor, type) {
            const isList = editor.value.blocks.some(node => node.type === LIST_ITEM);
            const isType = editor.value.blocks.some(block => !!editor.value.document.getClosest(block.key, parent => parent.type === type))

            if (isList && isType) {
               editor
                  .setBlocks(DEFAULT_NODE)
                  .unwrapBlock(UNORDERED_LIST)
                  .unwrapBlock(ORDERED_LIST)
            } else if (isList) {
               editor
                  // .setBlocks(type)
                  .unwrapBlock(type === UNORDERED_LIST ? ORDERED_LIST : UNORDERED_LIST)
                  .wrapBlock(type)
            } else {
               editor.setBlocks({ type: LIST_ITEM, data: { indent: 0 } })
                  .wrapBlock(type)
            }
         },
         indentList(editor) {
            const selectedBlocks = getHighestSelectedBlocks(editor);
            if (selectedBlocks.every(b => b.type === LIST_ITEM)) {
               selectedBlocks.map(b => {
                  let indent = b.data.get("indent") + 1;
                  indent = indent > 10 ? 10 : indent;
                  return editor.setNodeByKey(b.key, { data: b.data.merge({ indent }) })
               });
            }
         },
         outdentList(editor) {
            const selectedBlocks = getHighestSelectedBlocks(editor);
            if (selectedBlocks.every(b => b.type === LIST_ITEM)) {
               selectedBlocks.map(b => {
                  let indent = b.data.get("indent") - 1;
                  indent = indent < 0 ? 0 : indent;
                  return editor.setNodeByKey(b.key, { data: b.data.merge({ indent }) })
               });
            }
         }
      },
      queries: {
         isUnorderedList(editor) {
            const isList = editor.value.blocks.some(node => node.type === LIST_ITEM )
            if (isList) {
               return editor.value.blocks.some(block => !!editor.value.document.getClosest(block.key, parent => parent.type === UNORDERED_LIST))
            }
            return false;
         },
         isOrderedList(editor) {
            const isList = editor.value.blocks.some(node => node.type === LIST_ITEM )
            if (isList) {
               return editor.value.blocks.some(block => !!editor.value.document.getClosest(block.key, parent => parent.type === ORDERED_LIST))
            }
            return false;         }
      },
      onKeyDown(event, editor, next) {
         const isList = editor.value.blocks.some(node => node.type === LIST_ITEM);
         if (isList) {
            if (editor.value.selection.anchor.offset === 0 && editor.value.fragment.text === "" && (event.key === "Backspace" || event.key === "Enter")) {
               const type = editor.value.blocks.some(block => !!editor.value.document.getClosest(block.key, parent => parent.type === ORDERED_LIST)) ? ORDERED_LIST : UNORDERED_LIST;
               editor
                  .setBlocks(DEFAULT_NODE)
                  .unwrapBlock(type)
               return;
            }
            if (isKeyHotkey("shift+tab")(event)) {
               event.preventDefault();
               editor.outdentList();
               return;
            }
            if (event.key === "Tab") {
               event.preventDefault();
               editor.indentList();
               return;
            }
         }
         return next();
      },
      renderNode(props, editor, next) {
         const { attributes, children, node } = props;

         switch (node.type) {
            case UNORDERED_LIST:
               return <ul  {...attributes}>{children}</ul>;
            case ORDERED_LIST:
               return <ol {...attributes}>{children}</ol>;
            case LIST_ITEM: {
               const indent = node.data.get("indent");
               const type = getType(editor.value.document.getParent(node.key).type , indent);

               return <li type={type} style={{ marginLeft: `${indent * 50}px` }} {...attributes}>{children}</li>;
            }
            default:
               return next();
         }
      }
   };
}
