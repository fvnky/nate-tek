import Database from '../database';
import { IDatabase } from '../typescript';
import { IActionDatabase } from './types';

export const INIT_DB = 'init_db';

export function database(
   state: IDatabase = new Database(),
   action: IActionDatabase
) {
   switch (action.type) {
      case INIT_DB:
         state.init(action.payload);
         return state;
      default:
         return state;
   }
}
