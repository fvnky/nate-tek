#!/usr/bin/env python
from __future__ import print_function
import os

path = '../NounIcons'

files = os.listdir(path)

f = open("IconsSubject.jsx","w+")
f.write("import * as React from 'react';\n")
f.write("import IconQuestionMark from './IconQuestionMark';\n")

for fullName in files:
   name = os.path.splitext(fullName)[0]
   f.write("import " + name + " from '../NounIcons/" + name + "'\n")

f.write("\nconst iconsSubject = {\n")
f.write("   IconQuestionMark,\n")
for fullName in files:
   name = os.path.splitext(fullName)[0]
   f.write("   " + name + ",\n")

f.write("};\n")
f.write("\nexport { iconsSubject };\n")
