import { Value } from 'slate'

const initialValue = Value.fromJSON({
   document: {
     nodes: [
       {
         object: 'block',
         type: 'paragraph',
         nodes: [
           {
             object: 'text',
             leaves: [
               {
                 text: 'DEFAULT VALUE : This text shall not appear anywhere',
               },
             ],
           },
         ],
       },
     ],
   },
 });

 export default initialValue
