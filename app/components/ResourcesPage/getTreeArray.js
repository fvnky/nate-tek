import routes from '../../constants/routes.json';
import { FOLDER, SUBJECT } from '../../database/constants';

function getFolderOrSubject(id, folders, subjects) {
   const folder = folders.find(x => x._id === id);
   const subject = subjects.find(x => x._id === id);

   if (folder) return folder;
   if (subject) return subject;
   return null;
}

function addLinksAndSubject(array, lang) {
   let newArray = array.map((el, index) => {
      let link;
      if (el.collection === SUBJECT) {
         link = `${routes.SUBJECTS}/${el._id}`;
      } else if (el.collection === FOLDER) {
         link = `${routes.RESOURCES}${routes.FOLDERS}/${el._id}`;
      }
      el.opacity = 1 - index / 5;
      el.opacity = el.opacity < 0.3 ? 0.3 : el.opacity;
      el.link = link;
      return el;
   });
   let lastOpacity = 1 - array.length / 5;
   lastOpacity = lastOpacity < 0.3 ? 0.3 : lastOpacity;
   newArray.push({
      name: lang.subjects,
      opacity: lastOpacity,
      link: routes.SUBJECTS,
   });
   newArray = newArray.reverse();
   return newArray;
}

export function getTreeArray(pathname, folders, subjects, lang) {
   const splitPath = pathname.split('/');
   const array = [];
   if (splitPath.length > 0) {
      const id = splitPath[splitPath.length - 1];
      let last = getFolderOrSubject(id, folders, subjects);
      while (last && last.parent) {
         array.push(last);
         last = getFolderOrSubject(last.parent.id, folders, subjects);
      }
      array.push(last);
      if (array.length === 1 && !array[0]) return addLinksAndSubject([], lang);
      return addLinksAndSubject(array, lang);
   }
   return null;
}
