import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Home from '../components/Home';
// import * as QuizsActions from '../actions/quizs';

function mapStateToProps(state) {
   return {
      lang: state.language,
      theme: state.theme,
      quizs: state.quizs,
      subjects: state.subjects,
      folders: state.folders,
      dictionaries: state.dictionary,
      db: state.database
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         // ...QuizsActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(Home);
