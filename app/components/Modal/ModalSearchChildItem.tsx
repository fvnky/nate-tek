import * as React from 'react';
import cx from 'classnames';
import { RouteComponentProps } from 'react-router';
import { History } from 'history';

// Component
import PopUp, {
   PopUpElements,
   PopUpText,
   PopUpIcon,
   PopUpElement
} from '../Base/PopUp/PopUp';

import DraggableItem from '../Base/DraggableItem';
import DroppableItem from '../Base/DroppableItem';

// Utils
import { capitalize } from '../utils';
import { getName } from './ModalCalendar/utils';
import { timeDifference } from '../utils';

// Constant
import routes from '../../constants/routes.json';
import { FOLDER, QUIZ } from '../../database/constants';

// Icon
import IconCourse from '../../assets/icons/IconCourse';
import IconDelete from '../../assets/icons/IconDelete';
import IconEdit from '../../assets/icons/IconEdit';
import IconFolder from '../../assets/icons/IconFolder';
import IconMore from '../../assets/icons/IconMoreVertical';

// Style
import styles from './ModalSearchChildItem.less';

// typescript
import {
   ICourse,
   IFolder,
   ToggleModal,
   ILanguage,
   IMatch,
   IQuiz,
   ISubject
} from '../../typescript';
import { COURSE } from '../../constants/models';
import IconQuizz from '../../assets/icons/IconQuizz';

interface IProps extends RouteComponentProps {
   item: ICourse | IFolder | IQuiz;
   hovered: boolean;
   match: IMatch;
   toggleModal: ToggleModal;
   handleDisplayModal: () => void;
   history: History;
   lang: ILanguage;
   theme: string;
   folders: IFolder[];
   subjects: ISubject[];
}

interface IState {
   isDropdownOpened: boolean;
}

export default class ChildItem extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);
      this.state = {
         isDropdownOpened: false
      };
   }

   onVisibleChange = (display: boolean) => {
      this.setState({ isDropdownOpened: display });
   };

   handleClickOnMore = () => {
      const { isDropdownOpened } = this.state;

      this.setState({ isDropdownOpened: !isDropdownOpened });
   };

   handleOnClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      e.stopPropagation();
      const { hovered, item } = this.props;

      if (!hovered) return;

      const { history, handleDisplayModal } = this.props;

      const { collection, _id } = item;
      const folderLink = `${routes.RESOURCES}${routes.FOLDERS}/${_id}`;
      const courseLink = `${routes.EDITOR_LINK}/${_id}`;
      const quizLink = `${routes.QUIZ_LINK}/${_id}`;
      let link = '/';
      if (collection === FOLDER) link = folderLink;
      else if (collection === QUIZ) link = quizLink;
      else link = courseLink;
      history.push(link);
      handleDisplayModal();
   };

   handleMenuClick = (modalName: string) => () => {
      const { toggleModal, item } = this.props;
      toggleModal(modalName, { id: item._id });
   };

   renderMoreButton = () => {
      return <></>;
   };

   getIcon = collection => {
      switch (collection) {
         case FOLDER:
            return IconFolder;
         case COURSE:
            return IconCourse;
         case QUIZ:
            return IconQuizz;
         default:
            return IconCourse;
      }
   };

   render() {
      const { item, theme, hovered, subjects, folders } = this.props;
      const { isDropdownOpened } = this.state;
      const {
         collection,
         name,
         parent: { id },
         updatedAt
      } = item; // TODO Change icons to all possible icons
      // const ICON = collection === FOLDER ? IconFolder : IconCourse;
      const ICON = this.getIcon(collection);
      console.log('Name :', name);
      console.log('Parent :', id);
      console.log('Subjects: ', subjects);
      console.log('Array: ', [...subjects, ...folders]);
      console.log('Date :', timeDifference(new Date().getTime(), updatedAt));
      let value = (
         <div
            role="button"
            className={styles.iconAndNameContainer}
            onClick={this.handleOnClick}
         >
            <ICON className={styles.icon} />
            <span className={styles.name}>{name}</span>
         </div>
      );
      // value =
      //* </DraggableItem> */
      // <DraggableItem data={{ id: item._id }}>
      // collection === FOLDER ? (
      //    <DroppableItem data={{ parentId: item._id, key: 'id' }}>
      //       {value}
      //    </DroppableItem>
      // ) : (
      //    value
      // );

      return (
         <div className={cx(theme, styles.containerChildItem)}>
            {(hovered || isDropdownOpened) && this.renderMoreButton()}
            {value}
            <div className={styles.iconAndNameContainer}>
               {getName([...subjects, ...folders], id)}
            </div>
            <div className={styles.iconAndNameContainer}>
               {timeDifference(new Date().getTime(), updatedAt)}
            </div>
         </div>
      );
   }
}
