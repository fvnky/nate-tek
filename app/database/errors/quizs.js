import { CustomError } from '../factory';

export class QuizAlreadyExists extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The quiz already exists';
   }
}

export class QuizNotFound extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The quiz could not be found';
   }
}

export class NoQuizs extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'No quizs could be found';
   }
}
