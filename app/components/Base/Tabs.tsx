import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { History } from 'history';

// Constant
import routes from '../../constants/routes.json';

// Style
import styles from './Tabs.less';

// Typescript
import { IMatch, ILanguage } from '../../typescript';

interface IProps extends RouteComponentProps {
   match: IMatch;
   history: History;
   lang: ILanguage;
}

interface IState {
   current: string;
   index: number;
}

const TABS = ['browser', 'recent', 'drafts'];
const links = {
   browser: routes.SUBJECTS,
   recent: routes.RECENT,
   drafts: routes.DRAFTS
};

class Tabs extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);

      this.state = {
         current: '',
         index: 0
      };
   }

   static getDerivedStateFromProps(props: IProps, state: IState) {
      const { match } = props;
      const newState = state;

      Object.keys(links).map((link: string, index: number) => {
         const pathLink = links[link].split('/')[2];
         const pathMatch = match.path.split('/')[2];

         if (links[link] === match.path || pathLink === pathMatch) {
            newState.current = link;
            newState.index = index;
         }
         return null;
      });
      return newState;
   }

   currentTabs = (item: string) => () => {
      const { history } = this.props;

      history.push(links[item]);
      this.setState({ current: item });
   };

   renderTabs = () => {
      const { lang } = this.props;
      const { current } = this.state;

      return TABS.map((item: string) => {
         const color = current === item ? styles.current : styles.none;
         const name: string = lang[item];

         return (
            <div
               key={item}
               className={`${color} ${styles.tab}`}
               role="button"
               onClick={this.currentTabs(item)}
               id={item}
            >
               <div className={styles.name}>{name.toUpperCase()}</div>
            </div>
         );
      });
   };

   render() {
      const { index } = this.state;
      const marginLeft = `${index * (100 / 3.0)}%`;
      return (
         <div className={styles.tabsContainerContainer}>
            <div className={styles.tabsContainer}>
               <div className={styles.tabs}>{this.renderTabs()}</div>
               <div className={styles.dynamicLine} style={{ marginLeft }} />
            </div>
         </div>
      );
   }
}

export default withRouter(Tabs);
