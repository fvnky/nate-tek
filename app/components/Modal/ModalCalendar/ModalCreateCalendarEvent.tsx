import React from 'react';
import cx from 'classnames';

// Component
import Modal from '../../Base/Modal';
import NateSelect, { Option } from '../../Base/NateSelect';
// import NateTimePicker from '../../Base/NateTimePicker';
import TimePickerCalendarEvent from './TimePickerCalendarEvent';
import DeleteCalendarEvent from './DeleteCalendarEvent';

// Style
import styles from './ModalCreateCalendarEvent.less';

// Utils
import { getName, getSubjectId, dateCalendarEvent } from './utils';

// Constants
import { COLLECTION_DB, REMOVE, UPDATE } from '../../../constants/models';
import {
   TITLE_SCHEDULE,
   DATE_SCHEDULE,
   CLASSNAME_SCHEDULE,
   ROOM_SCHEDULE,
   NOTE_SCHEDULE,
   START_SCHEDULE,
   END_SCHEDULE,
   ID_SUBJECT_SCHEDULE
} from '../../../constants/models/schedule';

// Typescript
import {
   IArgDateClick,
   IDatabase,
   ISubject,
   ISchedule
} from '../../../typescript';
import { IModalManager } from '../ModalManager';

interface ISelectSubject extends ICreateSchedule {
   event: any;
   setEvent: React.Dispatch<React.SetStateAction<any>>;
}

// const getName = (array: any[], str) => {
//    let search = '';
//    array.forEach(elem => {
//       const { name, _id } = elem;
//       if (_id === str) search = name;
//    });
//    return search;
// };

// const getSubjectId = (id, subjects) => {
//    if (!id || id === '') return subjects[0]._id || '';
//    return id;
// };

const SelectSubject = (props: ISelectSubject) => {
   const { subjects, event, setEvent } = props;
   const { [ID_SUBJECT_SCHEDULE]: id } = event;
   const [input, setInput] = React.useState('select subject');
   // const [input, setInput] = React.useState(getSubjectId(id, subjects));

   React.useEffect(() => {
      const obj = {
         ...event,
         [TITLE_SCHEDULE]: getName(subjects, input),
         [ID_SUBJECT_SCHEDULE]: input
      };
      console.log('Obj: ', obj);
      // setEvent(obj);
      setEvent({ ...obj });
   }, [input]);

   const handleOnChange = (inputValue: any) => {
      setInput(inputValue);
   };

   return (
      <NateSelect value={input} onChange={handleOnChange} {...props}>
         {subjects.map((subject: ISubject) => {
            const { name, _id } = subject;
            return (
               <Option key={_id} value={_id}>
                  {name}
               </Option>
            );
         })}
      </NateSelect>
   );
};

interface IPropsInputCalendarEvent {
   event: any;
   setEvent: React.Dispatch<React.SetStateAction<any>>;
}

const InputCalendarEvent = (props: IPropsInputCalendarEvent) => {
   const {
      event: { [ROOM_SCHEDULE]: room = '' },
      setEvent,
      event
   } = props;

   const handleOnChange = (e: React.FormEvent<HTMLInputElement>) => {
      setEvent({ ...event, [ROOM_SCHEDULE]: e.currentTarget.value });
   };

   return <input value={room} onChange={handleOnChange} />;
};

const TextAreaCalendarEvent = props => {
   const { event, setEvent } = props;
   const { [NOTE_SCHEDULE]: comment = '' } = event;

   const handleOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      setEvent({ ...event, [NOTE_SCHEDULE]: e.currentTarget.value });
   };

   return (
      <textarea
         className={styles.toto}
         rows={4}
         cols={30}
         onChange={handleOnChange}
         value={comment}
      />
   );
};

// const getTime = date => {
// // console.error(date);
//    if (!date) return [0, 0];

//    const time = new Date(date);
// // console.log("TIME :", time);
//    return [time.getHours(), time.getMinutes()];
// };

// const TimePickerStartCalendarEvent = ({ event, setEvent }) => {
//    const { [START_SCHEDULE]: timeEvent } = event;
//    const [hours, minutes] = getTime(new Date(timeEvent));

//    const handleOnChange = (minute, hour) => {
//       try {
//          const date = new Date(event[START_SCHEDULE]);
//          date.setHours(hour);
//          date.setMinutes(minute);
//          setEvent({ ...event, [START_SCHEDULE]: date.toISOString() });
//       } catch (err) {
//          // console.error(err);
//       }
//    };

//    return (
//       <NateTimePicker
//          onChange={handleOnChange}
//          hours={hours}
//          minutes={minutes}
//          minHour={"0"}
//          minMin={"0"}
//       />
//    );
// };

// const TimePickerEndCalendarEvent = ({ event, setEvent }) => {
//    const { [END_SCHEDULE]: timeEvent, [START_SCHEDULE]: time } = event;
//    const [hours, minutes] = getTime(event.end);
//    const start = new Date(time);
//    const minHour = start.getHours();
//    const minMin = start.getMinutes();

//    const handleOnChange = (minute, hour) => {
//       try {
//          const date = new Date(event.end);
//          date.setHours(hour);
//          date.setMinutes(minute);
//          setEvent({ ...event, [END_SCHEDULE]: date.toISOString() });
//       } catch (err) {
//       }
//    };

//    React.useEffect(() => {
//       if (minHour > hours) {
//          handleOnChange(minutes, minHour);
//       }
//    }, [minHour]);

//    React.useEffect(() => {
//       if (hours === minHour && minutes < minMin) handleOnChange(minMin, hours);
//    }, [minMin]);

//    return (
//       <NateTimePicker
//          onChange={handleOnChange}
//          hours={hours}
//          minutes={minutes}
//          minHour={minHour}
//          minMin={minMin}
//       />
//    );
// };

// const monthNamesEn = [
//    "January", "February", "March",
//    "April", "May", "June", "July",
//    "August", "September", "October",
//    "November", "December"
// ];

// const monthNamesFr = [
//    "Janvier", "Février", "Mars",
//    "Avril", "Mai", "Juin", "Juillet",
//    "Aout", "Septembre", "Octobre",
//    "Novembre", "Décember"
// ];

// const dateCalendarEvent = (event) => {
//    // const { event } = props;
//    // if (!event) return "";
//    const { date } = event;
//    const time = new Date(date);

//    const day = time.getDate();
//    const monthIndex = time.getMonth();
//    const year = time.getFullYear();

//    const res = "" + day + " " + monthNamesEn[monthIndex] + " " + year + "";
//    // console.log("res :", res);
//    return res;
// }

const ElementSpacer = props => {
   return <div className={styles.elementSpacer}>{props.children}</div>;
};
interface IPropsArgEvent extends IArgDateClick, ISchedule {}

interface ICreateSchedule extends IModalManager {
   handleDisplayModal: () => void;
   arg: any;
   db: IDatabase;
   collection: string;
   subjects: ISubject[];
   method: string;
}

export default function ModalCreateCalendarEvent(props: ICreateSchedule) {
   const { lang, handleDisplayModal, theme, arg } = props;
   // const init = initEvent(arg);
   const [event, setEvent] = React.useState(arg);
   const [empty, setEmpty] = React.useState(true);

   const handleEvent = arg => {
      console.log('ARG: ', arg);
      setEmpty(false);
      setEvent({ ...arg });
   };

   const handleOnClick = () => {
      const { db, collection, method } = props;

      if (event.title === 'new event') {
         return handleDisplayModal();
      }

      if (
         event.end === event.start &&
         event.end === event.date &&
         event.start === event.end
      ) {
         event.start = event.dateStr;
         event.date = event.dateStr;
         event.end = event.dateStr;
         // console.log("SAME");
      }
      if (!event.dateStr) {
         event.dateStr = event.start;
      }
      // console.error("Event :", event);
      // console.error("State: ", state);
      db[COLLECTION_DB[collection]]
         [method](event)
         .then(() => handleDisplayModal())
         .catch(err => {
            console.log('Err :', err);
            handleDisplayModal();
         });
   };

   return (
      <Modal
         theme={theme}
         title={dateCalendarEvent(event)}
         onCancel={handleDisplayModal}
         onOk={handleOnClick}
         okText={'Save'}
         cancelText={lang.cancel}
      >
         <div className={styles.containerEventModal}>
            {/* <DateCalendarEvent event={event} /> */}
            <ElementSpacer>Subject</ElementSpacer>
            <ElementSpacer>
               <SelectSubject event={event} setEvent={handleEvent} {...props} />
            </ElementSpacer>
            <ElementSpacer>Room</ElementSpacer>
            <ElementSpacer>
               <InputCalendarEvent event={event} setEvent={handleEvent} />
            </ElementSpacer>
            <ElementSpacer>Note</ElementSpacer>
            <ElementSpacer>
               <TextAreaCalendarEvent event={event} setEvent={handleEvent} />
            </ElementSpacer>
            <ElementSpacer>Time</ElementSpacer>
            <ElementSpacer>
               <TimePickerCalendarEvent event={event} setEvent={handleEvent} />
            </ElementSpacer>
            {/* <ElementSpacer>

            </ElementSpacer> */}
            <ElementSpacer>
               <DeleteCalendarEvent
                  event={event}
                  setEvent={setEvent}
                  {...props}
               />
            </ElementSpacer>
         </div>
      </Modal>
   );
}
