import * as React from 'react';

const IconSettings = props => (
   <svg viewBox="0 0 100 100" {...props}>
      <path d="M83.1,42.3L77.9,41c-0.4-1.2-0.9-2.4-1.4-3.5l2.7-4.6c0.1-0.1,0.2-0.3,0.2-0.4c1.1-1.7,0.9-4.1-0.6-5.6l-5.3-5.3
		c-0.8-0.8-1.8-1.2-2.9-1.2c-0.9,0-1.9,0.3-3,0.9L63,24c-1.1-0.6-2.3-1-3.5-1.5l-1.3-5.3c0-0.1-0.1-0.2-0.1-0.3
		c-0.4-2.1-2.2-3.6-4.4-3.6h-7.5c-2.1,0-3.7,1.4-4.4,3.9l-1.3,5.2c-1.2,0.4-2.4,0.9-3.5,1.4L32.3,21c-0.1-0.1-0.2-0.1-0.3-0.2
		c-0.7-0.5-1.6-0.7-2.4-0.7c-1.2,0-2.3,0.4-3.2,1.3l-5.3,5.3c-1.5,1.5-1.6,3.6-0.3,5.9l2.7,4.6c-0.6,1.1-1,2.3-1.5,3.5l-5,1.5
		l-0.2,0.1c-0.1,0-0.1,0-0.2,0.1c-2.1,0.4-3.5,2.2-3.5,4.4v7.5c0,2.1,1.5,3.7,3.9,4.4l5.2,1.3c0.4,1.2,0.9,2.3,1.4,3.5l-2.8,4.7
		l-0.1,0.3c-1.2,1.8-0.9,4.1,0.6,5.6l5.3,5.3c0.8,0.8,1.8,1.2,2.9,1.2c0.9,0,1.9-0.3,3-0.9l4.6-2.7c1.1,0.6,2.3,1,3.5,1.5l1.3,5.2
		l0.1,0.4c0.4,2.1,2.2,3.6,4.4,3.6h7.5c2.1,0,3.7-1.4,4.4-3.9l1.3-5.2c1.2-0.4,2.4-0.9,3.5-1.4l4.7,2.8c0.1,0.1,0.2,0.1,0.3,0.2
		c0.7,0.5,1.6,0.7,2.4,0.7c1.2,0,2.3-0.5,3.2-1.3l5.3-5.3c0.7-0.7,2.1-2.7,0.3-5.9l-2.9-5c0.6-1.1,1-2.3,1.5-3.5l5.3-1.3
		c0.1,0,0.2-0.1,0.3-0.1c2.1-0.4,3.5-2.2,3.5-4.4v-7.5C87,44.6,85.6,43,83.1,42.3z M50,63.4c-7.2,0-13-5.8-13-13c0-7.2,5.8-13,13-13
		s13,5.8,13,13C63,57.6,57.2,63.4,50,63.4z"/>
   </svg>
);

export default IconSettings;
