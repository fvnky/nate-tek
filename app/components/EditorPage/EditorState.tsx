import React from 'react';
import { Base64 } from 'js-base64';
import { RouteComponentProps, withRouter } from 'react-router';
import { Value } from 'slate';

import EditorPage from './EditorPage';

import routes from '../../constants/routes.json';

import initialValue from '../../constants/initialValue';

// Typescript
import { ICourse, ILanguage, EditorTab } from '../../typescript';
import { ITodo } from '../../typescript/models';

function immutablySwapItems(items, firstIndex, secondIndex) {
   return items.map((element, index) => {
      if (index === firstIndex) return items[secondIndex];
      if (index === secondIndex) return items[firstIndex];
      return element;
   });
}

interface IProps extends RouteComponentProps {
   // Redux
   courses: ICourse[];
   lang: ILanguage;
   router: any;
   todos: Array<ITodo>;

   // DB redux
   db: any;
   updateCourse: (newCourse: ICourse, content?: Object) => void;
}

interface IState {
   activeIndex: number;
   tabs: EditorTab[];
   zoom: number;
   timeouts: any[]
}

class EditorState extends React.PureComponent<IProps, IState> {
   editor: any;

   state = {
      activeIndex: -1,
      tabs: [],
      zoom: 1,
      timeouts: []
   };

   componentDidMount() {
      // this.addNewTab();
   }

   componentDidUpdate(prevProps: IProps, prevState: IState) {
      // console.warn(" ---- DID UPDATE EDITOR STATE", this.props.todos)

      if (prevProps.todos.length !== this.props.todos.length) {
         this.state.timeouts.forEach(t => clearTimeout(t))
         const _timeouts = []
         this.props.todos.forEach(todo => {
            const timeDiff = todo.dueAt - new Date().getTime();
            // console.warn(" ---- TIME DIFF", timeDiff)
            if (timeDiff <= 0) return;
            setTimeout(() => {
               const timeout = new Notification('To Do', {
                  body: todo.name
               });
               _timeouts.push(timeout)
            }, timeDiff);
         })
         this.setState({ timeouts: _timeouts })
      }

      const { router, db, courses } = this.props;
      const { tabs, activeIndex } = this.state;
      // If we're not in an editor Page route, don't do anything.
      if (router.location.pathname.split('/')[1] !== 'editor') return;

      this.checkAndAddCourseFromRoute();
      // If there is no tab, create one
      // if (this.checkAndAddCourseFromRoute() === false && tabs.length === 0) {
      //    this.addNewTab();
      // }

      // if a "LOADING" tab was added, load it from the database.
      tabs.forEach((tab, index) => {
         if (
            tab.status === 'LOADING' &&
            prevState.tabs.find(t => t.id === tab.id) === undefined
         ) {
            db._main
               .get(tab.id, { attachments: true })
               .then((doc: any) => {
                  const base64 = doc._attachments['value.json'].data;
                  const value = JSON.parse(Base64.decode(base64));
                  this.updateEditorTabIndex(index, {
                     temporaryName: doc.name,
                     status: 'SAVED',
                     id: tab.id,
                     value: Value.fromJSON(value),
                     pdfViewer: tab.pdfViewer
                  });
               })
               .catch(err => console.error(err));
            console.log(db.courses);
            // db.courses
            //    .getCourseValue(tab.id)
            //    .then(value => {
            //       this.updateEditorTabIndex(index, {
            //          status: "SAVED",
            //          id: tab.id,
            //          value
            //       });
            //    })
            //    .catch(err => console.error(err));
         }
         // Check integrity of tabs in editor : If a course is missing, close this tab
         if (
            tab.status === 'SAVED' &&
            !courses.find((course: ICourse) => course._id === tab.id)
         ) {
            let newIndex = activeIndex > index ? activeIndex - 1 : activeIndex;
            newIndex = newIndex > tabs.length - 2 ? tabs.length - 2 : newIndex;
            const newTabs = tabs.filter((tab, i) => i !== index);
            this.setState({ tabs: newTabs, activeIndex: newIndex });
         }
      });

      // focus editor if need be
      const activeTab = tabs[activeIndex];
      const prevActiveTab = prevState.tabs[prevState.activeIndex];
      if (
         this.editor &&
         activeTab &&
         activeTab.value &&
         (prevProps.router.location.pathname.split('/')[1] !== 'editor' ||
            !prevActiveTab ||
            (prevActiveTab.id !== activeTab.id ||
               (prevActiveTab.status === 'LOADING' &&
                  activeTab.status === 'SAVED')))
      ) {
         this.editor.focus();
      }
   }

   componentWillUnmount() {
      console.warn('EXITING EDITOR: SAVING SHOULD BE DONE HERE');
   }

   addNewTab = () => {
      const { lang } = this.props;
      const { tabs } = this.state;
      const count = tabs.reduce(
         (total, item) => (item.status === 'NEW' ? total + 1 : total),
         0
      );
      const value = Value.fromJSON(initialValue as any);
      const temporaryName = `${lang.newCourse} ${
         count === 0 ? '' : count + 1 < 10 ? `0${count + 1}` : count + 1
         }`;
      const newTab = {
         status: 'NEW',
         id: `${count}`,
         temporaryName,
         value,
         pdfViewer: { file: null, page: 0 }
      };
      console.log('will add ', newTab);
      this.setState({ tabs: [...tabs, newTab], activeIndex: tabs.length });
   };

   setActiveEditorTabIndex = (index: number) => {
      this.setState({ activeIndex: index });
   };

   updateEditorTabIndex = (index: number, tab: EditorTab) => {
      const { tabs } = this.state;
      const newTabs = tabs.map((t, i) => (i === index ? tab : t));
      this.setState({ tabs: newTabs });
   };

   switchEditorTabIndex = (a: number, b: number) => {
      const { tabs, activeIndex } = this.state;
      // eslint-disable-next-line no-nested-ternary
      const newActiveIndex =
         activeIndex === a ? b : activeIndex === b ? a : activeIndex;
      this.setState({
         tabs: immutablySwapItems(tabs, a, b),
         activeIndex: newActiveIndex
      });
   };

   closeEditorTabIndex = (index: number) => {
      const { tabs, activeIndex } = this.state;
      const activeTab = tabs[activeIndex];
      if (activeTab.status === 'SAVED')
         this.saveCourse(activeTab.id, activeTab.value);

      let newIndex = activeIndex > index ? activeIndex - 1 : activeIndex;
      newIndex = newIndex > tabs.length - 2 ? tabs.length - 2 : newIndex;
      const newTabs = tabs.filter((tab, i) => i !== index);
      this.setState({ tabs: newTabs, activeIndex: newIndex });
   };

   saveCourse = (id: string, value: any) => {
      const { updateCourse, courses } = this.props;
      const course = courses.find((course: ICourse) => course._id === id);
      updateCourse(course, value);
   };

   changeCourseName = (id: string, newName: any) => {
      const { updateCourse, courses, lang } = this.props;
      const course = courses.find((course: ICourse) => course._id === id);
      if (course.name === newName) return;
      course.name = newName;
      // if (course.name === "")
      //    course.name = lang.untitled;
      updateCourse(course);
   };

   checkAndAddCourseFromRoute = () => {
      const { router, history } = this.props;

      if (router.location.pathname.split('/').length >= 3) {
         const { tabs } = this.state;
         const courseID = router.location.pathname.split('/')[2];
         console.warn('there is a courseID in route', courseID);

         const indexOfFound = tabs.findIndex(t => t.id === courseID);
         if (indexOfFound !== -1) {
            this.setState({ activeIndex: indexOfFound });
         } else {
            const newTab = {
               status: 'LOADING',
               id: courseID,
               value: null,
               pdfViewer: { file: null, page: 0 }
            };
            this.setState({
               tabs: [...tabs, newTab],
               activeIndex: tabs.length
            });
         }
         history.replace(routes.EDITOR_LINK);
         return true;
      }
      return false;
   };

   setZoom = (value: number) => {
      this.setState({ zoom: value });
   };

   onChange = ({ value }: any) => {
      const { tabs, activeIndex } = this.state;
      const activeTab = tabs[activeIndex];

      activeTab.value = value;
      this.updateEditorTabIndex(activeIndex, activeTab);
   };

   setRef = (editor: any) => {
      if (editor) {
         this.editor = editor.editor;
         this.forceUpdate();
      }
   };

   render() {
      const { tabs, activeIndex, zoom } = this.state;
      return (
         <EditorPage
            setRef={this.setRef}
            editor={this.editor}
            onChange={this.onChange}
            setZoom={this.setZoom}
            changeCourseName={this.changeCourseName}
            // saveCourse={this.saveCourse}
            closeEditorTabIndex={this.closeEditorTabIndex}
            switchEditorTabIndex={this.switchEditorTabIndex}
            updateEditorTabIndex={this.updateEditorTabIndex}
            setActiveEditorTabIndex={this.setActiveEditorTabIndex}
            addNewTab={this.addNewTab}
            tabs={tabs}
            activeIndex={activeIndex}
            zoom={zoom}
            show={this.props.location.pathname.split('/')[1] === 'editor'}
         />
      );
   }
}

export default withRouter(EditorState);
