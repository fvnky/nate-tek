import mammoth from 'mammoth';

function parseHTML(html) {
   var t = document.createElement('template');
   t.innerHTML = html;
   return t.content.cloneNode(true);
}

const headingMap = {
   H1: 1,
   H2: 2,
   H3: 3,
   H4: 4,
   H5: 4,
   H6: 4,
   H7: 4,
   H8: 4
};

const sousNodeMap = {
   STRONG: 'bold',
   B: 'bold',
   EM: 'italic',
   I: 'italic'
};

function createImageBlock(node) {
   const tre = node.childNodes[0] as any;
   const block = {
      object: 'block',
      type: 'image',
      data: { src: tre.getAttribute('src') },
      style: 'left',
      width: 100
   };
   return block;
}

function createBlock(node: Node) {
   const isHeading = headingMap[node.nodeName] ? true : false;
   const isParagraph = node.nodeName === 'P';
   if (!isHeading && !isParagraph) {
      console.error('Unknown node name', node.nodeName);
      return null;
   }
   if (node.childNodes[0].nodeName === 'IMG') {
      // Will be handled later
      return null;
   }
   const block = {
      object: 'block',
      type: isHeading ? 'heading' : 'paragraph',
      data: { level: isHeading ? headingMap[node.nodeName] : 1 },
      nodes: [
         {
            object: 'text',
            leaves: [...node.childNodes].map(sousNode => {
               // console.log("ssNode", sousNode.nodeName, nodeMap[sousNode.nodeName])
               if (
                  !sousNodeMap[sousNode.nodeName] &&
                  sousNode.nodeName !== '#text'
               ) {
                  console.error('Unknown sousNode name', sousNode.nodeName);
               }
               if (sousNodeMap[sousNode.nodeName]) {
                  return {
                     marks: [
                        {
                           type: sousNodeMap[sousNode.nodeName]
                        }
                     ],
                     text: sousNode.textContent
                  };
               } else return { text: sousNode.textContent };
            })
         }
      ]
   };
   return block;
}

function htmlToSlateJson(html) {
   console.log(html);
   const documentFragment: Node = parseHTML(html);
   console.log(documentFragment);
   let nodesModel = [];
   documentFragment.childNodes.forEach(node => {
      const block = createBlock(node);
      if (block) {
         nodesModel.push(block);
      }
      if (node.childNodes[0].nodeName === 'IMG') {
         nodesModel.push(createImageBlock(node));
      }
   });
   const content = {
      document: {
         nodes: nodesModel
      }
   };
   return content;
}

export function docxToSlateJson(file) {
   return mammoth.convertToHtml({ path: file.path }).then(r => {
      if (r.messages.length > 0) {
         console.error(r.messages);
      }
      return htmlToSlateJson(r.value);
   });
}

export function createCourseFromFile(file, id, collection, createCourse) {
   let textContent = '';
   console.log(file);
   // const file = e.target.files[0];
   mammoth
      .extractRawText({ path: file.path })
      .then(function(result) {
         textContent = result.value;
         return textContent;
      })
      .then(() => {
         docxToSlateJson(file)
            .then(content => {
               const newDocument = {
                  name: file.name.substr(0, file.name.length - 5),
                  parent: { id, collection },
                  textContent
               };
               createCourse(newDocument, content).then(() => {
                  console.log('created test');
               });
               console.warn(content);
            })

            .catch((err: any) => {});
      });
}
