#!/usr/bin/python3


import os
from os import listdir
from os.path import isfile, join


def generateIconsJs(fd, folder, f):
    fd.write("const icons = {\n")
    listFiles = [x for x in listdir(folder) if isfile(join(folder, x))]
    rm = []
    for x in listFiles:
        try:
            pos = x.index(".svg")
        except:
            rm.append(x)

    for x in rm:
        listFiles.remove(x)

    icons = ""
    for x in listFiles:
        svg = open(x, "r")
        svg = svg.read().replace("\n", "").replace("\t", "")
        icons += "\t" + x[:-4] + ": {\n\t\tvb: '"
        d = 0;
        pos = 0;

        while (pos != -1):
            try:
                pos = svg.index("viewBox")
                svg = svg[pos:]
                svg = svg[2:]
                sp = svg.split("\"")
                icons += sp[1] + "',\n"
            except:
                pos = 0
            try:
                pos = svg.index("<path d")
                svg = svg[pos:]
                svg = svg[2:]
                sp = svg.split("\"")
                d += 1
                if d < 2:
                    icons += "\t\td: '" + sp[1] + "',\n"
                else:
                    icons += "\t\td" + str(d) + ": '" + sp[1] + "',\n"
            except:
                pos = -1
        print("---------------------------------------------------------------------")
        print(icons)
        icons = icons[:-2]
        icons += "\n\t},\n"
    icons = icons[:-2]
    icons += "\n};\n\nexport default icons;"
    fd.write(icons)

def main():
    print("############################")
    print("############################\n")
    print("Tous les fichiers .svg du dossier courant seront ajouts s icons.js ")
    print("Dossier courant (pwd): " + os.getcwd())
    print("Est-ce bien le cas ? (Y/n)")
    text = input("> ")
    if (text == "Y" or text == "y" or text == ""):
        folder = os.getcwd();
        print("ok !!")
    else:
        print("Entrer le chemin complet du dossier d'eriture:")
        folder = input("> ")
        while (os.path.isdir(folder) == False):
            print("ce dossier ne semble pas exister ou n'est pas accessible")
            folder = input("> ")

    if folder[len(folder) - 1] != "/":
        folder += "/"

    print("Nom du fichier par defaut: icons.js\nOk ? (Y/n)")
    text = input("> ")
    if (text == "Y" or text == "y" or text == ""):
        f = "icons.js";
        print("ok !!")
    else:
        print("Nouveau nom:")
        f = input("> ")
        print("ok !!")


    fd = open(folder + f, "w+")
    generateIconsJs(fd, folder, f)


if __name__ == "__main__":
    main()
