import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Sidebar from '../components/Sidebar/Sidebar.tsx';
import * as UserActions from '../actions/user';
import * as ThemeActions from '../actions/theme';
import * as SidebarActions from '../actions/sidebar';
import * as ModalActions from '../actions/modal';

function mapStateToProps(state) {
   return {
      sidebar: state.sidebar,
      router: state.router,
      user: state.user,
      lang: state.language,
      theme: state.theme
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators({ ...SidebarActions, ...UserActions, ...ThemeActions, ...ModalActions }, dispatch);
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(Sidebar);
