import React from 'react';
import cx from 'classnames';
// Style
import styles from './Todo.less';
// Typescript
import { IDatabase, ITodo } from '../../typescript';

import {
    TODO_DB,
    REMOVE,
    UPDATE,
    COLLECTION_DB
 } from '../../constants/models/database';

 // Attributes of model todo
import { IS_DONE } from '../../constants/models/todo';

type IProps = {
   database: IDatabase;
   theme: string;
   todo: ITodo;
};

export default function Todo(props: IProps) {
       // Exemple of remove the first element in the list of todos
   const handleRemoveTodo = () => {
      const { database, todo } = props;
      const { collection } = todo;
      database[COLLECTION_DB[collection]][REMOVE](todo)
      .then(
          console.log(collection)
      ).then(
          console.log(todo)
      ).catch(err => console.warn(err));
   };

   const ToggleTodo = (/* event: React.ChangeEvent<HTMLInputElement> */) => {
    const { database, todo } = props;
    const oldstate = todo.status;
    const newstate = !oldstate;
    database[TODO_DB][UPDATE]({ ...todo, [IS_DONE]: newstate })
       .then(() => {})
       .catch(err => console.warn(err));
    };

    const renderDueDate = () => {
      const { database, todo } = props;
      var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
      var date = new Date(props.todo.dueAt)
      var datestr = date.toLocaleDateString('en-US', options)
      var time = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: false })
      return (
         <div className={styles.todoDueDate}>
            {"Due " + datestr +' ' + time}
            {/* <strong>{}</strong> */}

         </div>
      );
   };

   return (
      <div className={props.todo.status ? styles.todoDoneCardBorder : styles.todoCardBorder }>
         <ul className={styles.todoCardContents}>
            <li className={styles.todoCheck}>
               <input
                  type="checkbox"
                  checked={props.todo.status}
                  onClick={ToggleTodo}
               />
            </li>
            <li className={styles.todoText}>
               <div className={styles.todoTitle}>{props.todo.name}</div>
               {renderDueDate()}
            </li>
            <li>
                <input
                type="button"
                value="x"
                onClick={handleRemoveTodo}
                />
            </li>
         </ul>
      </div>
   );
}
