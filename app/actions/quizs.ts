import { GetState, Dispatch } from '../reducers/types';
import { IQuiz } from '../typescript';

function ExceptionFolder(error: any) {
   return error;
}

export function loadQuizs() {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.quizs.findAll()
         .then(() => {})
         .catch((err: any) => console.error(err.stack));
   };
}

export function createQuiz(newQuiz: {
   name: string;
   parent: { id: string; collection: string };
}) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.quizs.create(newQuiz)
         .then(() => {})
         .catch((err: any) => {
            console.log(err.stack);
            throw ExceptionFolder(err);
         });
}

export function updateQuiz(newQuiz: IQuiz) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.quizs.update(newQuiz)
         .then(() => {})
         .catch((err: any) => {
            console.log(err.stack);
            throw ExceptionFolder(err);
         });
}

export function removeQuiz(toRemove: IQuiz) {
   return (dispatch: Dispatch, getState: GetState) =>
      getState()
         .database.quizs.remove(toRemove)
         .then(() => {})
         .catch((err: any) => {
            console.log(err.stack);
            throw ExceptionFolder(err);
         });
}
