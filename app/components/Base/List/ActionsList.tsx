import * as React from 'react';

// Component
import DroppableItem from '../DroppableItem';
import DraggableItem from '../DraggableItem';

// Utils
import { destruct } from '../../utils';

// Typescript
import { IListItem, IOnClick, IDrop, IDrag } from '../../../typescript';

// Style
import styles from './ActionsList.less';

// Constant
export const ONCLICK_LIST = 'onClick';
export const DROPPABLE_LIST = 'droppable';
export const DRAGGABLE_LIST = 'draggable';
export const POPUP_LIST = 'popup';

export function ActionOnClick(
   Children: React.ReactNode | React.ReactNode[],
   props: IOnClick
): React.ReactNode | React.ReactNode[] {
   const handleOnClick = destruct([ONCLICK_LIST], props);
   // const { className = styles.iconAndNameContainer } = props;

   return (
      <div role="button" onClick={handleOnClick}>
         {Children}
      </div>
   );
}

export function ActionDrop(
   Children: React.ReactNode | React.ReactNode[],
   props: IDrop
): React.ReactNode | React.ReactNode[] {
   return <DroppableItem data={props}>{Children}</DroppableItem>;
}

export function ActionDrag(
   Children: React.ReactNode | React.ReactNode[],
   props: IDrag
): React.ReactNode | React.ReactNode[] {
   return <DraggableItem data={props}>{Children}</DraggableItem>;
}

export const buildAction = {
   [ONCLICK_LIST]: ActionOnClick,
   [DROPPABLE_LIST]: ActionDrop,
   [DRAGGABLE_LIST]: ActionDrag
};

interface IBuilderProps {
   descriptor: IListItem;
   children: React.ReactNode | React.ReactNode[];
}

const BuilderActions = (props: IBuilderProps) => {
   const { descriptor, children } = props;
   let child = children;
   Object.keys(descriptor).forEach((element: string) => {
      if (descriptor[element] && buildAction[element])
         child = buildAction[element](child, descriptor[element]);
   });
   return <div key={descriptor.id}>{child}</div>;
};

export default BuilderActions;
