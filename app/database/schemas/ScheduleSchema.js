import {
   EVENTS_SCHEDULE,
   TITLE_SCHEDULE,
   DATE_SCHEDULE,
   ID_SCHEDULE,
   CLASSNAME_SCHEDULE,
   ROOM_SCHEDULE,
   NOTE_SCHEDULE,
   START_SCHEDULE,
   END_SCHEDULE,
   ID_SUBJECT_SCHEDULE
} from '../../constants/models';
import { SCHEDULE } from '../constants';

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      // [EVENTS_SCHEDULE]: [
      //    {
      //       [ID_SCHEDULE]: { type: Number, required: true },
      [DATE_SCHEDULE]: { type: String, required: true },
      [TITLE_SCHEDULE]: { type: String, required: false },
      [CLASSNAME_SCHEDULE]: { type: String, required: false },
      [ROOM_SCHEDULE]: { type: String, required: false },
      [NOTE_SCHEDULE]: { type: String, required: false },
      [START_SCHEDULE]: { type: String, required: false },
      [END_SCHEDULE]: { type: String, required: false },
      [ID_SUBJECT_SCHEDULE]: { type: String, required: false },
      //    }
      // ],
      collection: SCHEDULE
   }),
   fields: [
      '_id',
      '_rev',
      'createdAt',
      'updatedAt',
      // EVENTS_SCHEDULE,
      TITLE_SCHEDULE,
      DATE_SCHEDULE,
      CLASSNAME_SCHEDULE,
      ROOM_SCHEDULE,
      NOTE_SCHEDULE,
      START_SCHEDULE,
      END_SCHEDULE,
      ID_SUBJECT_SCHEDULE,
      'collection'
   ]
};
