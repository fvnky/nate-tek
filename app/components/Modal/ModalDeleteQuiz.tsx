import * as React from 'react';

// Component
import Modal from '../Base/Modal';

// typescript
import { IQuiz, IDatabase, RemoveQuiz, ILanguage } from '../../typescript';

interface IProps {
   id: string;
   db: IDatabase;
   removeQuiz: RemoveQuiz;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   quiz: IQuiz | null;
   title: string;
}

export default class ModalDeleteQuiz extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);
      this.state = {
         quiz: null,
         title: ''
      };
   }

   componentDidMount() {
      const { id, db } = this.props;

      db.quizs
         .findById(id)
         .then((quiz: IQuiz) => this.setState({ quiz, title: quiz.name }))
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   }

   handleOk = () => {
      const { handleDisplayModal, removeQuiz } = this.props;
      const { quiz } = this.state;

      if (!quiz) return;
      removeQuiz(quiz)
         .then(() => handleDisplayModal())
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;
      const { title } = this.state;

      return (
         <Modal
            theme={theme}
            title={`${lang.deleteItem} "${title}" ?`}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.delete}
            cancelText={lang.cancel}
         />
      );
   }
}
