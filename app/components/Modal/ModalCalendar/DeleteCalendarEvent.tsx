import React from 'react';

// Constants
import { COLLECTION_DB, REMOVE, UPDATE } from '../../../constants/models';

// Styles
import styles from './ModalCreateCalendarEvent.less';

const DeleteCalendarEvent = props => {
   const { method } = props;

   const handleOnClick = () => {
      const { db, collection, handleDisplayModal, event } = props;

      db[COLLECTION_DB[collection]]
         [REMOVE](event)
         .then(() => handleDisplayModal())
         .catch(err => console.log('Err :', err));
   };

   if (method !== UPDATE) return null;

   return (
      <div className={styles.containerDeleteEvent}>
         <div onClick={handleOnClick} className={styles.deleteEvent}>
            <div>
               Delete Event
            </div>
         </div>
      </div>
   );
};

export default DeleteCalendarEvent;
