import React from "react";
import { Block } from "slate";
import DividerNode from "./DividerNode";

import { DEFAULT_NODE } from "../../constants";

export const DIVIDER = "divider";

export default function image(options = {}) {
   return {
      schema: {
         document: {
            last: { type: DEFAULT_NODE },
            normalize: (editor, { code, node, child }) => {
               // eslint-disable-next-line default-case
               switch (code) {
                  case "last_child_type_invalid": {
                     const paragraph = Block.create(DEFAULT_NODE);
                     return editor.insertNodeByKey(node.key, node.nodes.size, paragraph);
                  }
               }
            }
         },
         blocks: {
            divider: {
               isVoid: true
            }
         }
      },
      commands: {
         insertDivider(editor) {
            editor.setBlocks({type: DIVIDER})
            .moveForward();
         }
      },
      renderNode(props, editor, next) {
         const { attributes, node, isFocused } = props;
         switch (node.type) {
            case DIVIDER: {
               return ( <DividerNode isFocused={isFocused} />);
            }
            default: {
               return next();
            }
         }
      }
   };
}
