const RIGHT = 'right';
const LEFT = 'left';
const MIDDLE = 'middle';

const TOP = 'top';
const TOP_RIGHT = 'top_right';
const TOP_LEFT = 'top_left';

const BOTTOM = 'bottom';
const BOTTOM_RIGHT = 'bottom_right';
const BOTTOM_LEFT = 'bottom_left';

export {
   RIGHT,
   LEFT,
   TOP,
   TOP_RIGHT,
   TOP_LEFT,
   BOTTOM,
   BOTTOM_RIGHT,
   BOTTOM_LEFT,
   MIDDLE
};
