import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CalendarPage from '../components/CalendarPage/CalendarPage';
import * as ModalActions from '../actions/modal';

function mapStateToProps(state) {
   return {
      lang: state.language,
      theme: state.theme,
      schedules: state.schedules,
      todos: state.todo,
      db: state.database,
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...ModalActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(CalendarPage);
