import * as React from 'react';
import cx from 'classnames';

// Component
import Modal from '../Base/Modal';
import ModalSearchChildItem from './ModalSearchChildItem';
// import ChildItem from '../Base/ListItem/ChildItem';
import { RouteComponentProps, withRouter } from 'react-router';

//Utils
import { capitalize, errorModal } from '../utils';

// Icon
import IconCourse from '../../assets/icons/IconCourse';
import IconFolder from '../../assets/icons/IconFolder';
import IconClose from '../../assets/icons/IconCloseThin';
import IconSearch from '../../assets/icons/IconSearch';

// Styles
import styles from './ModalSearch.less';

import stylesErr from './ModalError.less';

// constants
import { COURSE, FOLDER, QUIZ } from '../../database/constants/index';

// typescript
import {
   ICourse,
   IFolder,
   ISubject,
   ToggleModal,
   ILanguage,
   IQuiz
} from '../../typescript';

const NONE = 'NONE';

interface IProps extends RouteComponentProps {
   item: ICourse | IFolder;
   courses: ICourse[];
   subjects: ISubject[];
   folders: IFolder[];
   quizs: IQuiz[];
   onCancel: () => void;
   onOk: () => void;
   handleDisplayModal: () => void;
   lang: ILanguage;
   toggleModal: ToggleModal;
   theme: string;
}

interface IState {
   hoveredChild: string;
   message: string;
   title: string;
}

export default class ModalSearch extends React.Component<IProps, IState> {
   input: React.RefObject<HTMLInputElement> = React.createRef<
      HTMLInputElement
   >();

   constructor(props: IProps) {
      super(props);

      this.state = {
         hoveredChild: NONE,
         message: '',
         title: ''
      };
   }

   handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { target } = event;
      const { value } = target;
      const { theme } = this.props;
      const input = this.input.current;

      if (input) {
         if (input.className !== styles.inputNameModal) {
            input.className = cx(theme, styles.inputNameModal);
            this.setState({ message: '' });
         }
      }
      this.setState({ title: capitalize(value) });
   };

   handleOk = () => {
      const { handleDisplayModal } = this.props;
      const input = this.input.current;

      handleDisplayModal();
   };

   renderInputName = () => {
      const { message, title } = this.state;
      const { lang, theme } = this.props;

      return (
         <>
            <div className={stylesErr.errorMessage}>{message}</div>
            <input
               ref={this.input}
               type="text"
               value={title}
               onChange={this.handleChange}
               className={cx(theme, styles.inputNameModal)}
               placeholder={lang.searchAnything}
               autoFocus
            />
         </>
      );
   };

   handleOnClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      e.stopPropagation();
      const { handleDisplayModal } = this.props;

      console.log('clicked on modal');
      handleDisplayModal();
   };

   setHoveredChild = (id: string) => () => {
      this.setState({ hoveredChild: id });
   };

   renderLine = (type: string, child: IFolder | ICourse | IQuiz) => {
      const { theme } = this.props;
      const { hoveredChild } = this.state;
      // console.log("CHILD :", child);
      const { _id } = child;

      return (
         <div
            onMouseOver={this.setHoveredChild(_id)}
            className={cx(theme, styles.renderLine)}
            onClick={this.handleOnClick}
         >
            <ModalSearchChildItem
               item={child}
               hovered={_id === hoveredChild}
               {...this.props}
            />
         </div>
      );
   };

   renderMatchingList = () => {
      const { folders, courses, lang, theme } = this.props;
      const { title } = this.state;

      if (title == '') return null;

      const folderMaches = folders.filter((folder: IFolder) =>
         folder.name.includes(title)
      );
      const courseMaches = courses.filter((cours: ICourse) =>
         cours.name.includes(title)
      );

      if (folderMaches.length === 0 && courseMaches.length === 0)
         return (
            <span className={cx(theme, styles.empty)}>No result so far</span>
         );
      return null;
   };

   renderMachingFolders = () => {
      const { folders, lang } = this.props;
      const { title } = this.state;
      const folderMaches = folders.filter((folder: IFolder) =>
         folder.name.includes(title)
      );

      if (title == '') return;

      return folderMaches.map((folder: IFolder) => {
         const { name, collection } = folder;

         if (collection === FOLDER) {
            return (
               <div key={folder._id}>{this.renderLine('Folder', folder)}</div>
            );
         }
         return <></>;
      });
   };

   renderMachingCourses = () => {
      const { courses, lang } = this.props;
      const { title } = this.state;
      const courseMaches = courses.filter((cours: ICourse) =>
         cours.name.includes(title)
      );

      if (title == '') return;

      return courseMaches.map((cours: ICourse) => {
         const { name, collection } = cours;

         if (collection === COURSE) {
            return (
               <div key={cours._id}>{this.renderLine('Courses', cours)}</div>
            );
         }
         return <></>;
      });
   };

   renderMachingQuizs = () => {
      const { quizs, lang } = this.props;
      const { title } = this.state;
      const quizMaches = quizs.filter((quiz: IQuiz) =>
         quiz.name.includes(title)
      );

      if (title == '') return;

      return quizMaches.map((quiz: IQuiz) => {
         const { name, collection } = quiz;

         if (collection === QUIZ) {
            return <div key={quiz._id}>{this.renderLine('Quiz', quiz)}</div>;
         }
         return <></>;
      });
   };

   handleCancel = (e: React.KeyboardEvent<HTMLInputElement>) => {
      const { handleDisplayModal } = this.props;

      handleDisplayModal();
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;
      const { title } = this.state;

      return (
         <div className={cx(theme, styles.mask)}>
            <div className={cx(theme, styles.container)}>
               <IconClose
                  className={cx(theme, styles.closeButton)}
                  onClick={handleDisplayModal}
                  tabIndex="0"
                  onKeyDown={this.handleCancel}
               />
               <div className={styles.inputLine}>
                  <div>
                     <IconSearch className={cx(theme, styles.icon)} />
                  </div>
                  <div className={styles.col2}>{this.renderInputName()}</div>
               </div>
               <div className={cx(theme, styles.name, styles.borderLayoutList)}>
                  {title != '' && this.renderMatchingList() == null && (
                     <>
                        <div className={styles.listHeaderElement}>Name</div>
                        <div className={styles.listHeaderElement}>Parent</div>
                        <div className={styles.listHeaderElement}>
                           Last Edited
                        </div>
                     </>
                  )}
               </div>
               <div
                  className={cx(styles.listContainer, styles.borderLayoutList)}
               >
                  {this.renderMatchingList()}
                  {this.renderMachingFolders()}
                  {this.renderMachingCourses()}
                  {this.renderMachingQuizs()}
               </div>
            </div>
         </div>
      );
   }
}
