import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DraftsPage from '../components/DraftsPage/DraftsPage';
import * as ModalActions from '../actions/modal';

function mapStateToProps(state) {
   return {
      lang: state.language,
      courses: state.courses,
      theme: state.theme
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...ModalActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(DraftsPage);
