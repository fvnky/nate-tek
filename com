npm install --save-dev typescript@3.2.2
npm install --save-dev @babel/core@7.2.0
npm install --save-dev @babel/cli@7.2.0
npm install --save-dev @babel/plugin-proposal-class-properties@7.2.1
yarn add --save-dev @babel/plugin-proposal-object-rest-spread@7.2.0
yarn add --save-dev @babel/preset-env@7.2.0
yarn add --save-dev @babel/preset-typescript@7.1.0