export interface IFormTrueFalse {
   type: string;
   form: ITrueFalse;
}

export interface ITrueFalse {
   question: string;
   answer: boolean;
}
