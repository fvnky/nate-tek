import React from 'react';
import cx from 'classnames';
import styles from './HeadingButton.less';

interface IProps {
   theme: string;
   heading: number;
   callback?: any;
   isActive?: (level: number) => boolean;
}

export default function HeadingButton(props: IProps) {
   const {
      theme,
      heading,
      callback = () => null,
      isActive = () => false
   } = props;

   function onMouseDown(event: React.MouseEvent) {
      event.preventDefault();
      event.stopPropagation();
      callback(heading);
   }

   let fontWeightClass;
   // const cb = callback()
   switch (heading) {
      case 1:
         fontWeightClass = styles.one;
         break;
      case 2:
         fontWeightClass = styles.two;
         break;
      case 3:
         fontWeightClass = styles.three;
         break;
      case 4:
         fontWeightClass = styles.four;
         break;
      default:
         fontWeightClass = styles.four;
   }
   return (
      <span
         role="button"
         className={styles.iconWrapper}
         onMouseDown={onMouseDown}
      >
         <span
            className={`${cx(theme, styles.textIcon)} ${fontWeightClass} ${
               isActive(heading) ? styles.textIconActive : ''
            }`}
         >
            H{heading}
         </span>
      </span>
   );
}
