import _ from 'lodash';
import { IActionFolder } from './types';
import { IFolder } from '../typescript';

export const ADD_FOLDER = 'add_folder';
export const FETCH_FOLDER = 'fetch_folder';
export const UPDATE_FOLDER = 'update_folder';
export const REMOVE_FOLDER = 'remove_folder';

export function folders(state: IFolder[] = [], action: IActionFolder) {
   switch (action.type) {
      case ADD_FOLDER:
         return _.union(state, [action.payload]);
      case FETCH_FOLDER:
         return action.payload;
      case UPDATE_FOLDER: {
         const ref = _.clone(state);
         const folder = _.find(ref, { _id: action.payload._id });

         _.assign(folder, action.payload);
         return ref;
      }
      case REMOVE_FOLDER: {
         const ref = _.clone(state);

         _.remove(ref, { _id: action.payload._id });
         return ref;
      }
      default: {
         // const ref = _.clone(state);

         return state;
      }
   }
}
