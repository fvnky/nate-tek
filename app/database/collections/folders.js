import _ from "lodash";

import { Store } from "../factory";
import { FolderSchema } from "../schemas";
import { UPDATE_SUBJECT } from "../../reducers/subjects";
import { SUBJECT, FOLDER, COURSE, QUIZ } from "../constants";

import {
   ADD_FOLDER,
   FETCH_FOLDER,
   UPDATE_FOLDER,
   REMOVE_FOLDER
} from "../../reducers/folders";

import {
   timestamp,
   generateId,
   sync,
   parseData,
   parseDoc,
   parseUpdate,
   isNotFound,
   findAllChildren
} from "../utils";

import {
   InvalidParameterFormat,
   FolderAlreadyExists,
   FolderNotFound,
   InvalidReference
} from "../errors";

// TODO: method getSubject
// TODO: method getParent

// ================================================ //
// ==================== Create ==================== //
// ================================================ //

function create(data) {
   console.log("create");
   return new Promise((ok, ko) => {
      const store = new Store();
      const mandatory = ["name", "parent.collection", "parent.id"];

      parseData(data, mandatory)
         .then(parsed => {
            parsed._id = generateId();
            parsed.createdAt = timestamp();
            parsed.updatedAt = parsed.createdAt;
            parsed.collection = FOLDER;
            return this._main.put(parsed);
         })
         .then(res => store.add("folderId", res.id))
         .then(id => this._main.get(id))
         .then(folder => parseDoc(FolderSchema, folder))
         .then(folder => isDuplicateFolderName.call(this, folder))
         .then(folder => this._main.put(folder))
         .then(res => this._main.get(res.id))
         .then(folder => {
            if (folder.parent.collection === FOLDER)
               return this.folders.findById(folder.parent.id);

            if (folder.parent.collection === SUBJECT)
               return this.subjects.findById(folder.parent.id);

            throw new InvalidReference(
               `The reference to collection '${
                  folder.parent.collection
               }' is not allowed`
            );
         })
         .then(() => this._main.get(store.folderId))
         .then(folder => {
            this.dispatch({ type: ADD_FOLDER, payload: folder });
            return folder;
         })
         .catch(
            err =>
               new Promise((next, spread) => {
                  if (!store.folderId) return spread(err);

                  this._main
                     .get(store.folderId)
                     .then(doc => this._main.remove(doc._id, doc._rev))
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .then(ok)
         .catch(ko);
   });
}

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
   return new Promise((ok, ko) => {
      this._main
         .find({ selector: { collection: FOLDER } })
         .then(res => {
            // console.warn('Queries Folders :', res.docs);
            this.dispatch({ type: FETCH_FOLDER, payload: res.docs });
            ok(res.docs);
         })
         .catch(ko);
   });
}

function findById(id) {
   return new Promise((ok, ko) => {
      if (!_.isString(id))
         return ko(
            new InvalidParameterFormat(`The id parameter is not a String`)
         );

      this._main
         .get(id)
         .then(doc => {
            if (doc.collection !== FOLDER)
               throw new FolderNotFound(
                  `The document's id '${doc._id}' does not reference a folder`
               );

            this.dispatch({ type: UPDATE_FOLDER, payload: doc });
            ok(doc);
         })
         .catch(err => {
            if (isNotFound(err))
               throw new FolderNotFound(
                  `The folder '${id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

function update(doc) {
   return new Promise((ok, ko) => {
      const store = new Store();
      const fields = [];

      parseDoc(FolderSchema, doc)
         .then(parsed => this._main.get(parsed._id))
         .then(old => parseUpdate(old, doc, fields))
         .then(folder => isDuplicateFolderName.call(this, folder))
         .then(folder => store.add("folder", folder))
         .then(folder => this._main.get(folder.parent.id))
         .then(parent => store.add("parent", parent))
         .then(() => {
            const { folder, parent } = store;
            if (
               folder._id === parent._id ||
               (parent.collection !== SUBJECT && parent.collection !== FOLDER)
            )
               throw new InvalidParameterFormat(
                  `The parameter to update ${folder.id} are not compatible`
               );
            folder.updatedAt = timestamp();
            return this._main.put(folder);
         })
         .then(res => this._main.get(res.id))
         .then(folder => {
            this.dispatch({ type: UPDATE_FOLDER, payload: folder });
            return folder;
         })
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               if (store.parent)
                  throw new FolderNotFound(
                     `The parent ${doc.parent.id} could not be found`
                  );
               else
                  throw new FolderNotFound(
                     `The folder '${doc._id}' could not be found`
                  );
            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Delete ==================== //
// ================================================ //

function remove(data) {
   console.log("remove");
   return new Promise((ok, ko) => {
      const store = new Store();

      parseData(_.pick(data, ["_id"]), ["_id"])
         .then(parsed => store.add("folderId", parsed._id))
         .then(parsedId => this._main.get(parsedId))
         .then(folder => {
            if (folder.collection !== FOLDER)
               throw new FolderNotFound(
                  `The document's id '${
                     folder._id
                  }' does not reference a folder`
               );
            return folder;
         })
         .then(folder =>
            this._main.find({
               selector: {
                  "parent.collection": FOLDER,
                  "parent.id": folder._id
               }
            })
         )
         .then(res => {
            console.log("res of remove Folder: ", res);
            return res.docs;
         })
         .then(children => {
            const task = function(child, index, next) {
               if (child.collection === FOLDER)
                  this.folders
                     .remove(child)
                     .then(() => next())
                     .catch(next);
               else if (child.collection === COURSE)
                  this.courses
                     .remove(child)
                     .then(() => next())
                     .catch(next);
               else if (child.collection === QUIZ)
                  this.quizs
                     .remove(child)
                     .then(() => next())
                     .catch(next);
               else next();
            };

            return sync(children, task.bind(this));
         })
         .then(() => this._main.get(store.folderId))
         .then(({ _id, _rev }) => {
            const promises = [
               this._main.remove(_id, _rev),
               this._cloud.remove(_id, _rev)
            ];

            return Promise.all(promises);
         })
         .then(() => this.dispatch({ type: REMOVE_FOLDER, payload: data }))
         .then(() => ok({ success: true }))
         .catch(err => {
            if (isNotFound(err))
               throw new FolderNotFound(
                  `The folder '${data._id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

export default function() {
   return {
      create: create.bind(this),
      findAll: findAll.bind(this),
      findById: findById.bind(this),
      update: update.bind(this),
      remove: remove.bind(this)
   };
}

// =============================================== //
// ==================== Utils ==================== //
// =============================================== //

function isDuplicateFolderName(folder) {
   return new Promise((ok, ko) => {
      this._main
         .get(folder.parent.id)
         .then(parent =>
            findAllChildren.call(this, parent._id, parent.collection)
         )
         .then(children => {
            if (_.isEmpty(children)) return ok(folder);

            if (
               _.some(
                  _.map(
                     children,
                     child =>
                        child._id !== folder._id && child.name === folder.name
                  )
               )
            )
               throw new FolderAlreadyExists(
                  `The folder's name '${folder.name}' already exists`
               );

            ok(folder);
         })
         .catch(ko);
   });
}
