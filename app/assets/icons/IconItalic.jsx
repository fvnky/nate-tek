import * as React from 'react';

const IconItalic = props => (
   <svg viewBox="0 0 100 100" {...props}>
      <g>
         <path d="M86.35,10c0,3.31-2.69,6-6,6h-7.87L40.77,84h6.27c3.32,0,6,2.69,6,6s-2.68,6-6,6H19.65c-3.31,0-6-2.69-6-6s2.69-6,6-6h7.87   l31.71-68h-6.27c-3.32,0-6-2.69-6-6s2.68-6,6-6h27.39C83.66,4,86.35,6.69,86.35,10z" />
      </g>
   </svg>
);

export default IconItalic;
