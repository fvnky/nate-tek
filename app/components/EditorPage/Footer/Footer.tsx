import * as React from 'react';
import cx from 'classnames';
import ZoomSlider from '../ZoomSlider/ZoomSlider';

import styles from './Footer.less';

import { ICourse, EditorTab } from '../../../typescript';

function timeDifference(current, previous) {
   const msPerMinute = 60 * 1000;
   const msPerHour = msPerMinute * 60;
   const msPerDay = msPerHour * 24;
   const msPerMonth = msPerDay * 30;
   const msPerYear = msPerDay * 365;

   const elapsed = current - previous;

   if (elapsed < msPerMinute) {
      // return `${Math.round(elapsed / 1000)} seconds ago`;
      return 'less than a minute ago';
   }
   if (elapsed < msPerHour) {
      return `${Math.round(elapsed / msPerMinute)} minutes ago`;
   }
   if (elapsed < msPerDay) {
      return `${Math.round(elapsed / msPerHour)} hours ago`;
   }
   if (elapsed < msPerMonth) {
      return `${Math.round(elapsed / msPerDay)} days ago`;
   }
   if (elapsed < msPerYear) {
      return `${Math.round(elapsed / msPerMonth)} months ago`;
   }
   return `${Math.round(elapsed / msPerYear)} years ago`;
}

function getLastSavedOrCreatedAt(course: ICourse) {
   if (course.updatedAt) return course.updatedAt;
   return course.createdAt;
}

interface IProps {
   courses: ICourse[];
   setZoom: (value: number) => void;
   theme: string;
   zoom: number;
   activeTab?: EditorTab;
   editor: any;
}

export default class Footer extends React.Component<IProps> {
   getWordCount = () => {
      const { editor } = this.props;
      if (editor) {
         const text = editor.getEntireText();
         if (text === '') return 0;
         return text.trim().split(/\s+./).length;
      }
      return '';
   };

   getLastSavedProse = () => {
      const { activeTab, courses } = this.props;
      if (!activeTab) return 'No active tab';
      const course = courses.find(c => c._id === activeTab.id);
      if (!course) return 'Course not found';
      return `Last saved ${timeDifference(
         Date.now(),
         getLastSavedOrCreatedAt(course)
      )}`;
   };

   render() {
      const { theme, setZoom, zoom, editor } = this.props;

      const wordCount = this.getWordCount();

      return (
         <div className={cx(theme, styles.footer)}>
            <div className={cx(theme, styles.wordsCount)}>
               {this.getLastSavedProse()}
            </div>
            <div
               className={cx(theme, styles.wordsCount)}
            >{`Words: ${this.getWordCount()}`}</div>
            <ZoomSlider theme={theme} setZoom={setZoom} zoom={zoom} />
         </div>
      );
   }
}
