import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Topbar from '../components/Topbar/Topbar.tsx';
import * as UserActions from '../actions/user';
import * as SubjectsActions from '../actions/subjects';
import * as FoldersActions from '../actions/folders';
import * as ModalActions from '../actions/modal';

function mapStateToProps(state) {
   return {
      folders: state.folders,
      subjects: state.subjects,
      lang: state.language,
      router: state.router,
      theme: state.theme
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...UserActions,
         ...SubjectsActions,
         ...FoldersActions,
         ...ModalActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(Topbar);
