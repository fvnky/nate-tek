import React, { useEffect, useState } from 'react';
import cx from 'classnames';
import { CSSTransition } from 'react-transition-group';
import withTheme from '../../../../withTheme';

import InsertMenuComponent from './InsertMenuComponent';
import IconPlusButton from '../../../../../assets/icons/IconPlusButton';

import styles from './PlusButton.less';

interface IProps {
   theme: string;
   editor: any;
   value: any;

}

const PlusButton: React.FC<IProps> = ({ editor, theme }) => {
   let plusButtonRef = React.createRef<HTMLDivElement>();
   const [show, setShow] = useState(false);
   const [expanded, setExpanded] = useState(false);

   const toggleOpen = () => {
      setExpanded(!expanded);
   };

   const close = () => {
      setExpanded(false);
   };

   const updateButtonPosition = () => {
      const plusButtonPNode = document.getElementById("plus-button-p-node");

      if (plusButtonPNode) {
         if (show && plusButtonRef && plusButtonRef.current.style.top !== plusButtonPNode.offsetTop + 'px') {
            plusButtonRef.current.style.top = plusButtonPNode.offsetTop + 'px';
            plusButtonRef.current.style.left = plusButtonPNode.offsetLeft + 'px';
            // console.warn('setting expanded false')
            setExpanded(false);
         } else if (!show) {
            plusButtonRef.current.style.top = plusButtonPNode.offsetTop + 'px';
            plusButtonRef.current.style.left = plusButtonPNode.offsetLeft + 'px';
            // console.warn('setting expanded false 2')
            // setExpanded(false);
            setShow(true)
         }
      } else if (!plusButtonPNode) {
         setShow(false)
      }
   }

   useEffect(() => {
      updateButtonPosition()
   })

   return (
      // <CSSTransition
      //    in={show}
      //    appear={true}
      //    timeout={200}
      //    classNames={{
      //       appear: styles.appear,
      //       appearActive: styles.appearActive,
      //       enter: styles.enter,
      //       enterActive: styles.enterActive,
      //       enterDone: styles.enterDone,
      //       exit: styles.exit,
      //       exitActive: styles.exitActive,
      //       exitDone: ''
      //    }}
      //    onExit={close}
      // >
         <div className={cx(styles.layout, show || expanded ? "" : styles.hidden)}  ref={plusButtonRef}>
            <InsertMenuComponent show={expanded} editor={editor} theme={theme} closeMenu={close}/>
            <IconPlusButton
               className={cx(theme, styles.plusButton, expanded ? styles.open : '')}
               onClick={toggleOpen}
            />
         </div>
      // </
      // CSSTransition
      // >
   );
};
export default withTheme(PlusButton);
