// Name
export const QUIZ = 'quiz';

// Attributes
export const QUESTIONS = 'questions';
export const FORM = 'form';
export const TYPE_FORM = 'type';
export const SCORE = 'score';
export const QUESTION = 'question';
export const ANSWER = 'answer';
