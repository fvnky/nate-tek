import * as React from 'react';

// Component
import Modal from '../Base/Modal';

// Constant
import { SUBJECT, FOLDER, COURSE, QUIZ } from '../../database/constants/index';
import { COLLECTION_DB } from '../../constants/models';

// typescript
import {
   IDatabase,
   UpdateCourse,
   UpdateFolder,
   ILanguage,
   IDbDocument,
   ToastOptions,
   ICourse,
   IFolder,
   IQuiz
} from '../../typescript';

interface IProps {
   showToast: (options: ToastOptions) => void;
   id: string;
   parentId: string;
   db: IDatabase;
   updateCourse: UpdateCourse;
   updateFolder: UpdateFolder;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   children: any;
   parent: IDbDocument | null;
   title: string;
}

export default class ModalConfirmMove extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);
      this.state = {
         children: null,
         parent: null,
         title: ''
      };
   }

   componentDidMount() {
      const { id, db, parentId, handleDisplayModal } = this.props;

      db._main
         .get(id)
         .then((children: any) => {
            const { collection } = children;

            if (
               collection !== COURSE &&
               collection !== FOLDER &&
               collection !== QUIZ
            )
               throw new Error(
                  `Move is only possible for FOLDER, COURSE or Quiz`
               );
            this.setState({ children, title: children.name });
         })
         .then(() => db._main.get(parentId))
         .then((parent: IDbDocument) => {
            const { collection } = parent;

            if (collection !== SUBJECT && collection !== FOLDER)
               throw new Error(`Move is only possible in FOLDER or SUBJECT`);
            this.setState({ parent });
         })
         .catch((err: any) => {
            handleDisplayModal();
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   }

   handleOk = () => {
      const { handleDisplayModal, showToast, db } = this.props;
      const { children, parent } = this.state;

      if (!children || !parent || children._id === parent._id)
         return handleDisplayModal();

      const { collection } = children;
      children.parent.id = parent._id;
      children.parent.collection = parent.collection;
      if (
         collection === COURSE ||
         collection === FOLDER ||
         collection === QUIZ
      ) {
         db[COLLECTION_DB[collection]]
            .update(children)
            .then(() => handleDisplayModal())
            .then(() => showToast({ text: 'Moved', iconName: 'check' }))
            .catch(err => console.error(err));
      } else handleDisplayModal();
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;
      const { title, parent } = this.state;

      if (!parent) return null;

      return (
         <Modal
            theme={theme}
            title={`${lang.update} "${title}" in ${parent.name}?`}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.update}
            cancelText={lang.cancel}
         />
      );
   }
}
