import * as React from "react";

const IconCheck = props => (
   <svg viewBox="0 0 100 100" {...props}>
      <path d="M29.052,52.905c-3.67-3.67,1.529-9.174,5.2-5.81l10.4,9.786L65.443,34.862c3.67-3.669,9.175,1.529,5.811,5.2L47.4,65.138a3.875,3.875,0,0,1-5.505.3Z" transform="translate(0)" />
      <path d="M49.847,0A50,50,0,1,1,0,49.847,49.922,49.922,0,0,1,49.847,0Zm0,7.645a42.355,42.355,0,1,0,42.508,42.2A42.287,42.287,0,0,0,49.847,7.645Z" transform="translate(0)" />
   </svg>
);

export default IconCheck;
