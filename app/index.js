import * as React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Root from './containers/Root';
import { configureStore, history } from './store/configureStore';

import './styles/app.global.css';
import './styles/animations.global.css';
import './styles/fonts.global.css';
import './styles/app.global.less';

const store = configureStore();

// require('electron-disable-file-drop');

render(
   <AppContainer>
      <Root store={store} history={history} />
   </AppContainer>,
   document.getElementById('root')
);

if (module.hot) {
   module.hot.accept('./containers/Root', () => {
      const NextRoot = require('./containers/Root').default;
      render(
         <AppContainer>
            <Root store={store} history={history} />
         </AppContainer>,
         document.getElementById('root')
      );
   });
}

export const _store = store ? "haha" : "hihi";
