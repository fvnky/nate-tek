import * as React from 'react';

const IconFolderFill = props => (
   <svg viewBox="0 0 100 100" {...props}>
		<path d="M88.9,18.8h-51l-4-4.9c-1.9-2.1-4.6-3.4-7.5-3.4H9.7c-4.8,0-8.7,3.9-8.7,8.7v61.6c0,4.8,3.9,8.7,8.7,8.7H89
			c4.8,0,8.7-3.9,8.7-8.7V27.4C97.6,22.6,93.7,18.8,88.9,18.8z"/>
   </svg>
);

export default IconFolderFill;
