import * as React from 'react';

// Component
import Modal from '../Base/Modal';

// Utils
import { capitalize, errorModal } from '../utils';

// Style
import styles from './ModalUpdate.less';

// typescript
import { UpdateUser, IUser, ILanguage } from '../../typescript';

interface IProps {
   user: IUser;
   updateUser: UpdateUser;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   mail: string;
}

export default class ModalUpdateMail extends React.Component<IProps, IState> {
   mailInput: React.RefObject<HTMLInputElement> = React.createRef<
      HTMLInputElement
   >();

   constructor(props: IProps) {
      super(props);
      const { user } = this.props;

      if (!user) return errorModal(this.constructor.name, user);
      this.state = {
         mail: user.email
      };
   }

   componentDidMount() {
      this.mailInput.current.focus();
   }

   handleChangeMail = (event: React.ChangeEvent<HTMLInputElement>) => {
      const {
         target: { value }
      } = event;

      this.setState({ mail: capitalize(value) });
   };

   handleOk = () => {
      const { handleDisplayModal, user, updateUser } = this.props;
      const { mail } = this.state;

      if (!user) return errorModal(this.constructor.name, user);
      user.email = mail;
      updateUser(user);
      handleDisplayModal();
   };

   renderInputMail = () => {
      const { lang } = this.props;
      const { mail } = this.state;

      return (
         <div>
            <span className={styles.nameModal}>{lang.email}</span>
            <input
               ref={this.mailInput}
               type="text"
               value={mail}
               onChange={this.handleChangeMail}
               className={styles.input}
               placeholder={lang.enterTitle}
            />
         </div>
      );
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;

      return (
         <Modal
            theme={theme}
            title={lang.changeAccount}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.update}
         >
            {this.renderInputMail()}
         </Modal>
      );
   }
}
