import * as React from 'react';
import cx from 'classnames';

import styles from './PrivacyPolicy.less';

// Typescript
import { ILanguage } from '../../typescript';

const { shell } = require('electron');

interface IProps {
   lang: ILanguage;
   acceptTerms: () => void;
   declineTerms: () => void;
}

const PrivacyPolicy: React.FC<IProps> = ({
   lang,
   acceptTerms,
   declineTerms
}) => {
   return (
      <div>
         <div className={cx('Light', styles.paragraph)}>
            <span>
               {lang.privacyPart1} <br />
               <br /> {lang.privacyPart2} <br />
               <br /> {lang.privacyPart3}
            </span>
            <span
               role="link"
               className={styles.termsLink}
               onClick={() => shell.openExternal('http://www.google.com')}
            >
               {' '}
               <u>{lang.privacyPart4} </u>
            </span>
            <span> {lang.privacyPart5} </span>
         </div>
         <div className={styles.row}>
            <button
               className={cx('Light', styles.button, styles.agree)}
               onClick={acceptTerms}
            >
               {lang.agree.toUpperCase()}
            </button>
            <button
               className={cx('Light', styles.button)}
               onClick={declineTerms}
            >
               {lang.disagree.toUpperCase()}
            </button>
         </div>
      </div>
   );
};

export default PrivacyPolicy;
