import { Dispatch as ReduxDispatch, Store as ReduxStore } from 'redux';
import {
   ICourse,
   IFolder,
   ISubject,
   ILanguage,
   IUser,
   IDatabase,
   IDraft,
   IQuiz,
   IDictionary,
   ITodo,
   ISchedule
} from '../typescript';

export interface IsubjectsStateType {
   subjects: ISubject[];
   folders: IFolder[];
   courses: ICourse[];
   router: any;
   themeName: string;
   database: any;
}

export interface IActionDraft {
   type: string;
   payload: IDraft;
}

export interface IActionCourse {
   type: string;
   payload: ICourse;
}

export interface IActionQuiz {
   type: string;
   payload: IQuiz;
}

export interface IActionSchedule {
   type: string;
   payload: ISchedule;
}

export interface IActionDictionary {
   type: string;
   payload: IDictionary;
}

export interface IActionTodo {
   type: string;
   payload: ITodo;
}

export interface IActionDefinition {
   type: string;
   payload: any;
}

export interface IAction {
   type: string;
   payload: any;
}

export interface IPayloadModal {
   currentModal: string;
   propsModal: any;
}

export interface IActionModal {
   type: string;
   payload: IPayloadModal;
}

export interface IActionSubject {
   type: string;
   payload: ISubject;
}

export interface IActionFolder {
   type: string;
   payload: IFolder;
}

export interface IActionLanguage {
   type: string;
   payload: string;
}

export interface IActionUser {
   type: string;
   payload: IUser;
}

export interface IActionDatabase {
   type: string;
   payload: any;
}

export interface IActionTheme {
   type: string;
   payload: string;
}

export interface IActionSpinner {
   type: string;
   payload: boolean;
}

export type GetState = () => IsubjectsStateType;

export type Dispatch = ReduxDispatch<IAction>;

export type Store = ReduxStore<GetState, IAction>;
