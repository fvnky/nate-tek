import * as React from 'react';
import IconEditorOutline from './IconEditorOutline';
import IconEditorFill from './IconEditorFill';
import IconNateOutline from './IconNateOutline';
import IconNateFill from './IconNateFill';
import IconProfileOutline from './IconProfileOutline';
import IconBetaFeedback from './IconBetaFeedback'
import IconProfileFill from './IconProfileFill';
import IconSettingsOutline from './IconSettingsOutline';
import IconSettingsFill from './IconSettingsFill';
import IconDev from './IconDev';
import IconResourcesOutline from './IconResourcesOutline';
import IconResourcesFill from './IconResourcesFill';
import IconCalendar from '../IconCalendar';
import IconCheck from '../IconCheck';

const iconsSidebar = {
   resources: { outline: <IconResourcesOutline />, fill: <IconResourcesFill />},
   library: { outline: <IconNateOutline />, fill: <IconNateFill />},
   editor: { outline: <IconEditorOutline />, fill: <IconEditorFill />},
   settings: { outline: <IconSettingsOutline />, fill: <IconSettingsFill />},
   betaFeedback: { outline: <IconBetaFeedback />, fill: <IconBetaFeedback />},
   dev: { outline: <IconDev />, fill: <IconDev />},
   profile: { outline: <IconProfileOutline />, fill: <IconProfileFill />},
   calendar: { outline: <IconCalendar />, fill: <IconCalendar />}
};

export { iconsSidebar };
