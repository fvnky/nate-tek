import * as React from 'react';
import cx from 'classnames';
import { RouteComponentProps } from 'react-router';
import { orderBy } from 'lodash';

// Component
import IconMore from '../../assets/icons/IconMore';
import IconPlusButton from '../../assets/icons/IconPlusButton';
import {
   fillDocument,
   EDIT_POPUP,
   DELETE_POPUP,
   ONCLICK_LIST,
   POPUP_LIST,
   ICON_LIST,
   NAME_LIST,
   LAST_UPDATED_LIST,
   POPUP_TRIGGER_LIST
} from '../Base/List/Descriptor';
import List from '../Base/List/List';

// Constant
import { ALL } from '../../constants/tabs';
import { MODAL_CREATE_SUBJECT } from '../../constants/modal';
import routes from '../../constants/routes.json';

// Style
import styles from './SubjectsPage.less';

// typescript
import {
   ISubject,
   ICourse,
   IFolder,
   ToggleModal,
   ILanguage,
   IListItem
} from '../../typescript';

const LIST_ACTIONS = [ONCLICK_LIST];
const LIST_COLUMNS = [ICON_LIST, NAME_LIST, LAST_UPDATED_LIST, POPUP_LIST];
const LIST_POPUP = [EDIT_POPUP, DELETE_POPUP];

interface IProps extends RouteComponentProps {
   subjects: ISubject[];
   courses: ICourse[];
   folders: IFolder[];
   toggleModal: ToggleModal;
   lang: ILanguage;
   theme: string;
   resourcesNavigation: { lastTab: string; lastBrowserPath: string };
   setLastActiveNavigationTab: (tab: string) => void;
   setLastActiveBrowserPath: (path: string) => void;
}

export default class SubjectsPage extends React.Component<IProps> {
   componentDidMount() {
      const {
         setLastActiveNavigationTab,
         setLastActiveBrowserPath,
         resourcesNavigation,
         history
      } = this.props;
      if (
         resourcesNavigation.lastTab !== ALL ||
         resourcesNavigation.lastTab !== history.location.pathname
      )
         // this.animateChildrenList();
         setLastActiveNavigationTab(ALL);
      setLastActiveBrowserPath(routes.SUBJECTS);
   }

   // animateChildrenList = () => {
   //    const childrenList = document.getElementById('childrenList');
   //    childrenList.className = cx(
   //       styles.childrenList,
   //       styles.listBeforeTransition
   //    );
   //    setTimeout(() => {
   //       childrenList.className = cx(
   //          styles.childrenList,
   //          styles.listAfterTransition,
   //          styles.listTransition
   //       );
   //    }, 0);
   //    setTimeout(() => {
   //       childrenList.className = styles.childrenList;
   //    }, 400);
   // };

   handleCreateSubject = () => {
      const { toggleModal } = this.props;

      toggleModal(MODAL_CREATE_SUBJECT, {});
   };

   subjectDescriptor = () => {
      const { subjects, history, theme, folders, courses } = this.props;
      const orderedSubjects = orderBy(subjects, 'name', 'asc');
      const descriptorSubjects: IListItem[] = [];
      orderedSubjects.forEach((subject: ISubject) => {
         const LIST_ITEM = LIST_COLUMNS.concat(LIST_ACTIONS);
         const descriptor = fillDocument(
            subject,
            history,
            LIST_POPUP,
            LIST_ITEM,
            [folders, courses]
         );
         descriptor.className = styles.subjectRow;
         descriptor.icon.className = styles.subjectIcon;
         descriptor.name.className = styles.subjectName;
         descriptor[LAST_UPDATED_LIST].className = styles.lastUpdated;
         descriptor.popup.className = styles.popupContainer;
         descriptor[POPUP_LIST][POPUP_TRIGGER_LIST] = (
            <IconMore className={styles.moreButton} />
         );
         descriptorSubjects.push(descriptor);
      });
      return [descriptorSubjects];
   };

   render() {
      const { theme, lang, subjects } = this.props;
      // console.warn(subjects)

      return (
         <>
            {subjects.length === 0 ? (
               <div className={cx(theme, 'empty-list')}>
                  {lang.emptySubjects}
               </div>
            ) : (
               <div id="childrenList" className={styles.subjectList}>
                  <List list={this.subjectDescriptor()} {...this.props} />
               </div>
            )}
            <IconPlusButton
               className={cx(theme, styles.addSubject)}
               onClick={this.handleCreateSubject}
            />
         </>
      );
   }
}
