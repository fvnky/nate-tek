import { QUIZ } from './quiz';
import { COURSE } from './course';
import { FOLDER } from './folder';
import { SUBJECT } from './subject';
import { DICTIONARY } from './dictionary';
import { NATE_TODO } from './todo';
import { SCHEDULE } from './schedule';

// Attributes
export const LIBRARY = 'library';
export const USER_DB = 'user';

export const QUIZS_DB = 'quizs';
export const COURSES_DB = 'courses';
export const FOLDERS_DB = 'folders';
export const SUBJECTS_DB = 'subjects';
export const DICTIONARY_DB = 'dictionaries';
export const TODO_DB = 'todos';
export const SCHEDULES_DB = 'schedules';

// Methods on Attribute_DB
export const FIND_ALL = 'findAll';
export const FIND_BY_ID = 'findById';
export const CREATE = 'create';
export const UPDATE = 'update';
export const REMOVE = 'remove';

// Maps collection
export const COLLECTION_DB = {
   [QUIZ]: QUIZS_DB,
   [COURSE]: COURSES_DB,
   [FOLDER]: FOLDERS_DB,
   [SUBJECT]: SUBJECTS_DB,
   [DICTIONARY]: DICTIONARY_DB,
   [NATE_TODO]: TODO_DB,
   [SCHEDULE]: SCHEDULES_DB
};
