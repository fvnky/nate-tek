import * as React from "react";
import cx from "classnames";
import { connect } from "react-redux";

// Style
import styles from "./Content.less";

interface IProps {
   children: React.ReactNode | React.ReactNode[];
   theme: string;
}

class Content extends React.Component<IProps> {
   render() {
      const { children, theme } = this.props;

      return <div className={cx(theme, styles.layout)}>{children}</div>;
   }
}

function mapStateToProps(state) {
   return {
      theme: state.theme
   };
}
export default connect(mapStateToProps)(Content);
