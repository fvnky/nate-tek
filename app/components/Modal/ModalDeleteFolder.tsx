import * as React from 'react';

// Component
import Modal from '../Base/Modal';

// typescript
import { IDatabase, IFolder, RemoveFolder, ILanguage } from '../../typescript';

interface IProps {
   id: string;
   db: IDatabase;
   removeFolder: RemoveFolder;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   folder: IFolder | null;
   title: string;
}

export default class ModalDeleteFolder extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);

      this.state = {
         folder: null,
         title: ''
      };
   }

   componentDidMount() {
      const { id, db } = this.props;

      db.folders
         .findById(id)
         .then((folder: IFolder) =>
            this.setState({ folder, title: folder.name })
         )
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   }

   handleOk = () => {
      const { handleDisplayModal, removeFolder } = this.props;
      const { folder } = this.state;

      if (!folder) return;
      removeFolder(folder)
         .then(() => {
            console.log('Remove folder');
            handleDisplayModal();
         })
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;
      const { title } = this.state;

      return (
         <Modal
            theme={theme}
            titleLess={true}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.delete}
            cancelText={lang.cancel}
         >
            {lang.deleteItem}
            {title} ?
         </Modal>
      );
   }
}
