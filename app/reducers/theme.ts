import { SET_THEME } from '../actions/theme';

// Constant
import { LIGHT_THEME, DARK_THEME } from '../constants/theme';

// Typescript
import { IActionTheme } from './types';

export function theme(state: string = LIGHT_THEME, action: IActionTheme) {
   switch (action.type) {
      case SET_THEME:
         return action.payload;
      default:
         return state;
   }
}
