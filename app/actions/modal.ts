import { Dispatch } from '../reducers/types';

const TOGGLE_MODAL = 'TOGGLE_MODAL';

const toggleModal = (currentModal: string, propsModal: any) => {
   return (dispatch: Dispatch) => {
      dispatch({
         type: TOGGLE_MODAL,
         payload: { currentModal, propsModal }
      });
   };
};

export { TOGGLE_MODAL, toggleModal };
