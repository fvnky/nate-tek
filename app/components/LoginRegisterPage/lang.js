export default {
   welcome: {
      en: 'Welcome to nate for desktop',
      fr: 'Bienvenue sur nate.',
      de: 'Wilkommen bei nate.'
   },
   welcomeDescription: {
      en:
         'Nate works fully offline, but you need to log in or sign up to sync your content.',
      fr:
         'Nate fonctionne en hors ligne, mais vous devez vous connecter ou vous inscrire afin de synchroniser le contenu.',
      de:
         'Nate funktioniert vollständig offline, aber Sie müssen sich anmelden oder registrieren, um Ihre Inhalte zu synchronisieren.'
   },
   continueWithGoogle: {
      en: 'Continue with Google',
      fr: 'Continuer avec Google',
      de: 'Fortfahren mit Google'
   },
   or: {
      en: 'or',
      fr: 'ou',
      de: 'oder'
   },
   emailPlaceholder: {
      en: 'Enter your email address...',
      fr: 'Entrez votre adresse mail...',
      de: 'Geben sie ihre E-Mailadresse ein...'
   },
   wrongConfirmCode: {
      en: 'The code you have entered is incorrect.',
      fr: 'Le code entré est incorrect',
      de: 'Der eingegebene Code ist falsch.'
   }
};
