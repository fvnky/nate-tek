import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Toast from '../components/Toast/Toast.tsx';
import * as ToastActions from '../actions/toast';

function mapStateToProps(state) {
   return {
      toast: state.toast,
      theme: state.theme
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...ToastActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(Toast);
