import React from 'react';
import { RouteComponentProps, withRouter, match } from 'react-router';
import { History } from 'history';
// import delay from "delay";

// Constant
import { NONE } from '../../reducers/modals';
import ErrorBoundary from './ModalErrorBoundary';
import {
   MODAL_CREATE_FOLDER,
   MODAL_UPDATE_FOLDER,
   MODAL_DELETE_FOLDER,
   MODAL_CREATE_COURSE,
   MODAL_UPDATE_COURSE,
   MODAL_DELETE_COURSE,
   MODAL_CREATE_SUBJECT,
   MODAL_UPDATE_SUBJECT,
   MODAL_DELETE_SUBJECT,
   MODAL_MOVE_RESOURCE,
   MODAL_UPDATE_QUIZ,
   MODAL_DELETE_QUIZ,
   MODAL_CREATE_SCHEDULE
} from '../../constants/modal';

// Style
import styles from './ModalManager.less';

// typescript
import { IModal, ToggleModal, ILanguage } from '../../typescript';

// Modal
import ModalCropProfilePicture from './ModalCropProfilePicture';

import ModalCreateResource from './ModalCreateResource';
import ModalCreateFolder from './ModalCreateFolder';
import ModalDeleteFolder from './ModalDeleteFolder';
import ModalEditFolder from './ModalEditFolder';

import ModalCreateCourse from './ModalCreateCourse';
import ModalDeleteCourse from './ModalDeleteCourse';
import ModalEditCourse from './ModalEditCourse';

import ModalCreateSubject from './ModalCreateSubject';
import ModalDeleteSubject from './ModalDeleteSubject';
import ModalEditSubject from './ModalEditSubject';

import ModalDeleteQuiz from './ModalDeleteQuiz';
import ModalEditQuiz from './ModalEditQuiz';

import ModalUpdateMail from './ModalUpdateMail';
import ModalUpdateName from './ModalUpdateName';

import ModalSearch from './ModalSearch';

import ModalMoveResource from './ModalMoveResource';

import ModalConfirmMove from './ModalConfirmMove';

import ModalCreateSchedule from './ModalCalendar/ModalCreateCalendarEvent';

// Utils
import { errorModal } from '../utils';

export const modalDictionary = {
   ModalCreatefolder: ModalCreateFolder,
   ModalUpdatefolder: ModalEditFolder,
   ModalDeletefolder: ModalDeleteFolder,
   ModalCreatecourse: ModalCreateCourse,
   ModalUpdatecourse: ModalEditCourse,
   ModalDeletecourse: ModalDeleteCourse,
   ModalCreatesubject: ModalCreateSubject,
   ModalUpdatesubject: ModalEditSubject,
   ModalDeletesubject: ModalDeleteSubject,
   ModalMoveResource: ModalMoveResource,
   ModalDeletequiz: ModalDeleteQuiz,
   ModalUpdatequiz: ModalEditQuiz,
   ModalCreateSchedule: ModalCreateSchedule,
   ModalUpdateName,
   ModalUpdateMail,
   ModalSearch,
   ModalConfirmMove,
   ModalCropProfilePicture,
   ModalCreateResource
};

export interface IModalManager extends RouteComponentProps {
   modal: IModal;
   toggleModal: ToggleModal;
   lang: ILanguage;
   match: match;
   history: History;
   theme: string;
   // folders: IF
}

class ModalManager extends React.Component<IModalManager> {
   handleToggleModal = () => {
      const { toggleModal } = this.props;

      toggleModal(NONE, {});
   };

   animationDone = () => {
      // const { toggleModal } = this.props;
      // const { entering } = this.state;
      // if (entering === false) {
      //    toggleModal(NONE, {});
      // }
   };

   render() {
      const { modal } = this.props;

      if (!modal) return errorModal(this.constructor.name, modal);
      const { propsModal, currentModal } = modal;
      // console.log("CurrentModal :", currentModal);
      const ELEM: any = modalDictionary[currentModal];

      if (!ELEM) {
         if (currentModal !== NONE)
            console.error('Unknow Modal :', currentModal);
         return null;
      }

      return (
         <div className={styles.wrapper}>
            <ErrorBoundary {...this.props}>
               <ELEM
                  key={currentModal}
                  handleDisplayModal={this.handleToggleModal}
                  {...this.props}
                  {...propsModal}
               />
            </ErrorBoundary>
         </div>
      );
   }
}
export default withRouter(ModalManager);
