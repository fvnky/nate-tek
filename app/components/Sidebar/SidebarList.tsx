import * as React from 'react';

// Component
import SidebarItem from './SidebarItem';

// Constant
import routes from '../../constants/routes.json';

// Icon
import { iconsSidebar } from '../../assets/icons/Sidebar/IconsSidebar';

// Typescript
import { ILanguage } from '../../typescript';

const orderNames = [
   'resources',
   'editor',
   'library',
   'profile',
   'calendar',
   'settings'
   // 'betaFeedback',
   // 'dev'
];

const links = {
   resources: routes.RESOURCES,
   library: routes.LIBRARY_RECOMMENDED,
   editor: routes.EDITOR_LINK,
   profile: routes.PROFILE,
   calendar: routes.CALENDAR,
   settings: routes.SETTINGS
   // betaFeedback: routes.BETA_FEEDBACK,
   // dev: routes.DEV
};

interface IProps {
   lang: ILanguage;
   router: any;
   theme: string;
   collapse: boolean;
}

export default class SidebarList extends React.Component<IProps> {
   isActive = (link: string, location: string) => {
      const actualCategory = location.split('/')[1];
      const item = link.split('/')[1];

      if (item === actualCategory) return true;
      return false;
   };

   render() {
      const { lang, router } = this.props;

      return orderNames.map((item: string, index: number) => {
         const link: string = links[item];
         const icon = iconsSidebar[item];
         const name: string = lang[item];
         const active = this.isActive(link, router.location.pathname);

         return (
            <SidebarItem
               key={item}
               name={name}
               link={link}
               icon={icon}
               active={active}
               marginTop={index * 69}
               {...this.props}
            />
         );
      });
   }
}
