import { CustomError } from '../factory';

export class FolderAlreadyExists extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The folder already exists';
   }
}

export class FolderNotFound extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The folder could not be found';
   }
}

export class InvalidReference extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The reference is not an allowed collection';
   }
}

export class NoFolders extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'No folders could be found';
   }
}
