import React from 'react';
import cx from 'classnames';
import styles from './highlight.less';

export const HIGHLIGHT = 'highlight';

export const HIGHLIGHT_COLORS = {
   1: '#F889A2',
   2: '#FEED6D',
   3: '#E7F3A5',
   4: '#B2DFF9',
   5: '#CDC6F2'
};

export default function highlight(options = {}) {
   return {
      commands: {
         setHighlight(editor, type) {
            if (editor.value.inlines.some(node => node.type === HIGHLIGHT)) {
               if (
                  editor.value.inlines.some(
                     node =>
                        node.type === HIGHLIGHT &&
                        node.data.get('type') === type
                  )
               ) {
                  editor.unwrapInline(HIGHLIGHT);
                  return;
               } else if (editor.value.fragment.text === '') {
                  editor.setInlines({ type: HIGHLIGHT, data: { type } });
                  return;
               }
            }
            editor.wrapInline({ type: HIGHLIGHT, data: { type } }).focus();
         }
      },
      queries: {
         isHighlight(editor) {
            return editor.value.inlines.some(node => node.type === HIGHLIGHT);
         },
         isHighlightOfType(editor, type) {
            return editor.value.inlines.some(
               node => node.type === HIGHLIGHT && node.data.get('type') === type
            );
         }
      },
      renderNode(props, editor, next) {
         const { attributes, children, node } = props;
         const type = node.data.get('type');

         switch (node.type) {
            case HIGHLIGHT:
               return (
                  <span
                     className={cx(
                        styles.highlight,
                        styles[`highlight-${type}`]
                     )}
                     {...attributes}
                  >
                     {children}
                  </span>
               );
            default:
               return next();
         }
      }
   };
}
