import { SUBJECT } from '../constants';

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      name: { type: String, required: true },
      icon: { type: String, required: false },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      collection: SUBJECT
   }),
   fields: [
      '_id',
      '_rev',
      'name',
      'icon',
      'createdAt',
      'updatedAt',
      'collection'
   ]
};
