import * as React from 'react';
import cx from 'classnames';
import { RouteComponentProps, Redirect } from 'react-router';

import IconNateOutline from '../../assets/icons/Sidebar/IconNateOutline';
import IconGoogle from '../../assets/icons/IconGoogle';
import IconArrowRight from '../../assets/icons/IconArrowRight';
import { REGISTER } from './LoginRegisterPage';

import routes from '../../constants/routes.json';
import { EN, FR, DE } from '../../constants/language';
import lang from './lang.js';

// Style
import styles from './LoginPage.less';

// Typescript
import { ILanguage, IUser } from '../../typescript';
import { logInUser } from '../../actions/user';
import { capitalize } from '../utils';

interface IProps extends RouteComponentProps {
   language: string;
   db: any;
   setMail: (email: string) => void;
   setStep: (step: string) => void;
   setLanguage: (language: string) => void;
}

interface IState {
   user: any;
   email: string;
   confirmCode: string;
   askingForConfirmationCode: boolean;
   wrongConfirmCode: boolean;
}

const LoginPage: React.FC<IProps> = ({
   setLanguage,
   setStep,
   setMail,
   language,
   db,
   history,
   location,
   match
}) => {
   const [
      askingForConfirmationCode,
      setAskingForConfirmationCode
   ] = React.useState(false);
   const [email, setEmail] = React.useState('');
   const [confirmCode, setConfirmCode] = React.useState('');
   const [wrongConfirmCode, setWrongConfirmCode] = React.useState(false);

   React.useEffect(() => {
      document.addEventListener('keydown', handleKeyDown);
      return () => {
         document.removeEventListener('keydown', handleKeyDown);
      };
   });

   const sendValidation = () => {
      console.log("clicked")
      db.user
         .sendValidation(email)
         .then(() => {
            console.log("step 2")

            setAskingForConfirmationCode(true);
         })
         .catch(err => {
            if (err.name === 'NetworkError') {
               window.alert('You are not connected to internet');
            } else {
               window.alert('Something went wrong');
            }
            console.error('error', err);
         });
   };

   const logInUser = () => {
      db.user
         .logIn(email, confirmCode)
         .then(() => history.push(routes.SUBJECTS))
         .catch((err: any) => {
            if (err.name === 'InvalidCode') {
               setWrongConfirmCode(true);
            } else if (err.name === 'CodeNotFound') {
               // Votre code a expiré
               window.alert('Votre code a expiré');
            } else if (err.name === 'UserNotFound') {
               // REGISTER
               setMail(email);
               setStep(REGISTER);
               // history.push(routes.REGISTER);
            } else if (err.name === 'ValidationError') {
               window.alert('The email was already sent');
            } else if (err.name === 'NetworkError') {
               window.alert('You are not connected to internet');
            } else {
               window.alert('Something went wrong');
               // SOMETHING WENT WRONG
            }
            console.error('error', err);
         });
   };

   const handleKeyDown = event => {
      if (
         event.code === 'Enter' &&
         !askingForConfirmationCode &&
         email.length > 0
      ) {
         sendValidation();
      } else if (
         event.code === 'Enter' &&
         askingForConfirmationCode &&
         confirmCode.length > 0
      ) {
         logInUser();
      }
   };

   const renderLogin = () => {
      return (
         <>
            <div className={styles.flags}>
               <span className={styles.flag} onClick={() => setLanguage(EN)}>
                  {' '}
                  🇬🇧{' '}
               </span>
               <span className={styles.flag} onClick={() => setLanguage(FR)}>
                  {' '}
                  🇫🇷{' '}
               </span>
               {/* <span className={styles.flag} onClick={setLanguage(DE)}> 🇩🇪</span> */}
            </div>

            <div className={styles.row}>
               <IconNateOutline className={cx('Light', styles.iconNate)} />
               <div className={styles.column}>
                  <span className={cx('Light', styles.welcome)}>
                     {lang.welcome[language]}
                  </span>
                  <span className={cx('Light', styles.detail)}>
                     {lang.welcomeDescription[language]}
                  </span>
               </div>
            </div>
            <div
               style={{
                  width: '100%',
                  display: 'flex',
                  justifyContent: 'center'
               }}
            >
               <div className={styles.buttons}>
                  {/* <div role="button" className={styles.google}>
                        <IconGoogle className={cx("Light", styles.iconGoogle)} />
                        <span className={cx("Light", styles.continueWithGoogle)}>{lang.continueWithGoogle[language]}</span>
                     </div>
                     <span className={cx("Light", styles.or)}>{lang.or[language]}</span> */}
                  <div className={cx('Light', styles.email)}>
                     <input
                        className={cx('Light', styles.emailInput)}
                        placeholder={lang.emailPlaceholder[language]}
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                     />
                     {!askingForConfirmationCode && (
                        <IconArrowRight
                           className={cx(
                              'Light',
                              styles.iconArrowInCircleRight
                           )}
                           onClick={sendValidation}
                        />
                     )}
                  </div>
               </div>
            </div>
         </>
      );
   };

   const renderConfirmationCodeForm = () => {
      return (
         <div
            style={{ width: '100%', display: 'flex', justifyContent: 'center' }}
         >
            <div style={{ width: '320px' }}>
               <div className={cx('Light', styles.confirmCodeMessage)}>
                  <span>We just sent you a temporary login code.</span>
                  <span>Please check your inbox.</span>
               </div>
               <div
                  className={cx(
                     'Light',
                     styles.confirmCode,
                     wrongConfirmCode ? styles.wrongConfirmCodeInput : ''
                  )}
               >
                  <input
                     type="confirmCode"
                     className={cx('Light', styles.confirmCodeInput)}
                     placeholder="Enter login code"
                     value={confirmCode}
                     onChange={e => setConfirmCode(e.target.value)}
                  />
                  <IconArrowRight
                     className={cx('Light', styles.iconArrowInCircleRight)}
                     onClick={logInUser}
                  />
               </div>
               {wrongConfirmCode && (
                  <div className={styles.wrongConfirmCode}>
                     {lang.wrongConfirmCode[language]}
                  </div>
               )}
            </div>
         </div>
      );
   };

   return (
      <div className={styles.layout}>
         <div className={styles.column}>
            {renderLogin()}
            {askingForConfirmationCode && renderConfirmationCodeForm()}
         </div>
      </div>
   );
};

export default LoginPage;
