import {
   DICTIONARY,
   NAME_DICTIONARY,
   DEFINITION_DICTIONARY,
   SUBJECT_DICTIONARY
} from '../../constants/models/dictionary';

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      [DEFINITION_DICTIONARY]: { type: String, required: false },
      [NAME_DICTIONARY]: { type: String, required: true },
      [SUBJECT_DICTIONARY]: { type: String, required: true },
      collection: DICTIONARY
   }),
   fields: [
      '_id',
      '_rev',
      'createdAt',
      'updatedAt',
      DEFINITION_DICTIONARY,
      NAME_DICTIONARY,
      SUBJECT_DICTIONARY,
      'collection'
   ]
};
