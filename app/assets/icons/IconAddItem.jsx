import * as React from "react";

const IconAlignCenter = props => (
   <svg viewBox="0 0 100 100" {...props}>
      <g>
         <path d="M50.9,34.6c5.6,0,10.1-4.5,10.1-10.1s-4.5-10.1-10.1-10.1s-10.1,4.5-10.1,10.1S45.3,34.6,50.9,34.6z" />
         <path d="M89.6,53.8v-42c0-3.6-2.9-6.5-6.5-6.5H8.3c-3.6,0-6.5,2.9-6.5,6.5v56.5c0,3.6,2.9,6.5,6.5,6.5h44.3   c0.2,1.3,0.5,2.6,0.9,3.9c3.1,9.5,11.8,15.9,21.9,15.9c12.7,0,23-10.3,23-23C98.2,64.7,95.1,58.2,89.6,53.8z M6.8,68.4V11.8   c0-0.8,0.7-1.5,1.5-1.5h74.8c0.8,0,1.5,0.7,1.5,1.5v38.9c-2.9-1.3-6.1-2-9.3-2c-0.5,0-1,0-1.5,0.1l-9-8.7c-2.3-2.3-6-2.4-8.6-0.4   l-5.8,4.6l-9.7-10.6c-1.2-1.3-2.9-2.1-4.7-2.1c0,0,0,0-0.1,0c-1.8,0-3.5,0.7-4.7,2h0L10.7,55.2c-0.7,0.7-0.9,1.8-0.5,2.7   c0.4,0.9,1.3,1.5,2.3,1.5h43.4c-1.9,3.1-3.2,6.6-3.5,10.4H8.3C7.4,69.9,6.8,69.2,6.8,68.4z M75.3,89.7c-7.8,0-14.7-5-17.1-12.5   c-0.5-1.5-0.7-3.1-0.8-4.6c0-0.1,0-0.1,0-0.2c0-0.2,0-0.3-0.1-0.5c0,0,0-0.1,0-0.1c0-9.9,8.1-18,18-18c4,0,7.8,1.3,11,3.7   c4.4,3.4,7,8.6,7,14.2C93.2,81.6,85.2,89.7,75.3,89.7z" />
         <path d="M84.5,69.2h-6.8v-6.8c0-1.4-1.1-2.5-2.5-2.5s-2.5,1.1-2.5,2.5v6.8H66c-1.4,0-2.5,1.1-2.5,2.5   s1.1,2.5,2.5,2.5h6.8V81c0,1.4,1.1,2.5,2.5,2.5s2.5-1.1,2.5-2.5v-6.8h6.8c1.4,0,2.5-1.1,2.5-2.5S85.9,69.2,84.5,69.2z" />
      </g>
   </svg>
);

export default IconAlignCenter;
