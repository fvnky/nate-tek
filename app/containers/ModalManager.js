import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ModalManager from '../components/Modal/ModalManager.tsx';
import * as UserActions from '../actions/user';
import * as SubjectsActions from '../actions/subjects';
import * as FoldersActions from '../actions/folders';
import * as CoursesActions from '../actions/courses';
import * as ModalsActions from '../actions/modal';
import * as QuizsActions from '../actions/quizs';
import { showToast } from '../actions/toast';

function mapStateToProps(state) {
   return {
      folders: state.folders,
      courses: state.courses,
      subjects: state.subjects,
      user: state.user,
      modal: state.modal,
      db: state.database,
      error: state.error,
      lang: state.language,
      theme: state.theme,
      quizs: state.quizs
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...UserActions,
         ...SubjectsActions,
         ...FoldersActions,
         ...CoursesActions,
         ...ModalsActions,
         ...QuizsActions,
         showToast
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(ModalManager);
