import * as fs from 'fs';
import * as React from 'react';
import { Link } from 'react-router-dom';

import styles from './DevPage.less';

import { sync } from '../../database/utils/index';

import { ISubject, ITodo } from '../../typescript';
import { routes } from '../../constants/routesJs';
import Dictionary from '../Dictionaries/Dictionaries';
import Todos from '../TodosTest/TodosTest';

interface IProps {
   db: any;
   subjects: ISubject[];
   todos: ITodo[];
}

export default class Dev extends React.Component<IProps> {
   constructor(props) {
      super(props);
   }

   createSubjects = () => {
      const { db } = this.props;

      const subjects = [
         { name: 'Socioeconomics', icon: 'HandMoney' },
         { name: 'Anthropology', icon: 'NounMan179773' },
         { name: 'Psychology', icon: 'NounPsychology392525' },
         { name: 'German', icon: 'NounGerman902390' }
      ];

      const task = (subject: any, index: any, cb: any) => {
         db.subjects
            .create(subject)
            .then(() => cb())
            .catch(cb);
      };

      return sync(subjects, task.bind(this));
   };

   createFolders = (subjectName: string, folderNames: string[]) => {
      const { subjects, db } = this.props;

      const subject = subjects.find((s: any) => s.name === subjectName);

      if (!subject) return;
      const task = (name: any, index: any, cb: any) => {
         db.folders
            .create({
               name,
               parent: { collection: subject.collection, id: subject._id }
            })
            .then(() => cb())
            .catch(cb);
      };

      return sync(folderNames, task.bind(this));
   };

   createCourses = (folderName: string, coursesNames: string[]) => {
      const { db } = this.props;

      db.folders
         .findAll()
         .then((folders: any) => {
            const folder = folders.find((f: any) => f.name === folderName);
            if (!folder) {
               console.warn('could not find folder ', folderName);
               return;
            }
            const task = (name: any, index: any, cb: any) => {
               db.courses
                  .create({
                     name,
                     parent: { collection: folder.collection, id: folder._id }
                  })
                  .then((doc: any) => {
                     const pathToFile = `${__dirname}/components/DevPage/courses/${name}.json`;
                     if (fs.existsSync(pathToFile)) {
                        // alert(`FOUND course for : ${pathToFile}`);
                        const content = JSON.parse(
                           fs.readFileSync(pathToFile, 'utf8')
                        );
                        return db.courses.update(doc, content);
                     }
                     console.warn(`DID NOT FIND: ${pathToFile}`);
                  })
                  .then(() => cb())
                  .catch(cb);
            };
            return sync(coursesNames, task.bind(this));
         })
         .catch((err: any) => console.log(err));
   };

   createCoursesLayer1 = () => {
      this.createCourses('Part I : Economic Theory and Tools', [
         'Theoratical Economics'
      ]);
      this.createCourses('Part II : Behavioral economics', ['Nudge theory']);
      this.createCourses('Part III :  Applied Economics', [
         'Political economy',
         'Microeconomics'
      ]);
      // this.createCourses('Part IV : Government and Social Change', [''])
      // this.createCourses('Part V : The Free Enterprise System', [''])

      this.createCourses('Part I: History of Antrophology', [
         'Overview of the modern discipline',
         'Proto-anthropology',
         'The Enlightenment roots of the discipline'
      ]);
      this.createCourses('Part II : Fields of anthropology', [
         'Biologocal anthropology',
         'Sociocultural anthropology',
         'Linguistic anthropology'
      ]);
      this.createCourses('Part III: Ethical Systems, Moral Dilemmas', [
         'Cultural relativism',
         'Moral dilemma case study: Military involvement'
      ]);

      this.createCourses('Part I : Perception, Cognition and Reality', [
         'The diferent types of perception',
         'Reality & Physiology',
         'Perception theories'
      ]);
      this.createCourses('Part II : Theories of Cognition and Emotion', [
         'Common experiments on human cognition',
         'Metacognition'
      ]);
      this.createCourses('Part III : Personal and Social Motivation', [
         'Psychological theories of emotions',
         'Push and pull theory',
         "Maslow's hierarchy of needs"
      ]);
      // this.createCourses('Part IV : Cognitive Neuroscience', [''])

      this.createCourses('Semester 1', [
         'Chapter 1 - How to introduce yourself',
         "Chapter 2 - Where I'm from",
         'Chapter 3 - Family',
         'Chapter 4 - My daily routine',
         'Chapter 5 - Free time & hobbies',
         'Chapter 6 - At the supermarket'
      ]);
      this.createCourses('Semester 2', ['']);
      this.createCourses('Vocabulary', [
         'Greetings',
         'Days',
         'Numbers',
         'Transportation'
      ]);
   };

   createFoldersLayer1 = () => {
      this.createFolders('Socioeconomics', [
         'Part I : Economic Theory and Tools',
         'Part II : Behavioral economics',
         'Part III :  Applied Economics',
         'Part IV : Government and Social Change',
         'Part V : The Free Enterprise System'
      ]);
      this.createFolders('Anthropology', [
         'Part I: History of Antrophology',
         'Part II : Fields of anthropology',
         'Part III: Ethical Systems, Moral Dilemmas'
      ]);
      this.createFolders('Psychology', [
         'Part I : Perception, Cognition and Reality',
         'Part II : Theories of Cognition and Emotion',
         'Part III : Personal and Social Motivation',
         'Part IV : Cognitive Neuroscience'
      ]);
      this.createFolders('German', [
         'Semester 1',
         'Semester 2',
         'Vocabulary',
         'Irregular verbs'
      ]);
   };

   removeSubjects = () => {
      const { db } = this.props;

      db.subjects
         .findAll()
         .then((subjects: any) => {
            console.log('clear subjects received:', subjects);
            const task = (subject: any, index: any, cb: any) => {
               db.subjects
                  .remove(subject)
                  .then(() => cb())
                  .catch(cb);
            };

            return sync(subjects, task.bind(this));
         })
         .then(() => db.folders.findAll())
         .then((folders: any) => {
            const task = (folder: any, index: any, cb: any) => {
               db.folders
                  .remove(folder)
                  .then(() => cb())
                  .catch(cb);
            };
            return sync(folders, task.bind(this));
         })
         .then(() => db.courses.findAll())
         .then((courses: any) => {
            const task = (course: any, index: any, cb: any) => {
               db.courses
                  .remove(course)
                  .then(() => cb())
                  .catch(cb);
            };
            return sync(courses, task.bind(this));
         })
         .then(() => console.log('clean done'))
         .catch((err: any) => console.log('error', err));
   };

   handleClick = () => {
      const { db } = this.props;
      this.createSubjects()
         .then(() => this.createFoldersLayer1())
         .then(() => db.folders.findAll())
         .then(() => this.createCoursesLayer1())
         .then(() => console.log('populate done'))
         .catch((err: any) => console.log('error', err));
   };

   todotests = () => {
      const { db } = this.props;
      const timestamp = Date.now();
      db.todos
      .create({name: "todo"+timestamp})
      .then(() => console.log('created todo ' + timestamp))
      .then(alert('ok'));
   };

   signUpUser = () => {
      const { db } = this.props;

      db.user
         .signUp({
            displayname: 'S. Smith',
            firstname: 'Tina',
            lastname: 'Smith',
            username: 'tina',
            password: 'TinaSmith123',
            mail: 'tina@smith.com',
            university: 'Griffith College',
            curriculum: 'Computer Science',
            year: '2020',
            country: 'France',
            lang: 'fr',
            theme: 'light',
            key: 'a25066b7'
         })
         .then(() => console.log('signed up'))
         .catch((err: any) => console.log('error', err));
   };

   logInUser = () => {
      const { db } = this.props;

      db.user
         .logIn({ name: 'tina', password: 'TinaSmith123' })
         .then(() => console.log('logged in'))
         .catch((err: any) => console.log('error', err));
   };

   logOutUser = () => {
      const { db } = this.props;

      db.user
         .logOut()
         .then(() => console.log('logged out'))
         .catch((err: any) => console.log('error', err));
   };

   render() {
      return (
         <>
            <div className={styles.flexContainer}>
               <div className={styles.flexItem}>
                  <h1>DEV PAGE</h1>
               </div>
               
               {/* <div className={styles.flexItem}>
                  <button onClick={this.removeSubjects}>Clear Database</button>
               </div>
               <div className={styles.flexItem}>
                  <button onClick={this.handleClick}>Populate Database</button>
               </div> */}
               <div className={styles.flexItem}>
                  <button onClick={this.todotests}>Create todos</button>
               </div>
               <div className={styles.flexItem}>
                  <button onClick={this.signUpUser}>SignUp Tina</button>
               </div>
               <div className={styles.flexItem}>
                  <button onClick={this.logInUser}>LogIn Tina</button>
               </div>
               <div className={styles.flexItem}>
                  <button onClick={this.logOutUser}>LogOut Tina</button>
               </div>
               <div className={styles.flexItem}>
                  <Link to={routes.HOME}>HOME PAGE</Link>
               </div>
               <Todos {...this.props}/>
            </div>
         </>
      );
   }
}
