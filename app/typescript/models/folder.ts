// Constant
import { FIND_BY_ID, CREATE, UPDATE, REMOVE } from '../../constants/models';

interface IParent {
   id: string;
   collection: string;
}

export interface IFolder {
   _id: string;
   _rev: string;
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   name?: string;
   parent: IParent;
   children: IParent[];
}

export type FindFolderById = (id: string) => Promise<IFolder>;
export type CreateFolder = (newFolder: {
   name: string;
   parent: { id: string; collection: string };
}) => Promise<IFolder>;
export type UpdateFolder = (toUpdate: IFolder) => Promise<IFolder>;
export type RemoveFolder = (toRemove: IFolder) => Promise<IFolder>;

export type IDatabaseFolder = {
   [FIND_BY_ID]: FindFolderById;
   [CREATE]: CreateFolder;
   [UPDATE]: UpdateFolder;
   [REMOVE]: RemoveFolder;
};
