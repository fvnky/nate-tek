import { Dispatch } from '../reducers/types';

const SET_SPIN = 'SET_SPIN';

const spin = (value: boolean) => {
   return (dispatch: Dispatch) => {
      dispatch({
         type: SET_SPIN,
         payload: value
      });
   };
};

export { SET_SPIN, spin };
