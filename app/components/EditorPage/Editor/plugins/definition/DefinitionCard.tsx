import React from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';

import withTheme from '../../../../withTheme';
import styles from './DefinitionCard.less';

type Props = {
   theme: string;
   editor: any;
   value: any;
};

const DefinitionCard: React.FC<Props> = ({
   editor,
   theme,
   value,
   children
}) => {
   return (
      <div className={styles.layout} contentEditable={false}>
         THIS IS A CARD
      </div>
   );
};

export default withTheme(DefinitionCard);
