import React from 'react';

// Component
import { useFindQuizParam } from './useFetchQuiz';

// Constant
import { SCHEMA_NAME } from '../../constants/models';

// Style
import styles from './QuizLayout.less';

// Typescript
import { IQuizPage } from './QuizPage';

interface IQuizFooterButton {
   text: string;
   style: string;
   handleOnClick: (data?: any) => any;
}

export const QuizFooterButton = (props: IQuizFooterButton) => {
   const { text, style, handleOnClick } = props;

   return (
      <div
         className={`${styles.headerQuizButton} ${style}`}
         onClick={handleOnClick}
      >
         {text}
      </div>
   );
};

interface IQuizHeader extends IQuizPage {
   title: string;
}

export const QuizHeader = (props: IQuizHeader) => {
   const { title } = props;
   const name = useFindQuizParam({ param: SCHEMA_NAME, ...props });

   return (
      <div className={styles.headerQuiz}>
         <div className={styles.headerQuizTitle}>{title}</div>
         <div className={styles.headerQuizName}>{name}</div>
      </div>
   );
};

interface IQuizContent {
   children: React.ReactNode | React.ReactNode[];
}

export const QuizContent = (props: IQuizContent) => {
   const { children } = props;

   return <div className={styles.contentQuiz}>{children}</div>;
};

interface IQuizFooter {
   children: React.ReactNode | React.ReactNode[];
}

export const QuizFooter = (props: IQuizFooter) => {
   const { children } = props;

   return <div className={styles.containerQuizButton}>{children}</div>;
};

interface IQuizLayout {
   children: React.ReactNode | React.ReactNode[];
}

export const QuizLayout = (props: IQuizLayout) => {
   const { children } = props;

   return <div className={styles.layoutQuiz}>{children}</div>;
};
