import * as React from 'react';
import cx from 'classnames';
import { RouteComponentProps, Redirect } from 'react-router';

import LoginPage from './LoginPage';
import PrivacyPolicy from './PrivacyPolicy';
import RegisterForm from './RegisterForm';

import styles from './LoginRegisterPage.less';

// Typescript
import { IUser, ILanguage, ToggleModal } from '../../typescript';
import { routes } from '../../constants/routesJs';
import { EN, FR, DE } from '../../constants/language';

export const LOGIN = 'login';
export const REGISTER = 'register';

interface IProps extends RouteComponentProps {
   toggleModal: ToggleModal;
   user: IUser;
   lang: ILanguage;
   db: any;
}

const LoginRegisterPage: React.FC<IProps> = ({
   toggleModal,
   user,
   lang,
   db,
   history,
   location,
   match
}) => {
   const [email, setMail] = React.useState('');
   const [step, setStep] = React.useState(LOGIN);
   const [termsAccepted, setTermsAccepted] = React.useState(false);
   const [language, setLanguage] = React.useState(EN);

   if (user._id) return <Redirect to={{ pathname: routes.SUBJECTS }} />;
   if (location.pathname === routes.LOGIN) {
      if (step === LOGIN)
         return (
            <LoginPage
               match={match}
               location={location}
               history={history}
               db={db}
               setMail={setMail}
               setStep={setStep}
               setLanguage={setLanguage}
               language={language}
            />
         );
      else if (step === REGISTER) {
         return (
            <div className={styles.layout}>
               <span className={cx('Light', styles.title)}>
                  {!termsAccepted
                     ? lang.privacyHeader
                     : lang.registerFormHeader}{' '}
               </span>
               <hr className={cx('Light', styles.divider)} />
               <div>
                  {!termsAccepted ? (
                     <PrivacyPolicy
                        lang={lang}
                        acceptTerms={() => setTermsAccepted(true)}
                        declineTerms={() => {
                           setMail('');
                           setStep(LOGIN);
                        }}
                     />
                  ) : (
                     <RegisterForm
                        toggleModal={toggleModal}
                        match={match}
                        location={location}
                        history={history}
                        lang={lang}
                        db={db}
                        language={language}
                        email={email}
                        back={() => setStep(LOGIN)}
                     />
                  )}
               </div>
            </div>
         );
      }
   } else return <div>You are not logged in. Unkown route.</div>;
};

export default LoginRegisterPage;
