import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import QuizPage from '../components/QuizPage/QuizPage';
import * as QuizsActions from '../actions/quizs';

function mapStateToProps(state) {
   return {
      lang: state.language,
      theme: state.theme,
      quizs: state.quizs,
      subjects: state.subjects,
      folders: state.folders,
      db: state.database
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...QuizsActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(QuizPage);
