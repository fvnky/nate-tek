import * as React from 'react';

const IconNateOutline = props => (
   <svg viewBox="0 0 100 100" {...props}>
        <path d="M93.743,97.068H75.981V46.673a25.982,25.982,0,0,0-51.963,0v50.4H6.257V46.673a43.743,43.743,0,0,1,87.486,0Zm-13.762-4h9.762V46.673a39.743,39.743,0,0,0-79.486,0v46.4h9.761V46.673a29.982,29.982,0,0,1,59.963,0Z"/>
   </svg>
);

export default IconNateOutline;
