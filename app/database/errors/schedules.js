import { CustomError } from '../factory';

export class ScheduleAlreadyExists extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The schedule already exists';
   }
}

export class ScheduleNotFound extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The schedule could not be found';
   }
}

export class NoSchedules extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'No schedule could be found';
   }
}
