import { View } from '@fullcalendar/core';

export interface IArgDateClick {
   date: Date;
   dateStr: string;
   allDay: boolean;
   resource?: any;
   dayEl: HTMLElement;
   jsEvent: MouseEvent;
   view: View;
}
