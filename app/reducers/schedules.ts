import _ from 'lodash';
import { ISchedule } from '../typescript';
import { IActionSchedule } from './types';

export const ADD_SCHEDULE = 'add_schedule';
export const FETCH_SCHEDULE = 'fetch_schedule';
export const UPDATE_SCHEDULE = 'update_schedule';
export const REMOVE_SCHEDULE = 'remove_schedule';

export function schedules(state: ISchedule[] = [], action: IActionSchedule) {
   switch (action.type) {
      case ADD_SCHEDULE:
         return _.union(state, [action.payload]);
      case FETCH_SCHEDULE:
         return action.payload;
      case UPDATE_SCHEDULE: {
         const ref = _.clone(state);
         const schedule = _.find(ref, { _id: action.payload._id });

         _.assign(schedule, action.payload);
         return ref;
      }
      case REMOVE_SCHEDULE: {
         const ref = _.clone(state);

         _.remove(ref, { _id: action.payload._id });
         return ref;
      }
      default: {
         return state;
      }
   }
}
