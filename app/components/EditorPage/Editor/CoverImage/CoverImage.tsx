import React from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';
import axios from 'axios';

import withTheme from '../../../withTheme';
import styles from './CoverImage.less';
import { ICourse } from '../../../../typescript';
import AddCoverPlaceholder from './AddCoverPlaceholder/AddCoverPlaceholder';
import IconDelete from '../../../../assets/icons/IconDelete';

type Props = {
   theme: string;
   editor: any;
   db: any;
   course?: ICourse;
   readOnly: boolean;
};

const CoverImage: React.FC<Props> = ({
   editor,
   theme,
   db,
   course,
   readOnly
}) => {

   const [mouseOver, setMouseOver] = React.useState(false)
   const [deleting, setDeleting] = React.useState(false)

   const onMouseMove = () => {
      if (!mouseOver) {
         setMouseOver(true)
      }
   }

   const onMouseLeave = () => {
      setMouseOver(false)
   }

   const onDeleteClick = () => {
      setDeleting(true);

      const newCourse = course;
      newCourse.coverImage.full = '';
      newCourse.coverImage.raw = '';
      newCourse.coverImage.regular = '';
      newCourse.coverImage.small = '';
      newCourse.coverImage.thumb = '';
      delete newCourse.coverImage;
      // console.log(image.urls)
      // newCourse.coverImage = image.urls;
      console.log("***** got", newCourse)
      // console.log("is frozen", Object.isFrozen(newCourse))

      db.courses.update(newCourse)
      .then((res) => console.warn("got", res))
      .catch((err) => console.error(err))
   }

   return (<>
      {(course && course.coverImage && course.coverImage.regular.length > 0) ?
         <div  className={styles.gimmespace}>
            <div className={cx(styles.coverImage, readOnly ? styles.readOnly : '')} style={{backgroundImage:  "url(" + course.coverImage.regular + ")"}} onMouseMove={onMouseMove} onMouseLeave={onMouseLeave}>
               {!readOnly && <div className={cx(styles.iconDeleteWrapper, mouseOver ? styles.showButton : '')} onClick={onDeleteClick}>
                  <IconDelete />
               </div>}
            </div>
            {/* <img className={styles.coverImage} src={course.coverImage.regular} /> */}
         </div>
          :
          <>
         {!readOnly && <AddCoverPlaceholder db={db} course={course} />}
         </>}
   </>);
};

export default withTheme(CoverImage);
