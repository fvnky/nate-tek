import { Dispatch, GetState } from '../reducers/types';

const SET_THEME = 'SET_THEME';

const setThemeName = (themeColor: string) => {
   return (dispatch: Dispatch) => {
      dispatch({
         type: SET_THEME,
         payload: themeColor
      });
   };
};

export { SET_THEME, setThemeName};
