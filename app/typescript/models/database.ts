import { AxiosInstance } from 'axios';

// Constant
import {
   QUIZS_DB,
   COURSES_DB,
   SUBJECTS_DB,
   FOLDERS_DB,
   DICTIONARY_DB,
   SCHEDULES_DB,
   LIBRARY
} from '../../constants/models';
import { SCORE } from '../../constants/models/quiz';

// Typescript
import {
   IDatabaseQuiz,
   IDatabaseFolder,
   IDatabaseSubject,
   IDatabaseCourse,
   IDatabaseSchedule,
   IDatabaseDictionary
} from './index';
import { Dispatch, GetState, Store } from '../../reducers/types';

export interface IDatabase {
   [SUBJECTS_DB]: IDatabaseSubject;
   [FOLDERS_DB]: IDatabaseFolder;
   [COURSES_DB]: IDatabaseCourse;
   [QUIZS_DB]: IDatabaseQuiz;
   [DICTIONARY_DB]: IDatabaseDictionary;
   [SCHEDULES_DB]: IDatabaseSchedule;
   [LIBRARY]: any;
   dispatch: Dispatch;
   _main: PouchDB.Database;
   _session: PouchDB.Database;
   _cloud: PouchDB.Database;
   request: AxiosInstance;

   _sync: any;
   init: (store: Store) => void;
   connect: (id: string, token: string) => void;
   sync: any;
   refresh: any;
   fetch: () => any;
   close: () => any;
}

export interface IDbDocument {
   _id: string;
   _rev: string;
   updatedAt?: number;
   createdAt?: number;
   icon?: string;
   collection?: string;
   name?: string;
   children?: Array<{ id: string }>;
   parent?: {
      id: string;
      collection: string;
   };
   [SCORE]?: number;
}

// TODO
export interface ICouchDB {
   _id: string;
   _rev: string;
}
