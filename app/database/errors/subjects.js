import { CustomError } from '../factory';

export class SubjectAlreadyExists extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The subject already exists';
   }
}

export class SubjectNotFound extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The subject could not be found';
   }
}

export class NoSubjects extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'No subjects could be found';
   }
}
