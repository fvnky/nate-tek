import {
   CREATE,
   FIND_BY_ID,
   UPDATE,
   REMOVE,
   DEFINITION_DICTIONARY,
   NAME_DICTIONARY,
   SUBJECT_DICTIONARY
} from '../../constants/models';

export interface IDictionary {
   _id: string;
   _rev: string;
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   // name?: string;
   [DEFINITION_DICTIONARY]: string;
   [NAME_DICTIONARY]: string;
   [SUBJECT_DICTIONARY]: string;
   // parent?: {
   //    id: string;
   //    collection: string;
   // };
   // questions?: any;
   // [key: string]: any;
}

export type UpdateDictionary = (toUpdate: IDictionary) => Promise<IDictionary>;
export type RemoveDictionary = (toRemove: IDictionary) => Promise<IDictionary>;
export type FindDictionaryById = (id: string) => Promise<IDictionary>;
export type CreateDictionary = (newDictionary: {
   [NAME_DICTIONARY]: string;
   [SUBJECT_DICTIONARY]: string;
}) => Promise<IDictionary>;

export type IDatabaseDictionary = {
   [FIND_BY_ID]: FindDictionaryById;
   [CREATE]: CreateDictionary;
   [UPDATE]: UpdateDictionary;
   [REMOVE]: RemoveDictionary;
};
