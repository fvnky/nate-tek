import React from 'react';

// Component
import { useFindQuizParam } from '../useFetchQuiz';

// Constants
import {
   QUESTIONS,
   QCM,
   QUESTION_ANSWER,
   TRUE_FALSE,
   FORM
} from '../../../constants/models';
import { PROPOSITION_QCM } from '../../../constants/models/questions';

// Styles
import styles from './DisplayQuestions.less';

// Typescript
import { IQuizGame } from '../QuizGame';

export interface IDisplayQuestions extends IQuizGame {
   setScore: () => void;
   index: number;
   answer: any;
   setAnswer: React.Dispatch<any>;
}

const Qcm = (props: IDisplayQuestions) => {
   const { index, answer, setAnswer } = props;
   const questions = useFindQuizParam({ param: QUESTIONS, ...props });
   const {
      [FORM]: { [PROPOSITION_QCM]: propositions }
   } = questions[index];

   const handleOnClick = (key: string) => () => setAnswer(key);

   return (
      <div className={styles.containerQcm}>
         {Object.keys(propositions).map((key: string, indexKey: number) => {
            const displaySelected = answer === key ? styles.selectQcm : '';

            return (
               <div
                  key={indexKey}
                  role="button"
                  onClick={handleOnClick(key)}
                  className={`${
                     styles.containerPropositionQcm
                  } ${displaySelected}`}
               >
                  <div className={styles.indexProposition}>{indexKey + 1}.</div>
                  <div className={styles.textProposition}>
                     {propositions[key]}
                  </div>
               </div>
            );
         })}
      </div>
   );
};

function QuestionAnswer(props: IDisplayQuestions) {
   const { answer, setAnswer } = props;

   const handleOnChange = e => {
      const { value } = e.target;

      setAnswer(value);
   };

   return (
      <input
         className={styles.containerQuestionAnswer}
         onChange={handleOnChange}
         value={answer ? answer : ''}
      />
   );
}

export default {
   [QCM]: Qcm,
   [QUESTION_ANSWER]: QuestionAnswer,
   [TRUE_FALSE]: QuestionAnswer
};
