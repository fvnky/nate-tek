// Typescript
import { IDbDocument } from '../typescript';

// Constant
import { day } from '../constants/time';
import { DRAFT, SUBJECT, FOLDER, COURSE } from '../database/constants/index';

export function upper(string: string) {
   // for(let cnt = 0; cnt < string.length; cnt++) {
   //    string.charAt(cnt).toUpperCase();
   //    console.log("string[" + cnt + "] = " + string[cnt]);
   // }
   // console.log("UPPER :", string.toUpperCase());
   return string.toUpperCase();
}

export function capitalize(string) {
   return string.charAt(0).toUpperCase() + string.slice(1);
}

export const str = varObj => Object.keys(varObj)[0];

export function errorModal(name, doc) {
   console.error(name, ' not found :', doc);
   return null;
}

export function errorModalEdit(id, docs) {
   if (
      !id ||
      !docs ||
      docs.constructor !== Array ||
      !docs.find(course => course._id === id)
   )
      return true;
   return false;
}

export function errorModalDelete(id, docs) {
   if (
      !id ||
      !docs ||
      docs.constructor !== Array ||
      !docs.find(course => course._id === id)
   )
      return true;
   return false;
}

export function timeDifference(current, previous) {
   const msPerMinute = 60 * 1000;
   const msPerHour = msPerMinute * 60;
   const msPerDay = msPerHour * 24;
   const msPerMonth = msPerDay * 30;
   const msPerYear = msPerDay * 365;

   const elapsed = current - previous;

   if (elapsed < msPerMinute) {
      // return `${Math.round(elapsed / 1000)} seconds ago`;
      return `A few seconds ago`;
   }
   if (elapsed < msPerHour) {
      return `${Math.round(elapsed / msPerMinute)} minutes ago`;
   }
   if (elapsed < msPerDay) {
      return `${Math.round(elapsed / msPerHour)} hours ago`;
   }
   if (elapsed < msPerMonth) {
      return `${Math.round(elapsed / msPerDay)} days ago`;
   }
   if (elapsed < msPerYear) {
      return `${Math.round(elapsed / msPerMonth)} months ago`;
   }
   return `${Math.round(elapsed / msPerYear)} years ago`;
}

export const destruct = (keys, obj) => keys.reduce((a, c) => obj[c], {});

export function today(time: number) {
   if (new Date().getTime() - day < time) return true;
   return false;
}

export function yesterday(time: number) {
   if (
      new Date().getTime() - day > time &&
      time > new Date().getTime() - 2 * day
   )
      return true;
   return false;
}

export function week(time: number) {
   if (
      new Date().getTime() - 2 * day > time &&
      time > new Date().getTime() - 7 * day
   )
      return true;
   return false;
}

export function old(time: number) {
   if (new Date().getTime() - 7 * day > time) return true;
   return false;
}

export function lastUpdate(doc: IDbDocument) {
   const { updatedAt, createdAt } = doc;

   if (updatedAt) return updatedAt;
   return createdAt;
}

export function lastUpdateRecursive(
   docToLookInto: IDbDocument,
   allOtherDocs: Array<Array<IDbDocument>>
) {
   if (
      !allOtherDocs ||
      (docToLookInto.collection !== FOLDER &&
         docToLookInto.collection !== SUBJECT)
   )
      return docToLookInto;

   let lastUpdatedDoc = null;
   allOtherDocs.forEach(collection => {
      let lastUpdatedDocOfCollection = null;
      collection.forEach(doc => {
         // Check that the doc is a children
         if (doc.parent && doc.parent.id === docToLookInto._id) {
            if (lastUpdatedDoc === null) lastUpdatedDoc = doc;
            if (lastUpdatedDocOfCollection === null)
               lastUpdatedDocOfCollection = doc;
            // If this children is a folder, recursively get the last updated in that folder
            if (doc.collection === FOLDER) {
               const lastUpdatedInFolder = lastUpdateRecursive(
                  doc,
                  allOtherDocs
               );
               if (
                  lastUpdate(lastUpdatedInFolder) >
                  lastUpdate(lastUpdatedDocOfCollection)
               )
                  lastUpdatedDocOfCollection = lastUpdatedInFolder;
            } else if (
               lastUpdate(doc) > lastUpdate(lastUpdatedDocOfCollection)
            ) {
               lastUpdatedDocOfCollection = doc;
            }
         }
      });
      if (
         lastUpdatedDoc &&
         lastUpdatedDocOfCollection &&
         lastUpdate(lastUpdatedDocOfCollection) > lastUpdate(lastUpdatedDoc)
      )
         lastUpdatedDoc = lastUpdatedDocOfCollection;
   });
   if (!lastUpdatedDoc) return docToLookInto;
   return lastUpdatedDoc;
}

export function createOrUpdate(doc: IDbDocument) {
   const { updatedAt, createdAt } = doc;

   if (updatedAt) return 'updatedAt';
   return 'createdAt';
}
