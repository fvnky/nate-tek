// Utils
import {
   generateId,
   timestamp,
   parseUpdate,
   parseData,
   parseDoc,
   isNotFound,
   parseQuizQuestions
} from '../utils';
import { Store } from '../factory';
import { ScheduleNotFound } from '../errors';
import { pick } from 'lodash';

// Typescript
import {
   IDatabase,
   ICouchDB,
   IDbDocument,
   ISchedule,
   ICreateSchedule
} from '../../typescript';

// Constant
import { ScheduleSchema } from '../schemas';
import { SCHEDULE } from '../constants';
import { FIND_BY_ID, CREATE, UPDATE, REMOVE } from '../../constants/models';
import {
   DATE_SCHEDULE,
   TITLE_SCHEDULE,
   CLASSNAME_SCHEDULE,
   ROOM_SCHEDULE,
   NOTE_SCHEDULE,
   START_SCHEDULE,
   END_SCHEDULE,
   ID_SUBJECT_SCHEDULE
} from '../../constants/models';
import {
   ADD_SCHEDULE,
   FETCH_SCHEDULE,
   UPDATE_SCHEDULE,
   REMOVE_SCHEDULE
} from '../../reducers/schedules';

// ================================================ //
// ==================== Create ==================== //
// ================================================ //

function create(this: IDatabase, data: ISchedule) {
   return new Promise((ok, ko) => {
      const store: any = new Store();
      const mandatory: string[] = [DATE_SCHEDULE];
      const optionals: string[] = [
         TITLE_SCHEDULE,
         CLASSNAME_SCHEDULE,
         ROOM_SCHEDULE,
         NOTE_SCHEDULE,
         START_SCHEDULE,
         END_SCHEDULE,
         ID_SUBJECT_SCHEDULE
      ];

      // console.log("CREATE");
      // console.log('DATA :', data);
      parseData(data, mandatory, optionals)
         .then((parsed: ISchedule) => {
            parsed._id = generateId();
            parsed.createdAt = timestamp();
            parsed.updatedAt = parsed.createdAt;
            parsed.collection = SCHEDULE;
            // parsed[ROOM_SCHEDULE] = "";
            // parsed[NOTE_SCHEDULE] = "";
            // parsed[START_SCHEDULE] = "";
            // console.warn('parsed :', parsed);
            return this._main.put(parsed);
         })
         .then(res => store.add('scheduleId', res.id))
         .then((id: string) => this._main.get(id))
         .then(schedule => parseDoc(ScheduleSchema, schedule))
         .then((schedule: ISchedule) => {
            this.dispatch({ type: ADD_SCHEDULE, payload: schedule });
            return schedule;
         })
         .then(ok)
         .catch(
            err =>
               new Promise((next, spread) => {
                  const { scheduleId } = store;
                  if (!scheduleId) return spread(err);
                  this._main
                     .get(scheduleId)
                     .then(doc => this._main.remove(doc._id, doc._rev))
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .catch(ko);
   });
}

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll(this: IDatabase): Promise<ISchedule | ISchedule[]> {
   return new Promise((ok, ko) => {
      this._main
         .find({ selector: { collection: SCHEDULE } })
         .then(res => {
            // console.warn('Queries Quiz :', res.docs);
            this.dispatch({ type: FETCH_SCHEDULE, payload: res.docs });
            const result: any = res.docs;
            ok(result);
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

function update(this: IDatabase, doc: ISchedule) {
   return new Promise((ok, ko) => {
      const store: any = new Store();
      const fields = [];
      //const fields = ['folderId'];

      parseDoc(ScheduleSchema, doc)
         .then(parsed => this._main.get(parsed._id))
         .then(old => parseUpdate(old, doc, fields))
         // .then(schedule => parseQuizQuestions(schedule))
         .then(schedule => store.add('schedule', schedule))
         // .then(quiz => {
         //    if (
         //       quiz.parent.collection !== SUBJECT &&
         //       quiz.parent.collection !== FOLDER &&
         //       quiz.parent.collection !== DRAFT
         //    )
         //       throw new InvalidParameterFormat(
         //          `The parent collection to update ${
         //             quiz.id
         //          } are not compatible`
         //       );
         // })
         .then(() => {
            const { schedule } = store;
            schedule.updatedAt = timestamp();
            return this._main.put(schedule);
         })
         .then(res => this._main.get(res.id))
         .then(schedule => {
            // console.log("QUIZ :", quiz);
            this.dispatch({ type: UPDATE_SCHEDULE, payload: schedule });
            return schedule;
         })
         .then(schedule => ok(schedule))
         .catch(err => {
            if (isNotFound(err))
               if (!store.schedule)
                  throw new ScheduleNotFound(
                     `The schema '${doc._id}' could not be found`
                  );
            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Delete ==================== //
// ================================================ //

function remove(data) {
   return new Promise((ok, ko) => {
      parseData(pick(data, ['_id']), ['_id']) // On peut pas simplement faire data._id ?
         .then(parsed => this._main.get(parsed._id))
         .then(schedule => {
            // console.log("Quiz :", quiz);
            if (schedule.collection !== SCHEDULE)
               throw new ScheduleNotFound(
                  `The document's id '${
                     schedule._id
                  }' does not reference a schedule`
               );
            return schedule;
         })
         .then(({ _id, _rev }) => {
            const promises = [
               this._main.remove(_id, _rev),
               this._cloud.remove(_id, _rev)
            ];

            return Promise.all(promises);
         })
         .then(() => this.dispatch({ type: REMOVE_SCHEDULE, payload: data }))
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               throw new ScheduleNotFound(
                  `The schedule '${data._id}' could not be found`
               );
            throw err;
         })
         .catch(ko);
   });
}

export default function() {
   return {
      [CREATE]: create.bind(this),
      findAll: findAll.bind(this),
      // [FIND_BY_ID]: findById.bind(this),
      [UPDATE]: update.bind(this),
      [REMOVE]: remove.bind(this)
   };
}
