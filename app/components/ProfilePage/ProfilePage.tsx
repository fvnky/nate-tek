import * as React from 'react';
import cx from 'classnames';

import Creatable from 'react-select/lib/Creatable';

// Component
import NateSelect, { Option } from '../Base/NateSelect';
import NateSearchableSelect from '../Base/NateSearchableSelect';

// import Select from '../Base/Select';

// Icon
import IconProfileOutline from '../../assets/icons/IconProfileOutline';
import IconCamera from '../../assets/icons/IconCamera';

// Utils
import { getProfilePictureUrl, userHasProfilePicture } from '../../utils/utils';
import { yearOptions } from '../LoginRegisterPage/RegisterForm';
import { getCountries } from '../LoginRegisterPage/countries';

// Style
import styles from './ProfilePage.less';

// typescript
import { ToggleModal, IUser, ILanguage } from '../../typescript';
import { capitalize } from '../utils';

// tslint:disable-next-line:no-implicit-dependencies
const { dialog } = require('electron').remote;

const options = [
   { value: 'chocolate', label: 'Chocolate' },
   { value: 'strawberry', label: 'Strawberry' },
   { value: 'vanilla', label: 'Vanilla' }
];

export const USER_NAME = 'username';
export const FIRST_NAME = 'firstname';
export const LAST_NAME = 'lastname';
export const UNIVERSITY = 'university';
export const CURRICULUM = 'curriculum';
export const YEAR = 'year';
export const COUNTRY = 'country';

export const mapLang = {
   [USER_NAME]: 'userName',
   [FIRST_NAME]: 'firstName',
   [LAST_NAME]: 'lastName',
   [UNIVERSITY]: UNIVERSITY,
   [CURRICULUM]: CURRICULUM,
   [YEAR]: YEAR,
   [COUNTRY]: COUNTRY
};

export const selectOpt = {
   [YEAR]: yearOptions,
   [COUNTRY]: getCountries
};

const FIELD_INPUT_PROFILE = [
   USER_NAME,
   FIRST_NAME,
   LAST_NAME,
   UNIVERSITY,
   CURRICULUM
];
const FIELD_SELECT_PROFILE = [YEAR];

interface IPropsInputFocus {
   field: string;
   updateFieldValue: (value: string, field: string) => Promise<any>;
   theme: string;
   text: string;
   initValue: string;
}

function InputFieldFocus(props: IPropsInputFocus) {
   const { theme, field, initValue = '', updateFieldValue } = props;
   const [value, setValue] = React.useState(initValue);
   const [error, setError] = React.useState(false);

   const handleOnChange = (e: React.FormEvent<HTMLInputElement>) =>
      setValue(e.currentTarget.value);

   const handleOnBlur = () =>
      updateFieldValue(value, field)
         .then(res => setError(!res))
         .catch();

   return (
      <Field error={error} {...props}>
         <input
            className={cx(theme, styles.infoItemInput)}
            onChange={handleOnChange}
            value={capitalize(value)}
            onBlur={handleOnBlur}
         />
      </Field>
   );
}

interface IPropsField {
   text: string;
   error: boolean;
   theme: string;
   children: React.ReactNode | React.ReactNode[];
}

function Field(props: IPropsField) {
   const { text, error, theme, children } = props;

   return (
      <div
         key={text}
         className={cx(
            theme,
            styles.infoItem,
            error ? styles.infoItemError : ''
         )}
      >
         <span
            className={cx(
               theme,
               styles.infoItemName,
               error ? styles.infoItemNameError : ''
            )}
         >
            {text}
         </span>
         <div className={styles.containerChildrenField}>{children}</div>
      </div>
   );
}

interface IPropsDropDownMenu {
   options: Array<{ value: any; label: string }>;
}

function Options(props: IPropsDropDownMenu) {
   const { options } = props;

   return options.map((option: { value: any; label: string }) => {
      const { value, label } = option;

      return (
         <Option key={value} value={value}>
            {label}
         </Option>
      );
   });
}

interface IPropsSelectField {
   field: string;
   updateFieldValue: (value: string, field: string) => Promise<any>;
   theme: string;
   text: string;
   options: Array<{ value: any; label: string }>;
   initValue: any;
}

function SelectField(props: IPropsSelectField) {
   const { initValue, options, updateFieldValue, field } = props;
   const [value, setValue] = React.useState(initValue);
   const [error, setError] = React.useState(false);

   const handleOnChange = (inputValue: any) => {
      setValue(inputValue);
      updateFieldValue(inputValue, field)
         .then(res => setError(!res))
         .catch();
   };

   return (
      <Field error={error} {...props}>
         {/* <Creatable
            isClearable
            // isDisabled={isLoading}
            // isLoading={isLoading}
            onChange={handleOnChange}
            // onCreateOption={this.handleCreate}
            options={options}
            value={value}
         /> */}
         <NateSelect value={value} onChange={handleOnChange}>
            {Options({ options })}
         </NateSelect>
      </Field>
   );
}

interface IPropsSearchableSelectField {
   field: string;
   updateFieldValue: (value: string, field: string) => Promise<any>;
   theme: string;
   text: string;
   options: Array<{ value: any; label: string }>;
   initValue: any;
   creatable: Boolean;
}

function SearchableSelectField(props: IPropsSearchableSelectField) {
   const { initValue, options, updateFieldValue, field, creatable } = props;
   const [value, setValue] = React.useState(initValue);
   const [error, setError] = React.useState(false);

   const handleOnChange = (inputValue: any) => {
      setValue(inputValue);
      updateFieldValue(inputValue, field)
         .then(res => setError(!res))
         .catch();
   };

   return (
      <Field error={error} {...props}>
         <NateSearchableSelect
            value={value}
            onChange={handleOnChange}
            options={options}
            creatable={creatable}
         />
      </Field>
   );
}

interface IProps {
   user: IUser;
   toggleModal: ToggleModal;
   updateUserProfilePicture: (imagePath: string, imageType: string) => void;
   lang: ILanguage;
   theme: string;
   setLanguage: (language: string) => void;
   db: any;
}

export default class ProfilePage extends React.Component<IProps> {
   showFileSelector = () => {
      const { updateUserProfilePicture } = this.props;

      dialog.showOpenDialog(
         {
            properties: ['openFile', 'multiSelections']
         },
         (files: any) => {
            if (files !== undefined) {
               updateUserProfilePicture(files[0], 'image/png');
            }
         }
      );
   };

   updateUserField = (value: string, field: string): Promise<any> => {
      const { db, user } = this.props;

      return db.user
         .updateUser(user._id, {
            [field]: field === YEAR ? parseInt(value) : value
         })
         .then(res => Promise.resolve(true))
         .catch(err => {
            console.log('Err : ', err);
            return Promise.resolve(false);
         });
   };

   render() {
      const { user, lang, theme } = this.props;

      if (!user) return null;
      console.warn('__________user', user);
      const has = userHasProfilePicture(user);

      return (
         <div className={cx(theme, styles.container)}>
            <div className={cx(theme, styles.title)}>Profile</div>
            <div className={styles.layout}>
               <div className={styles.infoList}>
                  {FIELD_INPUT_PROFILE.map((field: string) => (
                     <InputFieldFocus
                        text={lang[mapLang[field]]}
                        field={field}
                        updateFieldValue={this.updateUserField}
                        theme={theme}
                        initValue={user[field]}
                     />
                  ))}
                  {FIELD_SELECT_PROFILE.map((field: string) => (
                     <SelectField
                        text={lang[mapLang[field]]}
                        field={field}
                        updateFieldValue={this.updateUserField}
                        theme={theme}
                        initValue={user[field]}
                        options={
                           field === COUNTRY
                              ? selectOpt[field](lang.lang)
                              : selectOpt[field]
                        }
                     />
                  ))}
                  <SearchableSelectField
                     text={lang[mapLang[COUNTRY]]}
                     field={COUNTRY}
                     updateFieldValue={this.updateUserField}
                     theme={theme}
                     initValue={user[COUNTRY]}
                     options={getCountries(lang.lang)}
                     creatable
                  />
               </div>
               {/* <div className={styles.profilePictureContainer}>
                  {has ? (
                     <img
                        className={styles.cropcircle}
                        src={getProfilePictureUrl(user)}
                     />
                  ) : (
                        <IconProfileOutline
                           className={cx(theme, styles.profileIcon)}
                        />
                     )}
                  <div className={cx(theme, styles.iconAndName)}>
                     <IconCamera className={styles.iconCamera} />
                     <button
                        onClick={this.showFileSelector}
                        className={styles.editPicture}
                     >
                        Edit
                     </button>
                  </div>
               </div> */}
            </div>
         </div>
      );
   }
}
