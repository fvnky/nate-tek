import { isString, pick } from 'lodash';
import { InvalidParameterFormat, DictionaryNotFound } from '../errors';
import { Store } from '../factory';

// import {
//    ADD_QUIZ,
//    FETCH_QUIZ,
//    UPDATE_QUIZ,
//    REMOVE_QUIZ
// } from '../../reducers/quizs';

import { DictionarySchema } from '../schemas';

import {
   generateId,
   timestamp,
   parseUpdate,
   parseData,
   parseDoc,
   isNotFound,
   parseQuizQuestions
} from '../utils';

// Typescript
import {
   IQuiz,
   IDatabase,
   ICouchDB,
   IDbDocument,
   IDictionary
} from '../../typescript';

// Constant
import { FIND_BY_ID, CREATE, UPDATE, REMOVE } from '../../constants/models';
import { COURSE, FOLDER, SUBJECT, DRAFT, QUIZ, DICTIONARY } from '../constants';
import {
   NAME_DICTIONARY,
   SUBJECT_DICTIONARY,
   DEFINITION_DICTIONARY
} from '../../constants/models/dictionary';
import {
   ADD_DICTIONARY,
   REMOVE_DICTIONARY,
   FETCH_DICTIONARY,
   UPDATE_DICTIONARY
} from '../../reducers/dictionary';

// ================================================ //
// ==================== Create ==================== //
// ================================================ //

function create(this: IDatabase, data: IDictionary) {
   return new Promise((ok, ko) => {
      const store: any = new Store();
      const mandatory: string[] = [NAME_DICTIONARY, SUBJECT_DICTIONARY];
      const optionals: string[] = [DEFINITION_DICTIONARY];
      parseData(data, mandatory, optionals)
         .then((parsed: IQuiz) => {
            parsed._id = generateId();
            parsed.createdAt = timestamp();
            parsed.updatedAt = parsed.createdAt;
            parsed.collection = DICTIONARY;
            if (!parsed[DEFINITION_DICTIONARY])
               parsed[DEFINITION_DICTIONARY] = '';
            // parsed.questions = [];
            // parsed.score = 0;
            // console.log("PARSED :", parsed);
            return this._main.put(parsed);
         })
         .then(res => store.add('dictionaryId', res.id))
         .then((id: string) => this._main.get(id))
         .then(dictionary => parseDoc(DictionarySchema, dictionary))
         .then((dictionary: IDictionary) => {
            this.dispatch({ type: ADD_DICTIONARY, payload: dictionary });
            return dictionary;
         })
         .then(ok)
         .catch(
            err =>
               new Promise((next, spread) => {
                  const { dictionaryId } = store;
                  if (!dictionaryId) return spread(err);
                  // console.log("CRASH")
                  this._main
                     .get(dictionaryId)
                     .then(doc => this._main.remove(doc._id, doc._rev))
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .catch(ko);
   });
}

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll(this: IDatabase): Promise<IDictionary | IDictionary[]> {
   return new Promise((ok, ko) => {
      this._main
         .find({ selector: { collection: DICTIONARY } })
         .then(res => {
            // console.warn('Queries Quiz :', res.docs);
            this.dispatch({ type: FETCH_DICTIONARY, payload: res.docs });
            const result: any = res.docs;
            ok(result);
         })
         .catch(ko);
   });
}

// function findById(this: IDatabase, id: string): Promise<IQuiz> {
//    return new Promise((ok, ko) => {
//       if (!isString(id))
//          return ko(
//             new InvalidParameterFormat(`The id parameter is not a String`)
//          );

//       this._main
//          .get(id)
//          .then((doc: IDbDocument) => {
//             if (doc.collection !== QUIZ)
//                throw new QuizNotFound(
//                   `The document's id '${doc._id}' does not reference a quiz`
//                );

//             this.dispatch({ type: UPDATE_QUIZ, payload: doc });
//             ok(doc as IQuiz);
//          })
//          .catch(err => {
//             if (isNotFound(err))
//                throw new QuizNotFound(`The quiz '${id}' could not be found`);

//             throw err;
//          })
//          .catch(ko);
//    });
// }

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

function update(this: IDatabase, doc: IQuiz) {
   return new Promise((ok, ko) => {
      const store: any = new Store();
      const fields = [];

      parseDoc(DictionarySchema, doc)
         .then(parsed => this._main.get(parsed._id))
         .then(old => parseUpdate(old, doc, fields))
         .then(dictionary => store.add('dictionary', dictionary))
         // .then(dictionary => {
         //    if (
         //       quiz.parent.collection !== SUBJECT &&
         //       quiz.parent.collection !== FOLDER &&
         //       quiz.parent.collection !== DRAFT
         //    )
         //       throw new InvalidParameterFormat(
         //          `The parent collection to update ${
         //             quiz.id
         //          } are not compatible`
         //       );
         // })
         .then(dictionary => {
            // const { quiz } = store;
            dictionary.updatedAt = timestamp();
            return this._main.put(dictionary);
         })
         .then(res => this._main.get(res.id))
         .then(dictionary => {
            // console.log("QUIZ :", quiz);
            this.dispatch({ type: UPDATE_DICTIONARY, payload: dictionary });
            return dictionary;
         })
         .then(dictionary => ok(dictionary))
         .catch(err => {
            // if (isNotFound(err))
            //    if (!store.quiz)
            //       throw new DictionaryNotFound(
            //          `The quiz '${doc._id}' could not be found`
            //       );
            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Delete ==================== //
// ================================================ //

function remove(data: IDictionary) {
   return new Promise((ok, ko) => {
      parseData(pick(data, ['_id']), ['_id']) // On peut pas simplement faire data._id ?
         .then(parsed => this._main.get(parsed._id))
         .then(dictionary => {
            // console.log("Quiz :", quiz);
            if (dictionary.collection !== DICTIONARY)
               throw new DictionaryNotFound(
                  `The document's id '${
                     dictionary._id
                  }' does not reference a quiz`
               );
            return dictionary;
         })
         .then(({ _id, _rev }) => {
            const promises = [
               this._main.remove(_id, _rev),
               this._cloud.remove(_id, _rev)
            ];

            return Promise.all(promises);
         })
         .then(() => this.dispatch({ type: REMOVE_DICTIONARY, payload: data }))
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               throw new DictionaryNotFound(
                  `The dictionary '${data._id}' could not be found`
               );
            throw err;
         })
         .catch(ko);
   });
}

export default function() {
   return {
      [CREATE]: create.bind(this),
      findAll: findAll.bind(this),
      // [FIND_BY_ID]: findById.bind(this),
      [UPDATE]: update.bind(this),
      [REMOVE]: remove.bind(this)
   };
}
