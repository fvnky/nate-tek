import { QUESTION } from '../quiz';
import { SCHEMA_ID } from './schema';

// Name
export const QCM = 'qcm';

// Attributes
export const QUESTIONS_QCM = QUESTION;
export const ANSWER_QCM = 'answer';
export const PROPOSITION_QCM = 'proposition';
export const QUESTION_QCM_ID = 'qcm_id';
export const QUESTION_QCM_VALUE = 'qcm_value';

// Schema
export const SCHEMA_QCM = {
   [QUESTIONS_QCM]: { type: 'string' },
   [ANSWER_QCM]: { type: 'string' },
   [SCHEMA_ID]: { type: 'string' },
   [PROPOSITION_QCM]: { type: 'object' }
};
