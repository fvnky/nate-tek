import React from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';

import withTheme from "../../../../withTheme";
import styles from "./Definition.less";
import DefinitionCard from './DefinitionCard';

type Props = {
   theme: string;
   editor: any;
   value: any;
   children: any;
};


const DefinitionComponent: React.FC<Props> = (props) => {
   // let buttonRef = null;
   const [buttonRef, setButtonRef] = React.useState(null);

   const { editor, theme, value, children } = props;
   const [opened, setOpened] = React.useState(false);

   const open = () => {
      setOpened(true)
   }

   const close = () => {
      setOpened(false)
   }
   console.log('$$$$$$$', buttonRef)
   return (
      <>
         <button
            // onMouseEnter={open}
            // onMouseLeave={close}
            onClick={() => setOpened(!opened)}
            ref={i => setButtonRef(i)} >
            {children}
         </button>
         {opened &&
            <span contentEditable={false} style={{ position: "relative" }}>
               <DefinitionCard {...props} />
            </span>
         }
      </>
   )

}
export default withTheme(DefinitionComponent);
