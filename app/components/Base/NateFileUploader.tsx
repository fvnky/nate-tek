import React from 'react';

import styles from './NateFileUploader.less';

interface IProps {
   children: any;
   onChange: (e) => any;
   accept: string;
}

const NateFileUploader: React.FC<IProps> = ({ children, onChange, accept }) => {
   let uploaderRef = null;

   const onClick = () => {
      uploaderRef.click();
   };

   return (
      <div onClick={onClick} className={styles.uploaderContainer}>
         {children}
         <input
            ref={input => {
               uploaderRef = input;
            }}
            type="file"
            id="uploader"
            name="uploader"
            accept={accept}
            onChange={onChange}
         />
      </div>
   );
};

export default NateFileUploader;
