import * as React from 'react';
import cx from 'classnames';

// Icon
import IconCloseChubby from '../../../assets/icons/IconCloseChubby';
import IconPlus from '../../../assets/icons/IconPlus';

// Style
import styles from './Topbar.less';

// Typescript
import { ILanguage, EditorTab } from '../../../typescript';

interface IProps {
   lang: ILanguage;
   theme: string;

   // editor: any;
   activeIndex: number;
   tabs: EditorTab[];
   setActiveEditorTabIndex: (index: number) => void;
   switchEditorTabIndex: (a: number, b: number) => void;
   closeEditorTabIndex: (index: number) => void;
   addNewTab: () => void;

   // editorRightPanel: boolean;
   // toggleEditorRightPanel: () => void;
}

interface IState {
   hoveredIndex: number;
   dragging: boolean;
}

export default class TopBar extends React.Component<IProps, IState> {
   dragTarget: {
      target: HTMLElement;
      index: number;
      originalMouseX: number;
      memo: number;
   } = null;

   constructor(props: IProps) {
      super(props);
      this.state = {
         hoveredIndex: -1,
         dragging: false
      };
   }

   componentDidMount() {
      window.addEventListener('mousemove', this.handleMouseMove);
      window.addEventListener('mouseup', this.handleMouseUp);
   }

   componentWillUnmount() {
      window.removeEventListener('mousemove', this.handleMouseMove);
      window.removeEventListener('mouseup', this.handleMouseUp);
   }

   handleMouseDown = index => event => {
      event.preventDefault();
      event.stopPropagation();
      const { activeIndex, setActiveEditorTabIndex } = this.props;

      this.dragTarget = {
         target: event.currentTarget,
         index,
         originalMouseX: event.pageX,
         memo: 0
      };
      if (activeIndex !== index) setActiveEditorTabIndex(index);
   };

   handleMouseUp = event => {
      event.preventDefault();
      event.stopPropagation();

      if (this.dragTarget) {
         event.preventDefault();
         event.stopPropagation();
         this.setState({ dragging: false });

         this.dragTarget.target.style.transform = '';
         this.dragTarget = null;
      }
   };

   handleMouseMove = event => {
      if (this.dragTarget) {
         event.preventDefault();
         event.stopPropagation();
         const { dragging } = this.state;
         if (!dragging) this.setState({ dragging: true });

         const { tabs } = this.props;
         const { target, index, originalMouseX, memo } = this.dragTarget;
         const mouseOffset = event.pageX - originalMouseX + memo;

         // console.log(" el.style.transform: ",  target.style.transform)
         target.style.transform = `translate(${mouseOffset}px, 0px)`;
         target.style.transition = 'none';

         const targetMiddle =
            target.offsetLeft + target.clientWidth / 2 + mouseOffset;
         for (let i = 0; i < tabs.length; i += 1) {
            const el: HTMLElement = document.getElementById(`editor-tab-${i}`);
            el.style.transition = 'transform 200ms ease-out';
         }
         for (let i = 0; i < tabs.length; i += 1) {
            if (i !== index) {
               const el: HTMLElement = document.getElementById(
                  `editor-tab-${i}`
               );
               // console.log(" My middle (" + index+ ") is: " + targetMiddle)
               // console.log("middle of index " + i + " is : " + (el.offsetLeft + (el.clientWidth / 2)))
               const elMiddle = el.offsetLeft + el.clientWidth / 2;
               if (
                  (index < i && mouseOffset > 0 && targetMiddle > elMiddle) ||
                  (index > i && mouseOffset < 0 && targetMiddle < elMiddle)
               ) {
                  target.style.transform = '';
                  const memo: number =
                     (target.clientWidth / 2 - el.clientWidth / 2) *
                     (mouseOffset > 0 ? 1 : -1);
                  // console.log("memo is : ", memo)

                  el.style.transform = `translate(${memo}px, 0px)`;
                  this.onTabPositionChange(index, i);
                  this.dragTarget = {
                     target: el,
                     index: i,
                     originalMouseX: event.pageX,
                     memo
                  };
               }
            }
         }
      }
   };

   handleMouseOver = index => event => {
      event.preventDefault();
      event.stopPropagation();
      if (!this.dragTarget) {
         this.setState({ hoveredIndex: index });
      }
   };

   handleMouseLeave = event => {
      this.setState({ hoveredIndex: -1 });
   };

   onTabAdd = () => {
      console.log('new draft');
   };

   onTabPositionChange = (a: number, b: number) => {
      const { switchEditorTabIndex } = this.props;
      switchEditorTabIndex(a, b);
   };

   onTabClose = (index: number) => {
      // Change to double arrow function
      const { closeEditorTabIndex, tabs } = this.props;
      if (tabs[index].status === 'EDITED')
         console.error('CLOSING TAB, BUT WAS EDITED AND NOT SAVED TO DB');

      // const toClose = drafts.find((draft: IDraft) => draft._id === editorTabs.tabs[index]);
      // if (toClose && (!toClose.content || JSON.stringify(toClose.content) === JSON.stringify(initialValue))) {
      //    removeDraft(toClose);
      // }
      closeEditorTabIndex(index);
   };

   renderTabs = () => {
      const { tabs, activeIndex, theme, lang } = this.props;
      const { hoveredIndex } = this.state;
      return tabs.map((tab, index) => {
         const name =
            tab.temporaryName !== '' ? tab.temporaryName : lang.untitled;
         // const name =
         //    tab.status === "NEW"
         //       ? `${lang.newCourse} ${
         //            tab.id === "0" ? "" : parseInt(tab.id, 10) + 1 < 10 ? `0${parseInt(tab.id, 10) + 1}` : parseInt(tab.id, 10) + 1
         //         }`
         //       : courses.find((course: ICourse) => course._id === tab.id).name;
         const active = index === activeIndex;
         const hovered = index === hoveredIndex;
         return (
            <div
               // eslint-disable-next-line react/no-array-index-key
               key={index}
               role="tab"
               id={`editor-tab-${index}`}
               onMouseDown={this.handleMouseDown(index)}
               className={`${cx(theme, styles.tab)} ${
                  active ? styles.tabActive : ''
               } ${!active && hoveredIndex === index ? styles.tabHovered : ''}`}
               onMouseEnter={this.handleMouseOver(index)}
               onMouseLeave={this.handleMouseLeave}
            >
               <div />

               <div className={cx(theme, styles.tabNameWrapper)}>
                  <div className={cx(theme, styles.tabName)}>{name}</div>
               </div>

               <div
                  role="button"
                  onClick={() => this.onTabClose(index)}
                  className={`${cx(theme, styles.iconCloseTabWrapper)}`}
               >
                  <IconCloseChubby
                     className={`${cx(theme, styles.iconCloseTab)}`}
                  />
               </div>
               <div
                  className={
                     active || hovered ? '' : cx(theme, styles.separator)
                  }
               />
            </div>
         );
      });
   };

   render() {
      const { theme, addNewTab } = this.props;
      const { dragging } = this.state;
      return (
         <div className={cx(theme, styles.layoutTopbar)}>
            <div className={cx(theme, styles.tabs)}>
               {this.renderTabs()}

               <div
                  role="button"
                  onClick={addNewTab}
                  className={cx(theme, styles.iconAddNewTabWrapper)}
               >
                  {!dragging && (
                     <IconPlus className={cx(theme, styles.iconAddNewTab)} />
                  )}
               </div>
            </div>
         </div>
      );
   }
}
