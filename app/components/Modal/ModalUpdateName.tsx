import * as React from 'react';

// Component
import Modal from '../Base/Modal';

// Utils
import { capitalize, errorModal, str } from '../utils';

// Style
import styles from './ModalUpdate.less';

// typescript
import { UpdateUser, IUser, ILanguage } from '../../typescript';

// Type
interface IProps {
   user: IUser;
   updateUser: UpdateUser;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   firstName: string;
   lastName: string;
   [propName: string]: string;
}

export default class ModalUpdateName extends React.Component<IProps, IState> {
   lastNameInput: React.RefObject<HTMLInputElement> = React.createRef<
      HTMLInputElement
   >();

   firstNameInput: React.RefObject<HTMLInputElement> = React.createRef<
      HTMLInputElement
   >();

   constructor(props: IProps) {
      super(props);
      const { user } = this.props;

      if (!user) return errorModal(this.constructor.name, user);
      this.state = {
         firstName: user.firstName,
         lastName: user.lastName
      };
   }

   componentDidMount() {
      this.firstNameInput.current.focus();
   }

   handleCapitalize = (field: string) => (
      event: React.ChangeEvent<HTMLInputElement>
   ) => {
      const {
         target: { value }
      } = event;

      this.setState({
         [field]: capitalize(value)
      });
   };

   handleOk = () => {
      const { handleDisplayModal, user, updateUser } = this.props;
      const { firstName, lastName } = this.state;

      if (!user) return errorModal(this.constructor.name, user);
      user.firstName = firstName;
      user.lastName = lastName;
      updateUser(user);
      handleDisplayModal();
   };

   renderInputFirstName = () => {
      const { lang } = this.props;
      const { firstName } = this.state;

      return (
         <div>
            <span className={styles.nameModal}>{lang.firstName}</span>
            <input
               ref={this.firstNameInput}
               type="text"
               value={firstName}
               onChange={this.handleCapitalize(str({ firstName }))}
               className={styles.input}
               placeholder={lang.enterTitle}
            />
         </div>
      );
   };

   renderInputLastName = () => {
      const { lang } = this.props;
      const { lastName } = this.state;

      return (
         <div>
            <span className={styles.nameModal}>{lang.lastName}</span>
            <input
               ref={this.lastNameInput}
               type="text"
               value={lastName}
               onChange={this.handleCapitalize(str({ lastName }))}
               className={styles.input}
               placeholder={lang.enterTitle}
            />
         </div>
      );
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;

      return (
         <Modal
            theme={theme}
            title={lang.changeAccount}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.update}
         >
            {this.renderInputFirstName()}
            <br />
            {this.renderInputLastName()}
         </Modal>
      );
   }
}
