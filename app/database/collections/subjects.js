import _ from "lodash";

import { Store } from "../factory";
import { SubjectSchema } from "../schemas";
import { SUBJECT, FOLDER, COURSE, QUIZ } from "../constants";

import {
   ADD_SUBJECT,
   FETCH_SUBJECT,
   UPDATE_SUBJECT,
   REMOVE_SUBJECT
} from "../../reducers/subjects";

import {
   timestamp,
   generateId,
   sync,
   parseData,
   parseDoc,
   parseUpdate,
   isNotFound,
   findAllChildren
} from "../utils";

import {
   InvalidParameterFormat,
   SubjectAlreadyExists,
   SubjectNotFound
} from "../errors";

// ================================================ //
// ==================== Create ==================== //
// ================================================ //

function create(data) {
   return new Promise((ok, ko) => {
      const store = new Store();
      const mandatory = ["name"];
      const optionals = ["icon"];

      parseData(data, mandatory, optionals)
         .then(parsed => {
            parsed._id = generateId();
            parsed.icon = parsed.icon || "";
            parsed.createdAt = timestamp();
            parsed.updatedAt = parsed.createdAt;
            parsed.collection = SUBJECT;
            return this._main.put(parsed);
         })
         .then(res => store.add("subjectId", res.id))
         .then(id => this._main.get(id))
         .then(subject => parseDoc(SubjectSchema, subject))
         .then(subject => isDuplicateSubjectName.call(this, subject))
         .then(subject => this._main.put(subject))
         .then(res => this._main.get(res.id))
         .then(subject => {
            this.dispatch({ type: ADD_SUBJECT, payload: subject });
            return subject;
         })
         .then(ok)
         .catch(
            err =>
               new Promise((next, spread) => {
                  if (!store.subjectId) return spread(err);

                  this._main
                     .get(store.subjectId)
                     .then(doc => this._main.remove(doc._id, doc._rev))
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .catch(ko);
   });
}

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll() {
   return new Promise((ok, ko) => {
      this._main
         // .getIndexes()
         // .then(function (result) {
         // console.error("indexes", result)
         // })
         // .find({ selector: { collection: SUBJECT }})
         // .getIndexes()
         // .then((indexes) => console.log("indexes:" , indexes))
         // .explain({ selector: { 'collection': SUBJECT }})
         // .then(res => console.warn('FindAll Subject :', res))
         .find({ selector: { collection: SUBJECT } })
         .then(res => {
            // console.warn("QUERY SUBJECT :", res.docs);
            this.dispatch({ type: FETCH_SUBJECT, payload: res.docs });
            ok(res.docs);
         })
         .catch(ko);
   });
}

function findById(id) {
   return new Promise((ok, ko) => {
      if (!_.isString(id))
         return ko(
            new InvalidParameterFormat(`The id parameter is not a String`)
         );

      this._main
         .get(id)
         .then(doc => {
            if (doc.collection !== SUBJECT)
               throw new SubjectNotFound(
                  `The document's id '${doc._id}' does not reference a subject`
               );

            this.dispatch({ type: UPDATE_SUBJECT, payload: doc });
            ok(doc);
         })
         .catch(err => {
            if (isNotFound(err))
               throw new SubjectNotFound(
                  `The subject '${id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

function findByName(name) {
   return new Promise((ok, ko) => {
      if (!_.isString(name))
         return ko(
            new InvalidParameterFormat(`The name parameter is not a String`)
         );

      this._main
         .find({ selector: { collection: SUBJECT } })
         .then(res => {
            if (_.isEmpty(res.docs)) throw new SubjectNotFound();

            const subject = _.find(res.docs, { name });

            if (!subject) throw new SubjectNotFound();

            this.dispatch({ type: UPDATE_SUBJECT, payload: subject });
            ok(subject);
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

function update(doc) {
   return new Promise((ok, ko) => {
      const fields = ["children"];

      parseDoc(SubjectSchema, doc)
         .then(parsed => this._main.get(parsed._id))
         .then(old => parseUpdate(old, doc, fields))
         .then(subject => isDuplicateSubjectName.call(this, subject))
         .then(subject => {
            subject.updatedAt = timestamp();
            return this._main.put(subject);
         })
         .then(res => this._main.get(res.id))
         .then(subjects => {
            this.dispatch({ type: UPDATE_SUBJECT, payload: subjects });
            return subjects;
         })
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               throw new SubjectNotFound(
                  `The subject '${doc._id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Delete ==================== //
// ================================================ //

function remove(data) {
   return new Promise((ok, ko) => {
      const store = new Store();

      parseData(_.pick(data, ["_id"]), ["_id"])
         .then(parsed => this._main.get(parsed._id))
         .then(subject => store.add("subject", subject))
         .then(subject => {
            if (subject.collection !== SUBJECT)
               throw new SubjectNotFound(
                  `The document's id '${
                     subject._id
                  }' does not reference a subject`
               );
            return this._main.find({
               selector: {
                  "parent.collection": SUBJECT,
                  "parent.id": subject._id
               }
            });
         })
         .then(res => {
            console.log("res of remove Subject: ", res);
            return res.docs;
         })
         .then(children => {
            const task = function(child, index, next) {
               if (child.collection === FOLDER)
                  this.folders
                     .remove(child)
                     .then(() => next())
                     .catch(next);
               else if (child.collection === COURSE)
                  this.courses
                     .remove(child)
                     .then(() => next())
                     .catch(next);
               else if (child.collection === QUIZ)
                  this.quizs
                     .remove(child)
                     .then(() => next())
                     .catch(next);
               else next();
            };

            return sync(children, task.bind(this));
         })
         .then(() => this._main.get(store.subject._id))
         .then(({ _id, _rev }) => {
            const promises = [
               this._main.remove(_id, _rev),
               this._cloud.remove(_id, _rev)
            ];

            return Promise.all(promises);
         })
         .then(() => this.dispatch({ type: REMOVE_SUBJECT, payload: data }))
         .then(() => ok({ success: true }))
         .catch(err => {
            if (isNotFound(err))
               throw new SubjectNotFound(
                  `The subject '${data._id}' could not be found`
               );

            throw err;
         })
         .catch(ko);
   });
}

export default function() {
   return {
      create: create.bind(this),
      findAll: findAll.bind(this),
      findById: findById.bind(this),
      findByName: findByName.bind(this),
      update: update.bind(this),
      remove: remove.bind(this)
   };
}

// =============================================== //
// ==================== Utils ==================== //
// =============================================== //

function isDuplicateSubjectName(subject) {
   return new Promise((ok, ko) => {
      this._main
         .find({ selector: { collection: SUBJECT } })
         .then(res => {
            if (_.isEmpty(res.docs)) return ok(subject);

            if (
               _.some(
                  _.map(
                     res.docs,
                     item =>
                        item._id !== subject._id && item.name === subject.name
                  )
               )
            )
               throw new SubjectAlreadyExists(
                  `The subject's name '${subject.name}' already exists`
               );

            ok(subject);
         })
         .catch(ko);
   });
}
