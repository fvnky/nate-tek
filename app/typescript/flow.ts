// Models

export interface IReducerUser {
   _id?: string;
   pseudonym?: string;
   email?: string;
   createdAt?: number;
   firstName?: string;
   lastName?: string;
   academicYear?: number;
   _attachments?: any;
}

export interface IUser {
   _id: string;
   pseudonym: string;
   email: string;
   createdAt: number;
   firstName: string;
   lastName: string;
   academicYear: number;
   _attachments?: any;
}

export interface ISubject {
   _id: string;
   _rev: string;
   createdAt: number;
   updatedAt?: number;
   collection: string;
   name: string;
   icon: string;
   children: [];
}

export interface IFolder {
   _id: string;
   _rev: string;
   createdAt: number;
   updatedAt?: number;
   collection: string;
   name: string;
   parent: { id: string; collection: string };
   children: [];
}

export interface ICourse {
   _id: string;
   _rev: string;
   createdAt: number;
   updatedAt: number;
   collection: string;
   name: string;
   content?: JSON;
   parent: { id: string; collection: string };
}

export interface IParent {
   id: string;
   collection: string;
}

export interface IDbDocument {
   icon?: string;
   _id: string;
   _rev: string;
   collection?: string;
   updatedAt?: number;
   createdAt: number;
   name?: string;
   children?: Array<{ id: string }>;
   parent?: IParent;
}

/*
export interface INewFolder {
   name: string;
   parent: IParent;
}
*/

//  DATABASE FUNCTION
export type FindAll = () => Promise<ISubject[]>;
export type FindCourseById = (id: string) => Promise<ICourse>;
// export type CreateCourse = (newCourse: {
//    name: string,
//    folderId: string
// }) => Promise<ICourse>;
// export type CreateFolder = (newFolder: INewFolder) => Promise<IFolder>;
export type GetId = (id: string) => Promise<any>;

// REDUX FUNCTIONS
// ISubject
export type CreateSubject = (
   newSubject: {
      name: string;
      icon: string | null;
   }
) => Promise<ISubject>;
export type UpdateSubject = (toUpdate: ISubject) => Promise<ISubject>;
export type RemoveSubject = (toRemove: ISubject) => Promise<ISubject>;
// IFolder
export type CreateFolder = (
   newFolder: {
      name: string;
      parent: { id: string; collection: string };
   }
) => Promise<IFolder>;
export type UpdateFolder = (toUpdate: IFolder) => Promise<IFolder>;
export type RemoveFolder = (toRemove: IFolder) => Promise<IFolder>;
// ICourse
export type CreateCourse = (
   newCourse: {
      name: string;
      parent: { id: string; collection: string };
   }
) => Promise<ICourse>;
export type UpdateCourse = (toUpdate: ICourse) => Promise<ICourse>;
export type RemoveCourse = (toRemove: ICourse) => Promise<ICourse>;
// IUser
export type UpdateUser = (toUpdate: IUser) => Promise<IUser>;

// Database
export interface IDatabase {
   subjects: {
      findAll: FindAll;
      create: CreateSubject;
      update: UpdateSubject;
      remove: RemoveSubject;
   };
   folders: {
      create: CreateFolder;
      update: UpdateFolder;
      remove: RemoveFolder;
   };
   courses: {
      findById: FindCourseById;
      create: CreateCourse;
      update: UpdateCourse;
      remove: RemoveCourse;
   };
   _main: {
      get: GetId;
   };
   init: any;
}

// History
export interface IParams {
   SubjectID: string;
   FolderID: string;
   courseID: string;
}
export interface IMatchType {
   params: IParams;
   path: string;
}

export interface IModal {
   currentModal: string;
   propsModal: any;
}

export interface IModalProps {
   currentModal: string;
   propsModal: any;
}

export type ToggleModal = (modalName: string, modalProps: any) => void;

export type AnyJson =
   | boolean
   | number
   | string
   | null
   | IJsonArray
   | IJsonMap
   | object;
export interface IJsonMap {
   [key: string]: AnyJson;
}
export interface IJsonArray extends Array<AnyJson> {}
