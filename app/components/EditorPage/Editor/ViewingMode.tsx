import React, { Component } from 'react';
import { Base64 } from 'js-base64';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

import { Editor } from 'slate-react';
import { Value } from 'slate';

import cx from 'classnames';
import TextareaAutosize from 'react-autosize-textarea';

import defaultValue from '../../../constants/defaultValue';

import IconEditFromViewer from '../../../assets/icons/IconEditFromViewer';
import IconExportToPdf from '../../../assets/icons/IconExportToPdf';

import IconBack from '../../../assets/icons/IconBack';
import { FOLDER, SUBJECT, DRAFT, DRAFT_ID } from '../../../database/constants';

import styles from './Editor.less';
import VMstyles from './ViewingMode.less';

import { exportToPdf } from './exportPdfHelper';

// Typescript
import {
   ICourse,
   ISubject,
   IFolder,
   EditorTab,
   ToastOptions,
   ToggleModal
} from '../../../typescript';

import PdfViewer from './plugins/pdf/PdfViewer';

import CollapseOnEscape from './plugins/collapse-on-escape';
import SoftBreak from './plugins/soft-break';
import EditorShortcuts from './plugins/editorShortcuts';
import getEntireText from './plugins/getEntireText';
import HoverMenu from './plugins/hoverMenu/hoverMenu';
import Paragraph from './plugins/paragraph/paragraph';
import BasicFormatting from './plugins/basicFormatting';
import Highlight from './plugins/highlight/highlight';
import List from './plugins/list/list';
import Heading from './plugins/heading/heading';
import HtmlPaste from './plugins/htmlPaste';
import SuperScript from './plugins/superScript';
import pdf from './plugins/pdf/pdf';
import image from './plugins/image/image';
import Divider from './plugins/divider/divider';
import Quote from './plugins/quote/quote';
import InsertMenu from './plugins/insertMenu/insertMenu.js';
import Outline from './plugins/outline/outline.js';
import CoverImage from './CoverImage/CoverImage';
import Spinner from '../../Base/Spinner';
import { opacity } from 'html2canvas/dist/types/css/property-descriptors/opacity';
import IconDownload from '../../../assets/icons/IconDownload';

interface IProps {
   toggleModal: ToggleModal;
   edit: (docId: string) => void;
   back: () => void;
   db: any;
   theme: string;
   course: any;
   courses: ICourse[];
   subjects: ISubject[];
   folders: IFolder[];
   fromLibrary: boolean;
   // activeTab?: EditorTab;
}

interface IState {
   value: any;
   exporting: boolean;
}

export default class ViewingMode extends Component<IProps, IState> {
   editor: any;

   plugins = [
      // EditorShortcuts({ nateEditor: this }),
      Paragraph(),
      SoftBreak(),
      CollapseOnEscape(),
      BasicFormatting(),
      Heading(),
      HtmlPaste(),
      SuperScript(),
      image(),
      pdf(),
      Highlight(),
      List(),
      Divider(),
      Quote(),
      // HoverMenu(),
      getEntireText()
      // InsertMenu()
      // Outline()
   ];

   state = {
      value: null,
      exporting: false
   };

   componentDidMount() {
      const { db, course, back, fromLibrary } = this.props;

      if (!course) {
         back();
         return;
      }
      if (fromLibrary) {
         console.log('=====', course);
         const buffer = new Buffer(course.attachment);
         const base64data = buffer.toString('base64');

         console.log('base64data', base64data);
         const value = Value.fromJSON(JSON.parse(Base64.decode(base64data)));
         this.setState({ value });
      } else {
         db._main
            .get(course._id, { attachments: true })
            .then((doc: any) => {
               console.log('>>>>>>>>', doc);

               const base64 = doc._attachments['value.json'].data;
               const value = Value.fromJSON(JSON.parse(Base64.decode(base64)));
               this.setState({ value });
            })
            .catch(err => console.error(err));
      }
   }

   getParent = (course: ICourse) => {
      const { subjects, folders } = this.props;

      if (course.parent.collection === SUBJECT) {
         return subjects.find(s => s._id === course.parent.id);
      } else {
         return folders.find(f => f._id === course.parent.id);
      }
   };

   closePdfViewer = () => {
      if (this.editor) this.editor.closePdfViewer();
   };

   setRef = (editor: any) => {
      this.editor = editor;
   };

   onClickEdit = () => {
      const { edit, course } = this.props;
      if (course) edit(course._id);
   };

   exportPdf = () => {
      const { course } = this.props;
      this.setState({ exporting: true });
      exportToPdf(course.name)
         .then(res => this.setState({ exporting: false }))
         .catch(err => {
            console.error(err), this.setState({ exporting: false });
         });
   };

   saveToLocal = () => {
      const { toggleModal, course } = this.props;
      toggleModal('ModalMoveResource', { fromLibrary: true, course });
   };

   render() {
      const { theme, course, back, fromLibrary, db } = this.props;
      const { value, exporting } = this.state;

      if (!value) return <></>;

      console.warn('value', value.toJSON());
      const parentName = fromLibrary
         ? 'Nate Library course'
         : course.parent.collection === DRAFT
         ? 'Draft'
         : this.getParent(course).name;
      const pdfViewer =
         value && value.data ? value.data.get('pdfViewer') : null;
      if (this.editor) console.log(this.editor);
      return (
         <div className={VMstyles.layout}>
            <IconBack
               onClick={back}
               className={cx(theme, VMstyles.backButton)}
            />
            <div id="exporter-node" />
            <CoverImage db={db} course={course} readOnly={true} />
            <div className={VMstyles.editorWrapper}>
               <div
                  className={cx(theme, styles.editor5, VMstyles.editor)}
                  id="editor-node"
               >
                  <div className={styles.parentFolder}>
                     <div className={cx(theme, styles.parentName)}>
                        {parentName}
                     </div>
                  </div>
                  <TextareaAutosize
                     placeholder="Untitled"
                     className={cx(theme, styles.courseNameTextArea)}
                     value={course.name}
                     readOnly={true}
                  />
                  <div>
                     <Editor
                        placeholder="This course is Empty"
                        style={{ minHeight: '1000px' }} //, textAlign: 'justify' }}
                        value={value || defaultValue}
                        ref={this.setRef}
                        plugins={this.plugins}
                        spellCheck={true}
                        readOnly={true}
                     />
                  </div>

                  {pdfViewer && pdfViewer.visible && (
                     <PdfViewer
                        close={this.closePdfViewer}
                        file={pdfViewer.file}
                     />
                  )}
               </div>

               <div
                  style={{
                     transition: 'opacity 200ms',
                     opacity: exporting ? 1 : 0
                  }}
               >
                  <Spinner
                     className={VMstyles.exportButtonSpinner}
                     size={0.5}
                  />
               </div>
               <div
                  style={{
                     transition: 'opacity 200ms',
                     opacity: exporting ? 0 : 1
                  }}
               >
                  {!fromLibrary && (
                     <IconExportToPdf
                        onClick={this.exportPdf}
                        className={cx(theme, VMstyles.exportButton)}
                     />
                  )}
               </div>
               {fromLibrary ? (
                  <IconDownload
                     onClick={this.saveToLocal}
                     className={cx(theme, VMstyles.editButton)}
                  />
               ) : (
                  <IconEditFromViewer
                     onClick={this.onClickEdit}
                     className={cx(theme, VMstyles.editButton)}
                  />
               )}
            </div>
         </div>
      );
   }
}
