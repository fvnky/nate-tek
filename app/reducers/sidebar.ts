import { SET_COLLAPSE } from '../actions/sidebar';

// Typescript
import { ISidebar } from '../typescript';

import { IAction } from './types';

export function sidebar(state: ISidebar = { collapse: true }, action: IAction) {
   switch (action.type) {
      case SET_COLLAPSE: {
         return { collapse: action.payload };
      }
      default:
         return state;
   }
}
