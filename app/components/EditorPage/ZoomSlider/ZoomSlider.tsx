import * as React from "react";
import cx from "classnames";

import SliderInput from "../../Base/SliderInput";
import IconPlus from "../../../assets/icons/IconPlus";
import IconMinus from "../../../assets/icons/IconMinus";

// Style
import styles from "./ZoomSlider.less";

interface IProps {
   theme: string;
   setZoom: (value: number) => void;
   zoom: number;
}

interface IState {}

export default class ZoomSlider extends React.Component<IProps, IState> {
   onChange = e => {
      const { setZoom } = this.props;
      const scale = parseFloat(e.target.value);
      setZoom(scale);
   };

   zoomIn = () => {
      const { setZoom, zoom } = this.props;
      if (zoom + 0.25 <= 2) setZoom(zoom + 0.25);
   };

   zoomOut = () => {
      const { setZoom, zoom } = this.props;
      if (zoom - 0.25 >= 0.5) setZoom(zoom - 0.25);
   };

   render() {
      const { theme, zoom, setZoom } = this.props;
      return (
         <div className={styles.layout}>
            <div role="button" onClick={this.zoomOut} className={cx(theme, styles.buttonContainer)}>
               <IconMinus className={cx(theme, styles.button)}/>
            </div>
            <div className={styles.container}>
               <SliderInput value={zoom} onChange={this.onChange} min="0.5" max="2" step="0.25" />
            </div>
            <div role="button" onClick={this.zoomIn} className={cx(theme, styles.buttonContainer)}>
               <IconPlus className={cx(theme, styles.button)}  />
            </div>
         </div>
      );
   }
}
