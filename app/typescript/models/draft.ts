export interface IDraft {
   _id: string;
   _rev: string;
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   name?: string;
   parent: {
      id: string;
      collection: string;
   };
   content?: JSON;
}
