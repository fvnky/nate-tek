import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import EditorState from '../components/EditorPage/EditorState';
import * as CourseActions from '../actions/courses';

// eslint-disable-next-line no-unused-vars
function mapStateToProps(state) {
   return {
      db: state.database,
      courses: state.courses,
      todos: state.todo,
      lang: state.language,
      router: state.router,
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...CourseActions,
      },
      dispatch
   );
}

export default connect(mapStateToProps, mapDispatchToProps)(EditorState);
