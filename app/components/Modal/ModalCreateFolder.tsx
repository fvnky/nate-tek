import * as React from 'react';

// Component
import Modal from '../Base/Modal';

// Utils
import { capitalize } from '../utils';

// Style
import stylesErr from './ModalError.less';
import styles from './ModalFolder.less';

// typescript
import { CreateFolder, ILanguage } from '../../typescript';

interface IProps {
   parentId: string;
   collection: string;
   createFolder: CreateFolder;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   title: string;
   message: string;
}

export default class ModalCreateFolder extends React.Component<IProps, IState> {
   input: React.RefObject<HTMLInputElement> = React.createRef();

   constructor(props: IProps) {
      super(props);
      this.state = {
         title: '',
         message: ''
      };
   }

   componentDidMount() {
      const input = this.input.current;

      if (input) input.focus();
   }

   handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { target } = event;
      const input = this.input.current;

      if (input) {
         if (input.className !== styles.input) {
            input.className = styles.input;
            this.setState({ message: '' });
         }
      }
      this.setState({ title: capitalize(target.value) });
   };

   handleOk = () => {
      const {
         createFolder,
         handleDisplayModal,
         parentId,
         collection
      } = this.props;
      const { title } = this.state;
      const newFolder = {
         name: title,
         parent: { id: parentId, collection }
      };
      const input = this.input.current;

      createFolder(newFolder)
         .then(() => handleDisplayModal())
         .catch((err: any) => {
            if (input) {
               input.focus();
               input.setSelectionRange(0, input.selectionEnd);
               input.className = `${styles.input} ${stylesErr.inputErrorColor}`;
            }
            this.setState({ message: err.message });
         });
   };

   renderInputName = () => {
      const { title, message } = this.state;
      const { lang } = this.props;

      return (
         <div>
            <div className={stylesErr.errorMessage}>{message}</div>
            <input
               ref={this.input}
               type="text"
               value={title}
               onChange={this.handleChange}
               className={styles.input}
               placeholder={lang.enterTitle}
            />
         </div>
      );
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;

      return (
         <Modal
            theme={theme}
            title={lang.newFolder}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.create}
            cancelText={lang.cancel}
         >
            {this.renderInputName()}
         </Modal>
      );
   }
}
