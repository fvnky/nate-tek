import TEXT from './text.json';
import { ILanguage } from '../typescript';

export function loadLanguage(language: string): ILanguage {
   const doc = {};
   Object.keys(TEXT).forEach((element: string) => {
      doc[element] = TEXT[element][language];
   });
   return doc as ILanguage;
}
