// Constant
import { FIND_BY_ID, CREATE, UPDATE, REMOVE } from '../../constants/models';

interface IParent {
   id: string;
   collection: string;
}

export interface ICourse {
   _id: string;
   _rev: string;
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   name?: string;
   likes: Array<string>;
   comments: Array<object>;
   parent?: IParent;
   _attachments?: { 'value.json': { data: any } };
   textContent?: string;
   coverImage: {
      full: string;
      raw: string;
      regular: string;
      small: string;
      thumb: string;
   }
}

export type FindCourseById = (id: string) => Promise<ICourse>;
export type CreateCourse = (
   newCourse: {
      name: string;
      parent: { id: string; collection: string };
   },
   content?: Object
) => Promise<ICourse>;
export type UpdateCourse = (
   toUpdate: ICourse,
   content?: Object
) => Promise<ICourse>;
export type RemoveCourse = (toRemove: ICourse) => Promise<ICourse>;

export type IDatabaseCourse = {
   [FIND_BY_ID]: FindCourseById;
   [CREATE]: CreateCourse;
   [UPDATE]: UpdateCourse;
   [REMOVE]: RemoveCourse;
};
