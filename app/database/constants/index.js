// ================================================ //
// ==================== Schema ==================== //
// ================================================ //

export const USER = 'user';
export const PROFILE = 'profile';
export const SUBJECT = 'subject';
export const FOLDER = 'folder';
export const COURSE = 'course';
export const CONVERTIBLE = 'convertible';
export const DEFINITION = 'definition';
export const QUIZ = 'quiz';
export const DRAFT = 'draft';
export const DICTIONARY = 'dictionary';
export const SCHEDULE = 'schedule';

// =============================================== //
// ==================== Types ==================== //
// =============================================== //

export const DRAFT_ID = 'IamTheOnlyOneDraftIcanTrust';

// ================================================= //
// ==================== Actions ==================== //
// ================================================= //
