import * as React from "react";

const Italic = props => (
   <svg {...props} viewBox="0 0 18 18">
      <path
         d="M13,3H7C6.4,3,6,3.4,6,4s0.4,1,1,1h1.8l-1.6,8H5c-0.6,0-1,0.4-1,1s0.4,1,1,1h6c0.6,0,1-0.4,1-1s-0.4-1-1-1H9.2l1.6-8H13
	c0.6,0,1-0.4,1-1S13.6,3,13,3z"
      />
   </svg>
);

export default Italic;
