import React from 'react';
import cx from 'classnames';
import DatePicker from 'react-datepicker';

// Utils
import { capitalize } from '../utils';

// Style
import styles from './Todo.less';
// Typescript
import { IDatabase, ITodo } from '../../typescript';

import {
   TODO_DB,
   CREATE,
   REMOVE,
   UPDATE,
   COLLECTION_DB
} from '../../constants/models/database';

// Attributes of model todo
import { NAME_TODO, DUE_DATE, IS_DONE } from '../../constants/models/todo';

type IProps = {
   database: IDatabase;
   theme: string;
};

interface IState {
   title: string;
   dueDate: number;
   status: boolean;
}

export default class TodoCreate extends React.Component<IProps, IState> {
   input: React.RefObject<HTMLInputElement> = React.createRef();

   state = {
      title: '',
      dueDate: Date.parse(Date()),
      status: false
   };

   componentDidMount() {
      const input = this.input.current;

      if (input) input.focus();
   }

   handleStateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      this.setState({ status: event.target.checked });
   };

   handleTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { target } = event;
      this.setState({ title: capitalize(target.value) });
   };

   handleDateChange = (newDate: any) => {

      this.setState({ dueDate: newDate.getTime() });
   };

   handleCancel = () => {
      this.setState({
         title: '',
         dueDate: Date.parse(Date())
      });
   };

   handleCreateTodo = () => {
      const { title, dueDate, status } = this.state;
      const { database } = this.props;
      if (title == '') return;
      database[TODO_DB][CREATE]({
         [NAME_TODO]: title,
         [DUE_DATE]: dueDate,
         [IS_DONE]: status
      })
         // .then(() => {
         //    const timeDiff = dueDate - new Date().getTime();
         //    console.warn(timeDiff);
         //    if (timeDiff <= 0) return;
         //    setTimeout(() => {
         //       new Notification('To Do', {
         //          body: title
         //       });
         //    }, timeDiff);
         // })
         .then(() => {
            this.setState({
               title: '',
               dueDate: Date.parse(Date())
            });
         })
         .then(() => console.log('created todo ' + '"' + title + '"'))
         .catch(err => console.warn(err));
   };

   renderInputTitle = () => {
      const { title } = this.state;
      const { theme } = this.props;

      return (
         <div>
            <input
               ref={this.input}
               type="text"
               value={title}
               onChange={this.handleTitleChange}
               className={cx(theme, styles.input)}
               placeholder={"What's next ?"}
               autoFocus={true}
            />
         </div>
      );
   };

   renderInputDate = () => {
      const { dueDate } = this.state;
      const { theme } = this.props;

      return (
         <div>
            {/* <input
               type="datetime-local"
               value={
                  new Date(
                     new Date(dueDate).toString().split('GMT')[0] + ' UTC'
                  )
                     .toISOString()
                     .split('.')[0]
               }
               min={
                  new Date(new Date().toString().split('GMT')[0] + ' UTC')
                     .toISOString()
                     .split('.')[0]
               }
               onChange={this.handleDateChange}
               className={cx(theme, styles.inputDate)}
            /> */}
            <DatePicker
               className={styles.dateInput}
               // popperPlacement
               popperPlacement={"bottom-left"}
               selected={dueDate}
               onChange={this.handleDateChange}
               showTimeSelect
               timeFormat="HH:mm"
               timeIntervals={1}
               timeCaption="time"
               dateFormat="MMMM dd, yyyy HH:mm"
            />
         </div>
      );
   };

   render() {
      const { theme } = this.props;

      return (
         <div className={styles.todoCreateCardBorder}>
            <ul className={styles.todoCardContents}>
               <li className={styles.todoInputText}>
                  {this.renderInputTitle()}
                  {this.renderInputDate()}
               </li>

               <li>
                  {/* <input type="button" value="x" onClick={this.handleCancel} /> */}
               </li>
            </ul>
            <input
               type="button"
               className={styles.todoCreateButton}
               value={'Add'}
               onMouseDown={this.handleCreateTodo}
            />
         </div>
      );
   }
}
