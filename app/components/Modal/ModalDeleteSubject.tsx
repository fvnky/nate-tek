import * as React from 'react';
import cx from 'classnames';
// Component
import Modal from '../Base/Modal';

// Utils
import { errorModal } from '../utils';

// Icons
import { iconsSubject } from '../../assets/icons/Subject/IconsSubject';

// Style
import styles from './ModalDeleteSubject.less';

// typescript
import {
   IDatabase,
   RemoveSubject,
   ISubject,
   ILanguage
} from '../../typescript';

interface IProps {
   id: string;
   db: IDatabase;
   removeSubject: RemoveSubject;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   subject: ISubject | null;
   title: string;
   icon: string;
}

export default class ModalDeleteSubject extends React.Component<
   IProps,
   IState
> {
   constructor(props: IProps) {
      super(props);

      this.state = {
         title: '',
         icon: 'IconQuestionMark',
         subject: null
      };
   }

   componentDidMount() {
      const { id, db } = this.props;

      db.subjects
         .findById(id)
         .then((subject: ISubject) =>
            this.setState({ subject, title: subject.name, icon: subject.icon })
         )
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   }

   handleKeyDown = (e: KeyboardEvent) => {
      const { handleDisplayModal } = this.props;

      if (e.keyCode === 27) handleDisplayModal();
   };

   handleOk = () => {
      const { handleDisplayModal, removeSubject } = this.props;
      const { subject } = this.state;

      if (!subject) return;
      removeSubject(subject)
         .then(() => handleDisplayModal())
         .catch((err: any) => {
            throw new Error(`${this.constructor.name} ${err.message}`);
         });
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;
      const { title, icon } = this.state;
      const ICON = iconsSubject[icon];

      if (!ICON) return errorModal(this.constructor.name, ICON);
      return (
         <Modal
            theme={theme}
            titleLess={true}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.delete}
            cancelText={lang.cancel}
            handleKeyDown={this.handleKeyDown}
         >
            {lang.deleteItem}
            {title} ?
            {/* <div className={styles.layout} >
               <ICON className={cx(theme, styles.icon)} />
            </div> */}
         </Modal>
      );
   }
}
