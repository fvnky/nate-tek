
export interface ISidebar {
   collapse: boolean;
}

export type setCollapse = (collapse: boolean) => void;
