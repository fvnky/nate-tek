import * as React from 'react';
import cx from 'classnames';

// Style
import styles from './SliderInput.less';

interface IProps {
   onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
   min: string;
   max: string;
   step?: string;
   defaultValue?: string;
   value?: number;
   // theme: string;
}

interface IState {}

export default class SliderInput extends React.Component<IProps, IState> {
   render() {
      const {
         value,
         onChange,
         min,
         max,
         step = '0.01',
         defaultValue
      } = this.props;

      return (
         <input
            value={value}
            className={styles.scale}
            name="scale"
            type="range"
            onChange={onChange}
            min={min}
            max={max}
            step={step}
            defaultValue={defaultValue}
         />
      );
   }
}
