import * as React from 'react';
import IconQuiz from '../IconQuiz';
import IconQuestionMark from './IconQuestionMark';
import NounBiology1856897 from '../NounIcons/NounBiology1856897';
import NounArcheology977624 from '../NounIcons/NounArcheology977624';
import NounPsychology392525 from '../NounIcons/NounPsychology392525';
import NounMathematics1932994 from '../NounIcons/NounMathematics1932994';
import NounStatistics2011669 from '../NounIcons/NounStatistics2011669';
import NounCommunication1912201 from '../NounIcons/NounCommunication1912201';
import NounManagement1248511 from '../NounIcons/NounManagement1248511';
import NounHandshake501983 from '../NounIcons/NounHandshake501983';
import NounWrite1632591 from '../NounIcons/NounWrite1632591';
import NounLaw585610 from '../NounIcons/NounLaw585610';
import NounCoding1807264 from '../NounIcons/NounCoding1807264';
import NounEconomics1292432 from '../NounIcons/NounEconomics1292432';
import NounEconomics615480 from '../NounIcons/NounEconomics615480';
import NounSkull1642395 from '../NounIcons/NounSkull1642395';
import NounFrench902388 from '../NounIcons/NounFrench902388';
import NounEarth442803 from '../NounIcons/NounEarth442803';
import NounCommunication1305431 from '../NounIcons/NounCommunication1305431';
import NounPhysics1881546 from '../NounIcons/NounPhysics1881546';
import NounHistory152493 from '../NounIcons/NounHistory152493';
import NounPsychology1932987 from '../NounIcons/NounPsychology1932987';
import NounEarth1213079 from '../NounIcons/NounEarth1213079';
import IconCourse from '../NounIcons/IconCourse';
import NounScience103308 from '../NounIcons/NounScience103308';
import NounEconomics1831193 from '../NounIcons/NounEconomics1831193';
import NounPsychology381012 from '../NounIcons/NounPsychology381012';
import NounEducation851357 from '../NounIcons/NounEducation851357';
import NounCultures1122486 from '../NounIcons/NounCultures1122486';
import NounGerman902390 from '../NounIcons/NounGerman902390';
import NounLatin902431 from '../NounIcons/NounLatin902431';
import NounMedicine50184 from '../NounIcons/NounMedicine50184';
import NounPsychology314764 from '../NounIcons/NounPsychology314764';
import NounNetwork21268 from '../NounIcons/NounNetwork21268';
import NounEngineering1246951 from '../NounIcons/NounEngineering1246951';
import NounBuilding1009876 from '../NounIcons/NounBuilding1009876';
import NounSpeech1956248 from '../NounIcons/NounSpeech1956248';
import NounLanguage1824073 from '../NounIcons/NounLanguage1824073';
import NounComputer1173121 from '../NounIcons/NounComputer1173121';
import NounArt372614 from '../NounIcons/NounArt372614';
import NounEngineering1593557 from '../NounIcons/NounEngineering1593557';
import NounMathematics1910544 from '../NounIcons/NounMathematics1910544';
import NounSocialNetwork633318 from '../NounIcons/NounSocialNetwork633318';
import HandMoney from '../NounIcons/HandMoney';
import NounBiology1856222 from '../NounIcons/NounBiology1856222';
import NounHealth2005083 from '../NounIcons/NounHealth2005083';
import NounItalian902411 from '../NounIcons/NounItalian902411';
import NounMan179773 from '../NounIcons/NounMan179773';
import NounEnglish902379 from '../NounIcons/NounEnglish902379';
import NounHuman253328 from '../NounIcons/NounHuman253328';
import IconFolder from '../NounIcons/IconFolder';
import NounLaw1944907 from '../NounIcons/NounLaw1944907';
import NounLogic1997758 from '../NounIcons/NounLogic1997758';
import NounJapanese902413 from '../NounIcons/NounJapanese902413';
import NounChinese902369 from '../NounIcons/NounChinese902369';

const iconsSubject = {
   IconQuestionMark,
   NounBiology1856897,
   NounArcheology977624,
   NounPsychology392525,
   NounMathematics1932994,
   NounStatistics2011669,
   NounCommunication1912201,
   NounManagement1248511,
   NounHandshake501983,
   NounWrite1632591,
   NounLaw585610,
   NounCoding1807264,
   NounEconomics1292432,
   NounEconomics615480,
   NounSkull1642395,
   NounFrench902388,
   NounEarth442803,
   NounCommunication1305431,
   NounPhysics1881546,
   NounHistory152493,
   NounPsychology1932987,
   NounEarth1213079,
   IconCourse,
   NounScience103308,
   NounEconomics1831193,
   NounPsychology381012,
   NounEducation851357,
   NounCultures1122486,
   NounGerman902390,
   NounLatin902431,
   NounMedicine50184,
   NounPsychology314764,
   NounNetwork21268,
   NounEngineering1246951,
   NounBuilding1009876,
   NounSpeech1956248,
   NounLanguage1824073,
   NounComputer1173121,
   NounArt372614,
   NounEngineering1593557,
   NounMathematics1910544,
   NounSocialNetwork633318,
   HandMoney,
   NounBiology1856222,
   NounHealth2005083,
   NounItalian902411,
   NounMan179773,
   NounEnglish902379,
   NounHuman253328,
   IconFolder,
   NounLaw1944907,
   NounLogic1997758,
   NounJapanese902413,
   NounChinese902369,
   IconQuiz
};

export { iconsSubject };
