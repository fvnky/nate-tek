import * as React from 'react';
import cx from 'classnames';
import ZoomSlider from '../ZoomSlider/ZoomSlider';
import renderInsertButton from './InsertButton';
import FormatButton from '../FormatButtons/FormatButton';
import HeadingButton from '../FormatButtons/HeadingButton';
import HighlightButton from '../FormatButtons/HighlightButton';
import Bold from '../../../assets/icons/Editor/Bold';
import Italic from '../../../assets/icons/Editor/Italic';
import Underline from '../../../assets/icons/Editor/Underline';
import Superscript from '../../../assets/icons/Editor/Superscript';
import UnorderedList from '../../../assets/icons/Editor/UnorderedList';
import OrderedList from '../../../assets/icons/Editor/OrderedList';
import Indent from '../../../assets/icons/Editor/Indent';
import Outdent from '../../../assets/icons/Editor/Outdent';
import AddImage from '../../../assets/icons/Editor/AddImage';
import PdfDocument from '../../../assets/icons/Editor/PdfDocument';
import Link from '../../../assets/icons/Editor/Link';
import Divider from '../../../assets/icons/Editor/Divider';
import Quote from '../../../assets/icons/Editor/Quote';
import Formula from '../../../assets/icons/Editor/Formula';

import styles from './RightPanel.less';

interface IProps {
   setZoom: (value: number) => void;
   editor: any;
   theme: string;
   editorRightPanel: boolean;
   toggleEditorRightPanel: () => void;
   zoom: number;
}

export default class RightPanel extends React.Component<IProps> {
   render() {
      const {
         setZoom,
         zoom,
         editorRightPanel,
         editor,
         theme,
         toggleEditorRightPanel
      } = this.props;
      const visibility = editorRightPanel ? styles.visible : styles.hidden;
      if (!editor) return <></>;
      return (
         <div
            id="toolbarRight"
            className={`${cx(theme, styles.container)} ${visibility}`}
         >
            <div className={styles.widgetContainer}>
               <button onClick={toggleEditorRightPanel}> switch</button>
               <span className={cx(theme, styles.title)}>TABS</span>
            </div>

            <div className={styles.widgetContainer}>
               <span className={cx(theme, styles.title)}>FORMAT</span>
               <div className={`${styles.row} ${styles.center}`}>
                  <HighlightButton theme={theme} type={1} />
                  <HighlightButton theme={theme} type={2} />
                  <HighlightButton theme={theme} type={3} />
                  <HighlightButton theme={theme} type={4} />
                  <HighlightButton theme={theme} type={5} />
               </div>
               <div className={`${styles.row} ${styles.spaceEvenly}`}>
                  <HeadingButton theme={theme} heading={1} />
                  <HeadingButton theme={theme} heading={2} />
                  <HeadingButton theme={theme} heading={3} />
                  <HeadingButton theme={theme} heading={4} />
               </div>
               <div className={`${styles.row} ${styles.spaceEvenly}`}>
                  <FormatButton
                     theme={theme}
                     icon={<Bold />}
                     callback={editor.toggleBold}
                     isActive={editor.isBold}
                  />
                  <FormatButton
                     theme={theme}
                     icon={<Italic />}
                     callback={editor.toggleItalic}
                     isActive={editor.isItalic}
                  />
                  <FormatButton
                     theme={theme}
                     icon={<Underline />}
                     callback={editor.toggleUnderline}
                     isActive={editor.isUnderline}
                  />
                  <FormatButton theme={theme} icon={<Superscript />} />
               </div>
               <div className={`${styles.row} ${styles.spaceEvenly}`}>
                  <FormatButton theme={theme} icon={<UnorderedList />} />
                  <FormatButton theme={theme} icon={<OrderedList />} />
                  <FormatButton theme={theme} icon={<Indent />} />
                  <FormatButton theme={theme} icon={<Outdent />} />
               </div>
               <div className={`${styles.center}`}>
                  <FormatButton theme={theme} text="Clear" />
               </div>
            </div>

            <div className={styles.widgetContainer}>
               <span className={cx(theme, styles.title)}>INSERT</span>
               <div className={`${styles.row} ${styles.center}`}>
                  {renderInsertButton(theme, AddImage, 'Image', () => null)}
                  {renderInsertButton(theme, PdfDocument, 'Pdf', () => null)}
                  {renderInsertButton(theme, Link, 'Link', () => null)}
               </div>
               <div className={`${styles.row} ${styles.center}`}>
                  {renderInsertButton(theme, Quote, 'Quote', () => null)}
                  {renderInsertButton(theme, Divider, 'Divider', () => null)}
                  {renderInsertButton(theme, Formula, 'Formula', () => null)}
               </div>
            </div>

            <div className={styles.widgetContainer}>
               <span className={cx(theme, styles.title)}>VIEW</span>
               <ZoomSlider theme={theme} setZoom={setZoom} zoom={zoom} />
            </div>
         </div>
      );
   }
}
