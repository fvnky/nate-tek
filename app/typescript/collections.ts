export interface ICouchDB {
   _id: string;
   _rev: string;
}

export interface ICollection extends ICouchDB {
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   name?: string;
}

export interface IParent {
   id: string;
   collection: string;
}

export interface ISubject extends ICollection {
   icon: string;
   children: IParent[];
}

export interface IFolder extends ICollection {
   parent: IParent;
   children: IParent[];
}

export interface ICourse extends ICollection {
   parent?: IParent;
   _attachments?: { 'value.json': { data: any } };
   textContent?: string;
}

export interface IDraft extends ICollection {
   parent: IParent;
   content?: JSON;
}

export interface IQuiz extends ICollection {
   parent?: IParent;
   questions?: any;
}

export interface IDbDocument extends ICouchDB {
   updatedAt?: number;
   createdAt?: number;
   icon?: string;
   collection?: string;
   name?: string;
   children?: Array<{ id: string }>;
   parent?: IParent;
}

export interface IUser {
   country: string;
   curriculum: string;
   firstname: string;
   lang: string;
   lastname: string;
   mail: string;
   theme: string;
   university: string;
   username: string;
   year: number;
   _id: string;
   _attachments?: any;
}

// TODO Review this part /!\
export interface IReducerUser {
   _id?: string;
   pseudonym?: string;
   email?: string;
   createdAt?: number;
   firstName?: string;
   lastName?: string;
   academicYear?: number;
   _attachments?: any;
}

// Question

export interface IFormTrueFalse {
   type: string;
   form: ITrueFalse;
}

export interface ITrueFalse {
   question: string;
   answer: boolean;
}
