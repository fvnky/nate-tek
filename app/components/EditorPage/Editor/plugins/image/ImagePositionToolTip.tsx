import React, { Component } from "react";
import cx from "classnames";
import { CSSTransition } from 'react-transition-group';

import AlignImageLeft from "../../../../../assets/icons/Editor/AlignImageLeft";
import AlignImageCenter from "../../../../../assets/icons/Editor/AlignImageCenter";
import AlignImageExtended from "../../../../../assets/icons/Editor/AlignImageExtended";

import styles from './ImagePositionToolTip.less';

import {
   IMAGE_STYLE_CENTER,
   IMAGE_STYLE_LEFT,
   IMAGE_STYLE_EXTENDED
} from './image';

interface IProps {
   theme: string;
   isFocused: Boolean;
   editor: any;
   node: any;
   style: string;
}

export default class ImagePositionToolTip extends Component<IProps> {
   changeStyle = style => e => {
      e.preventDefault();
      e.stopPropagation();
      const { editor, node } = this.props;
      editor.setImageStyle(node, style);
   };

   render() {
      const { isFocused, theme, style } = this.props;
      return (
         <CSSTransition
         in={isFocused}
         timeout={200}
         classNames={{
            appear: '',
            appearActive: '',
            enter: styles.enter,
            enterActive: styles.enterActive,
            enterDone: '',
            exit: styles.exit,
            exitActive: styles.exitActive,
            exitDone: '',
         }}
         unmountOnExit
      >
         <div className={styles.layout} >
            <div className={cx(theme, styles.tooltip)} >
            <AlignImageLeft onMouseDown={this.changeStyle(IMAGE_STYLE_LEFT)} className={cx(theme,styles.icon, style === IMAGE_STYLE_LEFT ? styles.active : "")} />
            <AlignImageCenter onMouseDown={this.changeStyle(IMAGE_STYLE_CENTER)} className={cx(theme,styles.icon, style === IMAGE_STYLE_CENTER ? styles.active : "")}/>
            <AlignImageExtended onMouseDown={this.changeStyle(IMAGE_STYLE_EXTENDED)} className={cx(theme,styles.icon, style === IMAGE_STYLE_EXTENDED ? styles.active : "")} />

            </div>
         </div>
         </CSSTransition>
      );
   }
}
