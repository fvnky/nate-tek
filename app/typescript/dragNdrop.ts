export interface IDrop {
   [key: string]: string;
   key: string;
}

export interface IDrag {
   [key: string]: string;
}
