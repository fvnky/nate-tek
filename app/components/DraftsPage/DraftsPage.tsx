import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import cx from 'classnames';

// Component
import IconPlusButton from '../../assets/icons/IconPlusButton';
import List from '../Base/List/List';
import {
   fillDocument,
   MOVE_POPUP,
   EDIT_POPUP,
   DELETE_POPUP,
   ONCLICK_LIST,
   DROPPABLE_LIST,
   //DRAGGABLE_LIST,
   POPUP_LIST,
   ICON_LIST,
   NAME_LIST
} from '../Base/List/Descriptor';

// Style
import styles from './DraftsPage.less';

// Typescript
import { ILanguage, IDraft, ToggleModal, ICourse } from '../../typescript';

// Constant
import { DRAFTS } from '../../constants/tabs';
const LIST_ITEM = [
   ICON_LIST,
   NAME_LIST,
   ONCLICK_LIST,
   DROPPABLE_LIST,
   //DRAGGABLE_LIST,
   POPUP_LIST
];
const LIST_POPUP = [MOVE_POPUP, EDIT_POPUP, DELETE_POPUP];

interface IProps extends RouteComponentProps {
   lang: ILanguage;
   theme: string;
   toggleModal: ToggleModal;
   courses: ICourse[];
   setLastActiveNavigationTab: (tab: string) => void;
   setViewerDocId: (path: string) => void;
}

export default class DraftsPage extends React.Component<IProps> {
   componentDidMount() {
      const { setLastActiveNavigationTab } = this.props;
      // this.animateChildrenList();
      setLastActiveNavigationTab(DRAFTS);
   }

   courseDescriptor = () => {
      const { courses, history } = this.props;

      const drafts = courses.filter(
         course => course.parent.collection === 'draft'
      );

      const patternCourse = [];
      drafts.forEach((course: ICourse) => {
         const descriptor = fillDocument(
            course,
            history,
            LIST_POPUP,
            LIST_ITEM
         );
         patternCourse.push(descriptor);
      });
      return [patternCourse];
   };

   render() {
      const { courses, theme, lang } = this.props;

      const drafts = courses.filter(
         course => course.parent.collection === 'draft'
      );
      return (
         <div className={styles.layout}>
            {drafts.length === 0 ? (
               <div className={cx(theme, 'empty-list')}>{lang.emptyDrafts}</div>
            ) : (
               <List list={this.courseDescriptor()} {...this.props} />
            )}
            {/* <IconPlusButton className={cx(theme, styles.addSubject)} onClick={() => null} /> */}
         </div>
      );
   }
}
