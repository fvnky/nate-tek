
// import { Action } from './types';

import { CREATE_SUBJECT_ERROR } from '../actions/subjects';

export function error(state = {}, action) {
   switch (action.type) {
      case CREATE_SUBJECT_ERROR: {
         return action.payload;
      }
      default: {
         return state;
      }
   }
}
