import { match } from 'react-router';

export * from './language';
export * from './sidebar';
export * from './json';
export * from './modal';
export * from './dragNdrop';
export * from './list';
// export * from './database';
// export * from './collections';
export * from './theme';
export * from './models';
export * from './QuizPage';
export * from './calendar';

export interface IMatchParams {
   [key: string]: string;
}
export interface IMatch extends match<IMatchParams> {}

export type EditorTab = {
   status: string;
   id: string;
   temporaryName: string;
   value?: any;
   pdfViewer: { file: string; page: number };
};
export type ToastOptions = {
   text: string;
   iconName?: string;
   timeout?: number;
};

export interface IReducerUser {
   _id?: string;
   pseudonym?: string;
   email?: string;
   createdAt?: number;
   firstName?: string;
   lastName?: string;
   academicYear?: number;
   _attachments?: any;
}

export interface ITodo {
   _id: string;
   _rev: string;
   content: string;
   dueAt: number;
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   status?: boolean;
   name: string;
}

export type UpdateTodo = (toUpdate: ITodo) => Promise<ITodo>;
export type RemoveTodo = (toRemove: ITodo) => Promise<ITodo>;
export type FindTodoById = (id: string) => Promise<ITodo>;
export type CreateTodo = (newTodo: {}) => Promise<ITodo>;
