import * as React from 'react';

interface IProps {
   children: React.ReactNode | React.ReactNode[];
   data: { [key: string]: string };
}

export default function DraggableItem(props: IProps) {
   const { children, data } = props;
   const drag = (e: any) => {
      console.log('EVENT TARGET :', e.target);
      e.dataTransfer.setData(Object.keys(data)[0], e.target.id);
      e.dataTransfer.setDragImage(
         document.getElementById(`DRAG_${e.target.id}`),
         0,
         0
      );
      console.log('TARGET :', e.target);
   };

   return (
      <div
         draggable={true}
         id={data.id}
         onDragStart={drag}
         style={{ width: '100%' }}
      >
         {/* <canvas id="canvas" width="50" height="50"></canvas> */}
         <div
            id={`DRAG_${data.id}`}
            style={{ position: 'absolute', zIndex: -1, opacity: 0 }}
         >
            {data.id}
         </div>
         <div>{children}</div>
         {/* <span id="test56" style={{visibility: "hidden"}}></span> */}
      </div>
   );
}
