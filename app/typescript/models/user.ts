export interface IUser {
   _id: string;
   _rev: string;
   createdAt: number;
   pseudonym: string;
   email: string;
   firstName: string;
   lastName: string;
   academicYear: number;
   lang: string;
   _attachments?: any;
}

export type UpdateUser = (toUpdate: IUser) => Promise<IUser>;
