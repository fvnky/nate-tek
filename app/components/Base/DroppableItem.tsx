import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as ModalActions from '../../actions/modal';

// Typescript
import { ToggleModal } from '../../typescript';

interface IProps {
   children: React.ReactNode | React.ReactNode[];
   toggleModal: ToggleModal;
   data: {
      [key: string]: string;
      key: string;
   };
}

function getDataKey(data: { [key: string]: string; key: string }) {
   const keys = Object.keys(data);
   return keys.find((key: string) => key !== 'key');
}

function DroppableItem(props: IProps) {
   const { children, data, toggleModal } = props;

   const allowDrop = (e: any) => e.preventDefault();

   const drop = (e: any) => {
      e.preventDefault();
      const dataTransfer = e.dataTransfer.getData(data.key);
      // console.warn("DATA :", data);
      if (!dataTransfer) return console.warn("Can't drop this element here");
      const key = getDataKey(data);
      const valueData = data[key];
      if (valueData === dataTransfer)
         return console.warn('Cannot drop in itself');
      toggleModal('ModalConfirmMove', {
         [key]: valueData,
         [data.key]: dataTransfer
      });
   };

   return (
      <div onDrop={drop} onDragOver={allowDrop} style={{ width: '100%' }}>
         {children}
      </div>
   );
}

function mapStateToProps(state: any) {
   return {};
}

function mapDispatchToProps(dispatch: any) {
   return bindActionCreators<any, any>(ModalActions, dispatch);
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(DroppableItem);
