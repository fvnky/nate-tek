import * as React from 'react';
import cx from 'classnames';

import Select from '../Base/Select';
import { capitalize } from '../utils';

import NateSelect, { Option } from '../Base/NateSelect';

// Component
import SwitchButton from '../Base/SwitchButton';

// Style
import styles from './SettingsPage.less';

// Constant
import { LIGHT_THEME, DARK_THEME } from '../../constants/theme';
import { EN, FR, DE } from '../../constants/language';

// typescript
import { ToggleModal, IUser, ILanguage, SetThemeName } from '../../typescript';

interface IProps {
   db: any;
   user: IUser;
   toggleModal: ToggleModal;
   updateUserProfilePicture: (imagePath: string, imageType: string) => void;
   lang: ILanguage;
   theme: string;
   setThemeName: SetThemeName;
   setLanguage: (language: string) => void;
}

interface IState {
   cloud: boolean;
   current: string;
   newPwd: string;
   confirmPwd: string;
   changePwd: boolean;
}

export default class SettingsPage extends React.Component<IProps, IState> {
   state = {
      cloud: true,
      current: '',
      newPwd: '',
      confirmPwd: '',
      changePwd: false
   };

   onThemeChange = (value: any) => {
      const { setThemeName } = this.props;
      console.log(value);
      setThemeName(value);
   };

   onLanguageChange = (value: any) => {
      const { setLanguage, user, db } = this.props;
      user.lang = value;
      db.user
         .updateUser(user._id, user)
         .then(() => setLanguage(value))
         .catch(err => console.error(err));
   };

   handleChangeCloud = () => {
      const { cloud } = this.state;

      this.setState({ cloud: !cloud });
   };

   handleChangePwd = () => {
      const { changePwd } = this.state;
      this.setState({ changePwd: !changePwd });
   };

   handleNewPwd = () => {
      const { current, newPwd } = this.state;
      const { db, user } = this.props;

      db.user
         .updateUserSecure(user._id, { current, password: newPwd })
         .then(res => console.log('RES :', res))
         .catch(err => {
            console.log('Err :', err);
         });
   };

   logOutUser = () => {
      const { db } = this.props;

      db.user
         .logOut()
         .then(() => console.log('logged out'))
         .catch((err: any) => console.log('error', err));
   };

   render() {
      const { cloud, changePwd, current, newPwd, confirmPwd } = this.state;

      const { user, lang, theme } = this.props;
      // console.log("user", user);
      return (
         <div className={cx(theme, styles.layoutWrapper)}>
            <div className={cx(theme, styles.title)}>Settings</div>
            <div className={cx(theme, styles.layout)}>
               <div className={styles.settingsGroup}>
                  <span className={cx(theme, styles.settingsGroupTitle)}>
                     {lang.generalSettings.toUpperCase()}
                  </span>
                  <div className={styles.settingsList}>
                     <div className={cx(theme, styles.settingsItem)}>
                        <span className={cx(theme, styles.settingsItemName)}>
                           Cloud
                        </span>
                        <div className={styles.settingsItemOffsetRight}>
                           <SwitchButton
                              active={cloud}
                              onClick={this.handleChangeCloud}
                           />
                        </div>
                     </div>
                     <div className={cx(theme, styles.settingsItem)}>
                        <span className={cx(theme, styles.settingsItemName)}>
                           {lang.language}
                        </span>
                        {/* <Select className={cx(theme, styles.settingsSelect)} onChange={this.onLanguageChange} value={{value:EN, label: "English"}} options={[{value:EN, label: "English"}, {value:FR, label: "Français"}]} border={false}/> */}
                        <NateSelect
                           value={user.lang}
                           onChange={this.onLanguageChange}
                        >
                           <Option value={EN}>English</Option>
                           <Option value={FR}>Français</Option>
                           {/* <Option value={DE}>Deutsch</Option> */}
                        </NateSelect>
                     </div>
                     <div className={cx(theme, styles.settingsItem)}>
                        <span className={cx(theme, styles.settingsItemName)}>
                           {lang.theme}
                        </span>

                        {/* <select className={cx(theme, styles.settingsSelect)} onChange={this.onThemeChange} value={theme}>
                           <option value={LIGHT_THEME}>Light</option>
                           <option value={DARK_THEME}>Dark</option>
                        </select> */}
                        <NateSelect value={theme} onChange={this.onThemeChange}>
                           <Option value={LIGHT_THEME}>Light</Option>
                           <Option value={DARK_THEME}>Dark</Option>
                        </NateSelect>
                     </div>
                     <div className={cx(theme, styles.settingsItem)}>
                        <span className={cx(theme, styles.settingsItemName)}>
                           {lang.dateStyle}
                        </span>
                        {/* <select className={cx(theme, styles.settingsSelect)}>
                           <option value={EN}>{lang.relativeToNow}</option>
                           <option value={FR}>{lang.actualDate}</option>
                        </select> */}
                        <NateSelect value={EN}>
                           <Option value={EN}>{lang.relativeToNow}</Option>
                           <Option value={FR}>{lang.actualDate}</Option>
                        </NateSelect>
                     </div>
                  </div>
               </div>
               {/* <div className={styles.settingsGroup}/> */}

               <div className={styles.settingsGroup}>
                  <span className={cx(theme, styles.settingsGroupTitle)}>
                     {lang.accountSettings.toUpperCase()}
                  </span>
                  <div className={styles.settingsList}>
                     <div className={cx(theme, styles.settingsItem)}>
                        <span className={cx(theme, styles.settingsItemName)}>
                           {lang.email}
                        </span>
                        <input
                           className={cx(theme, styles.settingsInput)}
                           value={user.email}
                           disabled
                        />
                     </div>
                     <div className={cx(theme, styles.settingsItem)}>
                        <span className={cx(theme, styles.settingsItemName)}>
                           {lang.password}
                        </span>
                        {/* {!changePwd && <button onClick={this.handleChangePwd}>Change password</button>}
                        {
                           changePwd && (
                              <p>
                              Current
                              <input type="text"  value={current} onChange={e => this.setState({current: e.target.value})} />
                              <br />
                              New
                              <input type="text"  value={newPwd} onChange={e => this.setState({newPwd: e.target.value})} />
                              <br />
                              Confirm
                              <input type="text"  value={confirmPwd} onChange={e => this.setState({confirmPwd: e.target.value})} />
                              <br />
                              <button onClick={this.handleNewPwd}>Submit</button>
                              </p>
                           )
                        } */}
                        <input
                           className={cx(theme, styles.settingsInput)}
                           value="*******"
                           disabled
                        />
                     </div>
                     <div className={cx(theme, styles.settingsItem)}>
                        <div className={styles.logoutButtonContainer}>
                           <button
                              onClick={this.logOutUser}
                              className={styles.logoutButton}
                           >
                              Log out
                           </button>
                        </div>
                     </div>
                  </div>
               </div>

               <div className={styles.settingsGroup}>
                  <span className={cx(theme, styles.settingsGroupTitle)}>
                     {lang.editorSettings.toUpperCase()}
                  </span>
                  <div className={styles.settingsList}>
                     <div className={cx(theme, styles.settingsItem)}>
                        <span className={cx(theme, styles.settingsItemName)}>
                           {lang.font}
                        </span>
                        {/* <select className={cx(theme, styles.settingsSelect)}>
                           <option value={"1"}>Open Sans</option>
                           <option value={"2"}>Avenir</option>
                        </select> */}
                        <NateSelect
                           value={'1'}
                           className={cx(theme, styles.selectCustom)}
                        >
                           <Option value={'1'}>Open Sans</Option>
                           <Option value={'2'}>Avenir</Option>
                        </NateSelect>
                     </div>
                  </div>
               </div>
               {/* <div className={styles.fontApplicationSetting}>{lang.appSetting}</div>
               <div className={styles.containerCloud}>
                  <div className={styles.fontCloud}>Cloud</div>
                  <div>
                     <SwitchButton />
                  </div>
               </div>
               <div className={styles.containerCloud}>
                  <div className={styles.fontCloud}>{lang.theme}</div>
                  <div>
                     <SwitchButton onClick={this.handleChangeTheme} />
                  </div>
               </div>
               <div className={styles.containerCloud}>
                  <div className={styles.fontCloud}>{lang.language}</div>
                  <div>
                     <SwitchButton onClick={this.handleChangeLang} />
                  </div>
               </div> */}
            </div>
         </div>
      );
   }
}
