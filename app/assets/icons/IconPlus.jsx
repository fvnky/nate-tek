import * as React from "react";

const IconPlus = props => (
   <svg viewBox="0 0 394 394" {...props}>
    <path d="M197 0c20,0 37,16 37,37l0 123 123 0c21,0 37,17 37,37 0,20 -16,37 -37,37l-123 0 0 123c0,21 -17,37 -37,37 -20,0 -37,-16 -37,-37l0 -123 -123 0c-21,0 -37,-17 -37,-37 0,-20 16,-37 37,-37l123 0 0 -123c0,-21 17,-37 37,-37z"/>
   </svg>
);

export default IconPlus;
