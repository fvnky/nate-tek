import React from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';

import withTheme from '../../../../withTheme';
import Bold from '../../../../../assets/icons/Editor/Bold';
import Italic from '../../../../../assets/icons/Editor/Italic';
import Underline from '../../../../../assets/icons/Editor/Underline';
import Superscript from '../../../../../assets/icons/Editor/Superscript';

import FormatButton from './FormatButtons/FormatButton';
import HighlightButton from './FormatButtons/HighlightButton';
import styles from './HoverMenu.less';
import IconDictionnary from '../../../../../assets/icons/IconDictionnary';

type Props = {
   theme: string;
   editor: any;
   value: any;
};

class HoverMenu extends React.Component<Props> {
   menu: HTMLDivElement;

   componentDidMount = () => {
      this.updateMenu();
   };

   componentDidUpdate = () => {
      this.updateMenu();
   };

   updateMenu = () => {
      const { editor, value } = this.props;
      const menu = this.menu;

      if (!menu || !editor || !value) return;

      const { fragment, selection } = value;

      if (
         selection.isBlurred ||
         selection.isCollapsed ||
         fragment.text === ''
      ) {
         menu.removeAttribute('style');
         return;
      }

      const native = window.getSelection();
      const range = native.getRangeAt(0);
      const rect = range.getBoundingClientRect();
      menu.style.opacity = '1';
      menu.style.top = `${rect.top + window.pageYOffset - menu.offsetHeight}px`;
      menu.style.left = `${rect.left +
         window.pageXOffset -
         menu.offsetWidth / 2 +
         rect.width / 2}px`;
      menu.style.transform = 'scaleY(1)';
   };

   createDefinition = () => {};

   render() {
      const { theme, editor } = this.props;
      const root = window.document.getElementById('root');
      if (!editor) return <></>;
      return ReactDOM.createPortal(
         <div
            className={cx(theme, styles.layout)}
            ref={(menu: any) => (this.menu = menu)}
         >
            <HighlightButton
               type={1}
               callback={editor.setHighlight}
               isActive={editor.isHighlightOfType}
            />
            <HighlightButton
               type={2}
               callback={editor.setHighlight}
               isActive={editor.isHighlightOfType}
            />
            <HighlightButton
               type={3}
               callback={editor.setHighlight}
               isActive={editor.isHighlightOfType}
            />
            <HighlightButton
               type={4}
               callback={editor.setHighlight}
               isActive={editor.isHighlightOfType}
            />
            <HighlightButton
               type={5}
               callback={editor.setHighlight}
               isActive={editor.isHighlightOfType}
            />
            <div className={styles.divider} />
            <FormatButton
               theme={theme}
               icon={<Bold />}
               callback={editor.toggleBold}
               isActive={editor.isBold}
            />
            <FormatButton
               theme={theme}
               icon={<Italic />}
               callback={editor.toggleItalic}
               isActive={editor.isItalic}
            />
            <FormatButton
               theme={theme}
               icon={<Underline />}
               callback={editor.toggleUnderlined}
               isActive={editor.isUnderlined}
            />
            <FormatButton
               theme={theme}
               icon={<Superscript />}
               callback={editor.toggleSuperscript}
               isActive={editor.isSuperscript}
            />
            <div className={styles.divider} />

            <div
               className={styles.dictionnaryButton}
               onClick={this.createDefinition}
            >
               <IconDictionnary className={styles.dictionnaryIcon} />
            </div>
         </div>,
         root
      );
   }
}
export default withTheme(HoverMenu);
