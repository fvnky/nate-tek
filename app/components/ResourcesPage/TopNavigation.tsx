import * as React from 'react';
import cx from 'classnames';
import { RouteComponentProps } from 'react-router';
import { History } from 'history';
// Utils
import { capitalize } from '../utils';

// Constant
import routes from '../../constants/routes.json';

// Style
import styles from './TopNavigation.less';

// Typescript
import { IMatch, ILanguage } from '../../typescript';

interface IProps extends RouteComponentProps {
   match: IMatch;
   history: History;
   lang: ILanguage;
   theme: string;
   resourcesNavigation: { lastTab: string; lastBrowserPath: string };
}

interface IState {
   current: string;
   index: number;
}

const TABS = ['all', 'recent', 'drafts'];
const links = {
   all: routes.RESOURCES,
   recent: routes.RECENT,
   drafts: routes.DRAFTS
};

export default class TopNavigation extends React.Component<IProps, IState> {
   goTo = (item: string) => () => {
      const { history } = this.props;

      history.push(links[item]);
   };

   renderTabs = () => {
      const { theme, lang, match } = this.props;

      return TABS.map((item: string) => {
         let active = links[item] === match.path ? true : false;
         if (
            item === 'all' &&
            (match.path === routes.SUBJECT ||
               match.path === routes.SUBJECTS ||
               match.path === routes.FOLDER)
         ) {
            active = true;
         }
         const name: string = lang[item];

         return (
            <div
               key={item}
               className={cx(theme, styles.tab)}
               role="button"
               onClick={this.goTo(item)}
               id={item}
            >
               <div
                  className={cx(
                     theme,
                     styles.name,
                     active ? styles.nameActive : ''
                  )}
               >
                  {capitalize(name)}
               </div>
            </div>
         );
      });
   };

   render() {
      const { theme } = this.props;

      return (
         <div className={cx(theme, styles.layout)}>{this.renderTabs()}</div>
      );
   }
}
