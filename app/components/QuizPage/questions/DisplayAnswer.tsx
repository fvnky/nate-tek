import React from 'react';

// Component
import { useFindQuizParam } from '../useFetchQuiz';

// SVG
import IconCheck from '../../../assets/icons/IconCheck';

// Constant
import {
   QCM,
   TRUE_FALSE,
   QUESTION_ANSWER,
   QUESTIONS,
   FORM,
   PROPOSITION_QCM
} from '../../../constants/models';
import { ANSWER_Q_A } from '../../../constants/models/questions/question_answer';

// Styles
import styles from './DisplayAnswer.less';

// Typescript
import { IDisplayQuestions } from './DisplayQuestions';

const AnswerQcm = (props: IDisplayQuestions) => {
   const { index, setScore, answer } = props;
   const questions = useFindQuizParam({ param: QUESTIONS, ...props });
   const {
      [FORM]: { [ANSWER_Q_A]: goodAnswer, [PROPOSITION_QCM]: propositions }
   } = questions[index];

   React.useEffect(() => {
      if (answer === goodAnswer) setScore();
   }, [answer]);

   return (
      <div className={styles.containerQcm}>
         {Object.keys(propositions).map((key, index) => {
            let displayAnswer = '';
            if (answer === key)
               displayAnswer =
                  answer === goodAnswer ? styles.successQcm : styles.failQcm;
            else if (goodAnswer === key) displayAnswer = styles.successQcm;

            return (
               <div className={styles.containerGoodAnswerQcm} key={key}>
                  <div
                     key={key}
                     role="button"
                     className={`${
                        styles.containerPropostionQcm
                     } ${displayAnswer}`}
                  >
                     <div className={styles.indexProposition}>{index + 1}.</div>
                     <div className={styles.textProposition}>
                        {propositions[key]}
                     </div>
                  </div>
                  {goodAnswer === key && answer === goodAnswer && (
                     <IconCheck className={styles.iconCheckQcmSuccess} />
                  )}
               </div>
            );
         })}
      </div>
   );
};

const Answer_Q_A = (props: IDisplayQuestions) => {
   const { answer, setScore, index } = props;
   const questions = useFindQuizParam({ param: QUESTIONS, ...props });
   const {
      [FORM]: { [ANSWER_Q_A]: goodAnswer }
   } = questions[index];

   const displayAnswer =
      answer === goodAnswer
         ? styles.successQuestionAnswer
         : styles.failQuestionAnswer;
   const displayGoodAnswer =
      answer !== goodAnswer ? styles.displayGoodAnswer : styles.hideGoodAnswer;

   React.useEffect(() => {
      if (answer === goodAnswer) setScore();
   }, [answer]);

   return (
      <div>
         <input
            className={`${styles.containerQuestionAnswer} ${displayAnswer}`}
            value={answer}
            readOnly
         />
         <div className={displayGoodAnswer}>
            <div className={styles.containerGoodAnswer}>
               <div className={styles.textGoodAnswer}>{'Correct Answer:'}</div>
               <div className={styles.goodAnswer}>{goodAnswer}</div>
            </div>
         </div>
      </div>
   );
};

export default {
   [QCM]: AnswerQcm,
   [TRUE_FALSE]: Answer_Q_A,
   [QUESTION_ANSWER]: Answer_Q_A
};
