
import { Dispatch, GetState } from "../reducers/types";

export function signUpUser(data: any) {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.user.signUp(data)
         .then(() => {})
         .catch((err: any) => console.error(err.stack));
   };
}

export function logInUser(credentials: any) {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.user.logIn(credentials)
         .then(() => {})
         .catch((err: any) => console.error(err.stack));
   };
}

export function logOutUser() {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.user.logOut()
         .then(() => {})
         .catch((err: any) => console.error(err.stack));
   };
}
export function updateUser(toUpdate: any) {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.user.updateProfile(toUpdate)
         .then(() => {})
         .catch((err: any) => console.error(err.stack));
   };
}

export function updateUserProfilePicture(imagePath: string, imageType: string) {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.user.updateProfilePicture(imagePath, imageType)
         .then(() => {})
         .catch((err: any) => console.error(err.stack));
   };
}

export function removeUserProfilePicture() {
   return (dispatch: Dispatch, getState: GetState) => {
      getState()
         .database.user.removeProfilePicture()
         .then(() => {})
         .catch((err: any) => console.error(err.stack));
   };
}
