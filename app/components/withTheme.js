import React from "react";
import { connect } from "react-redux";

const withSecretToLife = WrappedComponent => {
   class HOC extends React.Component {
      render() {
         return <WrappedComponent {...this.props} />;
      }
   }
   function mapThemeToProps(state) {
      return {
         theme: state.theme
      };
   }
   return connect(mapThemeToProps)(HOC);
};

export default withSecretToLife;
