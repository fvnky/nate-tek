
import * as React from "react";

interface IProps {
   children: any;
}

export default class App extends React.Component<IProps> {
   props: IProps;

   render() {
      const { children } = this.props;
      return <React.Fragment>{children}</React.Fragment>;
   }
}
