import React, { useState, useEffect } from 'react';
import cx from 'classnames';
import { CSSTransition } from 'react-transition-group';

import IconCheck from '../../assets/icons/IconCheck';

import { ToastOptions } from '../../typescript';

// Style
import styles from './Toast.less';

const icons = {
   check: <IconCheck />
};

type Props = {
   theme: string;
   toast: ToastOptions | null;
};

const Toast: React.FC<Props> = ({ theme, toast }) => {
   const [visible, setVisible] = useState(false);

   useEffect(() => {
      if (toast) {
         setVisible(true);
         setTimeout(() => setVisible(false), 3000); // toast.timeout ? toast.timeout : 3000)
      }
   }, [toast]);

   if (!toast) return <></>;
   const icon = icons[toast.iconName];
   return (
      <CSSTransition
         in={visible}
         timeout={200}
         classNames={{
            appear: '',
            appearActive: '',
            enter: styles.enter,
            enterActive: styles.enterActive,
            enterDone: '',
            exit: styles.exit,
            exitActive: styles.exitActive,
            exitDone: ''
         }}
         unmountOnExit
      >
         <div className={cx(theme, styles.layout)}>
            {icon && <icon.type className={cx(theme, styles.icon)} />}
            <div className={cx(theme, styles.text)}>{toast.text}</div>
         </div>
      </CSSTransition>
   );
};

export default Toast;
