import { Dispatch } from '../reducers/types';

import { ToastOptions } from '../typescript';

const SHOW_TOAST = 'SHOW_TOAST';

const showToast = (options: ToastOptions) => {
   return (dispatch: Dispatch) => {
      dispatch({
         type: 'SHOW_TOAST',
         payload: options
      });
   };
};

export { SHOW_TOAST, showToast };
