import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import RecentPage from '../components/RecentPage/RecentPage';
import * as CoursesActions from '../actions/courses';
import * as ModalActions from '../actions/modal';

function mapStateToProps(state) {
   return {
      courses: state.courses,
      lang: state.language,
      theme: state.theme
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...CoursesActions,
         ...ModalActions
      },
      dispatch
   );
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(RecentPage);
