import * as React from 'react';

// constant
import routes from '../../constants/routes.json';

// Icon
import { iconsSubject } from '../../assets/icons/Subject/IconsSubject';

import styles from './Header.less';

// typescript
import {
   ISubject,
   IFolder,
   ICourse,
   IMatch,
   ILanguage
} from '../../typescript';

interface IProps {
   subjects: ISubject[];
   folders: IFolder[];
   courses: ICourse[];
   match: IMatch;
   lang: ILanguage;
}
interface IState {
   icon: string;
   name: string;
}

export default class Header extends React.Component<IProps, IState> {
   constructor(props: IProps) {
      super(props);

      this.state = {
         icon: '',
         name: ''
      };
   }

   static getDerivedStateFromProps(props: IProps) {
      const {
         lang,
         subjects,
         folders,
         match,
         match: {
            path,
            params: { SubjectID, FolderID }
         }
      } = props;
      const newState = { icon: '', name: '' };

      console.log('match', match);
      console.log('SubjectID', SubjectID);
      console.log('FolderID', FolderID);
      if (SubjectID) {
         const subject = subjects.find((s: ISubject) => s._id === SubjectID);
         if (subject) {
            newState.icon = subject.icon;
            newState.name = subject.name;
         }
      }

      if (path === routes.DRAFTS) {
         newState.name = lang.drafts.toUpperCase();
      } else if (path === routes.RECENT) {
         newState.name = lang.recent.toUpperCase();
      } else if (path === routes.SUBJECTS) {
         newState.name = lang.subjects.toUpperCase();
      }
      return newState;
   }

   renderIconSubject = () => {};

   render() {
      const { name, icon } = this.state;

      const ICON = iconsSubject[icon];
      return (
         <div className={styles.header}>
            <div>{ICON && <ICON.type className={styles.icon} />}</div>
            <div className={styles.name}>{name}</div>
         </div>
      );
   }
}
