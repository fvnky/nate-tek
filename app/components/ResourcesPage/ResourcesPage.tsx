import * as React from 'react';
import cx from 'classnames';
import { RouteComponentProps } from 'react-router';
import { FOLDER, SUBJECT } from '../../database/constants';

import TopNavigation from './TopNavigation';
import BrowserPage from './BrowserPage';
import RecentPage from '../RecentPage/RecentPage';
import DraftsPage from '../DraftsPage/DraftsPage';
import ViewingMode from '../EditorPage/Editor/ViewingMode';

// constant
import routes from '../../constants/routes.json';
import { ALL, RECENT, DRAFTS } from '../../constants/tabs';

import styles from './ResourcesPage.less';

// typescript
import {
   ISubject,
   IFolder,
   ICourse,
   IMatch,
   ILanguage,
   ToggleModal,
   IDraft,
   IQuiz,
   CreateCourse
} from '../../typescript';
import ResourceViewer from './ResourceViewer/ResourceViewer';
import { createCourseFromFile } from '../FolderPage/importDocxHelper';

interface IProps extends RouteComponentProps {
   // drafts: IDraft[];
   db: any;
   subjects: ISubject[];
   folders: IFolder[];
   courses: ICourse[];
   match: IMatch;
   lang: ILanguage;
   toggleModal: ToggleModal;
   theme: string;
   router: any;
   quizs: IQuiz[];
   resourcesNavigation: {
      lastTab: string;
      lastBrowserPath: string;
      viewerDocId: string;
   };
   setLastActiveNavigationTab: (tab: string) => void;
   setLastActiveBrowserPath: (path: string) => void;
   setViewerDocId: (id: string) => void;
   createDraftCourse: any;
   loadSubjects: any;
   createCourse: CreateCourse;
}

export default class ResourcesPage extends React.Component<IProps> {
   state = {
      droppingFile: false,
      file: null
   };

   componentDidMount() {
      document.addEventListener('drop', this.onDrop);
      document.addEventListener('dragover', this.onDragOver);
      // document.addEventListener('dragleave', this.onDragLeave)
      document.addEventListener('mouseout', this.onDragLeave);

      const { resourcesNavigation, history } = this.props;
      const { lastTab } = resourcesNavigation;

      // console.log("load subjects", loadSubjects())
      switch (lastTab) {
         case ALL: {
            if (resourcesNavigation.lastBrowserPath)
               history.replace(resourcesNavigation.lastBrowserPath);
            else history.replace(routes.SUBJECTS);
            break;
         }
         case RECENT:
            history.replace(routes.RECENT);
            break;
         case DRAFTS:
            history.replace(routes.DRAFTS);
            break;
         default: {
            history.replace(routes.SUBJECTS);
            break;
         }
      }
   }

   componentDidUpdate(prevProps: IProps) {
      const {
         resourcesNavigation,
         history,
         match,
         setViewerDocId
      } = this.props;
      const { path } = match;
      const { lastBrowserPath } = resourcesNavigation;

      if (
         prevProps.match.path !== routes.RESOURCES &&
         path === routes.RESOURCES
      ) {
         if (lastBrowserPath)
            history.replace(resourcesNavigation.lastBrowserPath);
         else history.replace(routes.SUBJECTS);
      }
      if (
         (prevProps.match.path === routes.FOLDER ||
            prevProps.match.path === routes.SUBJECT) &&
         path === routes.RESOURCES
      ) {
         history.replace(routes.SUBJECTS);
      }
   }

   componentWillUnmount() {
      document.removeEventListener('drop', this.onDrop);
      document.removeEventListener('dragover', this.onDragOver);
      // document.removeEventListener('dragleave', this.onDragLeave)
   }

   onDrop = event => {
      event.preventDefault();
      const files = [...event.dataTransfer.files];
      const docxFiles = files.filter(f => f.name.split('.').pop() === 'docx');
      console.log('files', files);
      // console.log("docxFiles", docxFiles)
      if (docxFiles.length > 0) {
         const {
            match: {
               params: { SubjectID, FolderID }
            },
            createCourse
         } = this.props;
         const id = FolderID ? FolderID : SubjectID;
         const collection = FolderID ? FOLDER : SUBJECT;

         docxFiles.forEach(file => {
            createCourseFromFile(file, id, collection, createCourse);
         });
      }
   };

   onDragOver = event => {
      event.preventDefault();
      // console.log(event.dataTransfer.files[0].type)
      if (this.state.droppingFile === false)
         this.setState({ droppingFile: true });
      return false;
   };

   onDragLeave = event => {
      event.preventDefault();
      this.setState({ droppingFile: false });
      return false;
   };

   backFromResourceViewer = () => {
      this.componentDidMount();
   };

   editFromResourceViewer = (docId: string) => {
      const { history } = this.props;
      history.push(routes.EDITOR_LINK + '/' + docId);
   };

   renderPage = () => {
      const { match } = this.props;
      const { path } = match;

      switch (path) {
         case routes.SUBJECTS:
            return <BrowserPage {...this.props} />;
         case routes.SUBJECT:
            return <BrowserPage {...this.props} />;
         case routes.FOLDER:
            return (
               <BrowserPage {...this.props} droppedFiles={this.state.file} />
            );
         case routes.RECENT:
            return <RecentPage {...this.props} />;
         case routes.DRAFTS:
            return <DraftsPage {...this.props} />;
         default:
            return <>NOTHING HERE</>;
      }
   };

   render() {
      const {
         quizs,
         theme,
         match,
         courses,
         db,
         subjects,
         folders,
         toggleModal
      } = this.props;

      // console.log("Quizs :", quizs);
      // console.log("Subject :", subjects);
      // console.log("Courses :", courses);
      // console.log("Folders :", folders);
      if (match.path === routes.DOC_VIEWER && match.params.docID) {
         const course = courses.find(c => c._id === match.params.docID);
         return (
            <ViewingMode
               toggleModal={toggleModal}
               theme={theme}
               db={db}
               course={course}
               edit={this.editFromResourceViewer}
               back={this.backFromResourceViewer}
               subjects={subjects}
               folders={folders}
               courses={courses}
               fromLibrary={false}
            />
         );
      }
      return (
         <div
            className={cx(
               theme,
               styles.layout,
               this.state.droppingFile ? styles.droppingFile : ''
            )}
         >
            {this.state.droppingFile && (
               <div className={cx(theme, styles.droppingFileBackground)}>
                  <div
                     className={cx(theme, styles.droppingFileBackgroundInside)}
                  >
                     Import Docx Here
                  </div>
               </div>
            )}
            <div className={styles.tabs}>
               <TopNavigation {...this.props} />
            </div>
            {this.renderPage()}
         </div>
      );
   }
}
