import { CustomError } from '../factory';

export class DictionaryAlreadyExists extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The dictionary already exists';
   }
}

export class DictionaryNotFound extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'The dictionary could not be found';
   }
}

export class NoDictionaries extends CustomError {
   constructor(message) {
      super(message);
      this.message = message || 'No dictionaries could be found';
   }
}
