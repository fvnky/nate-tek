import { Dispatch } from '../reducers/types';
import { IFolder } from '../typescript';

import { LOAD_LANG } from '../reducers/language';

import { loadLanguage } from '../language/loadLanguage';

export function setLanguage(language: string) {
   return (dispatch: Dispatch) => {
      dispatch({ type: LOAD_LANG, payload: language });
   };
}
