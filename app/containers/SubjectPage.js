import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SubjectsPage from '../components/SubjectPage/SubjectsPage.tsx';
import * as SubjectsActions from '../actions/subjects';
import * as ModalActions from '../actions/modal';

function mapStateToProps(state) {
   return {
      subjects: state.subjects,
      courses: state.courses,
      folders: state.folders,
      lang: state.language,
      theme: state.theme,
      router: state.router
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators({ ...SubjectsActions, ...ModalActions }, dispatch);
}

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(SubjectsPage);
