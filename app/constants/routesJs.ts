export const routes = {
   "HOME": "/",
   "LOGIN": "/login",
   "REGISTER": "/register",
   "RECENT": "/resources/recent",
   "DRAFTS": "/resources/drafts",
   "PROFILE": "/profile",
   "LIBRARY": "/library",
   "SETTINGS": "/settings",
   "RESOURCES": "/resources",
   "SUBJECTS": "/resources/subjects",
   "SUBJECT": "/resources/subjects/:SubjectID",
   "FOLDERS": "/folders",
   "FOLDER": "/resources/folders/:FolderID",
   "EDITOR_LINK": "/editor",
   "EDITOR": "/editor/:courseID",
   "DEV": "/dev"
}
