import React from 'react';

// Component
import {
   QuizLayout,
   QuizHeader,
   QuizContent,
   QuizFooter,
   QuizFooterButton
} from './QuizLayout';
import {
   useSwitchQuizPage,
   useFindQuizParam,
   useFindQuiz
} from './useFetchQuiz';
import DisplayQuestions from './questions/DisplayQuestions';
import DisplayAnswer from './questions/DisplayAnswer';
import { upper } from '../utils';

// Constant
import {
   QUESTIONS,
   TYPE_FORM,
   QUESTION,
   FORM,
   SCORE
} from '../../constants/models';
import routes from '../../constants/routes.json';

// Styles
import styles from './QuizGame.less';

// Typesript
import { UpdateQuiz } from '../../typescript';
import { IQuizPage } from './QuizPage';

const QUESTION_VIEW = 'questionView';
const ANSWER_VIEW = 'answerView';
const SCORE_VIEW = 'scoreView';

const MAP_VIEW = {
   [QUESTION_VIEW]: DisplayQuestions,
   [ANSWER_VIEW]: DisplayAnswer
};

export function displayScore(score: number) {
   return <>{Math.round(score * 100) / 100}%</>;
}

interface IQuizScore extends IQuizGame {
   score: number;
}

const QuizScore = (props: IQuizScore) => {
   const { score } = props;
   const bestScore = useFindQuizParam({ param: SCORE, ...props });

   return (
      <div className={styles.containerScore}>
         <div className={styles.containerScore}>
            <div className={styles.textScore}>Your score</div>
            <div className={styles.resultScore}>{displayScore(score)}</div>
         </div>
         <div className={styles.containerScore}>
            <div className={styles.textBestScore}>Best score</div>
            <div className={styles.resultBestScore}>
               {displayScore(bestScore)}
            </div>
         </div>
      </div>
   );
};

export interface IQuizGame extends IQuizPage {
   // setView: React.Dispatch<React.SetStateAction<string>>;
   updateQuiz: UpdateQuiz;
}

const QuizGame = (props: IQuizGame) => {
   const { lang } = props;
   const [score, setScore] = React.useState(0);
   const [answer, setAnswer] = React.useState(null);
   const [indexQuestion, setIndexQuestion] = React.useState(0);
   const [currentView, setCurrentView] = React.useState(QUESTION_VIEW);
   const questions = useFindQuizParam({ param: QUESTIONS, ...props });

   const displayScoreView = () => {
      const { updateQuiz } = props;
      const bestScore = useFindQuizParam({ param: SCORE, ...props });
      const quiz = useFindQuiz(props);

      if (score <= bestScore) setCurrentView(SCORE_VIEW);
      else
         updateQuiz({ ...quiz, [SCORE]: score })
            .then(() => setCurrentView(SCORE_VIEW))
            .catch(err => console.error(err));
   };

   const addScore = () => setScore(score + (1 * 100) / questions.length);

   const handleOnClickValidAnswer = () => setCurrentView(ANSWER_VIEW);

   const handleOnClickNextQuestion = () => {
      setAnswer(null);
      if (indexQuestion < questions.length - 1) {
         setIndexQuestion(indexQuestion + 1);
         setCurrentView(QUESTION_VIEW);
      } else {
         displayScoreView();
      }
   };

   const handleOnClickFinish = () =>
      useSwitchQuizPage({ page: routes.QUIZ_HOME_LINK, ...props });

   const renderFooterQuestion = () => {
      const enableOkButton =
         answer === null ? styles.disableButtonBBCW : styles.buttonBBlackCWhite;
      const onClickOk = answer === null ? null : handleOnClickValidAnswer;

      return (
         <>
            <QuizFooterButton
               text={upper(lang.ok)}
               style={enableOkButton}
               handleOnClick={onClickOk}
               {...props}
            />
            <QuizFooterButton
               text={upper(lang.skip)}
               style={styles.buttonBGreyCBlack}
               handleOnClick={handleOnClickNextQuestion}
               {...props}
            />
         </>
      );
   };
   const renderFooterAnswer = (
      <QuizFooterButton
         text={upper(lang.next)}
         style={styles.buttonBBlackCWhite}
         handleOnClick={handleOnClickNextQuestion}
         {...props}
      />
   );
   const renderFooterScore = (
      <QuizFooterButton
         text={upper(lang.finishQuiz)}
         style={styles.buttonBBlackCWhite}
         handleOnClick={handleOnClickFinish}
         {...props}
      />
   );

   const renderFooterView = () => {
      switch (currentView) {
         case QUESTION_VIEW:
            return renderFooterQuestion();
         case ANSWER_VIEW:
            return renderFooterAnswer;
         case SCORE_VIEW:
            return renderFooterScore;
         default:
            return renderFooterQuestion();
      }
   };

   const renderContentGame = () => {
      if (!questions || !questions[indexQuestion]) return null;

      const {
         [TYPE_FORM]: type,
         [FORM]: { [QUESTION]: question_name }
      } = questions[indexQuestion];
      const View = MAP_VIEW[currentView][type];

      return (
         <div className={styles.containerQuizGame}>
            <div className={styles.indexQuizQuestion}>
               {lang.question} {indexQuestion + 1} {lang.outOf}{' '}
               {questions.length}
            </div>
            <div className={styles.nameQuizQuestion}>{question_name}</div>
            <View
               answer={answer}
               setAnswer={setAnswer}
               index={indexQuestion}
               setScore={addScore}
               {...props}
            />
         </div>
      );
   };

   const renderContentView = () => {
      switch (currentView) {
         case QUESTION_VIEW:
            return renderContentGame();
         case ANSWER_VIEW:
            return renderContentGame();
         case SCORE_VIEW:
            return <QuizScore score={score} {...props} />;
         default:
            return renderContentGame();
      }
   };

   return (
      <QuizLayout>
         <QuizHeader title={lang.quiz} {...props} />
         <QuizContent>{renderContentView()}</QuizContent>
         <QuizFooter>{renderFooterView()}</QuizFooter>
      </QuizLayout>
   );
};

export default QuizGame;
