import * as React from 'react';
import cx from 'classnames';
import { RouteComponentProps } from 'react-router';
import { Form, Field } from 'react-final-form';

// import NateFileUploader from '../Base/NateFileUploader';

import isEmail from 'validator/lib/isEmail';
import Select from '../Base/Select';
import NateControlledSearchableSelect from '../Base/NateControlledSearchableSelect';

// import Field from "./Field";
// import IconProfileOutline from '../../assets/icons/Sidebar/IconProfileOutline';

import { getCountries } from './countries';

import styles from './RegisterForm.less';

import routes from '../../constants/routes.json';

// Typescript
import { IUser, ILanguage, ToggleModal } from '../../typescript';

function base64MimeType(encoded) {
   let result = null;

   if (typeof encoded !== 'string') {
      return result;
   }

   const mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

   if (mime && mime.length) {
      result = mime[1];
   }
   return result;
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const required = value => (value ? undefined : 'Required');
const composeValidators = (...validators) => value =>
   validators.reduce(
      (error, validator) => error || validator(value),
      undefined
   );

export const yearOptions = [
   { value: 1, label: '1st year' },
   { value: 2, label: '2nd year' },
   { value: 3, label: '3rd year' },
   { value: 4, label: '4th year' },
   { value: 5, label: '5th year' },
   { value: 6, label: '6th year' },
   { value: 7, label: '7th year' },
   { value: 8, label: '8th year' },
   { value: 9, label: '9th year' }
];

interface IProps extends RouteComponentProps {
   toggleModal: ToggleModal;
   lang: ILanguage;
   db: any;
   email: string;
   language: string;
   back: () => void;
}

interface IState {
   profilePicUrl: string;
   initialValues: any;
   universities: { value: any; label: string }[];
   university: string;
   curriculums: { value: any; label: string }[];
   curriculum: string;
}

const MyInput = ({ input, meta, label, type = 'text' }) => (
   <div className={styles.inputContainer}>
      <span className={cx('Light', styles.label)}>{label.toUpperCase()}</span>
      <input
         className={cx(
            'Light',
            styles.input,
            meta.error && meta.touched ? styles.inputError : ''
         )}
         {...input}
         type={type}
         placeholder={label}
      />
      <span className={cx('Light', styles.error)}>
         {meta.error && meta.touched ? meta.error : ' '}
      </span>
   </div>
);

const MySelect = ({ input, meta, label, options }) => (
   <div className={styles.inputContainer}>
      <span className={cx('Light', styles.label)}>{label.toUpperCase()}</span>
      <Select
         className={styles.select}
         options={options}
         placeholder={label}
         {...input}
      />
      <span className={cx('Light', styles.error)}>
         {meta.error && meta.touched ? meta.error : ' '}
      </span>
   </div>
);

export default class RegisterForm extends React.Component<IProps, IState> {
   state = {
      profilePicUrl: null,
      initialValues: {},
      universities: [],
      university: '',
      curriculums: [],
      curriculum: ''
   };

   componentDidMount() {
      const { email } = this.props;
      console.warn(email);
      if (isEmail(email)) {
         this.setState({ initialValues: { mail: email } });
      }
   }

   onSubmit = values => {
      const {
         firstname,
         lastname,
         username,
         country,
         university,
         curriculum,
         year,
         key
      } = values;
      console.log(values);
      const { db, language, email } = this.props;
      const { profilePicUrl } = this.state;

      const finalObj = {
         mail: email,
         firstname,
         lastname,
         username,
         country: country.value,
         university: university,
         curriculum: curriculum,
         year: year.value,
         lang: language,
         key
      };

      console.log(JSON.stringify(finalObj, null, 2));
      db.user
         .signUp(finalObj)
         .then(user => {
            console.log('User', JSON.stringify(user, null, 2));
            // return db.user.logIn(user)
            // return setTemporaryUser({ id: user._id, email: user.mail, password: finalObj.password})
         })
         // .then(() => db.user.updateProfilePicture(profilePicUrl, base64MimeType(profilePicUrl) ))
         .catch((err: any) => {
            if ((err.name = 'BetaKeyNotFound')) {
               console.log('BETAKEYYYYY');
            }
            console.log('error', err);
         });
   };

   removeProfilePicture = e => {
      e.stopPropagation();
      this.setState({ profilePicUrl: null });
   };

   setCroppedProfilePicUrl = (url: string) => {
      console.warn(url);
      this.setState({ profilePicUrl: url });
   };

   onProfilePictureChange = e => {
      // console.log("new profile pic is " + e.target.value);
      // const elem: any = document.getElementById('profilePicture');
      const { toggleModal } = this.props;
      const file = e.target.files[0];
      console.log(file);
      toggleModal('ModalCropProfilePicture', {
         profilePicUrl: file,
         setCroppedProfilePicUrl: this.setCroppedProfilePicUrl
      });
      // this.setState({ profilePicUrl: URL.createObjectURL(file) });

      // const reader = new FileReader();
      // reader.addEventListener("load", () => {
      //    preview.src = reader.result;
      //  }, false);

      // reader.readAsDataURL(file);
      // this.setState({profilePicUrl: URL.createObjectURL(file)})
   };

   onUniversityInputChange = newValue => {
      const { db } = this.props;
      db.user
         .getUniversities(newValue)
         .then(u =>
            this.setState({
               universities: u.map(univ => ({
                  value: univ.name,
                  label: univ.name
               }))
            })
         )
         .catch(err => console.error(err));
   };

   onCreateUniversity = newUniv => {
      const { db } = this.props;
      db.user
         .createUniversity(newUniv)
         .then(() => this.setState({ university: newUniv }))
         .catch(err => {
            console.error(err);
         });
   };

   onCurriculumInputChange = newValue => {
      const { db } = this.props;
      db.user
         .getCurriculums(newValue)
         .then(u =>
            this.setState({
               curriculums: u.map(c => ({ value: c.name, label: c.name }))
            })
         )
         .catch(err => console.error(err));
   };

   onCreateCurriculum = newCur => {
      const { db } = this.props;
      db.user
         .createCurriculum(newCur)
         .then(() => this.setState({ curriculum: newCur }))
         .catch(err => {
            console.error(err);
         });
   };

   render() {
      const { lang, history, email, back } = this.props;
      const {
         profilePicUrl,
         initialValues,
         universities,
         university,
         curriculums,
         curriculum
      } = this.state;
      return (
         <Form
            initialValues={initialValues}
            onSubmit={this.onSubmit}
            // tslint:disable-next-line:jsx-no-lambda
            //@ts-ignore
            render={({ handleSubmit, submitting }) => (
               <form onSubmit={handleSubmit} className={styles.layout}>
                  <div className={styles.column}>
                     <div className={styles.row}>
                        <Field
                           name="firstname"
                           validate={composeValidators(required)}
                        >
                           {({ input, meta }) => (
                              <MyInput
                                 input={input}
                                 meta={meta}
                                 label={lang.firstName}
                              />
                           )}
                        </Field>
                        <Field
                           name="lastname"
                           validate={composeValidators(required)}
                        >
                           {({ input, meta }) => (
                              <MyInput
                                 input={input}
                                 meta={meta}
                                 label={lang.lastName}
                              />
                           )}
                        </Field>
                     </div>
                     <div className={styles.row}>
                        <Field name="username" validate={required}>
                           {({ input, meta }) => (
                              <MyInput
                                 input={input}
                                 meta={meta}
                                 label={lang.username}
                              />
                           )}
                        </Field>
                        <Field name="country" validate={required}>
                           {({ input, meta }) => (
                              <MySelect
                                 input={input}
                                 meta={meta}
                                 label={lang.country}
                                 options={getCountries(lang.lang)}
                              />
                           )}
                        </Field>
                     </div>
                     <div className={styles.row}>
                        <Field name="university" validate={required}>
                           {({ input, meta }) => {
                              // console.log("input", input)
                              // console.log("meta", meta)
                              return (
                                 <div
                                    className={cx(
                                       styles.inputContainer,
                                       styles.inputFullWidth
                                    )}
                                 >
                                    <span className={cx('Light', styles.label)}>
                                       {lang.university.toUpperCase()}
                                    </span>
                                    <NateControlledSearchableSelect
                                       input={input}
                                       className={cx(
                                          'Light',
                                          styles.input,
                                          meta.error && meta.touched
                                             ? styles.inputError
                                             : ''
                                       )}
                                       value={university}
                                       onChange={e => {
                                          this.setState({ university: e });
                                       }}
                                       onInputChangeCb={
                                          this.onUniversityInputChange
                                       }
                                       options={universities}
                                       creatable={true}
                                       onCreateCb={this.onCreateUniversity}
                                    />
                                    <span className={cx('Light', styles.error)}>
                                       {meta.error && meta.touched
                                          ? meta.error
                                          : ' '}
                                    </span>
                                 </div>
                              );
                           }}
                        </Field>
                     </div>
                     <div className={styles.row}>
                        <Field name="curriculum" validate={required}>
                           {({ input, meta }) => (
                              <div
                                 className={cx(
                                    styles.inputContainer,
                                    styles.inputFullWidth
                                 )}
                              >
                                 <span className={cx('Light', styles.label)}>
                                    {lang.curriculum.toUpperCase()}
                                 </span>
                                 <NateControlledSearchableSelect
                                    input={input}
                                    className={cx(
                                       'Light',
                                       styles.input,
                                       meta.error && meta.touched
                                          ? styles.inputError
                                          : ''
                                    )}
                                    value={curriculum}
                                    onChange={e => {
                                       this.setState({ curriculum: e });
                                    }}
                                    onInputChangeCb={
                                       this.onCurriculumInputChange
                                    }
                                    options={curriculums}
                                    creatable={true}
                                    onCreateCb={this.onCreateCurriculum}
                                 />
                                 <span className={cx('Light', styles.error)}>
                                    {meta.error && meta.touched
                                       ? meta.error
                                       : ' '}
                                 </span>
                              </div>
                              // <MyInput
                              //    input={input}
                              //    meta={meta}
                              //    label={lang.curriculum}
                              // />
                           )}
                        </Field>
                     </div>
                     <div className={styles.row}>
                        <Field name="year" validate={required}>
                           {({ input, meta }) => (
                              <MySelect
                                 input={input}
                                 meta={meta}
                                 label={lang.year}
                                 options={yearOptions}
                              />
                           )}
                        </Field>
                     </div>
                     {/* <div className={styles.row}>
                        <Field name="key" validate={required}>
                           {({ input, meta }) => (
                              <MyInput
                                 input={input}
                                 meta={meta}
                                 label={'Private beta Key'}
                              />
                           )}
                        </Field>
                     </div> */}
                     <div className={styles.row}>
                        <button
                           type="submit"
                           // disabled={submitting}
                           className={cx('Light', styles.button, styles.next)}
                        >
                           NEXT
                        </button>
                        <button
                           className={cx('Light', styles.button)}
                           onClick={() => back}
                        >
                           {lang.cancel.toUpperCase()}
                        </button>
                     </div>
                  </div>
               </form>
            )}
         />
      );
   }
}
