import * as React from "react";

const Divider = props => (
   <svg {...props} viewBox="0 0 18 18">
      <path d="M16,9.9H2C1.5,9.9,1.1,9.5,1.1,9S1.5,8.1,2,8.1h14c0.5,0,0.9,0.4,0.9,0.9S16.5,9.9,16,9.9z" />
   </svg>
);

export default Divider;
