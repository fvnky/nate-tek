import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import LoginRegisterPage from '../components/LoginRegisterPage/LoginRegisterPage';
import * as UserAction from '../actions/user';
import * as ModalAction from '../actions/modal';

function mapStateToProps(state) {
   return {
      user: state.user,
      lang: state.language,
      db: state.database
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators({ ...UserAction, ...ModalAction }, dispatch);
}

export default withRouter(connect(
   mapStateToProps,
   mapDispatchToProps
)(LoginRegisterPage));
