export default function getEntireText() {
   return {
      queries: {
         getEntireText(editor) {
            return editor.value.document.getBlocks().reduce((memo, b) => {
               if (b.text !== "")
                  return (memo + b.text + '\n')
               return (memo + b.text)
            }, "")
         },
      }
   }
}
