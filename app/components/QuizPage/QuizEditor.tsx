import React from 'react';

// Component
import Manager from './questions/ManageQuestions';
import Descriptor from './questions/DescriptorQuestion';
import {
   QuizLayout,
   QuizHeader,
   QuizContent,
   QuizFooter,
   QuizFooterButton
} from './QuizLayout';
import {
   useSwitchQuizPage,
   useFindQuizParam,
   useFindQuiz
} from './useFetchQuiz';
import { upper } from '../utils';

// SVG
import IconPlus from '../../assets/icons/IconPlus';

// Constant
import {
   SCHEMA_QUESTIONS,
   TYPE_FORM,
   QCM,
   QUESTION_ANSWER,
   QUESTION,
   FORM,
   SCHEMA_ID,
   SCORE
} from '../../constants/models';
import routes from '../../constants/routes.json';

// Style
import styles from './QuizEditor.less';

// Typescript
import { UpdateQuiz, IQuiz } from '../../typescript';
import { IQuizPage } from './QuizPage';

type UpdateIndex = (index: number, state: any) => void;
type DeleteIndex = (indexState: number) => void;

export interface IPropsIndex {
   updateIndex: UpdateIndex;
   deleteIndex: DeleteIndex;
}

interface IQuestionInputName extends IManagerQuiz {
   question: any;
   index: number;
   display: boolean;
   updateIndex: UpdateIndex;
}

const InputQuestionName = (props: IQuestionInputName) => {
   const { question, updateIndex, index, display } = props;
   const {
      [FORM]: { [QUESTION]: name }
   } = question;
   const displayInput = display ? styles.displayInput : '';

   const handleOnChange = e => {
      const { value } = e.target;
      const update = { ...question };

      update[FORM][QUESTION] = value;
      updateIndex(index, update);
   };

   return (
      <input
         value={name}
         onChange={handleOnChange}
         className={`${styles.nameQuestion} ${displayInput}`}
         readOnly={!display}
      />
   );
};

interface IDisplayQuestionManager extends IManagerQuiz {
   questions: any;
   updateIndex: (index: number, state: any) => void;
   deleteIndex: (index: number) => void;
}

const DisplayQuestionManager = (props: IDisplayQuestionManager) => {
   const { questions, deleteIndex } = props;
   const [formId, setFormId] = React.useState('');

   const handleOnClick = (id: string) => () => setFormId(id);

   const removeQuestion = (index: number) => () => deleteIndex(index);

   return questions.map((question, index) => {
      const { [SCHEMA_ID]: id, [TYPE_FORM]: type } = question;

      const displayEdition =
         formId === id ? styles.displayEdition : styles.hideEdition;
      const displayBorder = formId === id ? styles.manageSelectQuestion : '';
      const displayOnClick =
         formId !== id
            ? handleOnClick(id)
            : e => {
                 e.stopPropagation();
              };
      const displayHide = formId !== id ? styles.displayQuestionHide : '';
      const displayDelete =
         formId === id ? styles.buttonDeleteQuestion : styles.hideEdition;

      if (!Manager[type]) return null;

      return (
         <div className={`${styles.manageQuestion} ${displayBorder}`} key={id}>
            <div
               className={`${styles.displayQuestion} ${displayHide}`}
               role="button"
               onClick={displayOnClick}
            >
               <div className={styles.containerQuestionInfo}>
                  <div className={styles.indexQuestion}>{index + 1}.</div>
                  <InputQuestionName
                     question={question}
                     index={index}
                     display={formId === id}
                     {...props}
                  />
               </div>
               {/* <div role="button" onClick={removeQuestion(index)} className={displayDelete}>
                     x
                  </div> */}
               <IconPlus
                  className={styles.iconPlusDelete}
                  onClick={removeQuestion(index)}
               />
            </div>
            <div className={displayEdition}>
               {Manager[type]({ index, state: question, ...props })}
            </div>
         </div>
      );
   });
};

interface IQuizAddPropButton {
   text: string;
   // onClick: (data: any) => any;
}

const QuizAddPropButton = (props: IQuizAddPropButton) => {
   const { text } = props;

   return <div className={styles.buttonAddProp}>{text}</div>;
};

interface IManagerQuiz extends IQuizPage {
   updateQuiz: UpdateQuiz;
   // setView: React.Dispatch<React.SetStateAction<string>>;
}

export default function QuizEditor(props: IManagerQuiz) {
   const { lang } = props;
   const questions = useFindQuizParam({ param: SCHEMA_QUESTIONS, ...props });
   const [questionState, setQuestionState] = React.useState(questions);

   const updateIndex = (index: number, state: any) => {
      setQuestionState([
         ...questionState.slice(0, index),
         state,
         ...questionState.slice(index + 1)
      ]);
   };

   const deleteIndex = (stateIndex: number) => {
      const filteredQuestions = questionState.filter(
         (state: any, index: number) => index !== stateIndex
      );

      if (filteredQuestions) setQuestionState([...filteredQuestions]);
   };

   const addQuestion = (question: string) => () => {
      if (!Descriptor[question])
         return console.log('Wrong question :' + question);

      setQuestionState([...questionState, Descriptor[question]()]);
   };

   const handleOnClick = (view: string) => () => {
      const { updateQuiz } = props;
      const quiz = useFindQuiz(props);

      if (!quiz) return useSwitchQuizPage({ page: view, ...props });

      updateQuiz({ ...quiz, [SCHEMA_QUESTIONS]: questionState, [SCORE]: 0 })
         .then(() => useSwitchQuizPage({ page: view, ...props }))
         .catch(err => console.log('Err :', err));
   };

   return (
      <QuizLayout>
         <QuizHeader title={lang.quizEditor} {...props} />
         <QuizContent>
            <div className={styles.containerContent}>
               <div className={styles.containerQuestion}>
                  <div className={styles.headerQuestions}>{lang.questions}</div>
                  <DisplayQuestionManager
                     updateIndex={updateIndex}
                     deleteIndex={deleteIndex}
                     questions={questionState}
                     {...props}
                  />
                  <div className={styles.containerButtonAddQuiz}>
                     <div
                        className={styles.containerAddQuestion}
                        onClick={addQuestion(QCM)}
                     >
                        <IconPlus className={styles.iconPlusQuestion} />
                        <QuizAddPropButton text={upper(lang.qcm)} />
                     </div>
                     <div
                        className={styles.containerAddQuestion}
                        onClick={addQuestion(QUESTION_ANSWER)}
                     >
                        <IconPlus className={styles.iconPlusQuestion} />
                        <QuizAddPropButton text={upper(lang.questionAnswer)} />
                     </div>
                  </div>
               </div>
            </div>
         </QuizContent>
         <QuizFooter>
            <QuizFooterButton
               text={upper(lang.finishEditing)}
               style={styles.quizFinishEditor}
               handleOnClick={handleOnClick(routes.QUIZ_HOME_LINK)}
               {...props}
            />
         </QuizFooter>
      </QuizLayout>
   );
}
