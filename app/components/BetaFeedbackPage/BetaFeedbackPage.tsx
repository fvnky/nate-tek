import * as React from 'react';

import styles from './BetaFeedbackPage.less';

export default class BetaFeedbackPage extends React.Component {
   render() {
      return (
         <div className={styles.layout}>
            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSd32qTIV8AgzhQPImd8tJcatu48RAc12u4KGqwD8mcbxprRLA/viewform?embedded=true" />
            {/* <iframe
               title="Inline Frame Example"
               src="https://docs.google.com/forms/d/14I_pKFasvxFPcMv4KvjdqjoeoGJUsb3PQuG4IY-dRl4"
            /> */}
         </div>
      );
   }
}
