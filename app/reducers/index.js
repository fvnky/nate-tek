import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { database } from './database';
import { user } from './user';
import { subjects } from './subjects';
import { folders } from './folders';
import { courses } from './courses';
import { modal } from './modals';
import { toast } from './toast';
import { error } from './error';
import { language } from './language';
import { theme } from './theme';
import { sidebar } from './sidebar';
import { editorRightPanel } from './editorRightPanel';
import { resourcesNavigation } from './resourcesNavigation';
import { quizs } from './quizs';
import { dictionary } from './dictionary';
import { todo } from './todos';
import { schedules } from './schedules';

export default function createRootReducer(history) {
   return combineReducers({
      router: connectRouter(history),
      database,
      user,
      subjects,
      folders,
      courses,
      modal,
      error,
      language,
      theme,
      sidebar,
      editorRightPanel,
      resourcesNavigation,
      toast,
      quizs,
      dictionary,
      todo,
      schedules
   });
}
