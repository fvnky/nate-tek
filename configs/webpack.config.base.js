/**
 * Base webpack config used across other specific configs
 */

import path from 'path';
import webpack from 'webpack';
import { dependencies } from '../package.json';

export default {
   externals: [...Object.keys(dependencies || {})],

   module: {
      rules: [
         {
            test: /\.[jt]sx?$/,
            exclude: /node_modules/,
            use: [
               {
                  loader: 'babel-loader',
                  options: {
                     cacheDirectory: false
                  }
               },
               {
                  loader: 'awesome-typescript-loader',
                  options: {
                     reportFiles: ['app/**/*.{ts,tsx}']
                  }
               }
            ]
         }
      ]
   },

   output: {
      path: path.join(__dirname, '..', 'app'),
      // https://github.com/webpack/webpack/issues/1114
      libraryTarget: 'commonjs2',
      // Needed for react-pdf. See https://github.com/wojtekmaj/react-pdf/issues/190
      globalObject: 'this'
   },

   /**
    * Determine the array of extensions that should be used to resolve modules.
    */
   resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
   },

   plugins: [
      new webpack.EnvironmentPlugin({
         NODE_ENV: 'production'
      }),

      new webpack.NamedModulesPlugin()
   ]
};
