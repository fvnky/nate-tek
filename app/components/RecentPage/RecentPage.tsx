import * as React from 'react';
import cx from 'classnames';
import { filter, orderBy } from 'lodash';
import { RouteComponentProps } from 'react-router';

// Component
import List from '../Base/List/List';
import {
   fillDocument,
   fillTimeHeaderList,
   MOVE_POPUP,
   EDIT_POPUP,
   LAST_UPDATED_LIST,
   DELETE_POPUP,
   ONCLICK_LIST,
   DROPPABLE_LIST,
   //DRAGGABLE_LIST,
   POPUP_LIST,
   ICON_LIST,
   NAME_LIST
} from '../Base/List/Descriptor';

// Utils
import { week, lastUpdate, yesterday, today, old } from '../utils';

// Styles
import styles from './RecentPage.less';

// Typescript
import { ICourse, ILanguage, ToggleModal, IQuiz } from '../../typescript';

// Constant
import { RECENT } from '../../constants/tabs';

const TODAY = 'Today';
const YESTERDAY = 'Yesterday';
const WEEK = 'Week';

const LIST_ACTIONS = [
   ONCLICK_LIST,
   DROPPABLE_LIST
   //DRAGGABLE_LIST
];
const LIST_ELEMENTS = [ICON_LIST, NAME_LIST, LAST_UPDATED_LIST, POPUP_LIST];
const LIST_POPUP = [MOVE_POPUP, EDIT_POPUP, DELETE_POPUP];

interface IProps extends RouteComponentProps {
   theme: string;
   lang: ILanguage;
   toggleModal: ToggleModal;
   courses: ICourse[];
   quizs: IQuiz[];
   resourcesNavigation: { lastTab: string; lastBrowserPath: string };
   setLastActiveNavigationTab: (tab: string) => void;
}

export default class RecentPage extends React.Component<IProps> {
   componentDidMount() {
      const { setLastActiveNavigationTab } = this.props;
      // this.animateChildrenList();
      setLastActiveNavigationTab(RECENT);
   }

   animateChildrenList = () => {
      const childrenList = document.getElementById('childrenList');
      childrenList.className = cx(
         styles.childrenList,
         styles.listBeforeTransition
      );
      setTimeout(() => {
         childrenList.className = cx(
            styles.childrenList,
            styles.listAfterTransition,
            styles.listTransition
         );
      }, 0);
      setTimeout(() => {
         childrenList.className = styles.childrenList;
      }, 400);
   };

   documentFiltered = (callback: (time: number) => boolean) => {
      const { courses, quizs } = this.props;

      return [
         ...filter(courses, (x: any) => {
            if (callback(lastUpdate(x))) return x;
         }),
         ...filter(quizs, (x: any) => {
            if (callback(lastUpdate(x))) return x;
         })
      ];
   };

   filterDocumentDescriptor = (
      header: string,
      callback: (time: number) => boolean
   ) => {
      const { history } = this.props;

      const filteredDocument = this.documentFiltered(callback);
      const orderedDocument = orderBy(
         filteredDocument,
         (e: any) => lastUpdate(e),
         'desc'
      );
      if (orderedDocument.length === 0) return [];

      const documentDescriptor = [];
      const LIST_ITEM = [...LIST_ELEMENTS, ...LIST_ACTIONS];
      orderedDocument.forEach((doc: any) => {
         const descriptor = fillDocument(doc, history, LIST_POPUP, LIST_ITEM);
         documentDescriptor.push(descriptor);
      });
      return [fillTimeHeaderList(header), documentDescriptor];
   };

   concatList = () => [
      ...this.filterDocumentDescriptor(TODAY, today),
      ...this.filterDocumentDescriptor(YESTERDAY, yesterday),
      ...this.filterDocumentDescriptor(WEEK, week)
   ];

   render() {
      const { theme, lang, courses } = this.props;

      return (
         <div className={styles.padding}>
            <div className={styles.wrapper}>
               {courses.length === 0 ? (
                  <div className={cx(theme, 'empty-list')}>
                     {lang.emptyRecent}
                  </div>
               ) : (
                  <>
                     <div
                        className={cx(
                           theme,
                           styles.gradient,
                           styles.gradientTop
                        )}
                     />
                     <div id="childrenList" className={cx(styles.childrenList)}>
                        <List list={this.concatList()} {...this.props} />
                     </div>
                  </>
               )}
            </div>
         </div>
      );
   }
}
