import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
// import * as _ from 'lodash';
import cx from 'classnames';

import { BuilderColumns } from './ElementsList';
import BuilderActions from './ActionsList';

// Style
import styles from './List.less';

// typescript
import { ToggleModal, ILanguage, IListItem } from '../../../typescript';

// Constant
export const NONE = 'NONE';

interface IProps extends RouteComponentProps {
   list: IListItem[][];
   lang: ILanguage;
   toggleModal: ToggleModal;
   theme: string;
}

const List = (props: IProps) => {
   const { list, theme } = props;
   const [hovered, setHovered] = React.useState(NONE);

   const handleOverPopUp = (id: string) => () => setHovered(id);

   // console.log("List :", list);
   return (
      <div className={styles.listContainer}>
         {list.map((listArray: IListItem[]) =>
            listArray.map((elem: IListItem) => (
               <div
                  key={elem.id}
                  onMouseOver={handleOverPopUp(elem.id)}
                  onMouseLeave={handleOverPopUp(NONE)}
               >
                  <BuilderActions descriptor={elem}>
                     <div
                        className={cx(
                           theme,
                           elem.className ? elem.className : styles.alignContent
                        )}
                     >
                        <BuilderColumns
                           descriptor={elem}
                           hovered={hovered === elem.id}
                           {...props}
                        />
                     </div>
                  </BuilderActions>
               </div>
            ))
         )}
      </div>
   );
};

export default withRouter(List);
