import * as React from 'react';

const IconNateOutline = props => (
   <svg viewBox="0 0 100 100" {...props}>
      <path
         d="M86.3,99.2c-0.1,0-0.2,0-0.3,0c-2.7,0.1-5.5-1-7.5-2.9c-2.1-1.9-3.3-4.7-3.2-7.6l0-39.6c0-3.6-0.6-7.2-1.9-10.6
	c-1.2-3.2-2.9-6-5.2-8.5c-2.2-2.4-4.9-4.4-7.8-5.7c-3.1-1.4-6.6-2.2-10-2.1c-3.6-0.1-7,0.6-10.2,2c-3,1.3-5.7,3.2-8,5.6
	c-2.3,2.4-4.1,5.3-5.3,8.4c-1.3,3.4-1.9,6.9-1.9,10.5v39.9c0.1,2.8-1,5.5-3,7.5c-2,2-4.7,3.1-7.5,3c-1.3,0-2.8-0.3-4.1-0.8
	c-1.2-0.5-2.4-1.2-3.4-2.1c-1-1-1.8-2.1-2.4-3.4c-0.6-1.3-0.9-2.7-0.9-4.2v-40C3.5,42.3,4.7,36,7.1,30c2.3-5.7,5.6-10.8,9.8-15.2
	c4.3-4.4,9.3-7.8,14.9-10.3C37.7,2,43.9,0.8,50.3,0.8C56.7,0.8,62.9,2,68.7,4.6c5.5,2.4,10.5,5.9,14.7,10.3
	c4.2,4.4,7.4,9.5,9.6,15.2c2.3,5.9,3.5,12.2,3.4,18.6l0,40c0.1,2.8-1,5.5-3,7.5C91.6,98.1,89,99.2,86.3,99.2z M86,95.2
	c1.8,0.1,3.4-0.6,4.6-1.8c1.2-1.2,1.9-2.9,1.8-4.6l0-40.1c0-5.9-1-11.7-3.2-17.1c-2-5.2-5-9.9-8.8-13.9c-3.8-4-8.3-7.1-13.4-9.3
	c-5.3-2.3-11-3.5-16.8-3.4c-5.9,0-11.6,1.1-16.9,3.4c-5.1,2.2-9.7,5.4-13.6,9.4c-3.9,4-6.9,8.7-9,13.9C8.6,37,7.5,42.7,7.5,48.6v40
	c0,0.9,0.2,1.8,0.5,2.6c0.3,0.8,0.8,1.5,1.4,2c0.6,0.6,1.4,1,2.2,1.4c0.9,0.3,1.7,0.5,2.7,0.5c1.8,0.1,3.4-0.6,4.6-1.8
	c1.2-1.2,1.9-2.9,1.8-4.6l0-40c0-4.1,0.7-8.1,2.2-12c1.4-3.6,3.5-6.9,6.2-9.8c2.7-2.8,5.8-5,9.3-6.5c3.8-1.6,7.7-2.4,11.8-2.4
	c4,0,8,0.8,11.7,2.5c3.5,1.6,6.6,3.8,9.1,6.7c2.6,2.9,4.6,6.2,6,9.8c1.4,3.8,2.2,7.9,2.1,12v39.6c0,1.8,0.7,3.5,1.9,4.6
	C82.5,94.6,84.2,95.2,86,95.2z"
      />
   </svg>
);

export default IconNateOutline;
