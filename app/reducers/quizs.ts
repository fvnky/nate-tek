import _ from 'lodash';
// import { Action } from './types';
import { IQuiz } from '../typescript';
import { IActionCourse, IActionQuiz } from './types';

export const ADD_QUIZ = 'add_quiz';
export const FETCH_QUIZ = 'fetch_quiz';
export const UPDATE_QUIZ = 'update_quiz';
export const REMOVE_QUIZ = 'remove_quiz';
// export const FETCH_COURSE = "fetch_course";
// export const UPDATE_COURSE = "update_course";
// export const REMOVE_COURSE = "remove_course";

export function quizs(state: IQuiz[] = [], action: IActionQuiz) {
   switch (action.type) {
      case ADD_QUIZ:
         return _.union(state, [action.payload]);
      case FETCH_QUIZ: {
         // console.log("HELLO");
         return action.payload;
      }
      case UPDATE_QUIZ: {
         const ref = _.clone(state);
         const quiz = _.find(ref, { _id: action.payload._id });

         _.assign(quiz, action.payload);
         return ref;
      }
      case REMOVE_QUIZ: {
         const ref = _.clone(state);

         _.remove(ref, { _id: action.payload._id });
         return ref;
      }

      // case UPDATE_COURSE: {
      //    const ref = _.clone(state);
      //    const subject = _.find(ref, { _id: action.payload._id });

      //    _.assign(subject, action.payload);
      //    return ref;
      // }
      // case REMOVE_COURSE: {
      //    const ref = _.clone(state);

      //    _.remove(ref, { _id: action.payload._id });
      //    return ref;
      // }
      default: {
         // const ref = _.clone(state);

         return state;
      }
   }
}
