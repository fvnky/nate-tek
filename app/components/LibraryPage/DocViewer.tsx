import React, { useEffect, useState } from 'react';
import Highlighter from 'react-highlight-words';
import cx from 'classnames';

// Utils
import { capitalize } from '../utils';

import routes from '../../constants/routes.json';

// Typescript
import {
   IMatch,
   ILanguage,
   IDatabase,
   ICourse,
   ISubject,
   IFolder
} from '../../typescript';

import styles from './DocViewer.less';
import ViewingMode from '../EditorPage/Editor/ViewingMode';
import { ToggleModal } from '../../typescript/flow';

interface IProps {
   subjects: ISubject[];
   folders: IFolder[];
   courses: ICourse[];
   setDoc: any;
   theme: string;
   match: IMatch;
   history: any;
   lang: ILanguage;
   db: IDatabase;
   doc: any;
   toggleModal: ToggleModal;
}

const DocViewer: React.FC<IProps> = ({
   courses,
   folders,
   subjects,
   theme,
   lang,
   match,
   history,
   db,
   doc,
   setDoc,
   toggleModal
}) => {
   const edit = (docId: string) => {
      // history.push(routes.EDITOR_LINK + "/" + docId)
   };

   const back = () => {
      setDoc(null);
   };

   console.warn('doc is from viewer', doc);

   return (
      <div className={cx(theme, styles.layout)}>
         <ViewingMode
            toggleModal={toggleModal}
            theme={theme}
            db={db}
            course={doc}
            edit={edit}
            back={back}
            subjects={subjects}
            folders={folders}
            courses={courses}
            fromLibrary={true}
         />
      </div>
   );
};

export default DocViewer;
