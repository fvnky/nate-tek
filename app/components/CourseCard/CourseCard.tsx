import React from 'react';
import cx from 'classnames';
import { History } from 'history';

import withTheme from '../withTheme';
import styles from './CourseCard.less';
import { ICourse } from '../../typescript';

const placehodler = require('../../assets/course_cover_placeholder.png');

import routes from "../../constants/routes.json"
import { url } from 'inspector';
import IconFolder from '../../assets/icons/NounIcons/IconFolder';
import IconDelete from '../../assets/icons/IconDelete';
import IconEdit from '../../assets/icons/IconEdit';

import { MODAL_UPDATE_COURSE, MODAL_MOVE_RESOURCE, MODAL_DELETE_COURSE } from '../../constants/modal';


type Props = {
   theme: string;
   course: ICourse;
   history: History;
   toggleModal: any;
   onClickCourse: (c: ICourse) => void;
   editable?: boolean;
};

const CourseCard: React.FC<Props> = ({ theme, course, history, toggleModal, onClickCourse, editable = false }) => {

   const [mouseOver, setMouseOver] = React.useState(false)

   const onMouseMove = () => {
      if (!mouseOver) {
         setMouseOver(true)
      }
   }

   const onMouseLeave = () => {
      setMouseOver(false)
   }

   const onCardClick = course => e => {
      if (onClickCourse) {
         onClickCourse(course)
         return;
      }

      if (course.textContent.length === 0)
         history.push(routes.EDITOR_LINK + '/' + course._id);
      else
         history.push(routes.DOC_VIEWER_LINK + '/' + course._id);
   }

   const onClickRename = course => e => {
      e.stopPropagation();
      toggleModal(MODAL_UPDATE_COURSE, {id: course._id});
   }

   const onClickMove = course => e => {
      e.stopPropagation();
      toggleModal(MODAL_MOVE_RESOURCE, {resource: course, collection: course.collection});

   }

   const onClickDelete = course => e => {
      e.stopPropagation();
      toggleModal(MODAL_DELETE_COURSE, {id: course._id});

   }

   const image = (course.coverImage && course.coverImage.small.length > 0) ? course.coverImage.small : placehodler
   // console.log(process.versions)
   return (
      <div className={cx(theme, styles.gridItem)}>
         <div className={cx(theme, styles.layout)} style={{ backgroundImage: "url(" + image + ")" }} onClick={onCardClick(course)} onMouseMove={onMouseMove} onMouseLeave={onMouseLeave}>
            <div className={cx(theme, styles.overlay)}>
               <div className={cx(theme, styles.title)}>{course.name}</div>
               <div className={cx(theme, styles.preview)}>{course.textContent.substr(0, 100)}</div>
            </div>

           {editable &&  <div className={cx(styles.buttons, mouseOver ? styles.showButtons : '')}>
               <div className={cx(styles.button, styles.folder)} onClick={onClickRename(course)}>
                  <IconEdit />
               </div>
               <div className={cx(styles.button, styles.folder)} onClick={onClickMove(course)}>
                  <IconFolder />
               </div>
               <div className={cx(styles.button, styles.delete)} onClick={onClickDelete(course)}>
                  <IconDelete />
               </div>
            </div>}
         </div>
      </div>
   );
};

export default withTheme(CourseCard);

export const CourseContainer = ({children}) => <div className={styles.coursesContainer}>{children}</div>
