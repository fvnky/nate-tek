// Collection
export const NATE_TODO = 'todo';

// Attributes
export const NAME_TODO = 'name';
export const IS_DONE = 'status';
export const DUE_DATE = 'dueAt';
