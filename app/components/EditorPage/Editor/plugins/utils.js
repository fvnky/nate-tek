import { Block } from "slate";
import { List } from "immutable";

export function getHighestSelectedBlocks(editor) {
   const { value } = editor;
   let { selection } = value;
   if (selection.isBackward) {
      selection = selection.flip();
   }
   const { document } = value;
   try {
      if (selection.start.key && selection.end.key) {
         let startBlock = document.getClosestBlock(selection.start.key);
         let endBlock = document.getClosestBlock(selection.end.key);
         if (startBlock && endBlock && startBlock !== endBlock) {
            if (selection.anchor.isAtEndOfNode(startBlock)) {
               startBlock = document.getNextBlock(startBlock.key);
            }
            if (selection.focus.isAtStartOfNode(endBlock)) {
               endBlock = document.getPreviousBlock(endBlock.key);
            }
         }
         if (startBlock && !endBlock) {
            return List([startBlock]);
         } if (endBlock && !startBlock) {
            return List([endBlock]);
         } if (startBlock === endBlock && startBlock) {
            return List([startBlock]);
         }
         if (startBlock && endBlock) {
            const ancestor = document.getCommonAncestor(
               startBlock.key,
               endBlock.key
            );
            if (ancestor || ancestor) {
               const startPath = ancestor.getPath(startBlock.key);
               const endPath = ancestor.getPath(endBlock.key);
               const blockStartPath = startPath
                  ? List(startPath).first()
                  : undefined;
               const blockEndPath = endPath ? List(endPath).first() : undefined;
               if (
                  blockStartPath !== undefined &&
                  blockEndPath !== undefined &&
                  typeof blockStartPath === "number" &&
                  typeof blockEndPath === "number"
               ) {
                  return ancestor.nodes
                     .slice(blockStartPath, blockEndPath + 1)
                     .filter(node => Block.isBlock(node));
               }
            }
         }
         return List([]);
      }
   } catch (e) {
      return List([]);
   }
   return List([]);
}

