import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import LibraryPage from '../components/LibraryPage/LibraryPage.tsx';
import * as UserActions from '../actions/user';
import * as SubjectsActions from '../actions/subjects';
import * as FoldersActions from '../actions/folders';
import * as CoursesActions from '../actions/courses';
import * as ModalActions from '../actions/modal';

function mapStateToProps(state) {
   return {
      folders: state.folders,
      courses: state.courses,
      subjects: state.subjects,
      lang: state.language,
      theme: state.theme,
      router : state.router,
      db: state.database
   };
}

function mapDispatchToProps(dispatch) {
   return bindActionCreators(
      {
         ...UserActions,
         ...SubjectsActions,
         ...FoldersActions,
         ...CoursesActions,
         ...ModalActions,
      },
      dispatch
   );
}

export default withRouter(
   connect(
      mapStateToProps,
      mapDispatchToProps
   )(LibraryPage)
);
