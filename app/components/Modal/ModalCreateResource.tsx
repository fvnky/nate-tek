import * as React from 'react';
import cx from 'classnames';

// Component
import Modal from '../Base/Modal';
import IconFolderOutline from '../../assets/icons/IconFolderOutline';
import IconCourse from '../../assets/icons/IconCourse';
import IconQuiz from '../../assets/icons/IconQuiz';

// Utils
import { capitalize } from '../utils';

// Style
import stylesErr from './ModalError.less';
import styles from './ModalCreateResource.less';

// typescript
import {
   CreateFolder,
   CreateCourse,
   ILanguage,
   CreateQuiz
} from '../../typescript';

const FOLDER = 'FOLDER';
const COURSE = 'COURSE';
const QUIZ = 'QUIZ';

interface IProps {
   parentId: string;
   collection: string;
   createFolder: CreateFolder;
   createCourse: CreateCourse;
   createQuiz: CreateQuiz;
   handleDisplayModal: () => void;
   lang: ILanguage;
   theme: string;
}

interface IState {
   title: string;
   type: string;
}

export default class ModalCreateResource extends React.Component<
   IProps,
   IState
> {
   input: React.RefObject<HTMLInputElement> = React.createRef();

   state = {
      title: '',
      type: COURSE
   };

   componentDidMount() {
      const input = this.input.current;

      if (input) input.focus();
   }

   handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { target } = event;

      this.setState({ title: capitalize(target.value) });
   };

   handleOk = () => {
      const {
         createFolder,
         createCourse,
         createQuiz,
         handleDisplayModal,
         parentId,
         collection
      } = this.props;
      const { title, type } = this.state;
      const newDocument = {
         name: title,
         parent: { id: parentId, collection }
      };

      if (type === FOLDER) {
         // const newFolder = {
         //    name: title,
         //    parent: { id: parentId, collection }
         // };

         createFolder(newDocument)
            .then(data => {
               handleDisplayModal();
               console.log('DATA :', data);
            })
            .catch((err: any) => {});
      } else if (type === COURSE) {
         // const newCourse = {
         //    name: title,
         //    parent: { id: parentId, collection }
         // };
         createCourse(newDocument)
            .then(() => handleDisplayModal())
            .catch((err: any) => {});
      } else if (type === QUIZ) {
         // const newQuiz = {
         //    name: title,
         //    parent: {}
         // }
         createQuiz(newDocument)
            .then(() => handleDisplayModal())
            .catch((err: any) => console.log(err));
      }
   };

   renderTypeSelector = () => {
      const { lang, theme } = this.props;
      const { type } = this.state;

      return (
         <div className={styles.typeSelectorContainer}>
            <div
               role="button"
               className={cx(
                  theme,
                  styles.typeContainer,
                  type === FOLDER ? styles.containerActive : ''
               )}
               onClick={() => {
                  this.setState({ type: FOLDER });
               }}
            >
               <IconFolderOutline
                  className={cx(styles.resourceIcon, styles.folderIcon)}
               />
               <span className={cx(theme, styles.typeName)}>
                  {capitalize(lang.folder)}
               </span>
            </div>
            <div
               role="button"
               className={cx(
                  theme,
                  styles.typeContainer,
                  type === COURSE ? styles.containerActive : ''
               )}
               onClick={() => {
                  this.setState({ type: COURSE });
               }}
            >
               <IconCourse
                  className={cx(styles.resourceIcon, styles.courseIcon)}
               />
               <span className={cx(theme, styles.typeName)}>
                  {capitalize(lang.course)}
               </span>
            </div>
            <div
               role="button"
               className={cx(
                  theme,
                  styles.typeContainer,
                  type === QUIZ ? styles.containerActive : ''
               )}
               onClick={() => {
                  this.setState({ type: QUIZ });
               }}
            >
               <IconQuiz
                  className={cx(styles.resourceIcon, styles.courseIcon)}
               />
               <span className={cx(theme, styles.typeName)}>{lang.quiz}</span>
            </div>
         </div>
      );
   };

   renderInputName = () => {
      const { title } = this.state;
      const { lang, theme } = this.props;

      return (
         <div>
            <input
               ref={this.input}
               type="text"
               value={title}
               onChange={this.handleChange}
               className={cx(theme, styles.input)}
               placeholder={lang.enterTitle}
               autoFocus={true}
            />
         </div>
      );
   };

   render() {
      const { handleDisplayModal, lang, theme } = this.props;

      return (
         <Modal
            theme={theme}
            title={lang.create}
            onCancel={handleDisplayModal}
            onOk={this.handleOk}
            okText={lang.create}
            cancelText={lang.cancel}
         >
            {this.renderTypeSelector()}
            {this.renderInputName()}
         </Modal>
      );
   }
}
