import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';
import cx from 'classnames';

import styles from './Gallery.less';

function getBase64Image(imgUrl: string, callback: (data: string) => void) {
   var img = new Image();

   // onload fires when the image is fully loadded, and has width and height

   img.onload = function() {
      var canvas = document.createElement('canvas');
      canvas.width = img.width;
      canvas.height = img.height;
      var ctx = canvas.getContext('2d');
      ctx.drawImage(img, 0, 0);
      console.log('1');
      var dataURL = canvas.toDataURL('image/png');
      console.log('2');

      // dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
      callback(dataURL); // the base64 string
   };

   // set attributes and src
   img.setAttribute('crossOrigin', 'anonymous'); //
   img.src = imgUrl;
}

interface IProps {
   theme: string;
   thumbnails: any[];
   editor: any;
   closeMenu: () => void;
}

const InsertMenu: React.FC<IProps> = ({
   closeMenu,
   thumbnails,
   editor,
   theme
}) => {
   if (thumbnails.length === 0) return <></>;
   console.log(thumbnails);

   // editor.focus();
   const insertImage = url => e => {
      console.warn(url);
      // imageDataURI.encodeFromURL(url)
      // .then((data) =>  editor.insertImage(data))
      // .catch(err => console.error(err));
      closeMenu();
      getBase64Image(url, data => editor.insertImage(data));
   };

   const newImages = thumbnails.map(image => (
      <div className={styles.imageContainer} key={image.thumbnail}>
         <img
            role="button"
            onClick={insertImage(image.original)}
            className={styles.image}
            src={image.thumbnail}
            alt=""
         />
      </div>
   ));

   return (
      <div className={styles.imagesContainerContainer}>
         <div className={styles.imagesContainer}>{newImages}</div>
      </div>
   );
};

export default InsertMenu;
