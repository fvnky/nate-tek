import {
   SET_LAST_ACTIVE_BROWSER_PATH,
   SET_LAST_ACTIVE_NAVIGATION_TAB,
   SET_VIEWER_DOC_ID
} from "../actions/resourcesNavigation";

const initialState = {
   lastTab: null,
   lastBrowserPath: null,
   viewerDocId: null
};

export function resourcesNavigation(state: { lastTab: string, lastBrowserPath: string, viewerDocId: string } = initialState, action: { type: string, payload: any }) {
   switch (action.type) {
      case SET_LAST_ACTIVE_NAVIGATION_TAB: {
         return { ...state, lastTab: action.payload }
      }
      case SET_LAST_ACTIVE_BROWSER_PATH: {
         return { ...state, lastBrowserPath: action.payload }
      }
      case SET_VIEWER_DOC_ID: {
         return { ...state, viewerDocId: action.payload }
      }
      default:
         return state;
   }
}
