import _ from 'lodash';
// import { Action } from './types';
import { ICourse } from '../typescript';
import { IActionCourse } from './types';

export const ADD_COURSE = 'add_course';
export const FETCH_COURSE = 'fetch_course';
export const UPDATE_COURSE = 'update_course';
export const REMOVE_COURSE = 'remove_course';

export function courses(state: ICourse[] = [], action: IActionCourse) {
   switch (action.type) {
      case ADD_COURSE:
         return _.union(state, [action.payload]);
      case FETCH_COURSE:
         return action.payload;
      case UPDATE_COURSE: {
         const ref = _.clone(state);
         const subject = _.find(ref, { _id: action.payload._id });

         _.assign(subject, action.payload);
         return ref;
      }
      case REMOVE_COURSE: {
         const ref = _.clone(state);

         _.remove(ref, { _id: action.payload._id });
         return ref;
      }
      default: {
         // const ref = _.clone(state);

         return state;
      }
   }
}
