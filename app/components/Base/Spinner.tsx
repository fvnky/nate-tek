import React from 'react';
import cx from 'classnames';
import withTheme from '../withTheme';

import styles from './Spinner.less';

interface IProps {
   theme: string;
   className?: string;
   size?: number;
}

const Spinner: React.FC<IProps> = ({ theme, className = '', size = 1 }) => {
   return (
      <div className={cx(theme, className, styles.spinnerContainer)}>
         <div
            className={cx(theme, styles.spinner)}
            style={{ transform: `scale(${size})` }}
         >
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
         </div>
      </div>
   );
};
export default withTheme(Spinner);
