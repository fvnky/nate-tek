import React from 'react';
// import cx from "classnames";
import styles from "./FormatButton.less";
import {HIGHLIGHT_COLORS} from "../../highlight/highlight"

interface IProps {
   type: number;
   callback?: (type: number) => void;
   isActive?: (type: number) => boolean;
}

export default function HighlightButton(props: IProps) {
   const { type, isActive = () => false, callback = () => null } = props;

   const onMouseDown = (event: React.MouseEvent) => {
      event.preventDefault();
      event.stopPropagation();
      callback(type);
   };

   return (
      <button
         className={`ql-background ${styles.highlight} ${styles.unstyle} ${
            isActive(type) ? styles.highlightActive : ''
         }`}
         style={{ backgroundColor: HIGHLIGHT_COLORS[type] }}
         onMouseDown={onMouseDown}
      />
   );
}
