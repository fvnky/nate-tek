import { COURSE } from '../constants';

export default {
   definition: Object.freeze({
      _id: { type: String, required: true },
      _rev: { type: String, required: true },
      name: { type: String, required: true },
      likes: { type: Array },
      comments: { type: Array },
      textContent: { type: String, required: false },
      coverImage: {
         full: { type: String, required: false },
         raw: { type: String, required: false },
         regular: { type: String, required: false },
         small: { type: String, required: false },
         thumb: { type: String, required: false }
      },
      parent: {
         collection: { type: String, required: true },
         id: { type: String, required: true }
      },
      coverImage: {
         full: { type: String, required: false },
         raw: { type: String, required: false },
         regular: { type: String, required: false },
         small: { type: String, required: false },
         thumb: { type: String, required: false }
      },
      createdAt: { type: Number, required: true },
      updatedAt: { type: Number, required: false },
      collection: COURSE
   }),
   fields: [
      '_id',
      '_rev',
      'name',
      'likes',
      'comments',
      'textContent',
      'coverImage.full',
      'coverImage.raw',
      'coverImage.regular',
      'coverImage.small',
      'coverImage.thumb',
      'parent.collection',
      'parent.id',
      'coverImage.full',
      'coverImage.raw',
      'coverImage.regular',
      'coverImage.small',
      'coverImage.thumb',
      'createdAt',
      'updatedAt',
      'collection'
   ]
};
