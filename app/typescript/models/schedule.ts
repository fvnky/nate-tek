import {
   DATE_SCHEDULE,
   TITLE_SCHEDULE,
   CLASSNAME_SCHEDULE,
   ROOM_SCHEDULE,
   NOTE_SCHEDULE,
   START_SCHEDULE,
   END_SCHEDULE,
   ID_SUBJECT_SCHEDULE
} from '../../constants/models';
import { CREATE, FIND_BY_ID, UPDATE, REMOVE } from '../../constants/models';

export interface ISchedule {
   _id: string;
   _rev: string;
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   [DATE_SCHEDULE]: Date;
   [TITLE_SCHEDULE]?: string;
   [CLASSNAME_SCHEDULE]?: string;
   [ROOM_SCHEDULE]?: string;
   [NOTE_SCHEDULE]?: string;
   [START_SCHEDULE]?: Date;
   [END_SCHEDULE]?: Date;
   [ID_SUBJECT_SCHEDULE]?: string;
}

export interface ICreateSchedule {
   [DATE_SCHEDULE]: Date;
   [TITLE_SCHEDULE]?: string;
   [CLASSNAME_SCHEDULE]?: string;
}

export type CreateSchedule = (
   newSchedule: ICreateSchedule
) => Promise<ISchedule>;

export type IDatabaseSchedule = {
   // [FIND_BY_ID]: FindDictionaryById;
   [CREATE]: CreateSchedule;
   [UPDATE]: any;
   [REMOVE]: any;
};
