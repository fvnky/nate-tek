import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';

import InsertImagePopup from "./InsertImagePopup"
import NateFileUploader from '../../../../Base/NateFileUploader';
import InsertButton from './InsertButton';
import IconSearch from '../../../../../assets/icons/IconSearch';
import AddImage from '../../../../../assets/icons/Editor/AddImage';
import PdfDocument from '../../../../../assets/icons/Editor/PdfDocument';
import Quote from '../../../../../assets/icons/Editor/Quote';
import Link from '../../../../../assets/icons/Editor/Link';
import Divider from '../../../../../assets/icons/Editor/Divider';

import styles from './InsertMenu.less';


interface IProps {
   theme: string;
   show: boolean;
   editor: any;
   closeMenu: () => void;
}

const InsertMenu: React.FC<IProps> = ({ closeMenu, show, editor, theme }) => {
   const [imagePopup, setImagePopup] = useState(false);

   useEffect(() => {
      if (!show && imagePopup) {
         setImagePopup(false);
      }
   })

   const toggleImagePopup = () => {
      setImagePopup(!imagePopup)
   }

   const onInsertPdf = e => {
      const file = e.target.files[0];
      editor.insertPdf(file.name, file.path);
   };

   const onInsertMenuClick = e => {
      if (imagePopup)
         setImagePopup(false)
   }

   return (
      <CSSTransition
         in={show}
         appear={true}
         timeout={{
            appear: 600,
            enter: 400,
            exit: 200
         }}
         classNames={{
            appear: styles.appear,
            appearActive: styles.appearActive,
            enter: styles.enter,
            enterActive: styles.enterActive,
            enterDone: styles.enterDone,
            exit: styles.exit,
            exitActive: styles.exitActive,
            exitDone: ''
         }}
         unmountOnExit
      >
         <>

            <div className={styles.layout} onClick={onInsertMenuClick}>
               {/* <NateFileUploader onChange={onInsertImage} accept="image/x-png,image/jpeg"> */}
                  <div onClick={toggleImagePopup}>
                     <InsertButton theme={theme} icon={<AddImage />} label={'Image'} />
                  </div>
               {/* </NateFileUploader> */}

               {/* <InsertButton theme={theme} icon={<IconSearch />} label={'Search'} /> */}
               <NateFileUploader onChange={onInsertPdf} accept="application/pdf">
                  <InsertButton theme={theme} icon={<PdfDocument />} label={'Pdf'} />
               </NateFileUploader>
               <InsertButton theme={theme} icon={<Divider />} label={'Divider'} onClick={editor.insertDivider} />
               <InsertButton theme={theme} icon={<Quote />} label={'Quote'} onClick={editor.insertQuote} />
               <InsertButton theme={theme} icon={<Link />} label={'Link'} />
            </div>
            <div className={styles.hideBackground} />
            <InsertImagePopup theme={theme} show={imagePopup} editor={editor} closeMenu={closeMenu}/>
         </>
      </CSSTransition>
   );
};

export default InsertMenu;
