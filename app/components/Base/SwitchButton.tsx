import * as React from 'react';
import cx from "classnames";
import withTheme from '../withTheme'
// Styles
import styles from './SwitchButton.less';

// tslint:disable-next-line:no-empty-interface
interface IProps {
   theme: string;
   active: boolean;
   onClick?: () => void;
}

class SwitchButton extends React.Component<IProps> {
   handleOnClick = () => {
      const { onClick } = this.props;

      if (onClick) onClick();
   };

   render() {
      console.log("props", this.props);
      const { active, theme } = this.props;

      return (
         <div
            className={cx(theme, styles.switchButton, active ? styles.switchButtonActive : "")}
            role="button"
            onClick={this.handleOnClick}
         >
            <div className={cx(theme, styles.switchCircle, active ? styles.switchCircleActive : "")} />
         </div>
      );
   }
}

export default withTheme(SwitchButton);
