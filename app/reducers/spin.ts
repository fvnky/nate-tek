import { SET_SPIN } from '../actions/spinner';

// Constant
import { LIGHT_THEME, DARK_THEME } from '../constants/theme';

// Typescript
import { IActionSpinner } from './types';

export function spinner(state: string = LIGHT_THEME, action: IActionSpinner) {
   switch (action.type) {
      case SET_SPIN:
         return action.payload;
      default:
         return state;
   }
}
