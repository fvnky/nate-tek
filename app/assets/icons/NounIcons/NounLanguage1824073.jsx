import * as React from 'react';

const SvgNounLanguage1824073 = props => (
   <svg viewBox="0 0 100 100" {...props}>
      <g data-name="Layer 9">
         <path d="M27.08 43h5.85L30 33.49 27.08 43z" />
         <path d="M38 14H18a8 8 0 0 0-8 8v68l24-24h18zm-3.84 33h-8.31L24 53h-4l8-26h4l8 26h-4zM82 14H42l14 52h26a8 8 0 0 0 8-8V22a8 8 0 0 0-8-8zM68 27h4v4h-4zm12 26a26.81 26.81 0 0 1-10.64-2.1A21.79 21.79 0 0 1 60 53v-4a17.9 17.9 0 0 0 5-.7c-3.09-2.46-5-5.69-5-9.3h4c0 2.88 2.08 5.51 5.32 7.36A13.54 13.54 0 0 0 76 35H60v-4h20v4a17.15 17.15 0 0 1-6.37 13.13A23.34 23.34 0 0 0 80 49z" />
      </g>
   </svg>
);

export default SvgNounLanguage1824073;
