import React, { useEffect, useState } from "react";
import cx from "classnames";
// import { Spring, config } from "react-spring/renderprops";
import { Transition, animated, config} from "react-spring/renderprops";

// Icon
import IconClose from "../../assets/icons/IconClose";

// Style
import styles from "./Modal.less";

interface IProps {
   titleLess?: boolean;
   okDisabled?: boolean;
   theme: string;
   zIndex?: number;
   title?: string;
   cancelText?: string;
   okText?: string;
   onCancel: () => void;
   onOk: () => void;
   children?: any;
   handleKeyDown?: (e: any) => void;
}

export default function Modal(props: IProps) {
   const { titleLess = false, onCancel = () => null, onOk = () => null, title = "", cancelText = "Cancel", okText = "Ok", children = null, theme, okDisabled = false } = props;
   const [show, toggleShow] = useState(true);
   // toggleShow(true);
   // const springProps = useSpring({ from: { marginTop: entering }, to: { marginTop: 0 }, config: { tension: 400, friction: 25 } });
   // const config = { tension: 400, friction: 25 };
   // const [springProps, set] = useSpring(() => ({to: { marginTop: 300 }, config}))
   // set({to: { marginTop: 0 }, config})
   // const transitions = useTransition(items, item => item.key, {
   //    from: { transform: 'translate3d(0,-40px,0)' },
   //    enter: { transform: 'translate3d(0,0px,0)' },
   //    leave: { transform: 'translate3d(0,-40px,0)' },
   //  })

   useEffect(() => {
      window.onkeydown = props.handleKeyDown ? props.handleKeyDown : handleKeyDown;
      return () => {
         window.onkeydown = undefined;
      };
   });

   function handleCancel() {
      toggleShow(false);
      onCancel();
   }

   function handleOk() {
      toggleShow(false);
      onOk();
   }

   function handleKeyDown(e: KeyboardEvent) {
      if (e.keyCode === 13) {
         handleOk();
      }
      if (e.keyCode === 27) {
         handleCancel();
      }
   }

   function animationDone() {}

   return (
      <div className={cx(theme, styles.layout)}>
         <Transition  items={show} from={{ opacity: 0 }} enter={[{ opacity: 1 }]} leave={{ opacity: 0 }} config={{ tension: 170, friction: 24 }}>
            {show =>
               show &&
               (props => (
                  <animated.div style={props}>
                     <div className={cx(theme, styles.mask)} />
                  </animated.div>
               ))
            }
         </Transition>
         <Transition
            items={show}
            from={{ marginTop: 300, opacity: 0 }}
            enter={[{ marginTop: 0, opacity: 1}]}
            leave={{ marginTop: 300, opacity: 0.7 }}
            config={{ tension: 270, friction: 30 }}
            trail={80}
         >
            {show =>
               show &&
               (props => (
                  <animated.div style={props}>
                     <div className={cx(theme, styles.container)}>
                        {!titleLess && <div className={cx(theme, styles.title)}>{title}</div>}
                        <div className={cx(theme, styles.content, titleLess ? styles.contentTitleLess : "")}>{children}</div>
                        <div className={cx(theme, styles.footer)}>
                           <div>
                              <input
                                 type="button"
                                 onMouseDown={handleCancel}
                                 className={cx(theme, styles.cancelButton)}
                                 value={cancelText.toUpperCase()}
                              />
                           </div>
                           <div>
                              <input
                                 disabled={okDisabled}
                                 type="button"
                                 className={cx(theme, styles.confirmButton, okDisabled ? styles.confirmButtonDisabled : "")}
                                 value={okText.toUpperCase()}
                                 onMouseDown={handleOk}
                              />
                           </div>
                        </div>
                     </div>
                  </animated.div>
               ))
            }
         </Transition>
      </div>
   );
}
