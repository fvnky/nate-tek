import {
   CREATE,
   FIND_BY_ID,
   UPDATE,
   REMOVE,
   SCORE
} from '../../constants/models';

export interface IQuiz {
   _id: string;
   _rev: string;
   createdAt?: number;
   updatedAt?: number;
   collection?: string;
   name?: string;
   [SCORE]: number;
   parent?: {
      id: string;
      collection: string;
   };
   questions?: any;
   // [key: string]: any;
}

export type UpdateQuiz = (toUpdate: IQuiz) => Promise<IQuiz>;
export type RemoveQuiz = (toRemove: IQuiz) => Promise<IQuiz>;
export type FindQuizById = (id: string) => Promise<IQuiz>;
export type CreateQuiz = (newQuiz: {
   name: string;
   parent: { id: string; collection: string };
}) => Promise<IQuiz>;

export type IDatabaseQuiz = {
   [FIND_BY_ID]: FindQuizById;
   [CREATE]: CreateQuiz;
   [UPDATE]: UpdateQuiz;
   [REMOVE]: RemoveQuiz;
};
