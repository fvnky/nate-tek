import * as React from 'react';

/*

interface IProps {
   styles: string,
   icons: any,
   onClick: (test: string) => void,
   activeStyle: string
}

interface IState {
   selected: string;
}
*/

export default class DisplayIcons extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         selected: ""
      };
   }

   handleSelection = key => {
      const { onClick } = this.props;

      console.log("selected: :", key)
      this.setState({ selected: key });
      onClick(key);
   };

   render() {
      const { styles, icons, activeStyle } = this.props;
      const { selected } = this.state;

      return Object.entries(icons).map(([key, value]) => {
         const style = selected.toString() === key.toString() ? activeStyle : styles;
         // const Item = value.type;
         const Value = value;

         return (
            <div
               key={key}
               onClick={() => this.handleSelection(key)}
               role="presentation"
            >
               {<Value className={style} />}
            </div>
         );
      });
   }
}
