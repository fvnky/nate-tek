import * as React from 'react';

const IconFolder = props => (
   <svg viewBox="0 0 14 12.1" {...props}>
      <path
         d="M1.2,0h2.7c0.4,0,0.7,0.2,0.9,0.5l0.5,0.6l0,0c0.2,0.3,0.6,0.5,0.9,0.5h6.6c0.6,0,1.2,0.5,1.2,1.2
	v8.2l0,0c0,0.6-0.5,1.2-1.2,1.2H1.2c-0.6,0-1.2-0.5-1.2-1.2V1.2l0,0C0,0.5,0.5,0,1.2,0z"
      />
   </svg>
);

export default IconFolder;
