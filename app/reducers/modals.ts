import { TOGGLE_MODAL } from "../actions/modal";

import { IActionModal, IPayloadModal } from "./types";

export const NONE = "NONE";

const initialState = {
   currentModal: NONE,
   propsModal: {}
};

export function modal(state: IPayloadModal = initialState, action: IActionModal) {
   switch (action.type) {
      case TOGGLE_MODAL:
         return action.payload;
      default:
         return state;
   }
}
