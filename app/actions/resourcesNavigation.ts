import { Dispatch } from "../reducers/types";

export const SET_LAST_ACTIVE_NAVIGATION_TAB = "SET_LAST_ACTIVE_NAVIGATION_TAB";
export const SET_LAST_ACTIVE_BROWSER_PATH = "SET_LAST_ACTIVE_BROWSER_PATH";
export const SET_VIEWER_DOC_ID = "SET_VIEWER_DOC_ID";

export const setLastActiveNavigationTab = (tab: string) => (dispatch: Dispatch) => {
   dispatch({
      type: SET_LAST_ACTIVE_NAVIGATION_TAB,
      payload: tab
   });
};

export const setLastActiveBrowserPath = (path: string) => (dispatch: Dispatch) => {
   dispatch({
      type: SET_LAST_ACTIVE_BROWSER_PATH,
      payload: path
   });
};

export const setViewerDocId = (id: string) => (dispatch: Dispatch) => {
   dispatch({
      type: SET_VIEWER_DOC_ID,
      payload: id
   });
};
