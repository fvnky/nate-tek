import { isString, pick } from 'lodash';
import {
   InvalidParameterFormat,
   QuizAlreadyExists,
   QuizNotFound,
   InvalidReference
} from '../errors';
import { Store } from '../factory';

// Constant
import { FIND_BY_ID, CREATE, UPDATE, REMOVE } from '../../constants/models';
import { COURSE, FOLDER, SUBJECT, DRAFT, QUIZ } from '../constants';

import {
   ADD_QUIZ,
   FETCH_QUIZ,
   UPDATE_QUIZ,
   REMOVE_QUIZ
} from '../../reducers/quizs';

import { QuizSchema } from '../schemas';

import {
   generateId,
   timestamp,
   parseUpdate,
   parseData,
   parseDoc,
   isNotFound,
   parseQuizQuestions
} from '../utils';

// Typescript
import { IQuiz, IDatabase, ICouchDB, IDbDocument } from '../../typescript';

// ================================================ //
// ==================== Create ==================== //
// ================================================ //

function create(this: IDatabase, data: IQuiz) {
   return new Promise((ok, ko) => {
      const store: any = new Store();
      const mandatory: string[] = ['name', 'parent.collection', 'parent.id'];
      const optionals: string[] = [];
      parseData(data, mandatory, optionals)
         .then((parsed: IQuiz) => {
            parsed._id = generateId();
            parsed.createdAt = timestamp();
            parsed.updatedAt = parsed.createdAt;
            parsed.collection = QUIZ;
            parsed.questions = [];
            parsed.score = 0;
            // console.log("PARSED :", parsed);
            return this._main.put(parsed);
         })
         .then(res => store.add('quizId', res.id))
         .then((id: string) => this._main.get(id))
         .then(quiz => parseDoc(QuizSchema, quiz))
         .then((quiz: IQuiz) => {
            this.dispatch({ type: ADD_QUIZ, payload: quiz });
            return quiz;
         })
         .then(ok)
         .catch(
            err =>
               new Promise((next, spread) => {
                  if (!store.quizId) return spread(err);
                  // console.log("CRASH")
                  this._main
                     .get(store.quizId)
                     .then(doc => this._main.remove(doc._id, doc._rev))
                     .then(() => spread(err))
                     .catch(() => spread(err));
               })
         )
         .catch(ko);
   });
}

// ============================================== //
// ==================== Read ==================== //
// ============================================== //

function findAll(this: IDatabase): Promise<IQuiz | IQuiz[]> {
   return new Promise((ok, ko) => {
      this._main
         .find({ selector: { collection: QUIZ } })
         .then(res => {
            // console.warn('Queries Quiz :', res.docs);
            this.dispatch({ type: FETCH_QUIZ, payload: res.docs });
            const result: any = res.docs;
            ok(result);
         })
         .catch(ko);
   });
}

function findById(this: IDatabase, id: string): Promise<IQuiz> {
   return new Promise((ok, ko) => {
      if (!isString(id))
         return ko(
            new InvalidParameterFormat(`The id parameter is not a String`)
         );

      this._main
         .get(id)
         .then((doc: IDbDocument) => {
            if (doc.collection !== QUIZ)
               throw new QuizNotFound(
                  `The document's id '${doc._id}' does not reference a quiz`
               );

            this.dispatch({ type: UPDATE_QUIZ, payload: doc });
            ok(doc as IQuiz);
         })
         .catch(err => {
            if (isNotFound(err))
               throw new QuizNotFound(`The quiz '${id}' could not be found`);

            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Update ==================== //
// ================================================ //

function update(this: IDatabase, doc: IQuiz) {
   return new Promise((ok, ko) => {
      const store: any = new Store();
      const fields = ['folderId'];

      parseDoc(QuizSchema, doc)
         .then(parsed => this._main.get(parsed._id))
         .then(old => parseUpdate(old, doc, fields))
         .then(quiz => parseQuizQuestions(quiz))
         .then(quiz => store.add('quiz', quiz))
         .then(quiz => {
            if (
               quiz.parent.collection !== SUBJECT &&
               quiz.parent.collection !== FOLDER &&
               quiz.parent.collection !== DRAFT
            )
               throw new InvalidParameterFormat(
                  `The parent collection to update ${
                     quiz.id
                  } are not compatible`
               );
         })
         .then(() => {
            const { quiz } = store;
            quiz.updatedAt = timestamp();
            return this._main.put(quiz);
         })
         .then(res => this._main.get(res.id))
         .then(quiz => {
            // console.log("QUIZ :", quiz);
            this.dispatch({ type: UPDATE_QUIZ, payload: quiz });
            return quiz;
         })
         .then(quiz => ok(quiz))
         .catch(err => {
            if (isNotFound(err))
               if (!store.quiz)
                  throw new QuizNotFound(
                     `The quiz '${doc._id}' could not be found`
                  );
            throw err;
         })
         .catch(ko);
   });
}

// ================================================ //
// ==================== Delete ==================== //
// ================================================ //

function remove(data) {
   return new Promise((ok, ko) => {
      parseData(pick(data, ['_id']), ['_id']) // On peut pas simplement faire data._id ?
         .then(parsed => this._main.get(parsed._id))
         .then(quiz => {
            // console.log("Quiz :", quiz);
            if (quiz.collection !== QUIZ)
               throw new QuizNotFound(
                  `The document's id '${quiz._id}' does not reference a quiz`
               );
            return quiz;
         })
         .then(({ _id, _rev }) => {
            const promises = [
               this._main.remove(_id, _rev),
               this._cloud.remove(_id, _rev)
            ];

            return Promise.all(promises);
         })
         .then(() => this.dispatch({ type: REMOVE_QUIZ, payload: data }))
         .then(ok)
         .catch(err => {
            if (isNotFound(err))
               throw new QuizNotFound(
                  `The quiz '${data._id}' could not be found`
               );
            throw err;
         })
         .catch(ko);
   });
}

export default function() {
   return {
      [CREATE]: create.bind(this),
      findAll: findAll.bind(this),
      [FIND_BY_ID]: findById.bind(this),
      [UPDATE]: update.bind(this),
      [REMOVE]: remove.bind(this)
   };
}
