
import * as React from "react";
import { Link } from "react-router-dom";

function PageNotFound() {
   return (
      <div>
         <p>Route does not match any component</p>
         <Link to="/">Back to Home</Link>
      </div>
   );
}

export { PageNotFound };
