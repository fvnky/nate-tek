import React from "react";
import cx from "classnames";
// import styles from "./definition.less";
import DefinitionComponent from "./DefinitionComponent";

export const DEFINITION = "definition";

export default function definition(options = {}) {
   return {
      schema: {
         marks: {
            DEFINITION: {
              isAtomic: true,
            },
          },
      },
      onChange(editor) {
         const { value } = editor
         const string = "haha"
         const texts = value.document.getTexts()
         const decorations = []

         texts.forEach(node => {
           const { key, text } = node
           const parts = text.split(string)
           let offset = 0

           parts.forEach((part, i) => {
             if (i !== 0) {
               decorations.push({
                 anchor: { key, offset: offset - string.length },
                 focus: { key, offset },
                 mark: { type: DEFINITION },
               })
             }

             offset = offset + part.length + string.length
           })
         })

         // Make the change to decorations without saving it into the undo history,
         // so that there isn't a confusing behavior when undoing.
         editor.withoutSaving(() => {
           editor.setDecorations(decorations)
         })

      },
      renderMark(props, editor, next) {
         const { children, mark, attributes } = props

         switch (mark.type) {
           case DEFINITION:
             return (
               <DefinitionComponent {...attributes}>
                 {children}
               </DefinitionComponent>
             )
           default:
             return next()
         }
      }
   };
}
