import * as React from 'react';
import cx from 'classnames';

// Component
import PopUp from '../PopUp/PopUp';

// Icon
import IconMoreVertical from '../../../assets/icons/IconMoreVertical';
// import IconMore from '../../../assets/icons/IconMore';
import { iconsSubject } from '../../../assets/icons/Subject/IconsSubject';

// Utils
import { timeDifference, destruct } from '../../utils';

// Style
import styles from './ElementsList.less';

// Typescript
import {
   IDocumentName,
   IDocumentIcon,
   IListItem,
   IDocumentLastUpdated,
   IActionPopUp,
   ILanguage,
   IDocumentHeaderTime
} from '../../../typescript';

// Constant
import { POPUP_TRIGGER_LIST } from '../PopUp/PopUpConstructor';
import { TIME_HEADER_LIST } from './HeaderList';

export { TIME_HEADER_LIST } from './HeaderList';
export const NAME_LIST = 'name';
export const ICON_LIST = 'icon';

export const POPUP_LIST = 'popup';
export const CREATE_LIST = 'create';
export const UPDATE_LIST = 'update';
export const LAST_UPDATED_LIST = 'lastUpdate';

export function BuildName(props: IDocumentName, id: string): React.ReactNode {
   const name = destruct([NAME_LIST], props);
   const { className = styles[NAME_LIST], theme } = props;

   return (
      <div key={`${id}_NAME`} className={cx(theme, className)}>
         {name}
      </div>
   );
}

export function BuildIcon(props: IDocumentIcon, id: string): React.ReactNode {
   const { className = styles[ICON_LIST], theme } = props;
   const iconName = destruct([ICON_LIST], props);
   const Icon = iconsSubject[iconName];

   if (!Icon) return null;

   return <Icon key={`${id}_ICON`} className={cx(theme, className)} />;
}

export function BuildLastUpdated(
   props: IDocumentLastUpdated,
   id: string
): React.ReactNode {
   const lastUpdate = destruct([LAST_UPDATED_LIST], props);
   const {
      className = styles[LAST_UPDATED_LIST],
      relativeToNow = true,
      theme
   } = props;

   const formattedDate = relativeToNow
      ? timeDifference(new Date().getTime(), lastUpdate)
      : lastUpdate.toString();
   return (
      <div key={`${id}_LAST_UPDATED`} className={cx(theme, className)}>
         {formattedDate}
      </div>
   );
}

export function BuildPopUp(props: IActionPopUp, id: string): React.ReactNode {
   const popup = destruct([POPUP_LIST], props);
   const Trigger = destruct([POPUP_TRIGGER_LIST], props);
   const { className = styles[POPUP_LIST], hovered, theme } = props;
   const [displayPopUp, setDisplayPopUp] = React.useState(false);
   // const style = `${cx(className, styles.hideMoreButton)} ${hovered || displayPopUp ? styles.showMoreButton : ''}`;

   const onVisibleChange = (display: boolean) => setDisplayPopUp(display);

   const handleClickOnMore = () => setDisplayPopUp(!displayPopUp);

   return (
      <div key={`${id}_POPUP`} className={className}>
         <PopUp
            content={popup}
            onVisibleChange={onVisibleChange}
            position={styles[POPUP_LIST]}
         >
            <div
               onClick={handleClickOnMore}
               className={cx(
                  styles.hideMoreButton,
                  hovered || displayPopUp ? styles.showMoreButton : ''
               )}
            >
               {
                  <Trigger.type
                     {...Trigger.props}
                     className={cx(theme, Trigger.props.className)}
                  />
               }
            </div>
         </PopUp>
      </div>
   );
}

export function BuildTimeHeader(props: IDocumentHeaderTime): React.ReactNode {
   const text = destruct([TIME_HEADER_LIST], props);
   const { className = styles[TIME_HEADER_LIST], theme } = props;

   return (
      <div key={`${text}_TIME_HEADER`} className={cx(theme, className)}>
         {text}
      </div>
   );
}

export const buildColumns = {
   [NAME_LIST]: BuildName,
   [ICON_LIST]: BuildIcon,
   [LAST_UPDATED_LIST]: BuildLastUpdated,
   [POPUP_LIST]: BuildPopUp,
   [TIME_HEADER_LIST]: BuildTimeHeader
};

interface IProps {
   descriptor: IListItem;
   hovered: boolean;
   lang: ILanguage;
   theme: string;
}

export const BuilderColumns = (props: IProps) => {
   const { descriptor, hovered, lang, theme } = props;
   const items = [];

   Object.keys(descriptor).forEach((element: string) => {
      // console.log("Element :", element);
      if (element === POPUP_LIST) descriptor[element].hovered = hovered;
      if (descriptor[element] && buildColumns[element]) {
         descriptor[element].lang = lang;
         descriptor[element].theme = theme;
         items.push(buildColumns[element](descriptor[element], descriptor.id));
      }
   });
   return <div className={styles.columnsContainer}>{items}</div>;
};
