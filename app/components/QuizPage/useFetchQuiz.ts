import React from 'react';

// Constant
import routes from '../../constants/routes.json';

// Typescript
import { IMatch, IQuiz } from '../../typescript';
import { IQuizPage } from './QuizPage';

interface IFindQuiz {
   match: IMatch;
   quizs: IQuiz[];
}

export function useFindQuiz({ match, quizs }: IFindQuiz) {
   const {
      params: { QuizID }
   } = match;
   const quiz = quizs.find(quizElem => quizElem._id === QuizID);
   // console.warn("Quizs :", quizs);
   // console.log("Quiz :", quiz);
   // console.log("ID :", QuizID);
   // console.log("MATCH :", match);

   if (!quiz) return null;
   return quiz;
}

interface IFindQuizParam extends IFindQuiz {
   param: string;
}

export function useFindQuizParam(props: IFindQuizParam) {
   const { param } = props;
   const quiz = useFindQuiz(props);

   if (!quiz || quiz[param] === undefined) return null;
   return quiz[param];
}

interface ISwitchQuiz extends IQuizPage {
   page: string;
}

export function useSwitchQuizPage(props: ISwitchQuiz) {
   const { match, page, history } = props;
   const {
      params: { QuizID }
   } = match;

   const link = routes.QUIZ_LINK + '/' + QuizID + page;
   // console.warn("LINK :", link);
   history.push(link);
}
