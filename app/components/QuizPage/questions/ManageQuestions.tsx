import React from 'react';

// Component
import { generateId } from '../../../database/utils';
import { QuizFooterButton } from '../QuizLayout';

// Constant
import {
   FORM,
   QUESTION,
   ANSWER,
   TRUE_FALSE,
   QCM,
   QUESTION_ANSWER,
   ANSWER_QCM,
   PROPOSITION_QCM
} from '../../../constants/models';
import { ANSWER_Q_A } from '../../../constants/models/questions/question_answer';

// Style
import styles from './ManageQuestion.less';

// Typescript
import { IFormTrueFalse } from '../../../typescript';
import { IPropsIndex } from '../QuizEditor';

interface IPropsTrueFalse extends IPropsIndex {
   index: number;
   state: IFormTrueFalse;
}

function TrueOrFalse(props: IPropsTrueFalse) {
   const { updateIndex, deleteIndex, index, state } = props;
   const {
      [FORM]: { [QUESTION]: question, [ANSWER]: answer }
   } = state;

   const handleOnChangeQuestion = (e: React.ChangeEvent<HTMLInputElement>) => {
      updateIndex(index, { ...state, [FORM]: { [QUESTION]: e.target.value } });
   };

   const handleOnChangeAnswer = (value: boolean) => () => {
      updateIndex(index, { ...state, [FORM]: { [ANSWER]: value } });
   };

   const handleOnClick = () => deleteIndex(index);

   return (
      <div>
         <div>
            <input
               type="text"
               value={question}
               onChange={handleOnChangeQuestion}
            />
         </div>
         <div>
            <label>
               true:
               <input
                  type="radio"
                  name={`${index}`}
                  defaultChecked={answer === true}
                  onChange={handleOnChangeAnswer(true)}
               />
            </label>
         </div>
         <div>
            <label>
               false:
               <input
                  type="radio"
                  name={`${index}`}
                  defaultChecked={answer === false}
                  onChange={handleOnChangeAnswer(false)}
               />
            </label>
         </div>
         <div>
            <input type="button" value="delete" onClick={handleOnClick} />
         </div>
      </div>
   );
}

interface IPropsQcm extends IPropsIndex {
   state: any;
   index: number;
}

function Qcm(props: IPropsQcm) {
   const { state, updateIndex, index, deleteIndex } = props;
   const {
      [FORM]: { [ANSWER_QCM]: indexAnswer, [PROPOSITION_QCM]: propositions }
   } = state;

   const handleOnClick = () => {
      const update = { ...state };

      propositions[generateId()] = '';
      update[FORM][PROPOSITION_QCM] = propositions;
      updateIndex(index, update);
   };

   const handleOnClickSelectAnswer = (indexProposition: string) => () => {
      const update = { ...state };

      update[FORM][ANSWER_QCM] = indexProposition;
      updateIndex(index, update);
   };

   const handleOnChange = (indexProposition: string) => e => {
      const { value } = e.target;
      const update = { ...state };

      update[FORM][PROPOSITION_QCM][indexProposition] = value;
      updateIndex(index, update);
   };

   return (
      <div className={styles.containerQcm}>
         <div className={styles.textQcm}>{'propositions'}</div>
         <div className={styles.textQcm}>
            {Object.keys(propositions).map((key, index: number) => {
               const proposition = propositions[key];
               const displayKey =
                  key === indexAnswer ? styles.buttonQcmAnswerSelect : '';

               return (
                  <div className={styles.containerProposition} key={key}>
                     <div
                        key={index}
                        role="button"
                        onClick={handleOnClickSelectAnswer(key)}
                        className={`${styles.buttonQcmAnswer} ${displayKey}`}
                     />
                     <input
                        value={proposition}
                        onChange={handleOnChange(key)}
                        className={styles.inputAnswerQCM}
                     />
                  </div>
               );
            })}
         </div>
         <QuizFooterButton
            text="+ PROPOSITION"
            style={`${styles.buttonQcmAddProp}`}
            handleOnClick={handleOnClick}
            {...props}
         />
      </div>
   );
}

interface IPropsQuestionAnswer extends IPropsIndex {
   state: any;
   index: number;
}

function QuestionAnswer(props: IPropsQuestionAnswer) {
   const { state, index, updateIndex } = props;
   const {
      [FORM]: { [ANSWER_Q_A]: answer_question }
   } = state;

   const handleOnChange = e => {
      const { value } = e.target;
      const update = { ...state };
      update[FORM][ANSWER_Q_A] = value;
      updateIndex(index, update);
   };

   return (
      <div className={styles.containerQA}>
         <div className={styles.answerQA}>{'correct answer'}</div>
         <input
            onChange={handleOnChange}
            value={answer_question}
            className={styles.inputAnswerQA}
         />
      </div>
   );
}

export default {
   [TRUE_FALSE]: TrueOrFalse,
   [QCM]: Qcm,
   [QUESTION_ANSWER]: QuestionAnswer
};
