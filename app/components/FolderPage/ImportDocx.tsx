import * as React from "react";
import { History } from "history";
import cx from "classnames";
import mammoth from "mammoth";
import { RouteComponentProps, withRouter, match } from "react-router";

// Style
import styles from "./FoldersPage.less";

// typescript
import {
   ICourse,
   IFolder,
   ISubject,
   IQuiz,
   ToggleModal,
   ILanguage,
   IMatchParams,
   CreateCourse
} from "../../typescript";
import NateFileUploader from "../Base/NateFileUploader";

import { createCourseFromFile } from "./importDocxHelper";

interface IProps extends RouteComponentProps {
   createCourse: CreateCourse;
   id: string;
   collection: string;
   folders: IFolder[];
   courses: ICourse[];
   subjects: ISubject[];
   quizs: IQuiz[];
   toggleModal: ToggleModal;
   match: match<IMatchParams>;
   history: History;
   lang: ILanguage;
   theme: string;
   router: any;
}

class ImportDocx extends React.Component<IProps> {
   onImportFileChange = e => {
      const { id, collection, createCourse } = this.props;
      createCourseFromFile(e.target.files[0], id, collection, createCourse);
      return;
   };

   render() {
      const {
         theme,
         folders,
         courses,
         quizs,
         id,
         lang,
         history,
         toggleModal,
         createCourse
      } = this.props;
      return (
         <div className={styles.layout}>
            <NateFileUploader onChange={this.onImportFileChange} accept=".docx">
               <div className={styles.button}>IMPORT</div>
            </NateFileUploader>
         </div>
      );
   }
}

export default withRouter(ImportDocx);
