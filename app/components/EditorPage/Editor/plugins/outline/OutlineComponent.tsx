import React from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';

import { HEADING } from "../heading/heading"
import withTheme from "../../../../withTheme";

import styles from "./Outline.less";

type Props = {
   theme: string;
   editor: any;
   value: any;
};
const Outline: React.FC<Props> = ({ editor, theme, value }) => {

   const scrollToHeading = (id: string) => e => {
      document.getElementById(id).scrollIntoView({
         behavior: 'smooth',
         block: "start",
         inline: "nearest"
       });
   }

   const blocks = editor.value.document.nodes.filter((node) => (node.type === HEADING));
   const offset = blocks.reduce((acc, val) => {
      const level = val.data.get("level");
      return level < acc ? level : acc;
   }, 4) - 1;
   const root = window.document.getElementById('editor-page');
   if (!root) return <></>;
   return ReactDOM.createPortal(
      <div className={cx(styles.layout)} >
         {
            blocks.map((heading) => {
               const key = heading.key;
               const level = heading.data.get("level");
               const text = heading.text;
               switch (level - offset) {
                  case 1:
                     return <div key={key} className={cx(theme, styles.text,  styles.levelOne)} onClick={scrollToHeading(key)}>{text}</div>
                  case 2:
                     return <div key={key} className={cx(theme, styles.text, styles.levelTwo)} onClick={scrollToHeading(key)}>{ text}</div>
                  case 3:
                     return <div key={key} className={cx(theme, styles.text, styles.levelThree)} onClick={scrollToHeading(key)}>{text}</div>
                  case 4:
                     return <div  key={key} className={cx(theme, styles.text, styles.levelFour)} onClick={scrollToHeading(key)}>{text}</div> // onClick={scrollToHeading()}
               }
            })
         }
      </div>
      , root);
};
export default withTheme(Outline);
