import React, { Component } from 'react';
import { Document, Page } from 'react-pdf/dist/entry.webpack';
import PdfViewer from './PdfViewer';

import styles from './PdfViewer.less';

interface IProps {
   isFocused: boolean;
   name: string;
   file: string;
   editor: any;
}

export default class PdfDocument extends Component<IProps> {
   showPdfViewer = () => {
      const { file, name, editor } = this.props;
      editor.showPdfViewer(file, 1);
   };

   render() {
      const { isFocused, file, name, editor } = this.props;
      return (
         <button
            className={styles.pdfThumbnail}
            style={{ border: isFocused ? '2px solid black' : '' }}
            onClick={this.showPdfViewer}
         >
            {name}
         </button>
      );
   }
}
