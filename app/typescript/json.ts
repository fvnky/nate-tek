export type AnyJson =  boolean | number | string | null | IJsonArray | IJsonMap | object;
export interface IJsonMap {  [key: string]: AnyJson; }
export interface IJsonArray extends Array<AnyJson> {}
